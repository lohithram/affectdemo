/**
 * Created by lram on 04/02/2015.
 */
package com.al.application
{

    import com.al.config.MainConfig;
    import com.al.model.VersionModel;
    import com.al.util.EventUtils;
    import com.flexspy.FlexSpy;
    import com.flexspy.KeySequence;
    import com.flexspy.impl.ComponentTreeWnd;

    import flash.display.DisplayObject;
    import flash.display.NativeMenuItem;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;

    import mx.events.FlexEvent;

    import org.spicefactory.parsley.core.context.Context;
    import org.spicefactory.parsley.core.events.ContextEvent;
    import org.spicefactory.parsley.flex.FlexContextBuilder;

    import spark.components.WindowedApplication;

    public class MainAirApplicationBase extends WindowedApplication
    {

        protected static const BOOTSTRAP_STATE: String = "bootstrap";
        protected static const LOADING_STATE: String = "loading";
        protected static const LOGIN_STATE: String = "login";
        protected static const READY_STATE: String = "ready";

        [Bindable]
        public var versionModel: VersionModel;

        public function MainAirApplicationBase(){

            showStatusBar=false;
            title = "Affect-Demo";

            addEventListener(FlexEvent.INITIALIZE, onInit);
            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            addEventListener(MouseEvent.CONTEXT_MENU, onContextMenu);
        }

        /**
         *   Event Handlers
         */
        public function onInit(event: Event): void{
        }

        private function onContextMenu(event: Event): void{

            var depth: int = 30;
            var component: DisplayObject = (event.target as DisplayObject);

            contextMenu.removeAllItems();

            while(depth>0 && component) {

                --depth;
                var menuItem:NativeMenuItem = new NativeMenuItem(component.name);
                menuItem.data = component;
                menuItem.addEventListener(Event.SELECT, function onSelect(event:Event) {

                    ComponentTreeWnd.showComponent(event.target.data);
                });
                contextMenu.addItem(menuItem);
                component = component.parent;
            }
        }

        /**
         * Sets up the very first main application Parsley context.
         * The MainAppConfig serves as the config file for this context
         * @param event
         */
        private function onAddedToStage(event: Event): void{

            EventUtils.removeListener(event, onAddedToStage);

            var context: Context = FlexContextBuilder.build(MainConfig, this);
            if(context.initialized)
                versionModel = context.getObjectByType(VersionModel) as VersionModel;
            else
                context.addEventListener(ContextEvent.INITIALIZED, onContextConfigured);

            FlexSpy.registerKey(new KeySequence(Keyboard.F), this);
        }

        private function onContextConfigured(event: ContextEvent): void{

            EventUtils.removeListener(event, onContextConfigured);

            var context: Context = event.target as Context;
            versionModel = context.getObjectByType(VersionModel) as VersionModel;
        }

        public function onComplete(event: FlexEvent): void{

            currentState = LOGIN_STATE; //loginRequired ? LOGIN_STATE : BOOTSTRAP_STATE;

        }
    }
}
