package com.al.application.controls
{
	import com.al.tile.ICombineElement;
	import com.al.util.EventUtils;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import spark.components.supportClasses.ButtonBase;

	[Style(name="overHCursorClass", type="Class")]
	[Style(name="grabHCursorClass", type="Class")]
	[Style(name="overVCursorClass", type="Class")]
	[Style(name="grabVCursorClass", type="Class")]

	[Style(name="overHCursorOffset", type="Array")]
	[Style(name="overVCursorOffset", type="Array")]
	[Style(name="grabHCursorOffset", type="Array")]
	[Style(name="grabVCursorOffset", type="Array")]
	
	public class GutterLane extends ButtonBase implements ICombineElement
	{
		private var _line: Rectangle;
		
		protected var overHCursor: Class;
		protected var grabHCursor: Class;
		
		protected var overVCursor: Class;
		protected var grabVCursor: Class;
		
		protected var overHCursorOffset: Array;
		protected var grabHCursorOffset: Array;
		
		protected var overVCursorOffset: Array;
		protected var grabVCursorOffset: Array;
		
		[Bindable]
		public var isVertical: Boolean;
		
		public function GutterLane(line: Rectangle = null) {
			
			_line = line;
			isVertical = line && line.width == 1;

			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			//buttonMode = true;
		}
		
		public function get line(): Rectangle {
			return _line;
		}

        public function set line(value: Rectangle): void{
            _line = value;
            isVertical = line && line.width==1;
        }
		//---------------------------------------------------
		// reducedX
		//---------------------------------------------------
		private var _reducedX: Number;
		public function get reducedX(): Number{
			return _reducedX;
		}
		public function set reducedX(value: Number): void{
			_reducedX = value;
		}
		
		//---------------------------------------------------
		// reducedY
		//---------------------------------------------------
		private var _reducedY: Number;
		public function get reducedY(): Number{
			return _reducedY;
		}
		public function set reducedY(value: Number): void{
			_reducedY = value;
		}
		
		//---------------------------------------------------
		// reducedWidth
		//---------------------------------------------------
		private var _reducedWidth: Number;
		public function get reducedWidth(): Number{
			return _reducedWidth;
		}
		public function set reducedWidth(value: Number): void{
			_reducedWidth = value;
		}
		
		//---------------------------------------------------
		// reducedHeight
		//---------------------------------------------------
		private var _reducedHeight: Number;
		public function get reducedHeight(): Number{
			return _reducedHeight;
		}
		public function set reducedHeight(value: Number): void{
			_reducedHeight = value;
		}		
		
		//---------------------------------------------------
		// includeInSizing
		//---------------------------------------------------
		public function get includeInSizing(): Boolean {
			return true;
		}
		
		//---------------------------------------------------
		// selected
		//---------------------------------------------------
		private var _selected: Boolean;
		public function set selected(value: Boolean): void{
			if(_selected != value){
				_selected = value;
				
				invalidateDisplayList();
			}
		}
		[Bindable]
		public function get selected(): Boolean{
			return _selected;
		}
		
		//---------------------------------------------------
		// visualIndex
		//---------------------------------------------------
		private var _visualIndex: int;
		public function get visualIndex(): int {
			return _visualIndex;
		}
		
		public function set visualIndex(value: int): void {
			_visualIndex = value;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Override methods
		//
		//--------------------------------------------------------------------------
		override protected function commitProperties():void{
			
			super.commitProperties();
			
			if(getStyle("overHCursorClass"))
				overHCursor = getStyle("overHCursorClass");
			if(getStyle("grabHCursorClass"))
				grabHCursor = getStyle("grabHCursorClass");
				
			if(getStyle("overVCursorClass"))
				overVCursor = getStyle("overVCursorClass");
			if(getStyle("grabVCursorClass"))
				grabVCursor = getStyle("grabVCursorClass");

			if(getStyle("overHCursorOffset"))
				overHCursorOffset = getStyle("overHCursorOffset") as Array;
			if(getStyle("grabHCursorOffset"))
				grabHCursorOffset = getStyle("grabHCursorOffset") as Array;
				
			if(getStyle("overVCursorOffset"))
				overVCursorOffset = getStyle("overVCursorOffset") as Array;
			if(getStyle("grabVCursorOffset"))
				grabVCursorOffset = getStyle("grabVCursorOffset") as Array;
				
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event Handlers
		//
		//--------------------------------------------------------------------------		
		protected function onMouseOver(event: MouseEvent): void {
			
			if(selected)
				return;
			 
			cursorManager.removeAllCursors();
			var cursorClass: Class = isVertical ? overVCursor : overHCursor;
			var offset: Array = isVertical ? overVCursorOffset : overHCursorOffset;
			if(isNaN(offset[0]))
				offset[0] = 0;
			if(isNaN(offset[1]))
				offset[1] = 0;
			
			if(cursorClass)
				cursorManager.setCursor(cursorClass, 1, offset[0], offset[1]);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);	

			selected = true;
		}
		
		protected function onMouseOut(event: MouseEvent): void {
			
			EventUtils.removeListener(event, onMouseOut);
			cleanUp();
		}
		
		protected function onMouseUp(event: MouseEvent): void {
			
            EventUtils.removeListener(event, onMouseUp);
			cleanUp();
		}
		
		protected function onMouseDown(event: MouseEvent): void {
			
			var offset: Array;
			var cursorClass: Class = isVertical ? grabVCursor : grabHCursor;

			if(cursorClass){

				offset = isVertical ? overVCursorOffset : overHCursorOffset;
				if(isNaN(offset[0]))
					offset[0] = 0;
				if(isNaN(offset[1]))
					offset[1] = 0;
				cursorManager.removeAllCursors();
				cursorManager.setCursor(cursorClass, 1, offset[0], offset[1]);
			}
				
			removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
            systemManager.getSandboxRoot().addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		//--------------------------------------------------------------------------
		//
		//  Private methods
		//
		//--------------------------------------------------------------------------
		private function cleanUp(): void{

			cursorManager.removeAllCursors();
			selected = false;
		}
		/**
		 * For grab area 
		 * 
		 * @param startX
		 * @param startY
		 * @param width
		 * @param height
		 * 
		 */
		private function drawRectangle(startX: Number, startY: Number, width: Number, height: Number): void{
			
			var g: Graphics = graphics;
			g.beginFill(0x739EE3, selected ? 1.0 : 0.01 );
			g.drawRect(startX, startY, width, height);
			g.endFill();
		}
	}
}