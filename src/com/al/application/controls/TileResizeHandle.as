package com.al.application.controls
{
	import com.al.combine.Corner;
	import com.al.enums.HandleOrientation;
	import com.al.tile.IResizeHandle;
	import com.al.util.EventUtils;
	
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;

	import spark.components.supportClasses.ButtonBase;

	[Style(name="overCursorClass", type="Class")]
	[Style(name="overCursorOffset", type="Array")]

	[Style(name="grabCursorClass", type="Class")]
	[Style(name="grabCursorOffset", type="Array")]
	
	[Style(name="overRCursorClass", type="Class")]
	[Style(name="overRCursorOffset", type="Array")]

	[Style(name="grabRCursorClass", type="Class")]
	[Style(name="grabRCursorOffset", type="Array")]

	[Style(name="overLCursorClass", type="Class")]
	[Style(name="overLCursorOffset", type="Array")]
	
	[Style(name="grabLCursorClass", type="Class")]
	[Style(name="grabLCursorOffset", type="Array")]
	
	[Style(name="overBCursorClass", type="Class")]
	[Style(name="overBCursorOffset", type="Array")]
	
	[Style(name="grabBCursorClass", type="Class")]
	[Style(name="grabBCursorOffset", type="Array")]
	
	[Style(name="overTCursorClass", type="Class")]
	[Style(name="overTCursorOffset", type="Array")]
	
	[Style(name="grabTCursorClass", type="Class")]
	[Style(name="grabTCursorOffset", type="Array")]
	
	[Style(name="overVCursorClass", type="Class")]
	[Style(name="overVCursorOffset", type="Array")]
	
	[Style(name="grabVCursorClass", type="Class")]
	[Style(name="grabVCursorOffset", type="Array")]
	
	[Style(name="overHCursorClass", type="Class")]
	[Style(name="overHCursorOffset", type="Array")]
	
	[Style(name="grabHCursorClass", type="Class")]
	[Style(name="grabHCursorOffset", type="Array")]
	


	public class TileResizeHandle extends ButtonBase implements IResizeHandle
	{
		protected var over4WayCursor: Class;
		protected var grab4WayCursor: Class;

		protected var overRCursor: Class;
		protected var grabRCursor: Class;
		
		protected var overLCursor: Class;
		protected var grabLCursor: Class;
		
		protected var overTCursor: Class;
		protected var grabTCursor: Class;
		
		protected var overBCursor: Class;
		protected var grabBCursor: Class;
		
		protected var overHCursor: Class;
		protected var grabHCursor: Class;
		
		protected var overVCursor: Class;
		protected var grabVCursor: Class;
		
		protected var over4WayCursorOffset: Array;
		protected var grab4WayCursorOffset: Array;
		
		protected var overRCursorOffset: Array;
		protected var grabRCursorOffset: Array;
		
		protected var overLCursorOffset: Array;
		protected var grabLCursorOffset: Array;
		
		protected var overBCursorOffset: Array;
		protected var grabBCursorOffset: Array;
		
		protected var overTCursorOffset: Array;
		protected var grabTCursorOffset: Array;
		
		protected var overHCursorOffset: Array;
		protected var grabHCursorOffset: Array;
		
		protected var overVCursorOffset: Array;
		protected var grabVCursorOffset: Array;
		
		public function TileResizeHandle(corner: Corner) {
			
			_corner = corner;
			height = width = 20;
			adjoiningTiles = new Dictionary();	
			
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		}
		
		//---------------------------------------------------
		// reducedX
		//---------------------------------------------------
		private var _reducedX: Number;
		public function get reducedX(): Number{
			return _reducedX;
		}
		public function set reducedX(value: Number): void{
			_reducedX = value;
		}
		
		//---------------------------------------------------
		// reducedY
		//---------------------------------------------------
		private var _reducedY: Number;
		public function get reducedY(): Number{
			return _reducedY;
		}
		public function set reducedY(value: Number): void{
			_reducedY = value;
		}
		
		//---------------------------------------------------
		// reducedWidth
		//---------------------------------------------------
		public function get reducedWidth(): Number{
			return NaN;
		}
		public function set reducedWidth(value: Number): void{
		}
		
		//---------------------------------------------------
		// reducedHeight
		//---------------------------------------------------
		public function get reducedHeight(): Number{
			return NaN;
		}
		public function set reducedHeight(value: Number): void{
		}		
		
		//---------------------------------------------------
		// adjoiningTiles
		//---------------------------------------------------
		private var _adjoiningTiles: Dictionary;
		public function get adjoiningTiles(): Dictionary{
			return _adjoiningTiles;
		}
		
		public function set adjoiningTiles(value: Dictionary): void{
			_adjoiningTiles = value;
		}
		
		//---------------------------------------------------
		// corner
		//---------------------------------------------------
		private var _corner: Corner;
		public function get corner(): Corner{
			return _corner;
		}
		
		//---------------------------------------------------
		// orientation
		//---------------------------------------------------
		private var _orientation: String;
		public function get orientation(): String{
			return _orientation;
		}
		public function set orientation(value: String): void{
			_orientation = value;
		}
		
		//---------------------------------------------------
		// includeInSizing
		//---------------------------------------------------
		public function get includeInSizing(): Boolean {
			return false;
		}
		
		//---------------------------------------------------
		// selected
		//---------------------------------------------------
		public function set selected(value: Boolean): void{
			
		}
		public function get selected(): Boolean{
			return false;
		}
		
		//---------------------------------------------------
		// visualIndex
		//---------------------------------------------------
		private var _visualIndex: int;
		public function get visualIndex(): int {
			return _visualIndex;
		}
		
		public function set visualIndex(value: int): void {
			_visualIndex = value;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Override methods
		//
		//--------------------------------------------------------------------------
		override protected function commitProperties():void{
			
			super.commitProperties();
			
			if(getStyle("overCursorClass"))
				over4WayCursor = getStyle("overCursorClass");
			if(getStyle("grabCursorClass"))
				grab4WayCursor = getStyle("grabCursorClass");

			if(getStyle("overRCursorClass"))
				overRCursor = getStyle("overRCursorClass");
			if(getStyle("grabRCursorClass"))
				grabRCursor = getStyle("grabRCursorClass");
			
			if(getStyle("overLCursorClass"))
				overLCursor = getStyle("overLCursorClass");
			if(getStyle("grabLCursorClass"))
				grabLCursor = getStyle("grabLCursorClass");
			
			if(getStyle("overRCursorClass"))
				overRCursor = getStyle("overRCursorClass");
			if(getStyle("grabRCursorClass"))
				grabRCursor = getStyle("grabRCursorClass");
			
			if(getStyle("overTCursorClass"))
				overTCursor = getStyle("overTCursorClass");
			if(getStyle("grabTCursorClass"))
				grabTCursor = getStyle("grabTCursorClass");
			
			if(getStyle("overBCursorClass"))
				overBCursor = getStyle("overBCursorClass");
			if(getStyle("grabBCursorClass"))
				grabBCursor = getStyle("grabBCursorClass");
			
			if(getStyle("overHCursorClass"))
				overHCursor = getStyle("overHCursorClass");
			if(getStyle("grabHCursorClass"))
				grabHCursor = getStyle("grabHCursorClass");
			
			if(getStyle("overVCursorClass"))
				overVCursor = getStyle("overVCursorClass");
			if(getStyle("grabVCursorClass"))
				grabVCursor = getStyle("grabVCursorClass");
			
			if(getStyle("overCursorOffset"))
				over4WayCursorOffset = getStyle("overCursorOffset") as Array;
			if(getStyle("grabCursorOffset"))
				grab4WayCursorOffset = getStyle("grabCursorOffset") as Array;
			
			if(getStyle("overRCursorOffset"))
				overRCursorOffset = getStyle("overRCursorOffset") as Array;
			if(getStyle("grabRCursorOffset"))
				grabRCursorOffset = getStyle("grabRCursorOffset") as Array;
			
			if(getStyle("overLCursorOffset"))
				overLCursorOffset = getStyle("overLCursorOffset") as Array;
			if(getStyle("grabLCursorOffset"))
				grabLCursorOffset = getStyle("grabLCursorOffset") as Array;
			
			if(getStyle("overTCursorOffset"))
				overTCursorOffset = getStyle("overTCursorOffset") as Array;
			if(getStyle("grabTCursorOffset"))
				grabTCursorOffset = getStyle("grabTCursorOffset") as Array;
			
			if(getStyle("overBCursorOffset"))
				overBCursorOffset = getStyle("overBCursorOffset") as Array;
			if(getStyle("grabBCursorOffset"))
				grabBCursorOffset = getStyle("grabBCursorOffset") as Array;

			if(getStyle("overHCursorOffset"))
				overHCursorOffset = getStyle("overHCursorOffset") as Array;
			if(getStyle("grabHCursorOffset"))
				grabHCursorOffset = getStyle("grabHCursorOffset") as Array;
			
			if(getStyle("overVCursorOffset"))
				overVCursorOffset = getStyle("overVCursorOffset") as Array;
			if(getStyle("grabVCursorOffset"))
				grabVCursorOffset = getStyle("grabVCursorOffset") as Array;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event Handlers
		//
		//--------------------------------------------------------------------------		
		private var isMouseDownOnMouseOver: Boolean;
		
		protected function onMouseOver(event: MouseEvent): void {
			
			if(selected)
				return;
			
			cursorManager.removeAllCursors();
			var cursorClass: Class = determineCursorClass();
			var offset: Array;
			
			if(cursorClass){
				offset = determineCursorOffset();
				cursorManager.setCursor(cursorClass, 1, offset[0], offset[1]);
			}
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);	
			isMouseDownOnMouseOver = event.buttonDown;
			
			selected = true;
		}
		
		protected function onMouseOut(event: MouseEvent): void {
			
			EventUtils.removeListener(event, onMouseOut);
			if(event.buttonDown == isMouseDownOnMouseOver){
				
				cursorManager.removeAllCursors();
				selected = false;
			}
		}
		
		protected function onMouseUp(event: MouseEvent): void {
			
			cursorManager.removeAllCursors();
			selected = false;
		}
		
		protected function onMouseDown(event: MouseEvent): void {
			
			var cursorClass: Class = determineCursorClass(true);
			var offset: Array;
			
			if(cursorClass){
			
				cursorManager.removeAllCursors();
				offset = determineCursorOffset(true);
				cursorManager.setCursor(cursorClass, 1, offset[0], offset[1]);
			}
			
			selected = true;
		}
		
		private function determineCursorClass(grabbed: Boolean = false): Class{
			
			var cursor: Class;
			switch(orientation){
				
				case HandleOrientation.FOUR_WAY_SPLIT:
					cursor = !grabbed ? over4WayCursor : grab4WayCursor;
					break;
				case HandleOrientation.RIGHT_SPLIT:
					cursor = !grabbed ? overRCursor : grabRCursor;
					break;
				case HandleOrientation.LEFT_SPLIT:
					cursor = !grabbed ? overLCursor : grabLCursor;
					break;
				case HandleOrientation.BOTTOM_SPLIT:
					cursor = !grabbed ? overBCursor : grabBCursor;
					break;
				case HandleOrientation.TOP_SPLIT:
					cursor = !grabbed ? overTCursor : grabTCursor;
					break;
				case HandleOrientation.VERTICAL_SPLIT:
					cursor = !grabbed ? overVCursor : grabVCursor;
					break;
				case HandleOrientation.HORIZONTAL_SPLIT:
					cursor = !grabbed ? overHCursor : grabHCursor;
					break;
			}	
			return cursor;
		}
		
		private function determineCursorOffset(grabbed: Boolean = false): Array{
			
			var offset: Array;
			switch(orientation){
				
				case HandleOrientation.FOUR_WAY_SPLIT:
					offset = !grabbed ? over4WayCursorOffset : grab4WayCursorOffset;
					break;
				case HandleOrientation.RIGHT_SPLIT:
					offset = !grabbed ? overRCursorOffset : grabRCursorOffset;
					break;
				case HandleOrientation.LEFT_SPLIT:
					offset = !grabbed ? overLCursorOffset : grabLCursorOffset;
					break;
				case HandleOrientation.BOTTOM_SPLIT:
					offset = !grabbed ? overBCursorOffset : grabBCursorOffset;
					break;
				case HandleOrientation.TOP_SPLIT:
					offset = !grabbed ? overTCursorOffset : grabTCursorOffset;
					break;
				case HandleOrientation.VERTICAL_SPLIT:
					offset = !grabbed ? overVCursorOffset : grabVCursorOffset;
					break;
				case HandleOrientation.HORIZONTAL_SPLIT:
					offset = !grabbed ? overHCursorOffset : grabHCursorOffset;
					break;
			}	
			return sanitizeOffsets(offset);
		}
		
		private function sanitizeOffsets(offset: Array): Array{
			
			if(isNaN(offset[0]))
				offset[0] = 0;
			if(isNaN(offset[1]))
				offset[1] = 0;
			
			return offset;
		}
		
	}
}