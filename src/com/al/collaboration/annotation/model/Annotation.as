package com.al.collaboration.annotation.model
{
	import com.adobe.rtc.messaging.MessageItem;
	import com.al.collaboration.model.ICollaborationItem;
	import com.al.descriptors.TileContainerDescriptor;
	import com.al.events.ItemEvent;
    import com.al.events.ItemEvent;

    import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayList;
	import mx.utils.UIDUtil;

	public class Annotation extends EventDispatcher implements ICollaborationItem
	{
		[Bindable]
		public var reducedX: Number;
		
		[Bindable]
		public var reducedY: Number;
		
		[Bindable]
		public var pageNumber: int;

		[Bindable]
		public var annotationText: String;

		[Bindable]
		public var contentUnitId: String;

		[Bindable]
		public var collaborationSessionId: String = "";

		[Bindable]
		[Transient]
		public var isConversationOpen: Boolean;


		[Transient]
		public var tileDescriptor: TileContainerDescriptor;
		
		[Bindable]
		[ArrayElementType("com.al.collaboration.annotation.model.Comment")]
		public var comments: ArrayList;

		public function Annotation(id: String = ""){
			_id = id;
			comments = new ArrayList();
		}

        public function highlightComment(comment:Comment):void
        {
            dispatchEvent(new ItemEvent(ItemEvent.HIGHLIGHT_COMMENT,comment));
        }
		
		//--------------------------------------------------
		// universal unique identifier
		//--------------------------------------------------
		private var _id: String;
		public function get id(): String{
			return _id;
		}
		
		//--------------------------------------------------
		// conversation identifier
		//--------------------------------------------------
		private var _conversationId: uint;
		[Bindable("conversationIdChange")]
		public function get conversationId(): uint{
			return _conversationId;
		}
		
		public function set conversationId(value: uint): void{
			
			if(_conversationId != value){
				_conversationId = value;
				dispatchEvent(new Event("conversationIdChange"));
			}
			
		}
		
		//--------------------------------------------------
		// color associated with this annotation
		//--------------------------------------------------
		private var _color: uint; 
		public function set color(value: uint): void{
			
			_color = value;
		}
		
		public function get color(): uint{
			
			return _color;
		}
		
		public function getObject():Object
		{
			return {contentUnitId: contentUnitId,
				reducedX: reducedX,
				reducedY: reducedY,
				conversationId: conversationId,
				collaborationSessionId: collaborationSessionId
				};
		}
		
		public function update(withObject: Object):void
		{
			_id = (withObject as MessageItem).itemID;

			if(withObject.body){
				
				if(withObject.body.hasOwnProperty("reducedX") && !isNaN(withObject.body.reducedX))
					reducedX = withObject.body.reducedX;
				if(withObject.body.hasOwnProperty("reducedY") && !isNaN(withObject.body.reducedY))
					reducedY = withObject.body.reducedY;
				if(withObject.body.hasOwnProperty("pageNumber") && !isNaN(withObject.body.pageNumber))
					pageNumber = withObject.body.pageNumber;
				if(withObject.body.hasOwnProperty("conversationId"))
					conversationId = withObject.body.conversationId;
				if(withObject.body.hasOwnProperty("contentUnitId"))
					contentUnitId = withObject.body.contentUnitId;
				if(withObject.body.hasOwnProperty("collaborationSessionId"))
					collaborationSessionId = withObject.body.collaborationSessionId;
				
			}
		}
	}
}