package com.al.collaboration.annotation.model
{
	import com.adobe.rtc.messaging.MessageItem;
	import com.al.collaboration.model.ICollaborationItem;

	public class Comment implements ICollaborationItem
	{
		public function Comment(id: String, commentStr: String = null)
		{
			_id = id;
			if(commentStr){
				commentString = commentStr;
			}
		}

        [Bindable]
        public var conversationId:uint;

		[Bindable]
		public var userName: String;
		
		[Bindable]
		public var timestamp: Date;
		
		[Bindable]
		public var commentString: String;
		
		public var annotationId: String;

		[Transient]
		public var annotation: Annotation;

		// used for display reference
		public var commentsId: uint;

		private var _id: String;
		public function get id(): String{
			return _id;
		}
		
		public function getObject():Object
		{
			return {userName: userName, timestamp: timestamp.time, 
					commentString: commentString, annotationId: annotationId, commentsId: commentsId};
		}
		
		public function update(withObject: Object):void
		{
			_id = (withObject as MessageItem).itemID;
			
			if(withObject.body){
				
				if(withObject.body.hasOwnProperty("userName"))
					userName = withObject.body.userName;
				if(withObject.body.hasOwnProperty("timestamp") && !isNaN(withObject.body.timestamp)){
					timestamp = new Date();
					timestamp.time = withObject.body.timestamp;
				}
				if(withObject.body.hasOwnProperty("commentsId") && !isNaN(withObject.body.commentsId)){
					commentsId = withObject.body.commentsId;
				}
				if(withObject.body.hasOwnProperty("commentString")){
					commentString = withObject.body.commentString;
				}
				if(withObject.body.hasOwnProperty("annotationId")){
					annotationId = withObject.body.annotationId;
				}
			}
		}
	}
}