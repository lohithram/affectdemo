package com.al.collaboration.annotation.model
{
	import com.adobe.rtc.messaging.MessageItem;
	import com.al.collaboration.model.ICollaborationItem;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;

	import mx.collections.ArrayCollection;

	import mx.collections.ArrayList;
	import mx.utils.UIDUtil;

	public class LiveText extends EventDispatcher implements ICollaborationItem
	{
		[Bindable]
		public var reducedX: Number;
		
		[Bindable]
		public var reducedY: Number;

		[Bindable]
		public var width: Number;

		[Bindable]
		public var height: Number;

		[Bindable]
		public var htmlText: String = "";

		[Bindable]
		public var verticalScrollPos: Number;

		[Bindable]
		public var selection: Object;

		[Bindable]
		public var contentUnitId: String;

		[Bindable]
		public var isRemoteEditing: Boolean;

        [Bindable]
        public var remoteUser: String;

		public var usersEditing: ArrayCollection;

		public function LiveText(id: String = ""){

			_id = id;
			usersEditing = new ArrayCollection();
		}
		
		//--------------------------------------------------
		// universal unique identifier
		//--------------------------------------------------
		private var _id: String;
		public function get id(): String{
			return _id;
		}


		/**
		 * Determines whether the current user is editing.
		 */
		public function iAmEditing():void
		{
			/*if (!_collectionNode.isSynchronized) {
				//too early, ignore
				return;
			}

			if (	!_usersEditing.contains(_userManager.myUserID)
					&& _usersEditing.toArray().length < TOO_MANY_EDITING_THRESHOLD
			) {
				//I'm not typing yet and we're below the threshold, publish my item
				_collectionNode.publishItem(new MessageItem(EDITING_NODE_NAME, _userManager.myUserID, _userManager.myUserID));

				//the receiveItem will start the timer
			}

			//Extend the timer if it's running (and it's only running if we received our own typing item)
			if (_editingTimer.running) {
				_editingTimer.reset();
				_editingTimer.start();
			}*/
		}

		public function getObject():Object
		{
			return {contentUnitId: contentUnitId,
					reducedX: reducedX, reducedY: reducedY,
					width: width, height: height,
                    htmlText: htmlText};
		}
		
		public function update(withObject: Object):void
		{
			_id = (withObject as MessageItem).itemID;

			if(withObject.body){
				
				if(withObject.body.hasOwnProperty("reducedX") && !isNaN(withObject.body.reducedX))
					reducedX = withObject.body.reducedX;
				if(withObject.body.hasOwnProperty("reducedY") && !isNaN(withObject.body.reducedY))
					reducedY = withObject.body.reducedY;
				if(withObject.body.hasOwnProperty("width") && !isNaN(withObject.body.width))
					width = withObject.body.width;
				if(withObject.body.hasOwnProperty("height") && !isNaN(withObject.body.height))
					height = withObject.body.height;
				if(withObject.body.hasOwnProperty("htmlText"))
					htmlText = withObject.body.htmlText;
				if(withObject.body.hasOwnProperty("contentUnitId"))
					contentUnitId = withObject.body.contentUnitId;
				
			}
		}
	}
}