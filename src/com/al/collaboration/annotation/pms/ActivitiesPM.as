package com.al.collaboration.annotation.pms
{

    import com.adobe.rtc.sharedModel.CollectionNode;
    import com.al.messages.PublishActivitiesMsg;
    import com.al.model.Activity;
    import com.al.model.ActivityModel;
    import com.al.model.Task;
    import com.al.model.UserModel;
    import com.al.util.DateUtils;

    import flash.events.EventDispatcher;

    import mx.collections.ArrayList;
    import mx.utils.UIDUtil;

    /**
	 * @author lram
	 * 
	 */
	public class ActivitiesPM extends EventDispatcher
	{

        [Inject]
        public var userModel: UserModel;

        //[Inject]
		[Bindable]
		public var model: ActivityModel;

        [Bindable]
        public var currentTemplate: String = "Budget Report";

        [Bindable]
        public var templateList: ArrayList;

        [MessageDispatcher]
        public var dispatcher: Function;

        protected var collectionNode: CollectionNode;

        private var activityCounter: int = 0;

        [Init]
        public function onInit(): void{

            model = new ActivityModel();
            createTemplate();
        }

        public function addNewActivity(): Activity{

            var activity: Activity = new Activity(model.projectId);
            activity.startDate = new Date();
            activity.completionDate = DateUtils.addDays(new Date(), 3);

            model.activities.addItem(activity);
            addNewTask(activity);

            return activity;
        }

        public function removeActivity(activity: Activity): void{

            if(model.activities.getItemIndex(activity) >= 0)
                model.activities.removeItem(activity);
        }

        public function addNewTask(activity: Activity): Task{

            var task: Task = new Task(UIDUtil.createUID());
            task.activity = activity;

            activity.tasks.addItem(task);

            return task;
        }

        public function removeTask(task: Task): void{

            if(task.activity && task.activity.tasks.getItemIndex(task ) >= 0){

                task.activity.tasks.removeItem(task);
            }
        }

        public function publish(): void{

            dispatcher(new PublishActivitiesMsg(model.activities.source, model.projectId));
        }

        private function createTemplate(): void{

            var activity: Activity = addNewActivity();
            activity.title = "Advertisements";
            activity.unit = "prints";
            activity.totalPlanned = "400";
            activity.totalActual = "330";

            activity = addNewActivity();
            activity.title = "Total Revenue";
            activity.unit = "£";
            activity.totalPlanned = "730000";
            activity.totalActual = "650000";

            activity = addNewActivity();
            activity.title = "Exports";
            activity.unit = "$";
            activity.totalPlanned = "50000";
            activity.totalActual = "40330";

            activity = addNewActivity();
            activity.title = "Brochures";
            activity.unit = "units";
            activity.totalPlanned = "400";
            activity.totalActual = "560";

            activity = addNewActivity();
            activity.title = "National Air Time";
            activity.unit = "seconds";
            activity.totalPlanned = "3000";
            activity.totalActual = "200";

            activity = addNewActivity();
            activity.title = "Merchandise";
            activity.unit = "piece";
            activity.totalPlanned = "20000";
            activity.totalActual = "6000";
        }

	}
}