package com.al.collaboration.annotation.pms
{

    import com.adobe.rtc.events.CollectionNodeEvent;
    import com.adobe.rtc.events.SharedPropertyEvent;
    import com.adobe.rtc.messaging.MessageItem;
    import com.adobe.rtc.session.IConnectSession;
    import com.adobe.rtc.sharedModel.CollectionNode;
    import com.al.collaboration.annotation.model.Annotation;
    import com.al.collaboration.annotation.model.Comment;
    import com.al.collaboration.messages.CollaborationItemReceived;
    import com.al.collaboration.messages.CollaborationItemRetracted;
    import com.al.collaboration.messages.CollectionNodeSynchronized;
    import com.al.collaboration.messages.CreateAnnotation;
    import com.al.collaboration.messages.RemoveAnnotation;
    import com.al.collaboration.messages.UpdateAnnotation;
    import com.al.collaboration.model.ContentCollaborationModel;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.TileDataFormats;
    import com.al.model.CollaborationConfigurationModel;
    import com.al.model.IContentUnit;
    import com.al.model.TileDataModel;
    import com.al.model.UserModel;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.utils.Dictionary;

    import mx.binding.utils.BindingUtils;
    import mx.collections.ArrayCollection;
    import mx.utils.UIDUtil;

    /**
	 * This Class (during Demo stage) handles all aspects of establishing connection and
	 * synchronization with LCS
	 * @author lram
	 * 
	 */
	public class AnnotationPM extends EventDispatcher
	{
		public static const COMMENTS: String = "Comments";
		public static const ANNOTATIONS: String = "Annotations";

		public static const ANNOTATION_UPDATE: String = "annotationsUpdate";
		
		
		[Inject]
		public var collaborationConfig: CollaborationConfigurationModel;
		
		[Inject]
		[Bindable]
		public var connectSession: IConnectSession;
		
		[Bindable]
		[Inject(id="assetNode")]
		public var collectionNode: CollectionNode;
		
		[Bindable]
		[ArrayElementType("com.al.collaboration.annotation.model.Annotation")]
		public var annotations: ArrayCollection;
		
		[Inject]
		public var userModel: UserModel;
		
		[Inject]
		public var collaborationModel: ContentCollaborationModel;

        [Inject]
        public var tileDataModel: TileDataModel;

        private var tileDescriptor: TileContainerDescriptor;

		private var commentsMap: Dictionary = new Dictionary(true);
		private var annotationsMap: Dictionary = new Dictionary(true);
		private var contentUnitAnnotations: Dictionary = new Dictionary(true);
		[ArrayElementType("com.al.collaboration.annotation.model.Annotation")]
		private var allAnnotations: ArrayCollection;


		[Init]
		public function init(): void{
		
			annotations = new ArrayCollection();
            allAnnotations = new ArrayCollection();
            tileDataModel.setTileData(TileDataFormats.ANNOTATIONS, allAnnotations);
            tileDescriptor = tileDataModel.getTileData(TileDataFormats.TILE_DESCRIPTOR) as TileContainerDescriptor;

            BindingUtils.bindSetter(onContentUnitChange, collaborationModel, "currentContentUnit");
		}
		
		/**
		 * Use the shapeID as the id of the annotation 
		 * @param msg
		 * 
		 */
		[MessageHandler(scope="local")]
		public function create(msg: CreateAnnotation): void{
			
			createNewAnnotation(msg.reducedX, msg.reducedY, msg.shapeID);
		}
				
		[MessageHandler(scope="local")]
		public function update(msg: UpdateAnnotation): void{
			
			updateAnnotationWithNewLoc(msg.reducedX, msg.reducedY, msg.shapeID);
		}
				
		[MessageHandler(scope="local")]
		public function remove(msg: RemoveAnnotation): void{
			
			removeAnnotation(msg.shapeID);
		}
		
		[MessageHandler(selector="Comments", scope="local")]
		public function onCommentsRetract(msg: CollaborationItemRetracted): void{
			
			var commentId: String = msg.event.item.itemID;
			var comment: Comment = commentsMap[commentId];
			if(comment){
				
				var annotation: Annotation = annotationsMap[comment.annotationId];
				if(annotation){
					
					annotation.comments.removeItem(comment);
				}
				delete commentsMap[commentId];
			}
		}
		
		[MessageHandler(selector="Annotations", scope="local")]
		public function onAnnotationRetract(msg: CollaborationItemRetracted): void{
			
			var annotationId: String = msg.event.item.itemID;
			var annotation: Annotation = annotationsMap[annotationId];
			if(annotation){
				
				annotations.removeItem(annotation);
				allAnnotations.removeItem(annotation);

				delete annotationsMap[annotationId];
			}
		}

        [MessageHandler(scope="local")]
        public function onSynchronized(msg: CollectionNodeSynchronized): void{

            if(msg.nodeName == collectionNode.sharedID){

                if(tileDescriptor.currentContent.pendingAnnotations){

                    while(tileDescriptor.currentContent.pendingAnnotations.length > 0){

                        var annotation: Annotation = tileDescriptor.currentContent.pendingAnnotations.pop();
                        createNewAnnotation(annotation.reducedX, annotation.reducedY, annotation.id, Number(annotation.conversationId));
                    }
                }
            }
        }
			
		/**
		 * TODO: Its the responsibility of the annotationPM to take localX and localY 
		 * and reduce it
		 * To create a new annoation 
		 * @param x
		 * @param y
		 * 
		 */
		public function createNewAnnotation(x: Number, y: Number, id: String=null, conversationId: Number = NaN): void{
			
			var annotation: Annotation = new Annotation(id ? id : UIDUtil.createUID());
			annotation.reducedX = x;
			annotation.reducedY = y;
			annotation.contentUnitId = 
				collaborationModel.currentContentUnit ? collaborationModel.currentContentUnit.id : "";
			annotation.conversationId =
                    isNaN(conversationId) ? collaborationConfig.getAnnotationsCount() : conversationId;
            annotation.pageNumber = getPageNumber(annotation.contentUnitId);
			
			if(collectionNode.isSynchronized)
				collectionNode.publishItem( 
					new MessageItem( ANNOTATIONS, annotation.getObject(), annotation.id ) );
		}
		
		public function updateAnnotationWithNewLoc(x: Number, y: Number, id: String): void{
			
			var annotation: Annotation = annotationsMap[id];
			if(annotation){
				
				annotation.reducedX = x;
				annotation.reducedY = y;
				updateAnnotation(annotation);
			}
		}
		
		public function updateAnnotation(annotation: Annotation): void{
			
			if(collectionNode.isSynchronized)
				collectionNode.publishItem( 
					new MessageItem( ANNOTATIONS, annotation.getObject(), annotation.id ), true );
		}
		
		public function removeAnnotation(id: String): void{
			
			if(collectionNode.isSynchronized){
				collectionNode.retractItem( ANNOTATIONS, id );
				removeAssociatedComments(annotationsMap[id]);	
			}
		}
		
		/**
		 *  
		 * @param commentText
		 * @param anotation
		 * 
		 */
		public function addComment(commentText: String, annotation: Annotation): void {
			
			var comment: Comment = new Comment(UIDUtil.createUID());
			comment.commentString = commentText;
			comment.annotationId = annotation.id;
			comment.commentsId = collaborationConfig.getCommentsCount();
			comment.timestamp = new Date();
			comment.userName = userModel.displayName;
			if(collectionNode.isSynchronized)
				collectionNode.publishItem(
					new MessageItem( COMMENTS, comment.getObject(), comment.id) );
		}
		
		[MessageHandler(selector="Annotations", scope="local")]
		public function onAnnotationReceive(msg: CollaborationItemReceived):void
		{
			var event: CollectionNodeEvent = msg.event;
			var annotation: Annotation = annotationsMap[event.item.itemID] as Annotation;
			if(annotation){
				annotation.update(event.item);
			}else
			{
				annotation = new Annotation(event.item.itemID);
                annotation.tileDescriptor = tileDescriptor;
				annotation.update(event.item);
                annotation.pageNumber = getPageNumber(annotation.contentUnitId);

				if(annotation.contentUnitId){

					getAnnotationsForContentUnit(annotation.contentUnitId).addItem(annotation);
				}
				else{

					annotations.addItem(annotation);
                }

                annotationsMap[event.item.itemID] = annotation;
                allAnnotations.addItem(annotation);
            }
			dispatchEvent( new Event(ANNOTATION_UPDATE) );
		}
		
		[MessageHandler(selector="Comments", scope="local")]
		public function onCommentsReceive(msg: CollaborationItemReceived):void
		{
			
			var event: CollectionNodeEvent = msg.event;
			var comment: Comment = commentsMap[event.item.itemID] as Comment;
			if(comment){

                comment.update(event.item);
			}else{

				comment = new Comment(event.item.itemID);
				commentsMap[event.item.itemID] = comment;
				comment.update(event.item);
				var annotation: Annotation = 
					annotationsMap[comment.annotationId] as Annotation;
				if(!annotation){

					annotation = new Annotation(event.item.itemID);
                    annotation.tileDescriptor = tileDescriptor;
					annotationsMap[comment.annotationId] = annotation;
					annotations.addItem(annotation);
					allAnnotations.addItem(annotation);
				}
                comment.annotation = annotation;
				annotation.comments.addItem(comment);
			}
		}

		protected function onContentUnitChange(oldObject: Object=null, newObject: Object=null): void{
			
			if(collaborationModel && collaborationModel.currentContentUnit){

                annotations = getAnnotationsForContentUnit(collaborationModel.currentContentUnit.id);
            }
		}

        protected function getAnnotationsForContentUnit(contentUnitId: String): ArrayCollection {

            if(!contentUnitAnnotations[contentUnitId])
                contentUnitAnnotations[contentUnitId] = new ArrayCollection();

            return contentUnitAnnotations[contentUnitId] as ArrayCollection;
        }

		protected function removeAssociatedComments(annotation: Annotation): void{
			
			if(!annotation) return;
			for each( var comment: Comment in annotation.comments.source){
				removeComment(comment.id);
			}
		}
		
		protected function removeComment(id: String): void{
			
			if(collectionNode.isSynchronized){

				collectionNode.retractItem( COMMENTS, id );
			}
		}

        protected function getPageNumber(contentUnitId: String): Number{

            var pageNumber: Number = 1;
            if(contentUnitId && tileDescriptor.currentContent.contentUnits)
            {

                for each(var content: IContentUnit in tileDescriptor.currentContent.contentUnits.source){

                    if(content.id == contentUnitId) {

                        pageNumber = tileDescriptor.currentContent.contentUnits.getItemIndex(content) + 1;
                        break;
                    }
                }
            }
            return pageNumber;
        }
		
		protected function onSharedNodeChange(event:SharedPropertyEvent):void
		{
			//dispatchEvent(new SharedPropertyEvent(p_evt.type,p_evt.publisherID));
		}

	}
}