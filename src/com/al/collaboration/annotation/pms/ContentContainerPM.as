package com.al.collaboration.annotation.pms
{

	import com.al.collaboration.messages.SubscribeToCollaborationNode;
	import com.al.collaboration.model.ContentCollaborationModel;
	import com.al.enums.ContentType;
	import com.al.messages.ToolSelectMessage;
	import com.al.model.ContentUnit;
	import com.al.model.DrawingToolsDescriptorModel;
	import com.al.model.IContentUnit;
	import com.al.model.TileContainerModel;
	import com.al.model.TileContentDescriptor;

	import flash.events.EventDispatcher;

	/**
	 * @author lram
	 * 
	 */
	public class ContentContainerPM extends EventDispatcher
	{
		[MessageDispatcher]
		public var dispatcher: Function;

		[Bindable]
		public var tileContainerModel: TileContainerModel;

        [Inject]
        [Bindable]
        public var collaborationModel: ContentCollaborationModel;
		
		public var contentDescriptor: TileContentDescriptor;

		public var cursorClass: Class;
		public var cursorXOffset: Number;
		public var cursorYOffset: Number;

		[MessageHandler]
		public function onToolSelect(msg: ToolSelectMessage): void{

			cursorClass = null;
			var toolDescriptor: Object = msg.toolDescriptor;

			if(toolDescriptor.hasOwnProperty("cursorClass")){

				cursorClass = toolDescriptor.cursorClass;
				cursorXOffset = cursorYOffset = 0;
			}
			if(toolDescriptor.hasOwnProperty("cursorXOffset")){

				cursorXOffset = toolDescriptor.cursorXOffset;
			}
			if(toolDescriptor.hasOwnProperty("cursorYOffset")){

				cursorYOffset = toolDescriptor.cursorYOffset;
			}

			// Cannot zoom in, out or exact fit for the below type of content
			if(contentDescriptor.contentFactory ||
				contentDescriptor.contentType == ContentType.MULTI_PAGE ||
				contentDescriptor.contentType == ContentType.CONCEPT_BOARD ||
				contentDescriptor.contentType == ContentType.NO_CONTENT ) {

				cursorClass =
						toolDescriptor.toolName == DrawingToolsDescriptorModel.ANNOTATE ? cursorClass : null;
			}
		}


		public function subscribe(model: TileContentDescriptor): void {
		
			contentDescriptor = model;
			if(contentDescriptor.contentUnits && contentDescriptor.contentUnits.length > 0)
				collaborationModel.currentContentUnit = (contentDescriptor.contentUnits.getItemAt(0) as ContentUnit);
			
			dispatcher(new SubscribeToCollaborationNode(model.id));
		}

        public function setCurrentContentUnit(unit: IContentUnit): void {

            collaborationModel.currentContentUnit = unit;
        }
	}
}