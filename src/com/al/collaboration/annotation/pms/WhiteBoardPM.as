package com.al.collaboration.annotation.pms
{

    import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBSimpleShape;
    import com.adobe.rtc.events.CollectionNodeEvent;
    import com.adobe.rtc.messaging.MessageItem;
    import com.adobe.rtc.session.IConnectSession;
    import com.adobe.rtc.sharedModel.CollectionNode;
    import com.al.collaboration.events.AddShapeEvent;
    import com.al.collaboration.events.UpdateShapeEvent;
    import com.al.collaboration.messages.CollaborationItemReceived;
    import com.al.collaboration.messages.CollaborationItemRetracted;
    import com.al.collaboration.messages.CreateAnnotation;
	import com.al.collaboration.messages.CreateLiveTextEdit;
	import com.al.collaboration.messages.RemoveAnnotation;
    import com.al.collaboration.messages.UpdateAnnotation;
    import com.al.collaboration.model.ContentCollaborationModel;
    import com.al.collaboration.whiteboardClasses.ShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.SharedWBModel;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBArrowShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBEllipseShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBHighlightAreaShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBLineShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBMarkerShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBRectangleShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBRoundedRectangleShapeDescriptor;
    import com.al.collaboration.whiteboardClasses.shapeDescriptors.WBTextShapeDescriptor;
    import com.al.messages.ToolSelectMessage;
    import com.al.model.CollaborationConfigurationModel;
	import com.al.model.DrawingModel;
	import com.al.model.UserModel;

    import flash.events.EventDispatcher;
    import flash.utils.Dictionary;

    import mx.binding.utils.BindingUtils;
    import mx.events.CollectionEvent;
    import mx.utils.UIDUtil;

    /**
	 * @author lram
	 * 
	 */
	public class WhiteBoardPM extends EventDispatcher
	{
		protected static const SHAPE_DEFINITION_NODE:String = "shapeDefinitionNode";
		protected static const SHAPE_PROPERTIES_NODE:String = "shapePropertiesNode";
		protected static const SHAPE_CONTAINER_NODE:String = "shapeContainerNode";
		
		[Inject]
		public var collaborationConfig: CollaborationConfigurationModel;
		
		[Inject]
		[Bindable]
		public var connectSession: IConnectSession;
		
		[Bindable]
		[Inject(id="assetNode")]
		public var collectionNode: CollectionNode;

		[Inject]
		[Bindable]
		public var whiteBoardModel: SharedWBModel;

		[Inject]
		public var drawingModel: DrawingModel;

        [Inject]
        public var userModel: UserModel;

		[Inject]
		[Bindable]
		public var collaborationModel: ContentCollaborationModel;

		[MessageDispatcher]
		public var dispatcher: Function;

		[Init]
		public function init(): void{
		
			whiteBoardModel.collectionNode = collectionNode;
			whiteBoardModel.connectSession = connectSession;	
			
			whiteBoardModel.shapesMap = new Dictionary(true);
            BindingUtils.bindSetter(onContentUnitChange, collaborationModel, "currentContentUnit");
		}
		
		[Destroy]
		public function cleanUp(): void{
			
			//delete shapesMap;
		}

		[MessageHandler(selector="WhiteBoard", scope="local")]
		public function onItemsReceive(msg: CollaborationItemReceived):void
		{
			var event: CollectionNodeEvent = msg.event;
			if(sanityCheckFail(event)) return;


            var shapeDesc:ShapeDescriptor = parseBody(event);
            var oldDesc:ShapeDescriptor = whiteBoardModel.shapesMap[shapeDesc.shapeID];
            var newShape: Boolean = !oldDesc;

			if(oldDesc){
                oldDesc.readValueObject(event.item.body);
			}
            else {
                whiteBoardModel.shapes.addItem(shapeDesc);
                whiteBoardModel.shapesMap[shapeDesc.shapeID] = shapeDesc;
            }

			if (event.nodeName==SHAPE_DEFINITION_NODE) {
				// it's a totally new shape

				if(!collaborationModel.currentContentUnit ||
                        shapeDesc.contentUnitId == collaborationModel.currentContentUnit.id) {

                    whiteBoardModel.onShapeAddProxy(shapeDesc, !newShape);
                    dispatchEvent(new AddShapeEvent(shapeDesc));
                }
			}
			else if(!newShape) {
				// we'd seen the shape, but not added it
				//onShapeAdd(shapeDesc, didIChangeIt);
				
				// we'd already added the shape, it's now had either its properties or layout changed
				if (!oldDesc) {
					// possible to get a vestigal update belonging to nothing
					return;
				}
				if (event.nodeName==SHAPE_CONTAINER_NODE) {
					// the layout changed	
					whiteBoardModel.onShapeLayoutChangeProxy(shapeDesc, false); 
					dispatchEvent(new UpdateShapeEvent(UpdateShapeEvent.UPDATE_SHAPE_LAYOUT, shapeDesc));
				} else if (event.nodeName==SHAPE_PROPERTIES_NODE) {
					// the properties changed
					whiteBoardModel.onShapePropertiesChangeProxy(shapeDesc, false); 
					dispatchEvent(new UpdateShapeEvent(UpdateShapeEvent.UPDATE_SHAPE_PROPERTIES, shapeDesc));
				}
			}
		}
		
		[MessageHandler(selector="WhiteBoard", scope="local")]
		public function onItemRetract(msg: CollaborationItemRetracted):void
		{
			var event:CollectionNodeEvent = msg.event;
			
			if (event.nodeName!=SHAPE_DEFINITION_NODE) {
				return;
			}
			var shapeID:String = event.item.itemID;
			var shapeDesc:ShapeDescriptor = whiteBoardModel.shapesMap[shapeID];
			
			if(shapeDesc){
				whiteBoardModel.shapes.removeItem(shapeDesc);
				delete whiteBoardModel.shapesMap[shapeDesc.shapeID];
				
				whiteBoardModel.onItemRetractProxy(event);
			}
		}

		public function onShapeSelect(selectedShapeId: String): void {

            var propertyData: Object;
			var selectedShapeDesc: WBShapeDescriptor = whiteBoardModel.shapesMap[selectedShapeId];
			if(selectedShapeDesc)
                propertyData = selectedShapeDesc.propertyData;

            drawingModel.selectedShape = selectedShapeDesc;
            drawingModel.shapePropertyData = propertyData;
		}

		/**
		 * API for creating a new shape. Input is a shapedescriptor object. 
		 * Assign a unique shapeID... 
		 */
		public function createShape(shapeDesc:ShapeDescriptor):void
		{
			//trace("SharedWhiteBoard - createShape");
			if( !shapeDesc.shapeID ){

                var contentUnitId: String =
                        collaborationModel.currentContentUnit ? collaborationModel.currentContentUnit.id : "";
				shapeDesc.shapeID = UIDUtil.createUID();
                shapeDesc.contentUnitId = contentUnitId;

                whiteBoardModel.shapes.addItem(shapeDesc);
				whiteBoardModel.shapesMap[shapeDesc.shapeID] = shapeDesc;
				var shapeItem: Object = shapeDesc.createValueObject();
				var item:MessageItem = new MessageItem(SHAPE_DEFINITION_NODE, shapeItem, shapeDesc.shapeID);
				collectionNode.publishItem(item);
				
				// now dispatch event to add annotation corresponding to this shape...
				var reducedX: Number = shapeDesc.x + shapeDesc.width;
				var reducedY: Number = shapeDesc.y;
				reducedX = reducedX / collaborationModel.canvasWidth * 100;
				reducedY = reducedY / collaborationModel.canvasHeight * 100;
				
				dispatcher( new CreateAnnotation(reducedX, reducedY, shapeDesc.shapeID) );
			}
		}

		public function createText(localX: Number, localY: Number): void{

			var reducedX: Number;
			var reducedY: Number;
			reducedX = localX / collaborationModel.canvasWidth * 100;
			reducedY = localY / collaborationModel.canvasHeight * 100;
			dispatcher(new CreateLiveTextEdit(reducedX, reducedY));
		}
		
		public function removeShape(shapeID:String):void
		{
			try{
				//trace("SharedWhiteBoard - removeShape");
				collectionNode.retractItem(SHAPE_DEFINITION_NODE, shapeID);
				collectionNode.retractItem(SHAPE_PROPERTIES_NODE, shapeID);
				collectionNode.retractItem(SHAPE_CONTAINER_NODE, shapeID);
			}
			catch(e: Error){
				//trace("ERROR - "+e.message);
			}
			finally{
				
				dispatcher( new RemoveAnnotation(shapeID) );
			}
			
		}
		
		/**
		 * API for adding an existing shape with a shape ID to the canvas. 
		 * Input is the shapeDescriptor
		 * 
		 */
		public function addShape(shapeDesc: ShapeDescriptor):void
		{
			//trace("SharedWhiteBoard - addShape");
			var item:MessageItem = new MessageItem(SHAPE_DEFINITION_NODE, shapeDesc.createValueObject(), shapeDesc.shapeID);
			collectionNode.publishItem(item);
			
		}
		
		/**
		 * API for changing shape layout like x,y position width, height, rotation.
		 */
		public function moveSizeRotateShape(shapeDescID:String, p_x:Number, p_y:Number, p_w:Number, p_h:Number, p_rotation:int, p_allowLocalChange:Boolean = false):void
		{
			//trace("SharedWhiteBoard - moveSizeRotateShape");
			var shapeDesc:ShapeDescriptor = whiteBoardModel.shapesMap[shapeDescID];
			if (!shapeDesc) {
				return;
			}
			shapeDesc.shapeID = shapeDescID;
			shapeDesc.x = p_x;
			shapeDesc.y = p_y;
			shapeDesc.width = p_w;
			shapeDesc.height = p_h;
			shapeDesc.rotation = p_rotation;
			
			var shapeDescObject:Object = shapeDesc.createValueObject();
			if (p_allowLocalChange) {
				shapeDescObject.allowLocalChange = p_allowLocalChange;
			}
			
			var item:MessageItem = new MessageItem(SHAPE_CONTAINER_NODE, shapeDescObject, shapeDescID);
			collectionNode.publishItem(item);
			
			var reducedX: Number = shapeDesc.x + shapeDesc.width;
			var reducedY: Number = shapeDesc.y;
			reducedX = reducedX / collaborationModel.canvasWidth * 100;
			reducedY = reducedY / collaborationModel.canvasHeight * 100;
			
			dispatcher( new UpdateAnnotation(reducedX, reducedY, shapeDesc.shapeID) );
			
		}
		
		/**
		 * API for changing shapeID and property Data
		 */
		public function changeShapeProperties(shapeDescID:String, p_properties:*):void
		{
			//trace("SharedWhiteBoard - changeShapeProperties");
			var shapeDesc:ShapeDescriptor = whiteBoardModel.shapesMap[shapeDescID];
			shapeDesc.shapeID = shapeDescID;
			shapeDesc.propertyData = p_properties;
			
			var item:MessageItem = new MessageItem(SHAPE_PROPERTIES_NODE, shapeDesc.createValueObject(), shapeDescID);
			collectionNode.publishItem(item);
		}
		
		protected function sanityCheckFail(event: CollectionNodeEvent): Boolean {
			
			return (event.nodeName!=SHAPE_DEFINITION_NODE && 
				event.nodeName!=SHAPE_CONTAINER_NODE && 
				event.nodeName!=SHAPE_PROPERTIES_NODE);
		}

        protected function onContentUnitChange(newObject: Object=null): void{

            if(collaborationModel.currentContentUnit)
            {
                whiteBoardModel.contentUnitId = collaborationModel.currentContentUnit.id;
                whiteBoardModel.dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE));
            }
        }
		
		protected function parseBody(event: CollectionNodeEvent): ShapeDescriptor{
			
			var shapeDesc:ShapeDescriptor = new ShapeDescriptor();
			var shapePropertyData:Object = event.item.body.propertyData;
			var shapeDefinitionData:Object = event.item.body.definitionData;
			var factoryID:String = event.item.body.factoryID;
			
			if (factoryID == "com.adobe.coreUI.controls.whiteboardClasses.shapes::WBArrowShapeFactory") {
				if (shapeDefinitionData.arrowHead == true) {
					var arrowShapeDesc:WBArrowShapeDescriptor = new WBArrowShapeDescriptor();
					arrowShapeDesc.readValueObject(event.item.body);
					shapeDesc = arrowShapeDesc;
				} else {
					var lineShapeDesc:WBLineShapeDescriptor = new WBLineShapeDescriptor();
					lineShapeDesc.readValueObject(event.item.body);
					shapeDesc = lineShapeDesc;
				}
			} else if (factoryID == "com.adobe.coreUI.controls.whiteboardClasses.shapes::WBSimpleShapeFactory") {
				if (shapeDefinitionData == WBSimpleShape.ELLIPSE) {
					var ellipseShapeDesc:WBEllipseShapeDescriptor = new WBEllipseShapeDescriptor();
					ellipseShapeDesc.readValueObject(event.item.body);
					shapeDesc = ellipseShapeDesc;
				} else if (shapeDefinitionData == WBSimpleShape.ROUNDED_RECTANGLE) {
					if (shapePropertyData.primaryColor == 0xffff00 || shapePropertyData.alpha == 0.5 ) {
						var highLightAreaShapeDesc:WBHighlightAreaShapeDescriptor = new WBHighlightAreaShapeDescriptor();
						highLightAreaShapeDesc.readValueObject(event.item.body);
						shapeDesc = highLightAreaShapeDesc;
					} else {
						var rRectangleAreaShapeDesc:WBRoundedRectangleShapeDescriptor = new WBRoundedRectangleShapeDescriptor();
						rRectangleAreaShapeDesc.readValueObject(event.item.body);
						shapeDesc = rRectangleAreaShapeDesc;
					}
				} else if (shapeDefinitionData == WBSimpleShape.RECTANGLE) {
					var rectangleShapeDesc:WBRectangleShapeDescriptor = new WBRectangleShapeDescriptor();
					rectangleShapeDesc.readValueObject(event.item.body);
					shapeDesc = rectangleShapeDesc;
				} else {
					shapeDesc.readValueObject(event.item.body);
				}
			} else if (factoryID == "com.adobe.coreUI.controls.whiteboardClasses.shapes::WBTextShapeFactory") {
				var textShapeDesc:WBTextShapeDescriptor = new WBTextShapeDescriptor();
				textShapeDesc.readValueObject(event.item.body);
				shapeDesc = textShapeDesc;
			} else if (factoryID == "com.adobe.coreUI.controls.whiteboardClasses.shapes::WBMarkerShapeFactory") {
				var markerShapeDesc:WBMarkerShapeDescriptor = new WBMarkerShapeDescriptor();
				markerShapeDesc.readValueObject(event.item.body);
				shapeDesc = markerShapeDesc;
			} else {
				shapeDesc.readValueObject(event.item.body);
			}
			
			return shapeDesc;
		}
	}
}