package com.al.collaboration.components
{
	
	import spark.components.Label;
	import spark.components.supportClasses.SkinnableComponent;

	public class AnnotationMarker extends SkinnableComponent {
		
		[SkinPart]
		public var label: Label;

		public function AnnotationMarker() {
		}
		
		//--------------------------------------------------------------------------
		//
		//  Properties
		//
		//--------------------------------------------------------------------------
		//--------------------------------------------------
		// conversationId
		//--------------------------------------------------
		private var _conversationId: String;
		public function get conversationId(): String {
			
			return _conversationId;
		}
		
		public function set conversationId(value: String): void {
			
			if(value != _conversationId){
				_conversationId = value;
				invalidateProperties();
			}
		}

		//--------------------------------------------------------------------------
		//
		//  Override methods
		//
		//--------------------------------------------------------------------------
		override protected function partAdded(partName:String, instance:Object):void{
		
			super.partAdded(partName, instance);
		}
		
		override protected function commitProperties():void {
			
			super.commitProperties();
			
			if(label){
				label.text = conversationId ? conversationId : "";
			}
		}
	}
}