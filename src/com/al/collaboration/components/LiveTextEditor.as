/**
 * Created by lohithram on 13/05/15.
 */
package com.al.collaboration.components {

    import com.al.components.RichTextEditor;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import spark.components.Button;
    import spark.components.Group;

    [Event(name='close', type='flash.events.Event')]
    public class LiveTextEditor extends RichTextEditor {

        [SkinPart]
        public var dragArea: Group;

        [SkinPart]
        public var closeButton: Button;

        [Bindable]
        public var isTyping: Boolean;

        [Bindable]
        public var usersTyping: String;


        //-------------------------------------------------------
        // Constructor
        //-------------------------------------------------------
        public function LiveTextEditor() {
            super();
        }

        //-------------------------------------------------------
        //
        // Overrides
        //
        //-------------------------------------------------------
        override protected function partAdded(partName:String, instance:Object):void {

            super.partAdded(partName, instance);
            if(instance == closeButton){
                closeButton.addEventListener(MouseEvent.CLICK, onCloseClick);
            }
        }

        //-------------------------------------------------------
        //
        // Privates / Event handlers
        //
        //-------------------------------------------------------

        private function onCloseClick(event: Event): void {

            dispatchEvent(new Event(Event.CLOSE));
        }
    }
}
