package com.al.collaboration.components
{
	import com.adobe.coreUI.controls.whiteboardClasses.WBShapeContainer;
	import com.al.visualElement.IReducedVisualElement;
	
	public class WBShapeContainer extends com.adobe.coreUI.controls.whiteboardClasses.WBShapeContainer 
		implements IReducedVisualElement
	{
		//---------------------------------------------------
		// reducedX
		//---------------------------------------------------
		private var _reducedX: Number;
		public function get reducedX(): Number {
			
			return _reducedX ;
		}
		
		public function set reducedX(value: Number): void {
			
			_reducedX = value;
		}
		
		//---------------------------------------------------
		// reducedY
		//---------------------------------------------------
		private var _reducedY: Number;
		public function get reducedY(): Number {
			
			return _reducedY;
		}
		
		public function set reducedY(value: Number): void {
			
			_reducedY = value;
		}
		
	}
}