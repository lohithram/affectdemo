package com.al.collaboration.controller
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.events.SessionEvent;
	import com.adobe.rtc.session.IConnectSession;
	import com.adobe.rtc.sharedModel.CollectionNode;
	import com.adobe.rtc.sharedModel.SharedProperty;
	import com.al.collaboration.messages.CollaborationItemReceived;
	import com.al.collaboration.messages.CollaborationItemRetracted;
	import com.al.collaboration.messages.CollectionNodeSynchronized;
	import com.al.collaboration.messages.SubscribeToCollaborationNode;
	import com.al.collaboration.whiteboardClasses.SharedWBModel;

	/**
	 * This Class (during Demo stage) handles all aspects of establishing connection and
	 * synchronization with LCS
	 * @author lram
	 * 
	 */
	public class CollaborationAction
	{
		
		public static const COMMENTS: String = "Comments";
		public static const ANNOTATIONS: String = "Annotations";

		public static const TEXT_NODE: String = "Live_Text";
		public static const EDITING_NODE: String = "Live_Text_Editing";
		public static const SELECTION_NODE: String = "Live_Text_Selection";
		public static const SCROLL_POSITION_NODE: String = "Live_Text_ScrollPosition";

		public static const FORM_FIELDS: String = "Form_Fields";


		[Inject]
		[Bindable]
		public var connectSession: IConnectSession;
		
		[Inject(id="assetNode")]
		[Bindable]
		public var collectionNode: CollectionNode;
		
		protected var sharedId: String;
		
		[MessageDispatcher]
		public var dispatcher: Function;
		
		[Init]
		public function init(): void {
			
		}
		
		[Destroy]
		public function cleanUp(): void{
			
			//if(connectSession.isSynchronized)
			//	connectSession.logout();
			// Logoout only on application close or explicit app event.
		}
				
		[MessageHandler(scope="local")]
		public function subscribe(msg: SubscribeToCollaborationNode): void {
			
			var id: String = msg.sharedID;
			if(!collectionNode.isSynchronized){
				//trace("Subscribing to collection node - " + id);

				collectionNode.sharedID = id;
				collectionNode.subscribe();
				
				collectionNode.addEventListener(CollectionNodeEvent.SYNCHRONIZATION_CHANGE, onSynchronizationChange);
				collectionNode.addEventListener(CollectionNodeEvent.ITEM_RECEIVE, onItemReceive);
				collectionNode.addEventListener(CollectionNodeEvent.ITEM_RETRACT, onItemRetract);
				
				sharedId = id;
			}
		}
		
		protected function onSynchronizationChange(event:CollectionNodeEvent):void
		{
			if(event.target is SharedProperty)
				trace("Synchronization change -> " + (event.target as SharedProperty).nodeName);
			else if(event.target is CollectionNode)
				trace("Synchronization change -> " + (event.target as CollectionNode).sharedID);

			if(collectionNode.isSynchronized)
				dispatcher(new CollectionNodeSynchronized(collectionNode.sharedID, event));
		}
		
		protected function onItemReceive(event:CollectionNodeEvent):void{
			
			var msg: CollaborationItemReceived;
			switch(event.nodeName){

				case COMMENTS:
				case ANNOTATIONS:
				case TEXT_NODE:
				case FORM_FIELDS:
				case EDITING_NODE:
				case SELECTION_NODE:
				case SCROLL_POSITION_NODE:
					msg = new CollaborationItemReceived(event.nodeName, event);
					break;
				default:
					msg = new CollaborationItemReceived(CollaborationItemReceived.WHITE_BOARD, event);
			}
			dispatcher(msg);
		}
		
		protected function onItemRetract(event:CollectionNodeEvent): void{
			
			var msg: CollaborationItemRetracted;

			switch(event.nodeName){

				case COMMENTS:
				case ANNOTATIONS:
				case TEXT_NODE:
				case EDITING_NODE:
				case SELECTION_NODE:
				case SCROLL_POSITION_NODE:
				case FORM_FIELDS:
					msg = new CollaborationItemRetracted(event.nodeName, event);
					break;
				default:
					msg = new CollaborationItemRetracted(CollaborationItemReceived.WHITE_BOARD, event);
			}

			dispatcher(msg);
			
		}
	}
}