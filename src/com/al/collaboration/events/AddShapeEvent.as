package com.al.collaboration.events
{
	import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
	
	import flash.events.Event;
	
	public class AddShapeEvent extends Event
	{
		public static const ADD_SHAPE: String = "addShape";
		
		public function AddShapeEvent(desc: WBShapeDescriptor, bubbles: Boolean = false)
		{
			super(ADD_SHAPE, bubbles);
			_desc = desc;
		}
		
		private var _desc: WBShapeDescriptor;
		public function get desc(): WBShapeDescriptor{
			return _desc;
		}
	}
}