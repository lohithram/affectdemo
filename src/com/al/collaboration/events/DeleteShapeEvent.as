package com.al.collaboration.events
{
	import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
	
	import flash.events.Event;
	
	public class DeleteShapeEvent extends Event
	{
		public static const DELETE_SHAPE: String = "deleteShape";
		
		public function DeleteShapeEvent(desc: WBShapeDescriptor, bubbles: Boolean = false)
		{
			super(DELETE_SHAPE, bubbles);
			_desc = desc;
		}
		
		private var _desc: WBShapeDescriptor;
		public function get desc(): WBShapeDescriptor{
			return _desc;
		}
	}
}