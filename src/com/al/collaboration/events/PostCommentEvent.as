package com.al.collaboration.events
{
	import com.al.collaboration.annotation.model.Annotation;
	
	import flash.events.Event;
	
	public class PostCommentEvent extends Event
	{
		public static const POST_COMMENT: String = "postComment";
		
		public function PostCommentEvent(comment: String, annotation: Annotation) {
			
			super(POST_COMMENT, true);
			_commentText = comment;
			_annotation = annotation;			
		}
		
		private var _commentText: String;
		public function get commentText(): String {
			return _commentText;	
		}
		
		private var _annotation: Annotation;
		public function get annotation(): Annotation {
			return _annotation;
		}
		
		override public function clone(): Event{
			
			return new PostCommentEvent(commentText, annotation);
		}
	}
}