package com.al.collaboration.events
{
	import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
	
	import flash.events.Event;
	
	public class ShapeSelectedEvent extends Event
	{
		public static const SHAPE_SELECT: String = "shapeSelect";
		
		public function ShapeSelectedEvent(desc: WBShapeDescriptor)
		{
			super(SHAPE_SELECT, bubbles);
			_desc = desc;
		}
		
		private var _desc: WBShapeDescriptor;
		public function get desc(): WBShapeDescriptor{
			return _desc;
		}

		override public function clone(): Event {

			return new ShapeSelectedEvent(desc);
		}
	}
}