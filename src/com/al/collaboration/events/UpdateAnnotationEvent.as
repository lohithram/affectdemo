package com.al.collaboration.events
{
	import com.al.collaboration.annotation.model.Annotation;
	
	import flash.events.Event;
	
	public class UpdateAnnotationEvent extends Event
	{
		public static const UPDATE_ANNOTATION: String = "updateAnnotation";
		
		public function UpdateAnnotationEvent(annotation: Annotation) {
			
			super(UPDATE_ANNOTATION, true);
			_annotation = annotation;			
		}
		
		private var _annotation: Annotation;
		public function get annotation(): Annotation {
			return _annotation;
		}
		
		override public function clone(): Event{
			
			return new UpdateAnnotationEvent(annotation);
		}
	}
}