package com.al.collaboration.events
{
	import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
	
	import flash.events.Event;
	
	public class UpdateShapeEvent extends Event
	{
		public static const UPDATE_SHAPE_LAYOUT: String = "updateShapeLayout";
		public static const UPDATE_SHAPE_PROPERTIES: String = "updateShapeProperties";
		
		public function UpdateShapeEvent(type: String, desc: WBShapeDescriptor, bubbles: Boolean = false)
		{
			super(type, bubbles);
			_desc = desc;
		}
		
		private var _desc: WBShapeDescriptor;
		public function get desc(): WBShapeDescriptor{
			return _desc;
		}
	}
}