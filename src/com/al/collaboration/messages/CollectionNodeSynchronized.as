package com.al.collaboration.messages
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.messaging.MessageItem;
	
	public class CollectionNodeSynchronized
	{
		public function CollectionNodeSynchronized(nodeName: String, event: CollectionNodeEvent) {
			
			_event = event;
			this.nodeName = nodeName;
		}
		
		[Selector]
		public var nodeName: String;
		
		private var _event: CollectionNodeEvent;
		public function get event(): CollectionNodeEvent{ return _event; }
		
		public function get item(): MessageItem {
			
			return _event ? _event.item : null;
		}
	}
}
