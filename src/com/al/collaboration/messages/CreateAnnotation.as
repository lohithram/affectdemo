package com.al.collaboration.messages
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.messaging.MessageItem;
	
	public class CreateAnnotation
	{
		public function CreateAnnotation(reducedX: Number, reducedY: Number, shapeID: String) {
			
			_reducedX = reducedX;
			_reducedY = reducedY;
			_shapeID = shapeID;
		}
		
		private var _reducedX: Number;
		public function get reducedX(): Number{ return _reducedX; }

		private var _reducedY: Number;
		public function get reducedY(): Number{ return _reducedY; }
		
		private var _shapeID: String;
		public function get shapeID(): String{ return _shapeID; }
	}
}
