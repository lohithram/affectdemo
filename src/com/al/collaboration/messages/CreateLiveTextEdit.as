package com.al.collaboration.messages
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.messaging.MessageItem;
	
	public class CreateLiveTextEdit
	{
		public function CreateLiveTextEdit(reducedX: Number, reducedY: Number) {
			
			_reducedX = reducedX;
			_reducedY = reducedY;
		}
		
		private var _reducedX: Number;
		public function get reducedX(): Number{ return _reducedX; }

		private var _reducedY: Number;
		public function get reducedY(): Number{ return _reducedY; }
	}
}
