package com.al.collaboration.messages
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.messaging.MessageItem;
	
	public class RemoveAnnotation
	{
		public function RemoveAnnotation(shapeID: String) {
			
			_shapeID = shapeID;
		}
		
		private var _shapeID: String;
		public function get shapeID(): String{ return _shapeID; }
	}
}
