package com.al.collaboration.messages
{
	public class SubscribeToCollaborationNode
	{
		public function SubscribeToCollaborationNode(shareID: String) {
			
			_shareID = shareID;
		}
		
		private var _shareID: String;
		
		public function get sharedID(): String{ return _shareID; }
	}
}