package com.al.collaboration.messages
{

	import com.al.collaboration.annotation.model.LiveText;

	public class UpdateLiveText
	{
		public function UpdateLiveText(liveText: LiveText) {

			_liveText = liveText;
		}
		
		private var _liveText: LiveText;
		public function get liveText(): LiveText{ return _liveText; }

	}
}
