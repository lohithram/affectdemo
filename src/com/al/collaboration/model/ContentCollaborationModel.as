package com.al.collaboration.model
{
	import com.al.model.IContentUnit;

	import mx.collections.ArrayList;

	// IMPORTANT: For non-scaling shapes(Text, annotation) on the collaboration layer we use the
	// absolute dimension of the collaboration layer
	// For scaling like drawings we use canvas width and height(currently the reference dimension is 800x800.

	public class ContentCollaborationModel
	{
		[Bindable]
		public var collaborationLayerWidth: Number;

		[Bindable]
		public var collaborationLayerHeight: Number;
		
		[Bindable]
		public var canvasWidth: Number;
		
		[Bindable]
		public var canvasHeight: Number;
		
		[Bindable]
		public var currentContentUnit: IContentUnit;
	}
}