package com.al.collaboration.model
{
	public interface ICollaborationItem
	{
		function getObject(): Object;
		
		function update(withObject: Object): void;
	}
}