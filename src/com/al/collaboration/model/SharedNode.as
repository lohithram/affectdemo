package com.al.collaboration.model
{
	import com.adobe.rtc.session.IConnectSession;
	import com.adobe.rtc.session.ISessionSubscriber;
	import com.adobe.rtc.sharedModel.CollectionNode;
	import com.adobe.rtc.sharedModel.SharedProperty;
	
	import flash.events.EventDispatcher;

	public class SharedNode extends SharedProperty
	{
		public function SharedNode(name: String, collectionNode: CollectionNode,
									connectSession: IConnectSession){
			
			super();
			this.nodeName = name;
			this.updateInterval = 1000;
			this.collectionNode = collectionNode;
			this.connectSession = connectSession;
		}
	}
}