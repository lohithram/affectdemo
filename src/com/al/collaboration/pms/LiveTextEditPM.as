package com.al.collaboration.pms
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.messaging.MessageItem;
	import com.adobe.rtc.session.IConnectSession;
	import com.adobe.rtc.sharedModel.CollectionNode;
	import com.al.collaboration.annotation.model.LiveText;
	import com.al.collaboration.messages.CollaborationItemReceived;
	import com.al.collaboration.messages.CollaborationItemRetracted;
	import com.al.collaboration.messages.CreateLiveTextEdit;
    import com.al.collaboration.messages.RemoveAnnotation;
    import com.al.collaboration.messages.UpdateLiveText;
	import com.al.collaboration.model.ContentCollaborationModel;
	import com.al.model.CollaborationConfigurationModel;
	import com.al.model.UserModel;

    import flash.events.Event;

    import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayList;
	import mx.utils.UIDUtil;

	/**
	 * This Class (during Demo stage) handles all aspects of establishing connection and
	 * synchronization with LCS
	 * @author lram
	 * 
	 */
	public class LiveTextEditPM extends EventDispatcher
	{
		public static const EDITING_NODE: String = "Live_Text_Editing";
		public static const SCROLL_POSITION_NODE: String = "Live_Text_ScrollPosition";

		public static const SELECTION_NODE: String = "Live_Text_Selection";
		public static const TEXT_NODE: String = "Live_Text";

        public static const LIVE_TEXT_UPDATE: String = "liveTextUpdate";
		
		[Inject]
		public var collaborationConfig: CollaborationConfigurationModel;
		
		[Inject]
		[Bindable]
		public var connectSession: IConnectSession;

		[Inject(id="assetNode")]
		public var collectionNode: CollectionNode;
		
		[Bindable]
		[ArrayElementType("com.al.collaboration.annotation.model.LiveText")]
		public var liveTexts: ArrayList;
		
		[Inject]
		public var userModel: UserModel;
		
		[Inject]
		public var collaborationModel: ContentCollaborationModel;
		
		[Bindable]
		public var sharedId: String;

		private var liveTextsMap: Dictionary = new Dictionary(true);
		private var contentUnitLiveTexts: Dictionary = new Dictionary(true);
		
		[Init]
		public function init(): void{

			liveTexts = new ArrayList();
			BindingUtils.bindSetter(onContentUnitChange, collaborationModel, "currentContentUnit");
		}
		
		/**
		 * Use the shapeID as the id of the annotation 
		 * @param msg
		 * 
		 */
		[MessageHandler(scope="local")]
		public function create(msg: CreateLiveTextEdit): void{
			
			createNewTextEdit(msg.reducedX, msg.reducedY);
		}

		[MessageHandler(scope="local")]
		public function update(msg: UpdateLiveText): void{

			updateLiveTextWithNewLoc(msg.liveText.reducedX, msg.liveText.reducedY, msg.liveText.id);
		}

		/*[MessageHandler(scope="local")]
		public function remove(msg: RemoveAnnotation): void{
			
			//removeAnnotation(msg.shapeID);
		}*/
		
		[MessageHandler(selector="Live_Text", scope="local")]
		public function onLiveTextRetract(msg: CollaborationItemRetracted): void{
			
			var id: String = msg.event.item.itemID;
			var liveText: LiveText = liveTextsMap[id];
			if(liveText){
				
				liveTexts.removeItem(liveText);
				delete liveTextsMap[id];
			}
		}
		[MessageHandler(selector="Live_Text_Editing", scope="local")]
		public function onLiveTextEditingRetract(msg: CollaborationItemRetracted): void{

            var username: String = msg.event.item.itemID;
			var id: String = msg.event.item.body.liveTextId;
			var liveText: LiveText = liveTextsMap[id];
			if(liveText){
				removeUserEditingStatusFromLiveText(liveText, username);
			}
            if(userToLiveTextEditMap[username])
                delete userToLiveTextEditMap[username];
        }

		/**
		 * TODO: Its the responsibility of the annotationPM to take localX and localY 
		 * and reduce it
		 * To create a new annoation 
		 * @param x
		 * @param y
		 * 
		 */
		public function createNewTextEdit(x: Number, y: Number, id: String=null): void{
			
			var liveText: LiveText = new LiveText(id ? id : UIDUtil.createUID());
			liveText.reducedX = x;
			liveText.reducedY = y;
			liveText.width = 160;
			liveText.height = 120;
			liveText.contentUnitId =
				collaborationModel.currentContentUnit ? collaborationModel.currentContentUnit.id : "";
			
			if(collectionNode.isSynchronized)
				collectionNode.publishItem( 
					new MessageItem( TEXT_NODE, liveText.getObject(), liveText.id ) );
		}
		
		public function updateLiveTextWithNewLoc(x: Number, y: Number, id: String): void{
			
			var liveText: LiveText = liveTexts[id];
			if(liveText){

				liveText.reducedX = x;
				liveText.reducedY = y;
				updateLiveText(liveText);
			}
		}
		
		public function updateEditingStatus(liveText: LiveText, isEditing: Boolean): void {

            var overwrite: Boolean = userToLiveTextEditMap[liveText.id] != null;
            if (collectionNode.isSynchronized) {

                if(isEditing)
                    collectionNode.publishItem(
                            new MessageItem(EDITING_NODE, {liveTextId: liveText.id}, userModel.displayName), overwrite);
                else
                    collectionNode.retractItem(EDITING_NODE, userModel.displayName);
            }
		}

		public function updateLiveText(liveText: LiveText): void{

			if(collectionNode.isSynchronized)
				collectionNode.publishItem(
					new MessageItem( TEXT_NODE, liveText.getObject(), liveText.id ), true );
		}
		
		public function removeLiveText(id: String): void{
			
			if(collectionNode.isSynchronized){
				collectionNode.retractItem( TEXT_NODE, id );
				removeAssociatedData(liveTextsMap[id]);
			}
		}
		
		[MessageHandler(selector="Live_Text", scope="local")]
		public function onLiveTextReceive(msg: CollaborationItemReceived):void
		{
			var event: CollectionNodeEvent = msg.event;
			var liveText: LiveText = liveTextsMap[event.item.itemID] as LiveText;
			if(liveText){
				liveText.update(event.item);
                dispatchEvent( new Event(LIVE_TEXT_UPDATE) );
			}else
			{
				liveText = new LiveText(event.item.itemID);
				liveTextsMap[event.item.itemID] = liveText;
				liveText.update(event.item);
				
				if(liveText.contentUnitId){
					
					getLiveTextsForContentUnit(liveText.contentUnitId).addItem(liveText);
				}
				else
					liveTexts.addItem(liveText);
			}
		}

        protected var userToLiveTextEditMap: Dictionary = new Dictionary();
		[MessageHandler(selector="Live_Text_Editing", scope="local")]
		public function onLiveTextTypingReceive(msg: CollaborationItemReceived):void
		{
			var event: CollectionNodeEvent = msg.event;
            if(event.item.itemID != userModel.displayName) {

                var userName: String = event.item.itemID;
                var liveTextId: String = event.item.body.liveTextId;

                var liveText:LiveText = liveTextsMap[liveTextId] as LiveText;
                if (liveText) {

                    liveText.isRemoteEditing = true;
                    if(!liveText.usersEditing.contains(userName)){

                        liveText.remoteUser = userName;
                        liveText.usersEditing.addItem(userName);
                    }
                }

                var prevRemoteUserLiveTextEdit: String = userToLiveTextEditMap[userName];
                if(prevRemoteUserLiveTextEdit && prevRemoteUserLiveTextEdit != liveTextId) {
                    var prevLiveText:LiveText = liveTextsMap[liveTextId] as LiveText;
                    if (prevLiveText) {
                        removeUserEditingStatusFromLiveText(prevLiveText, userName);
                    }
                }
                userToLiveTextEditMap[userName] = liveTextId;
            }
		}

        protected function removeUserEditingStatusFromLiveText(liveText: LiveText, username: String): void{

            if(liveText.usersEditing.contains(username)){
                liveText.usersEditing.removeItem(username);
            }
            liveText.isRemoteEditing = liveText.usersEditing.length > 0;
            if(liveText.isRemoteEditing){
                liveText.remoteUser = liveText.usersEditing.getItemAt(liveText.usersEditing.length-1) as String;
            }
        }

		protected function onContentUnitChange(oldObject: Object=null, newObject: Object=null): void{
			
			if(collaborationModel && collaborationModel.currentContentUnit){

                liveTexts = getLiveTextsForContentUnit(collaborationModel.currentContentUnit.id);
            }
		}

        protected function getLiveTextsForContentUnit(contentUnitId: String): ArrayList {

            if(!contentUnitLiveTexts[contentUnitId])
                contentUnitLiveTexts[contentUnitId] = new ArrayList();

            return contentUnitLiveTexts[contentUnitId] as ArrayList;
        }

		protected function removeAssociatedData(liveText: LiveText): void{
			
			if(!liveText) return;
			//TODO: Remove the associated items in the other three nodes
		}
	}
}