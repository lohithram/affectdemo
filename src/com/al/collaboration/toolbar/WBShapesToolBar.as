/*
*
* ADOBE CONFIDENTIAL
* ___________________
*
* Copyright [2007-2010] Adobe Systems Incorporated
* All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
*/
package com.al.collaboration.toolbar
{
import com.adobe.coreUI.controls.whiteboardClasses.IWBShapeFactory;
import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBShapeToolBarDescriptor;
import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBToolBarDescriptor;
import com.adobe.coreUI.events.WBToolBarEvent;
import com.adobe.coreUI.localization.ILocalizationManager;
import com.adobe.coreUI.localization.Localization;
import com.al.collaboration.view.WBCanvas;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.utils.Dictionary;
import flash.xml.XMLNode;

import mx.collections.ArrayCollection;
import mx.collections.ICollectionView;
import mx.collections.IViewCursor;
import mx.collections.ListCollectionView;
import mx.collections.XMLListCollection;
import mx.controls.menuClasses.IMenuDataDescriptor;
import mx.controls.treeClasses.DefaultDataDescriptor;
import mx.core.UIComponent;
import mx.events.CollectionEvent;
import mx.events.FlexEvent;
import mx.managers.ToolTipManager;

import spark.components.BorderContainer;
import spark.components.Button;
import spark.components.Group;
import spark.components.Label;
import spark.components.ToggleButton;
import spark.components.supportClasses.ButtonBase;
import spark.layouts.HorizontalAlign;
import spark.layouts.VerticalLayout;

[Event(name="toolBarClick", type="com.adobe.coreUI.events.WBToolBarEvent")]
	[Event(name="toolBarChange", type="com.adobe.coreUI.events.WBToolBarEvent")]
	
	/**
	 * @private
	 */
	public class  WBShapesToolBar extends BorderContainer
	{

		protected static const SIDE_BUFFERS:int = 6;

		public var useTitleBar:Boolean = true;
		public var allowSave:Boolean = true;

        [SkinPart]
		public var titleBar: Group;

		protected var _lm:ILocalizationManager;

		protected var _dataDescriptor:IMenuDataDescriptor = new DefaultDataDescriptor();
		protected var _rootModel:ICollectionView;
		protected var _invDataProviderChanged:Boolean = false;
		protected var _hasRoot:Boolean = false;
		protected var _selectedButt: ToggleButton;
		protected var _currentSubToolBar: WBShapesToolBar;

		protected var _controls:Array = new Array();
		protected var _buttonHeight:int = 32;
		protected var _buttonWidth:int = 42;

		//protected var _wbCanvas:WBCanvas;

		public function set wbCanvas(p_canvas:WBCanvas):void
		{
			_wbCanvas = p_canvas;
			invalidateProperties();
			//			_wbCanvas.addEventListener("selectionChange", switchToSelectionTool);
		}

		override protected function createChildren():void
		{
            super.createChildren();
			_lm = Localization.impl;
            layout = new VerticalLayout();
            (layout as VerticalLayout).horizontalAlign = HorizontalAlign.CENTER;
		}

        override protected function partAdded(name: String, instance: Object): void {

            super.partAdded(name, instance);
            if(instance == titleBar){

                titleBar.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
            }
        }

		protected function startDragging(p_evt:MouseEvent):void
		{
			stage.addEventListener(MouseEvent.MOUSE_UP, stopDragging, false, 0, true);
			var rect:Rectangle;
			if (parent is UIComponent) {
				rect = new Rectangle(0,0,Math.max(0,parent.width-width),Math.max(0,parent.height-30));
			}
			startDrag(false, rect);
		}

		protected function stopDragging(p_evt:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, stopDragging);
			stopDrag();
		}

		private var buttonMap: Dictionary = new Dictionary();
		override protected function commitProperties():void
		{
			if (!_rootModel) {
				var data:WBToolBarDescriptor = new WBToolBarDescriptor(true);
				processDP(data);
				_invDataProviderChanged = true;
			}

			if (_invDataProviderChanged) {
				_invDataProviderChanged = false;

				if (_currentSubToolBar) {
					closeSubToolBar();
				}
				for (var i:int=0; i<_controls.length; i++) {
					var b:Button = _controls[i] as Button;
					if (b) {
						b.removeEventListener(MouseEvent.CLICK, processClick);
					}
					removeElement(_controls[i]);
				}
				_controls = new Array();


				var topLevel:ICollectionView;

				var rootItem:* = _rootModel.createCursor().current;
				if (rootItem != null &&
					_dataDescriptor.isBranch(rootItem, _rootModel) &&
					_dataDescriptor.hasChildren(rootItem, _rootModel))
				{
					// then get rootItem children
					topLevel = _dataDescriptor.getChildren(rootItem, _rootModel);
				} else {
					topLevel = _rootModel;
				}

				var cursor:IViewCursor = topLevel.createCursor();
				var curr:Object = cursor.current;
				while (curr) {
					if (curr.type=="label") {
						var lbl: Label = new Label();
						addElement(lbl);
						lbl.text = curr.label;
						_controls.push(lbl);
					} else {
						if (curr.icon==WBToolBarDescriptor.ICON_SAVE && !allowSave) {
							cursor.moveNext();
							curr = cursor.current;
							continue
						}
						var butt:ButtonBase = (curr.type=="tool") ? new ToggleButton() : new Button();
                        butt.styleName = (butt is ToggleButton) ? "iconToggleButton iconToggleButtonBlue" : "iconButton iconButtonBlue";

                        addElement(butt);
						buttonMap[butt] = curr;

						butt.setActualSize(_buttonWidth, _buttonHeight);
						butt.addEventListener(MouseEvent.CLICK, processClick);
						_controls.push(butt);
						if (curr.icon) {
							butt.setStyle("icon", curr.icon);
						}
						if (curr.toolTip) {
							butt.toolTip = curr.toolTip;
						}
						
						if (_dataDescriptor.hasChildren(curr, _rootModel)) {
							// it's a sub-toolbar creator. 
							openSubToolBar(butt, curr);
							closeSubToolBar();
						}

					}
;
					if (measuredWidth<20) { 
						measuredWidth = _buttonWidth + 2*SIDE_BUFFERS;
					}
					cursor.moveNext();
					curr = cursor.current;
				}
			}
		}
		
		override protected function measure():void
		{
			//
		}
		
		override protected function updateDisplayList(p_w:Number, p_h:Number):void
		{
			
			var buttW:int = p_w - 2*SIDE_BUFFERS;
			for (var i:int=0; i<_controls.length; i++) {
				var b:Button = _controls[i] as Button;
				if (b) {
					b.setActualSize(buttW, b.height);
				}
			}
		}
		
		protected function closeSubToolBar():void
		{
			removeElement(_currentSubToolBar);
			_currentSubToolBar = null;
		}
		
		protected function openSubToolBar(p_button:ButtonBase, node: Object):void
		{
			_currentSubToolBar = new WBShapesToolBar();
			_currentSubToolBar.dataProvider = _dataDescriptor.getChildren(node, _rootModel);
			_currentSubToolBar.useTitleBar = false;
			_currentSubToolBar.wbCanvas = _wbCanvas;
			addElement(_currentSubToolBar);
			_currentSubToolBar.validateNow();
			_currentSubToolBar.setActualSize(_currentSubToolBar.measuredWidth, _currentSubToolBar.measuredHeight);
			_currentSubToolBar.move(p_button.x+p_button.width+2, p_button.y-10);
		}
		
		protected function processClick(p_evt:Event):void
		{
			ToolTipManager.enabled = false;
			var target:ButtonBase = p_evt.target as ButtonBase;
			var node: Object = buttonMap[target];

			if (node.type=="tool") {
				if (_currentSubToolBar) {
					closeSubToolBar();
				}
				if (_dataDescriptor.hasChildren(node, _rootModel)) {
					// it's a sub-toolbar creator. 
					openSubToolBar(target, node);
				} else {
					_wbCanvas.enableShapeSelection = (node.shapeFactory==null);
					var factory:IWBShapeFactory = node.shapeFactory as IWBShapeFactory;
					if (node.shapeData) {
						factory.shapeData = node.shapeData;
					}
					_wbCanvas.currentShapeFactory = factory;
				}
				if (_selectedButt) {
					_selectedButt.selected = false;
				}
				_selectedButt = target as ToggleButton;
				_selectedButt.selected = true;
			} else if (node.type=="command") {
				var command:String = node.command;
				if (command==WBToolBarDescriptor.COMMAND_UNDO) {
					_wbCanvas.undo();
				} else if (command==WBToolBarDescriptor.COMMAND_REDO) {
					_wbCanvas.redo();
				} else if (command==WBToolBarDescriptor.COMMAND_DELETE_SELECTED) {
					_wbCanvas.removeSelectedShapes();
				}
			}
			dispatchEvent(new WBToolBarEvent(WBToolBarEvent.TOOL_BAR_CLICK, node));
			dispatchEvent(new WBToolBarEvent(WBToolBarEvent.TOOLBAR_CHANGE, node));
			ToolTipManager.enabled = true;
		}
		
		public function set dataProvider(p_data:Object):void
		{
			if (_rootModel) {
				//	            _rootModel.removeEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);
			}
			//flag for processing in commitProps
			processDP(p_data);
			_invDataProviderChanged = true;
			invalidateProperties();			
		}
		
		/**
		 * Add a toolBar component to the Toolbar
		 * 
		 * @param p_WBShapesToolBarDescriptor Descriptor for the tool bar component.
		 */ 
		public function addCustomShapeToToolBar(p_WBShapeToolBarDescriptor:WBShapeToolBarDescriptor):void
		{
			if (_rootModel) {
				var topLevel:ListCollectionView;
				
				var rootItem:* = _rootModel.createCursor().current;
				if (rootItem != null &&
					_dataDescriptor.isBranch(rootItem, _rootModel) &&
					_dataDescriptor.hasChildren(rootItem, _rootModel))
				{
					// then get rootItem children
					topLevel = _dataDescriptor.getChildren(rootItem, _rootModel) as ListCollectionView;
				} else {
					topLevel = _rootModel as ListCollectionView;
				}
				topLevel.addItem(p_WBShapeToolBarDescriptor);
			}
			processDP(_rootModel);
			_invDataProviderChanged = true;
			commitProperties();
			dispatchEvent(new WBToolBarEvent(WBToolBarEvent.TOOLBAR_CHANGE));
		}
		
		public function get dataProvider():Object
		{
			return _rootModel;
		}
		
		public function get dataDescriptor():IMenuDataDescriptor
		{
			return _dataDescriptor;
		}
		
		protected function collectionChangeHandler(p_evt:CollectionEvent):void
		{
			// TODO: rebuild!
		}
		
		protected function processDP(p_data:Object):void
		{
			
			// handle strings and xml
			if (typeof(p_data)=="string")
				p_data = new XML(p_data);
			else if (p_data is XMLNode)
				p_data = new XML(XMLNode(p_data).toString());
			else if (p_data is XMLList)
				p_data = new XMLListCollection(p_data as XMLList);
			
			if (p_data is XML)
			{
				_hasRoot = true;
				var xl:XMLList = new XMLList();
				xl += p_data;
				_rootModel = new XMLListCollection(xl);
			}
				//if already a collection dont make new one
			else if (p_data is ICollectionView)
			{
				_rootModel = ICollectionView(p_data);
				
				if (_rootModel.length == 1)
					_hasRoot = true;
			}
			else if (p_data is Array)
			{
				_rootModel = new ArrayCollection(p_data as Array);
			}
				//all other types get wrapped in an ArrayCollection
			else if (p_data is Object)
			{
				_hasRoot = true;
				// convert to an array containing this one item
				var tmp:Array = [];
				tmp.push(p_data);
				_rootModel = new ArrayCollection(tmp);
			}
			else
			{
				_rootModel = new ArrayCollection();
			}
		}
		
	}
}