/*
*
* ADOBE CONFIDENTIAL
* ___________________
*
* Copyright [2007-2010] Adobe Systems Incorporated
* All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
*/
package com.al.collaboration.view
{

	import com.adobe.coreUI.controls.whiteboardClasses.IWBShapeFactory;
	import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBShapeToolBarDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.WBModel;
	import com.adobe.coreUI.events.WBCanvasEvent;
	import com.adobe.coreUI.events.WBModelEvent;
	import com.adobe.coreUI.events.WBToolBarEvent;
	import com.adobe.rtc.session.IConnectSession;
	import com.adobe.rtc.session.ISessionSubscriber;
	import com.al.collaboration.annotation.pms.WhiteBoardPM;
	import com.al.collaboration.whiteboardClasses.SharedCursorPane;
	import com.al.collaboration.whiteboardClasses.SharedWBModel;
	import com.al.events.ShapePropertiesChangeEvent;

	import flash.events.IEventDispatcher;

	/**
	 * Dispatched when the whiteboard goes in and out of sync.
	 */
	[Event(name="synchronizationChange", type="com.adobe.coreUI.events.WBModelEvent")]	
	
	/**
	 * The SharedWhiteBoard is a UIComponent that allows multiple users to 
	 * collaboratively draw on a shared canvas. In general, users with a publisher
	 * role and higher are allowed to publish and users with a viewer role can 
	 * see the drawing.
	 * 
	 * TODO: Due to extreme complexity, full documentation is still forthcoming... I know we said we would do this, 
	 * it's on the list for the next beta drop =)
	 */
	public class  SharedWhiteBoard extends WhiteBoard implements ISessionSubscriber
	{
		
		/**
		 * @private
		 */
		protected var _cursorPane:SharedCursorPane;
		
		[Inspectable(enumeration="false,true", defaultValue="false")]
		/**
		 * Whether or not the whiteboard should be cleaned upon the end of the session.
		 */
		public var sessionDependent:Boolean = false;
		
		/**
		 * @private
		 */
		public var isStandalone:Boolean = true;

		/**
		 * @private
		 */
		protected var _subscribed:Boolean = false ;
		
		/**
		 * @private 
		 */		
		protected var _connectSession:IConnectSession;
		
		private var _pm: WhiteBoardPM;
		public function get pm(): WhiteBoardPM {
			return _pm;
		}
		
		public function set pm(value: WhiteBoardPM): void{
			
			if(_pm != value){
				_pm = value;
				invalidateProperties();
			}
		}
			
		
		/**
		 * Disposes all listeners to the network and framework classes. Recommended 
		 * for proper garbage collection of the component.
		 */
		public function close():void
		{
			if (_cursorPane ) {
				_cursorPane.close();
			}
			if ( _model ) {
				SharedWBModel(_model).close();
			}
			_canvas.close();
			if (contains(_canvas)) {
				removeChild(_canvas);
			}
			_canvas = null;
		}
				
		/**
		 * The IConnectSession with which this component is associated. 
		 * Note that this may only be set once before <code>subscribe()</code>
		 * is called; re-sessioning of components is not supported. 
		 * Defaults to the first IConnectSession created in the application.
		 */
		public function get connectSession():IConnectSession
		{
			return _connectSession;
		}
		
		/**
		 * Sets the IConnectSession with which this component is associated. 
		 */
		public function set connectSession(p_session:IConnectSession):void
		{
			_connectSession = p_session;
			invalidateProperties();
		}
		
		private var _sharedID: String;
		public function get sharedID():String{
			return _sharedID;
		}
		
		public function set sharedID(value:String): void{
			
			if(_sharedID != value){
				_sharedID = value;
				invalidateProperties();
			}
		}

		protected var _toolBar: IEventDispatcher;
		/**
		 * The shapes tool Bar
		 */
		public function get toolBar(): IEventDispatcher
		{
			return _toolBar;
		}

		/**
		 * @private
		 */
		public function set toolBar(value: IEventDispatcher):void
		{
			removeToolbarListeners();
			_toolBar = value;
			addToolbarListeners();
		}

		/**
		 * @private
		 */
		override protected function createChildren():void
		{
			super.createChildren();
		}
		
		/**
		 * Tells the component to begin synchronizing with the service. 
		 * For UIComponent-based components such as this one,
		 * <code>subscribe()</code> is called automatically upon being added to the <code>displayList</code>. 
		 * For "headless" components, this method must be called explicitly.
		 */
		public function subscribe():void
		{
			// if(!connectSession) return;
			
			if (_model) {
				
				//trace("Subscribing to Whiteboard, sharedID - " + sharedID);
				_canvas.model = _model;
				var sharedWBModel:SharedWBModel = SharedWBModel(_model) ;
				//sharedWBModel.sessionDependent = sessionDependent;
				//sharedWBModel.connectSession = connectSession;
				//sharedWBModel.sharedID = sharedID;
				//sharedWBModel.addEventListener(WBModelEvent.MY_ROLE_CHANGE, onMyRoleChange);
				//sharedWBModel.addEventListener("synchronizationChange", onSyncChange);
				// since the canvas's model has changed, we need to assign it to the toolbar


				// But get the cursor pane to listen to collection node
				if(_cursorPane) {

                    //trace("Subscribing to shared cursor collection, sharedID - " + sharedID);
					_cursorPane.connectSession = connectSession;
                    _cursorPane.collectionNode = sharedWBModel.collectionNode;
                    _cursorPane.user = pm.userModel;
					_cursorPane.subscribe();
				}

				_subscribed = true;
			}
		}
		
		/**
		 * 
		 * @inheritdoc
		 * 
		 */
		override public function set model(p_wbModel:WBModel):void
		{
			super.model = p_wbModel;
			if (p_wbModel.isSynchronized) {
				onSyncChange(); 
			}
			var sharedWBModel:SharedWBModel = p_wbModel as SharedWBModel;
			sharedWBModel.addEventListener(WBModelEvent.MY_ROLE_CHANGE, onMyRoleChange);
			sharedWBModel.addEventListener("synchronizationChange", onSyncChange);
		} 
		
		/**
		 *  Sets the role of a given user for the WhiteBoard.
		 * 
		 * @param p_userRole The role value to set on the specified user.
		 * @param p_userID The ID of the user whose role should be set.
		 */
		public function setUserRole(p_userID:String,p_userRole:int):void
		{
			if ( p_userID == null ) 
				return ;
			
			SharedWBModel(_model).setUserRole(p_userID,p_userRole);
		}
		
		
		/**
		 *  Returns the role of a given user for the WhiteBoard.
		 * 
		 * @param p_userID The user ID for the user being queried.
		 */
		public function getUserRole(p_userID:String):int
		{
			if ( p_userID == null ) {
				throw new Error("CameraModel: USerId can't be null");
			}
			
			return SharedWBModel(_model).getUserRole(p_userID);
		}
		
		[Bindable(event="synchronizationChange")]
		/**
		 * Returns true if the model is synchronized; otherwise, false.
		 */
		public function get isSynchronized():Boolean
		{
			if ( !_model ) {
				return false ;
			}
			
			return _model.isSynchronized ;
		}
		
		/**
		 * Registers the factory class for creating a custom shape. Custom Shapes are not drawn if the Shapes class is not 
		 * registered. The custom shapes are always registered with the SharedWhiteBoard when they are added to the toolBar.
		 * This method is needed when shapes are added programatically or using other UIComponents such as the button.
		 * <code>SharedWhiteBoard.registerFactory(new WBCustomShapeFactory(CustomShape,null,null))</code>  
		 */
		public function registerFactory(p_factory:IWBShapeFactory):void
		{
			_canvas.registerFactory(p_factory);
		}
		
		
		/**
		 * @private
		 */
		override protected function commitProperties():void
		{
			if (!_cursorPane) {

                _cursorPane = new SharedCursorPane();
				addElement(_cursorPane);

				SharedWBModel(_model).sharedCursorPane = _cursorPane;
                _canvas.addEventListener(WBCanvasEvent.CURSOR_CHANGE, onCursorChange);
			}

            canvas.pm = pm;

            if ( !_subscribed ) {
				subscribe();
			}

            super.commitProperties();
		}
		
		
		/**
		 * @private
		 */
		protected function onCursorChange(p_evt:WBCanvasEvent):void
		{
			_cursorPane.myCursorClass = _canvas.currentCursorClass;
		}
		
		/**
		 * @private
		 */
		protected function onSyncChange(p_evt:WBModelEvent=null):void
		{
			if (SharedWBModel(_model).isSynchronized) {
				onMyRoleChange();
			} else {
				// we disconnected
				_canvas.selectedShapeIDs = [];
				_canvas.enableShapeSelection = false;
				_canvas.currentShapeFactory = null;
			}
			if (p_evt!=null) {
				dispatchEvent(p_evt);
			}
		}
		
		/**
		 * @private
		 */
		protected function onMyRoleChange(p_evt:WBModelEvent=null):void
		{
			if (SharedWBModel(_model).canUserDraw(_connectSession.userManager.myUserID)) {
				_canvas.selectedShapeIDs = [];
				_canvas.enableShapeSelection = false;
				_canvas.currentShapeFactory = null;
				_canvas.clearTextEditor();
			}
			if (p_evt) {
				dispatchEvent(p_evt);
			}
		}
		
		/**
		 * @private
		 */
		override protected function updateDisplayList(p_width:Number, p_height:Number):void
		{
			super.updateDisplayList(p_width, p_height);
			if (_cursorPane) {
				if (!isStandalone) {
					_cursorPane.setActualSize(p_width*Math.min(_zoomLevel, 1), p_height*Math.min(_zoomLevel, 1));
				} else {
					_cursorPane.setActualSize(p_width, p_height);
				}
			}
			_canvas.width = p_width;
			_canvas.height = p_height;
		}

		//------------------------------------------------------------------------------------------
		//
		// Event Listeners
		//
		//------------------------------------------------------------------------------------------

		private function onShapePropertyChange(event: ShapePropertiesChangeEvent): void {

			canvas.applyPropertySet(event.propertySet);
		}

		private function onToolSelection(event: WBToolBarEvent): void {

			var desc: WBShapeToolBarDescriptor = event.item as WBShapeToolBarDescriptor;
			if(!desc) {

				canvas.enableShapeSelection = false;
				canvas.currentShapeFactory = null;
			}
			else if(desc.type == 'tool'){

				canvas.enableShapeSelection = (desc.shapeFactory == null);
				if (desc.shapeData) {
					desc.shapeFactory.shapeData = desc.shapeData;
				}
				canvas.currentShapeFactory = desc.shapeFactory;
			}
		}

		//------------------------------------------------------------------------------------------
		//
		// Private
		//
		//------------------------------------------------------------------------------------------

		private function addToolbarListeners(): void{

			if(!toolBar) return;

			toolBar.addEventListener(WBToolBarEvent.TOOLBAR_CHANGE, onToolSelection);
			toolBar.addEventListener(ShapePropertiesChangeEvent.PROPERTY_CHANGE, onShapePropertyChange);
		}

		private function removeToolbarListeners(): void{

			if(!toolBar) return;

			toolBar.removeEventListener(WBToolBarEvent.TOOLBAR_CHANGE, onToolSelection);
			toolBar.removeEventListener(ShapePropertiesChangeEvent.PROPERTY_CHANGE, onShapePropertyChange);
		}





	}
}