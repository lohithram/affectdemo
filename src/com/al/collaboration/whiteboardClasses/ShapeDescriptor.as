/**
 * Created by synesis on 21/04/2015.
 */
package com.al.collaboration.whiteboardClasses
{

    import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;

    public class ShapeDescriptor extends WBShapeDescriptor
    {

        public var contentUnitId: String;

        public function ShapeDescriptor() {
            super();
        }

        override public function readValueObject(value: Object): void {

            super.readValueObject(value);

            if (value["contentUnitId"]) {
                contentUnitId= value["contentUnitId"];
            }
        }

        override public function createValueObject(): Object {

            var retObj:Object = super.createValueObject();
            retObj.contentUnitId = contentUnitId;

            return retObj;
        }
    }
}
