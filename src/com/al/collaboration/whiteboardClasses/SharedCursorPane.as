/**
 * Created by lohithram on 28/04/15.
 */
package com.al.collaboration.whiteboardClasses {

    import com.adobe.rtc.collaboration.SharedCursorPane;
    import com.adobe.rtc.events.CollectionNodeEvent;
    import com.adobe.rtc.messaging.MessageItem;
    import com.al.model.UserModel;
    import com.al.util.EventUtils;

    import flash.desktop.NativeApplication;

    import flash.events.Event;

    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.utils.Dictionary;
    import flash.utils.Timer;
    import flash.utils.getQualifiedClassName;

    public class SharedCursorPane extends com.adobe.rtc.collaboration.SharedCursorPane {

        public var user: UserModel;

        public function SharedCursorPane() {
            super();
        }

        override public function subscribe(): void {

            if(!collectionNode)
                return;

            _userMgr = connectSession.userManager;

            collectionNode.addEventListener(CollectionNodeEvent.ITEM_RECEIVE, onItemReceive);
            collectionNode.addEventListener(CollectionNodeEvent.ITEM_RETRACT, onItemRetract);

            if(parent)
                parent.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);

            addEventListener(Event.REMOVED_FROM_STAGE, cleanUp);
            NativeApplication.nativeApplication.addEventListener(Event.EXITING, cleanUp);
        }

        private var pendingDeletion: Array = [];
        override protected function onItemReceive(event: CollectionNodeEvent): void{

            cursorHouseKeeping(event);

            _myID = _userMgr.myUserID;

            // Do not show the user's own cursor
            if(event.item.publisherID == _userMgr.myUserID)
                return;

            super.onItemReceive(event);

            if(_cursorsByID[event.item.publisherID] as RemoteUserCursor)
                (_cursorsByID[event.item.publisherID] as RemoteUserCursor).displayName = event.item.body.user;

        }

        override protected function addNewCursor(p_userID:String, p_cursorClass:Class, p_x:Number, p_y:Number):void
        {
            var newCursor:RemoteUserCursor = new RemoteUserCursor();
            addChild(newCursor);
            newCursor.cursorClass = p_cursorClass;
            newCursor.x = p_x;
            newCursor.y = p_y;
            _cursorsByID[p_userID] = newCursor;

            newCursor.reveal();
        }


        /**
         * @private
         */
        override protected function removeCursor(p_userID:String):void
        {
            if (_cursorsByID[p_userID]) {
                if (_cursorsByID[p_userID] is RemoteUserCursor) {
                    RemoteUserCursor(_cursorsByID[p_userID]).hide();
                }
                delete _cursorsByID[p_userID];
            }
        }

        /**
         * @private
         */
        override protected function moveCursorTo(p_userID:String, p_cursorClass:Class, p_x:Number, p_y:Number):void
        {
            var cursor:RemoteUserCursor = _cursorsByID[p_userID] as RemoteUserCursor;
            cursor.cursorClass = p_cursorClass;
            cursor.moveTo(p_x, p_y);
        }


        override protected function sendMyCursor(p_evt: TimerEvent=null) :void {

            _myID = _userMgr.myUserID;
            // Start a timer to time out if we haven't moved.
            if (mouseX == _lastMouseX && mouseY == _lastMouseY)
            {
                if (!_inactivityTimer)
                {
                    _inactivityTimer = new Timer(INACTIVITY_TIME, 1);
                }
                if (!_inactivityTimer.running)
                {
                    _inactivityTimer.reset();
                    _inactivityTimer.start();
                    _inactivityTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onInactivity);
                }
                // Don't renew the point timer; we're at a standstill. Instead, wait for a mouseMove.
                if (parent && !parent.hasEventListener(MouseEvent.MOUSE_MOVE)) {
                    stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
                }

            } else
            {
                if (!_myCursorClass)
                {
                    _myCursorClass = defaultCursorClass;
                }

                var item:MessageItem;
                if (_sizingMode == com.adobe.rtc.collaboration.SharedCursorPane.RELATIVE_MODE)
                {

                    var relativeX:Number = mouseX / width;
                    var relativeY:Number = mouseY / height;
                    item = new MessageItem(_nodeName,
                            {
                                x: relativeX, y: relativeY,
                                cursor: getQualifiedClassName(_myCursorClass),
                                user: user.displayName
                            }, _myID);
                }
                else if (_sizingMode == com.adobe.rtc.collaboration.SharedCursorPane.ABSOLUTE_MODE)
                {

                    item = new MessageItem(_nodeName,
                            {
                                x: mouseX, y: mouseY,
                                cursor: getQualifiedClassName(_myCursorClass),
                                user: user.displayName
                            }, _myID);
                }

                if (_collectionNode.isSynchronized)
                {
                    // safety check.
                    _hasRetracted = false;
                    _collectionNode.publishItem(item);
                }
                _lastMouseX = mouseX;
                _lastMouseY = mouseY;

                // Start the timers for the next poll; switch from polling mouseMove to timers to reduce message intensity.
                removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
                _pointTimer.reset();
                _pointTimer.start();
            }
        }

        override protected function onMouseMove(event: MouseEvent): void{

            // This is to circumvent the Influxis SCP component listening to stage move and we don't want that.
            if(event.currentTarget == stage){

                EventUtils.removeListener(event, onMouseMove);
                return;
            }
            super.onMouseMove(event);
        }

        protected function cleanUp(event: Event = null): void {

            _myID = _userMgr.myUserID;
            if(_cursorsByID[_myID] && collectionNode.isSynchronized){

                collectionNode.retractItem(nodeName, _myID);
                //trace("Removed shared cursor on exit for tile  - " + collectionNode.sharedID);
            }
        }

        protected function traceInfo(item: MessageItem): void {

            //trace("CURSOR:");
            trace("Tile - " + item.collectionName +
                    " x - " + item.body.x +
                    " y - " + item.body.y +
                    " user - " + item.body.user +
                    " publisher id - "+ item.publisherID);
        }

        private var cursorTimestamps: Dictionary = new Dictionary();
        private var LOITERING_THRESHOLD: int = 5000;
        /**
         * Identify and remove loitering cursors
         */
        private function cursorHouseKeeping(event: CollectionNodeEvent = null): void{

            if(nodeName != event.nodeName) return;

            var timeNow: Number = new Date().getTime();
            cursorTimestamps[event.item.itemID] = event.item.timeStamp;

            for(var itemId: String in cursorTimestamps){

                var timeStamp: Number = cursorTimestamps[itemId];
                if( (timeNow - timeStamp) >  LOITERING_THRESHOLD)
                    pendingDeletion.push(itemId);
            }
            cleanUpCursors();
        }

        /**
         * Cleans up loitering cursors
         */
        private function cleanUpCursors(){

            if(!collectionNode.isSynchronized) return;

            while(pendingDeletion.length>0){

                var itemID: String = pendingDeletion.pop() as String;
                if(itemID) {

                    delete cursorTimestamps[itemID];
                    collectionNode.retractItem(nodeName, itemID);
                }
            }
        }

        override protected function refreshLabelFieldAndFunction() :void {
            // we are not depending on the influxis user manager to identy the label
        }
    }
}
