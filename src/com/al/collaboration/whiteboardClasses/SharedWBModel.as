package com.al.collaboration.whiteboardClasses
{
	import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBArrowShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBEllipseShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBHighlightAreaShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBLineShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBMarkerShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBRectangleShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBRoundedRectangleShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapeDescriptors.WBTextShapeDescriptor;
	import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBSimpleShape;
	import com.adobe.coreUI.events.WBModelEvent;
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.messaging.MessageItem;
	import com.adobe.rtc.messaging.NodeConfiguration;
	import com.adobe.rtc.pods.sharedWhiteBoardClasses.SharedWBModel;
	import com.adobe.rtc.session.IConnectSession;
	import com.adobe.rtc.sharedModel.CollectionNode;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayList;
	import mx.utils.UIDUtil;
	
	public class SharedWBModel extends com.adobe.rtc.pods.sharedWhiteBoardClasses.SharedWBModel
	{
		[Bindable]
		[ArrayElementType("com.al.collaboration.whiteboardClasses.ShapeDescriptor")]
		public var shapes: ArrayList;

        public var contentUnitId: String;

        public var shapesMap: Dictionary;

		/**
		 * Constructor
		 */
		public function SharedWBModel() {
			
			shapes = new ArrayList();
		}
		
		public function set collectionNode(value: CollectionNode): void {
			
			if(_collectionNode != value){
				_collectionNode = value;
			}
		}

		public function get collectionNode(): CollectionNode {

			return _collectionNode;
		}
		
		override public function canUserDraw(p_userID:String):Boolean
		{
			return true;
		}
		
		public function onShapeCreateProxy(shape:WBShapeDescriptor, local:Boolean):void {
			onShapeCreate(shape, local);
		}
		/**
		 * Unnecessary evil. Will have to be kept until we can refactor or create a proper WBCanvas 
		 * @param p_shape
		 * @param p_local
		 * 
		 */
		override protected function onShapeCreate(p_shape:WBShapeDescriptor, p_local:Boolean):void
		{
			var evt:WBModelEvent = new WBModelEvent(WBModelEvent.SHAPE_CREATE);
			evt.shapeID = p_shape.shapeID;
			evt.isLocalChange = p_local;
			dispatchEvent(evt);
		}

		public function onShapeAddProxy(shape:WBShapeDescriptor, local:Boolean):void {
			onShapeAdd(shape, local);
		}
		/**
		 * @private
		 */
		override protected function onShapeAdd(p_shape:WBShapeDescriptor, p_local:Boolean):void
		{
			var evt:WBModelEvent = new WBModelEvent(WBModelEvent.SHAPE_ADD);
			evt.shapeID = p_shape.shapeID;
			evt.isLocalChange = p_local;
			
			dispatchEvent(evt);
		}	
		
		public function onItemRetractProxy(event:CollectionNodeEvent):void{
			onItemRetract(event);
		}
		
		override protected function onItemRetract(event:CollectionNodeEvent):void
		{
			var shapeID:String = event.item.itemID;
			var shapeDesc:WBShapeDescriptor = getShapeDescriptor(shapeID);
			
			var evt:WBModelEvent = new WBModelEvent(WBModelEvent.SHAPE_REMOVE);
			evt.shapeID = shapeID;
			evt.deletedShape = shapeDesc;
			evt.isLocalChange = (event.item.publisherID==_myUserID);
			
			dispatchEvent(evt);
			
		}
		
		public function onShapeLayoutChangeProxy(shape:WBShapeDescriptor, local:Boolean):void {
			onShapeLayoutChange(shape, local);
		}
		
		/**
		 * @private
		 */
		override protected function onShapeLayoutChange(p_shape:WBShapeDescriptor, p_local:Boolean):void
		{
			var currShape:WBShapeDescriptor = getShapeDescriptor(p_shape.shapeID);
			currShape.x = p_shape.x;
			currShape.y = p_shape.y;
			currShape.width = p_shape.width;
			currShape.height = p_shape.height;
			currShape.rotation = p_shape.rotation;
			
			var evt:WBModelEvent = new WBModelEvent(WBModelEvent.SHAPE_POSITION_SIZE_ROTATION_CHANGE);
			evt.shapeID = p_shape.shapeID;
			evt.isLocalChange = p_local;
			
			dispatchEvent(evt);			
		}
		
		public function onShapePropertiesChangeProxy(shape:WBShapeDescriptor, local:Boolean):void {
			onShapePropertiesChange(shape, local);
		}
		
		/**
		 * @private
		 */
		override protected function onShapePropertiesChange(p_shape:WBShapeDescriptor, p_local:Boolean):void
		{
			getShapeDescriptor(p_shape.shapeID).propertyData = p_shape.propertyData;
			
			var evt:WBModelEvent = new WBModelEvent(WBModelEvent.SHAPE_PROPERTIES_CHANGE);
			evt.shapeID = p_shape.shapeID;
			evt.isLocalChange = p_local;
			
			dispatchEvent(evt);			
		}


		override public function subscribe():void
		{
			// NO NEED FOR SUBSCRIBTION. DOne by top level controller
		}

		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//--------------------------------------------------------------------------
		//
		//  Neutralized methods ie, for demo we do not care about user configuration, node configuration etc
		//
		//--------------------------------------------------------------------------

		/**
		 * This should go into controller 
		 * @param p_evt
		 * 
		 */
		override protected function onItemReceive(p_evt:CollectionNodeEvent):void
		{
		}
		
		override public function close():void
		{
		}
		
		/**
		 * When called by an owner, <code>setUserRole()</code> sets the role of the specified 
		 * user on a specific node or the collection Node itself. The following rules apply: 
		 * @param p_userID The <code>userID</code> of the user to set the role for. 
		 * @param p_role The new role for that user.  
		 * @param p_nodeName The nodename, if nothing is specified , it sets on the complete collectionNode
		 */
		override public function setUserRole(p_userID:String, p_role:Number, p_nodeName:String=null):void
		{
			// NO user management for demo
		}
		
		
		/**
		 *  User Role of an user who can access/publish on the shared white board model
		 * 
		 * @param p_userID UserID of the user whose role we get to have
		 */
		override public function getUserRole(p_userID:String):int
		{
			//trace("getUserRole - SHOULDNT BE CALLED");
			return -1; //_collectionNode.getUserRole(p_userID);
		}
		
		/**
		 * The <code>sharedID</code> is the ID of the class 
		 */
		override public function set sharedID(p_id:String):void
		{
			super.uniqueID = p_id;
			_sharedID = p_id;
		}
		
		/**
		 * @private
		 */
		override public function get sharedID():String
		{
			return _sharedID;
		}
		
		/**
		 * The IConnectSession with which this component is associated. Note that this may only be set once before <code>subscribe</code>
		 * is called; re-sessioning of components is not supported. Defaults to the first IConnectSession created in the application.
		 */
		override public function get connectSession():IConnectSession
		{
			return null; //_connectSession;
		}
		
		/**
		 * @private
		 */
		override public function set connectSession(p_session:IConnectSession):void
		{
			//_connectSession = p_session;
		}
		
		/**
		 * Gets the NodeConfiguration on a specific node in the WhiteBoardmodel. If the node is not defined, it will return null
		 * @param p_nodeName The name of the node.
		 */
		override public function getNodeConfiguration(p_nodeName:String):NodeConfiguration
		{	
			//if ( _collectionNode.isNodeDefined(p_nodeName)) {
			//	return _collectionNode.getNodeConfiguration(p_nodeName).clone();
			//}
			//trace("getNodeConfiguration - Shouldn't be called");
			return null ;
		}
		
		/**
		 * Sets the NodeConfiguration on a already defined node in WhiteBoardmodel. If the node is not defined, it will not do anything.
		 * @param p_nodeConfiguration The node Configuration on a node in the NodeConfiguration.
		 * @param p_nodeName The name of the node.
		 */
		override public function setNodeConfiguration(p_nodeName:String,p_nodeConfiguration:NodeConfiguration):void
		{	
			//if ( _collectionNode.isNodeDefined(p_nodeName)) {
				//_collectionNode.setNodeConfiguration(p_nodeName,p_nodeConfiguration) ;
			//}
			//trace("setNodeConfiguration - SHOULDNT BE CALLING THIS METHOD");
		}
		
		/**
		 * API for creating a new shape. Input is a shapedescriptor object. 
		 * The shape ID is assigned from the server
		 */
		override public function createShape(p_shape:WBShapeDescriptor):void
		{
			if (!canUserDraw(_myUserID)) {
				throw new Error("SharedWhiteBoard - insufficient permissions to draw");
			}
		}
		
		/**
		 * API for adding an existing shape with a shape ID to the canvas. Input is the shapeDescriptor
		 * 
		 */
		override public function addShape(p_shape:WBShapeDescriptor):void
		{
			if (!canUserDraw(_myUserID)) {
				throw new Error("SharedWhiteBoard - insufficient permissions to draw");
			}
		}
		
		/**
		 * API for changing shape layout like x,y position width, height, rotation.
		 */
		override public function moveSizeRotateShape(p_shapeID:String, p_x:Number, p_y:Number, p_w:Number, p_h:Number, p_rotation:int, p_allowLocalChange:Boolean = false):void
		{
			if (!canUserDraw(_myUserID)) {
				throw new Error("SharedWhiteBoard - insufficient permissions to draw");
			}
		}
		
		/**
		 * API for changing shapeID and property Data
		 */
		override public function changeShapeProperties(p_shapeID:String, p_properties:*):void
		{
			if (!canUserDraw(_myUserID)) {
				throw new Error("SharedWhiteBoard - insufficient permissions to draw");
			}
		}
		
		/**
		 * API for changing properties and attributes of the Shape. Ensure that all the properties of the shape are set for expected behaviour
		 */
		override public function modifyShapeDescriptor(p_shapeID:String, p_newShapeDescriptor:WBShapeDescriptor):void
		{
			if (!canUserDraw(_myUserID)) {
				throw new Error("SharedWhiteBoard - insufficient permissions to draw");
			}
			var shapeDesc:WBShapeDescriptor = getShapeDescriptor(p_shapeID);
			var mutatedShapeDescriptor:WBShapeDescriptor = shapeDesc.clone();
			mutatedShapeDescriptor.readValueObject(p_newShapeDescriptor);
			if(shapeDesc.compareShapeAttributes(mutatedShapeDescriptor)) {
				moveSizeRotateShape(shapeDesc.shapeID, mutatedShapeDescriptor.x, mutatedShapeDescriptor.y, mutatedShapeDescriptor.width, mutatedShapeDescriptor.height, mutatedShapeDescriptor.rotation, true);
			}
			
			for (var i:String in mutatedShapeDescriptor.propertyData) {
				if (shapeDesc.propertyData.hasOwnProperty(i)) {
					if (shapeDesc.propertyData[i] != mutatedShapeDescriptor.propertyData[i]) {
						changeShapeProperties(shapeDesc.shapeID, mutatedShapeDescriptor.propertyData);
						break;
					}
				}
			}
		}
		
		/**
		 * API for removing a shape with a given shape ID
		 */
		override public function removeShape(p_shapeID:String):void
		{
			if (!canUserDraw(_myUserID)) {
				throw new Error("SharedWhiteBoard - insufficient permissions to draw");
			}
		}
		
		/**
		 * Returns the shape with the given shape ID if it exists
		 */
		override public function getShapeDescriptor(p_shapeID:String):WBShapeDescriptor
		{
			return shapesMap[p_shapeID];
		}
		
		/**
		 * Returns array of all shape IDs of shapes currently in the whiteboard
		 */
		override public function getShapeIDs():Array
		{
            var returnArray:Array = new Array();
			for each( var shape: ShapeDescriptor in shapes.source) {

                if(!contentUnitId || shape.contentUnitId == contentUnitId)
                    returnArray.push(shape.shapeID);
			}
			//trace("getShapeIDs length - " + returnArray.length);
			return returnArray;
		}
		
		
		/**
		 * returns if the shape is already added to the canvas... 
		 */
		override public function getIsAdded(p_shapeID:String):Boolean
		{
			//trace("getIsAdded - SHOULDNT BE CALLING THIS METHOD");
			return (_addedShapes[p_shapeID]!=null);
		}	
		
		[Bindable(event="synchronizationChange")]
		/**
		 * Dispatches when the collectionNode is synchronized.
		 */
		override public function get isSynchronized():Boolean
		{
			////trace("isSynchronized - SHOULDNT BE CALLING THIS METHOD");
			return true;
		}
		
		/**
		 * @private
		 */
		override protected function onSyncChange(p_evt:CollectionNodeEvent):void
		{
			// SharedWBModel is all set up to hold the shape data and not bother about subsciptions and synchronization
			if (_collectionNode.isSynchronized) {
			}
			
		}	
		
		/**
		 * @private
		 */
		override protected function onReconnect(p_evt:CollectionNodeEvent):void
		{
			//_seenIDs = new Object();
			//_addedShapes = new Object();
			//_shapes = new Object();
			//dispatchEvent(p_evt);
		}
		
		/**
		 * @private
		 */
		override protected function onMyRoleChange(p_evt:CollectionNodeEvent):void
		{
			var evt:WBModelEvent = new WBModelEvent(WBModelEvent.MY_ROLE_CHANGE);
			dispatchEvent(evt);			
		}
		
		/**
		 * @private
		 */
		override protected function onNodeCreate(p_evt:CollectionNodeEvent):void
		{
			// do nothing man.. enough of spaghetti code !
		}
		
	}
}