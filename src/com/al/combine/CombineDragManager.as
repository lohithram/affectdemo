/**
 * Created by lram on 27/02/2015.
 */
package com.al.combine
{

    import com.al.components.ReducedElement;
    import com.al.containers.ProxyContainer;
    import com.al.containers.TileContainer;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.DragSourceFormats;
    import com.al.enums.TileDataFormats;
    import com.al.enums.TileDropAction;
    import com.al.enums.TileSide;
    import com.al.events.CombineDropEvent;
    import com.al.events.PlaceHolderDropEvent;
    import com.al.events.StartDraggingEvent;
    import com.al.events.TileDropEvent;
    import com.al.events.TileRemoveEvent;
    import com.al.factory.TileContainerFactory;
    import com.al.layouts.CombineLayout;
    import com.al.model.Cell;
    import com.al.tile.ICombineElement;
    import com.al.tile.ITileContainer;
    import com.al.tile.ITileDropIndicator;
    import com.al.util.EventUtils;
    import com.al.util.SnapshotUtil;

    import flash.desktop.Clipboard;
    import flash.desktop.ClipboardFormats;
    import flash.desktop.NativeDragActions;
    import flash.desktop.NativeDragManager;
    import flash.display.InteractiveObject;
    import flash.events.NativeDragEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.utils.Dictionary;

    import mx.collections.ArrayList;
    import mx.collections.IList;
    import mx.core.DragSource;
    import mx.core.FlexGlobals;
    import mx.core.IFlexDisplayObject;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;
    import mx.events.DragEvent;
    import mx.events.FlexNativeMenuEvent;
    import mx.managers.DragManager;

    import spark.components.Group;
    import spark.components.Image;
    import spark.components.SkinnableContainer;
    import spark.events.ElementExistenceEvent;

    /**
     * Handles all the dragging and dropping in the combine
     /**
     *
     * TODO: Re-use placeholder objects
     *
     * Laoyout Pipeline: Adjust -> Shrink -> Restore -> Adjust
     * */
    public class CombineDragManager
    {
        // The girth of the tile drop place holders
        private static const PLACE_HOLDER_GIRTH: Number = 2.5;

        // the combine which is currently being managed
        private var combine: ICombine;
		
		private var fillStrategy: EmptySpaceFillStrategy;
		private var multiSelectStrategy: MultiSelectStrategy;

        public function manageCombine(combineContainer: ICombine): void{

            combine = combineContainer;
			fillStrategy = new EmptySpaceFillStrategy(combine);
			multiSelectStrategy = new MultiSelectStrategy(combine);
			
            // add Event_ADD, EVENT_REMOVE listener
            combine.addEventListener(ElementExistenceEvent.ELEMENT_ADD, onElementAdd);
            combine.addEventListener(ElementExistenceEvent.ELEMENT_REMOVE, onElementRemove);
            combine.addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
            combine.addEventListener(DragEvent.DRAG_EXIT, onDragExit);

            combine.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER,onNativeDragIn);
            combine.addEventListener(NativeDragEvent.NATIVE_DRAG_EXIT,onNativeDragExit);

            FlexGlobals.topLevelApplication.menu.addEventListener(
				FlexNativeMenuEvent.ITEM_CLICK, onMenuItemSelect);
        }

        //--------------------------------------------------------------------------
        //
        //  Private methods
        //
        //--------------------------------------------------------------------------
        // TODO: to rename the below flags appropriately
		private var aspectRatio: Number;
        private var isDragging: Boolean;
        private var isNativeDragging: Boolean;
        private var emptySpace: Rectangle;
        private var placeHoldersArray: Array;
        private var canDropOnTopBoundary: Boolean;
        private var canDropOnLeftBoundary: Boolean;
        private var canDropOnRightBoundary: Boolean;
        private var canDropOnBottomBoundary: Boolean;


		/**
		 * 
		 * @param event
		 * 
		 */
        private function onElementAdd(event: ElementExistenceEvent): void {

            if(event.element is ICombineElement && !(event.element is ITileDropIndicator)){

                event.element.addEventListener(TileDropEvent.DROP, onTileDrop);
                event.element.addEventListener(TileRemoveEvent.REMOVE, onTileRemove);
                event.element.addEventListener(StartDraggingEvent.START_DRAGGING, onStartDragging);
            }
        }

        private function onElementRemove(event:ElementExistenceEvent):void {

            var combineElement: ICombineElement = event.element as ICombineElement;
            if(combineElement && !(combineElement is ITileDropIndicator)){

                combine.animate = true;
                combineElement.removeEventListener(TileDropEvent.DROP, onTileDrop);
                combineElement.removeEventListener(TileRemoveEvent.REMOVE, onTileRemove);
                combineElement.removeEventListener(StartDraggingEvent.START_DRAGGING, onStartDragging);
            }
        }

        /**
         * Do not remove tile if it is the last one standing !
         * @param event
         */
        private function onTileRemove(event: TileRemoveEvent): void{

            event.stopImmediatePropagation();

            if(combine.numElements > 1) {

                // QUICK AND DIRTY
                // Todo: We should be only removing the descriptor
                // and the combine should remove the tile corresponding to the descriptor
                combine.removeElement(event.tile);

                EventUtils.removeListener(event, onTileRemove);

                //consume the empty space
                recordEmptySpace(event.tile);
                fillStrategy.consumeEmptySpace(emptySpace, event.tile);
                invalidateCombine();
            }
        }

        private function onDragExit(event: DragEvent): void{

            if(!isDragging)
                return;

            reStoreTiles();
            isDragging = false;
        }

        private function onDragEnter(event: DragEvent): void{

            if(isDragging)
                return;

            aspectRatio = (combine.width/combine.height);

			if(event.dragSource.hasFormat(DragSourceFormats.TILE)){

                prepContainerForTileDrag(event.dragInitiator as ICombineElement);
                isDragging = true;
            }
            else if(event.dragSource.hasFormat(DragSourceFormats.TILE_DESCRIPTOR)){

                prepContainerForNewTile();
                isDragging = true;
            }
            else if(event.dragSource.hasFormat(DragSourceFormats.COMBINE)) {

                prepContainerForCombineDrag(event.dragInitiator);
                isDragging = true;
            }

            if(isDragging){

                combine.animate = true;
                combine.resizeGroup.visible = false;
                event.dragInitiator.addEventListener(DragEvent.DRAG_COMPLETE, onDragComplete);
            }
        }

        private function prepContainerForCombineDrag(dragInitiator: IUIComponent): void{

            var placeHoldersList: ArrayList = new ArrayList();
            canDropOnLeftBoundary = canDropOnRightBoundary = canDropOnTopBoundary = canDropOnBottomBoundary = true;

            createPerimeterPlaceHolders(placeHoldersList);

            for ( var i: int = 0; i<combine.numElements; ++i) {

                var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
				if(!element) continue;
                checkAndAdjustBounds(element);
            }

            placeHoldersArray = placeHoldersList.source;
            addAllPlaceHolders();
        }

        private function getTiles(): Array {

            var tiles: Array = [];
            for ( var i: int = 0; i<combine.numElements; ++i) {

                var element: ITileContainer = combine.getElementAt(i) as ITileContainer;
                if(element)
                    tiles.push(element);
            }
            return tiles;
        }

        /**
         * First pass - create place holder for each side of each tile
         * Second pass - identify and remove adjacent duplicate placeholders
         * Third pass - Add placeholders for the container as a whole
         * @param draggedTile
         */
        private function prepContainerForTileDrag(draggedTile: ICombineElement): void{

			if(!draggedTile)
				return;

            var placeHoldersList: ArrayList = new ArrayList();
            var noOfTiles: int = getTiles().length;
            if(getTiles().length == 2){

                canDropOnBottomBoundary = canDropOnLeftBoundary = canDropOnRightBoundary = canDropOnTopBoundary = false;
            }
            else
            {

                canDropOnTopBoundary = !(draggedTile.reducedY == 0 && draggedTile.reducedWidth == 100);
                canDropOnLeftBoundary = !(draggedTile.reducedX == 0 && draggedTile.reducedHeight == 100);
                canDropOnRightBoundary = !((draggedTile.reducedX + draggedTile.reducedWidth) == 100 && draggedTile.reducedHeight == 100);
                canDropOnBottomBoundary = !((draggedTile.reducedY + draggedTile.reducedHeight) == 100 && draggedTile.reducedWidth == 100);
            }

            // First pass - create place holders
            for ( var i: int = 0; i<combine.numElements; ++i) {

                var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
				if(!element) continue;
				
                checkAndAdjustBounds(element);
                if(element != draggedTile) {
                    placeHoldersList.addAll(createPlaceHolders(element));
                }
            }

            // Step 1.2 - eliminate adjacent same dimension placeholder
            removeRedundantPlaceHolders(draggedTile, placeHoldersList);


            // Step 1.5 - shrink the tile
            for ( i = 0; i<combine.numElements; ++i) {
				
                element = combine.getElementAt(i) as ICombineElement;
				if(!element) continue;
                if(element != draggedTile) {
                    shrink(element, placeHoldersList.source);
                }
            }

            // Second pass
            // removeAdjoiningDuplicates(placeHoldersList);

            // Lastly create encompassing place holders
            createPerimeterPlaceHolders(placeHoldersList);

            placeHoldersArray = placeHoldersList.source;
            addAllPlaceHolders();
        }

        /**
         * First pass - create place holder for each side of each tile
         * Second pass - Add placeholders for the container as a whole
         */
        private function prepContainerForNewTile(): void{

            var placeHoldersList: ArrayList = new ArrayList();
            canDropOnBottomBoundary = canDropOnLeftBoundary = canDropOnRightBoundary = canDropOnTopBoundary = true;

            // First pass - create place holders
            for ( var i: int = 0; i<combine.numElements; ++i) {

                var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
                if(!element) continue;

                checkAndAdjustBounds(element);
                placeHoldersList.addAll(createPlaceHolders(element));
            }

            // Step 1.5 - shrink the tile
            for ( i = 0; i<combine.numElements; ++i) {

                element = combine.getElementAt(i) as ICombineElement;
                if(!element) continue;
                shrink(element, placeHoldersList.source);
            }

            // Step 1.2 - eliminate adjacent same dimension placeholder
            //removeRedundantPlaceHolders(draggedTile, placeHoldersList);

            // Lastly create encompassing place holders
            createPerimeterPlaceHolders(placeHoldersList);

            placeHoldersArray = placeHoldersList.source;
            addAllPlaceHolders();
        }

        private function addAllPlaceHolders(): void{

            for each( var element: ICombineElement in placeHoldersArray)
                combine.addElement(element);
        }
		
		private function getDescriptorForTile(tile: TileContainer): TileContainerDescriptor {
			
			if(!tile) return null;
			
			var descriptor: TileContainerDescriptor = 
				tile.tileDescriptor ? tile.tileDescriptor.clone() : new TileContainerDescriptor();
			descriptor.reducedX = tile.reducedX;
			descriptor.reducedY = tile.reducedY;
			descriptor.reducedWidth = tile.reducedWidth;
			descriptor.reducedHeight = tile.reducedHeight;
            descriptor.model = tile.tileContainerModel.tileDataModel ? tile.tileContainerModel.tileDataModel.getTileData(TileDataFormats.MODEL) : null;
			
			return descriptor;
		}

		private var tileMap: Dictionary;
        /**
         * user has initiated drag action
         * */
        private function onStartDragging(event: StartDraggingEvent): void{

			event.stopImmediatePropagation();
            var draggedTile: ICombineElement = event.currentTarget as ICombineElement;
            if(!draggedTile)
                return;

            //TODO: Revisit
			var xOffset: Number = 0;
			var yOffset: Number = 0;
			var selectedElements: Array;
            var dragSource: DragSource = new DragSource();

			if(!draggedTile.selected){
				
				multiSelectStrategy.selectElement(draggedTile);
			}
			selectedElements  = multiSelectStrategy.selectedElements;
			if(selectedElements.length > 1){
				
				tileMap = new Dictionary(true);
				var tileDescriptors: Array = [];
				for each( var element: ICombineElement in selectedElements){
					
					tileMap[element] = getDescriptorForTile(element as TileContainer)
					tileDescriptors.push( tileMap[element] );
				}
				tileDescriptors = distributeIntoTwoRows(tileDescriptors);
				dragSource.addData(tileDescriptors, DragSourceFormats.MULTI_TILE);	
				var dragImage: IFlexDisplayObject = getMultiTileDragImage(selectedElements);
				xOffset = event.mouseEvent.localX - dragImage.width/2;
				yOffset = 0;
			}
			else if(selectedElements.length == 1){
				
				var desc: TileContainerDescriptor = getDescriptorForTile(draggedTile as TileContainer);
				dragImage = getSingleTileDragImage(draggedTile as TileContainer);
				dragSource.addData(desc, DragSourceFormats.TILE_DESCRIPTOR);
				dragSource.addData(draggedTile, DragSourceFormats.TILE);

				xOffset = event.mouseEvent.localX - dragImage.width/2;
				yOffset = 0;//event.mouseEvent.localY * 0.6;
			}

            DragManager.doDrag(draggedTile as IUIComponent, dragSource, event.mouseEvent, dragImage, 
				xOffset, yOffset, 0.67 );
        }
		
		private function getSingleTileDragImage(tile: TileContainer): IFlexDisplayObject{
			
			return SnapshotUtil.getSnapShot(tile, tile.width*0.6, tile.height*0.6);
		}
		/**
		 * 
		 * @param tileMap
		 * @return 
		 * 
		 */
			
		private function getMultiTileDragImage(tiles: Array): IFlexDisplayObject{
			
			var proxyContainer: Group = new ProxyContainer();
			proxyContainer.width = 200; 
			proxyContainer.height = 150;
			proxyContainer.layout = new CombineLayout();

			for each(var tile: TileContainer in tiles){
				
				var snapshot: Image = SnapshotUtil.getSnapShot(tile);
				snapshot.percentWidth = snapshot.percentHeight = 100;
				
				var desc: TileContainerDescriptor = tileMap[tile] as TileContainerDescriptor;
				var reducedElement: ReducedElement = new ReducedElement();
				reducedElement.reducedX = desc.reducedX;
				reducedElement.reducedY = desc.reducedY;
				reducedElement.reducedWidth = desc.reducedWidth;
				reducedElement.reducedHeight = desc.reducedHeight;
				reducedElement.addElement(snapshot);
				
				proxyContainer.addElement(reducedElement);
			}

			//proxyContainer.validateNow();
			return proxyContainer;
		}
		
		/**
		 * TODO: This can be extracted to a 'Strategy' class 
		 * @return 
		 * 
		 */
		private function distributeIntoTwoRows(tileDescriptors: Array): Array{
		
			var topRowTileCount: int = Math.ceil(tileDescriptors.length/2);
			var bottomRowTileCount: int = tileDescriptors.length - topRowTileCount;
			var count: int = 0;
			for each(var desc: TileContainerDescriptor in tileDescriptors){
				
				desc.reducedY = 0;
				desc.reducedHeight = 50;
				if(count < topRowTileCount){
					desc.reducedX = (100/topRowTileCount) * count;
					desc.reducedWidth = 100/topRowTileCount;
				}
				else{
					desc.reducedY = 50;
					desc.reducedX = (100/bottomRowTileCount) * (count-topRowTileCount);
					desc.reducedWidth = 100/bottomRowTileCount;
				}
				++count;
			}
			return tileDescriptors;
		}

        /**
         * @param event
         */
        private function onDragComplete(event: DragEvent): void {

            EventUtils.removeListener(event, onDragComplete);
            cleanUpPostDrag();
        }

        private function cleanUpPostDrag(): void{

            //if(!(isDragging || isNativeDragging) )
            //    return;

            traceUtil("CLEANUPPOSTDRAG");

            reStoreTiles();
            emptySpace = null;
            isDragging = false;
            isNativeDragging = false;
            combine.resizeGroup.visible = true;
            invalidateCombine();
        }

        /**
         * Restore size of all the tileContainers
         * Remove all placeholders
         * @param event
         */
        private function reStoreTiles(): void {

            if (placeHoldersArray && placeHoldersArray.length > 0) {

                for (var i:int = 0; i < combine.numElements; ++i) {

                    var element:ICombineElement = combine.getElementAt(i) as ICombineElement;
					if(!element) continue;
                    if (!(element is ITileDropIndicator)){

                        restoreSize(element, placeHoldersArray);
                        checkAndAdjustBoundsInReverse(element);
                    }
                }
            }
            removeAllPlaceHolders();
            combine.animate = true;
        }

        private function removeAllPlaceHolders(): void {

            for each(var element:ICombineElement in placeHoldersArray) {
                combine.removeElement(element);
                //removeRegisteredListeners();
            }
            placeHoldersArray = [];
        }

        private function onTileDrop(event: TileDropEvent): void {

            reStoreTiles();
			recordCurrentLayout();
            recordEmptySpace(event.draggedTile);

            var draggedTile: ITileContainer = event.draggedTile as ITileContainer;

            if(event.dropAction == TileDropAction.SWAP_ACTION) {

                swapProperties(event.draggedTile, event.droppedTile);
                combine.swapElements(event.draggedTile,event.droppedTile);
            }
            else if(event.dropAction == TileDropAction.OVERLAY_ACTION) {

                // Remove the dragged tile so it doesn't interfere with
                // logic to fill the empty space
                combine.removeElement(event.draggedTile);

                if(draggedTile.owner != combine ){

                    recordEmptySpace(draggedTile as ICombineElement);
                    fillStrategy.consumeEmptySpace(emptySpace);
                }

                // Add all the content of the dragged tile into the dropped tile
				// TODO: Use a strategy class to do the same, so that we can swap or
				// only touch the strategy file if we need to make changes in the swapping behaviour

				for( var i: int = draggedTile.numElements-1; i>=0; --i) {
					
					var element: IVisualElement = draggedTile.removeElementAt(i);
					(event.droppedTile as ITileContainer).addElement(element); 
				}
            }
			
			// This will re-wire the resize handles to correct adjacent tiles
			invalidateCombine();
        }

        /**
         * This is the case for dropping resources which is neither a Combine nor a Tile but the
         * data represents a content.
         * For example dragging asset cells from utility panel.
         * @param event
         */
        private function handleResourceDropOnPlaceHolder(event: PlaceHolderDropEvent): void{

            var newTile: TileContainer;

            var cell: Cell = event.dragSource.dataForFormat(DragSourceFormats.ASSET_CELL) as Cell;
            if(cell){

                newTile = TileContainerFactory.createTileContainerForCell(cell);
            }

            if(!newTile) {

                var tileDescriptor: TileContainerDescriptor =
                        event.dragSource.dataForFormat(DragSourceFormats.TILE_DESCRIPTOR) as TileContainerDescriptor;
               if(tileDescriptor)
                   newTile = TileContainerFactory.createTileContainer(tileDescriptor);
            }
            if(!newTile) return;

            // setting x and y helps in properly animating the drop
            var stagePoint: Point = new Point(combine.stage.mouseX, combine.stage.mouseY);
            var localPoint: Point = combine.globalToLocal(stagePoint);
            newTile.x = localPoint.x;
            newTile.y = localPoint.y;
            newTile.width = 80;
            newTile.height = 60;

            reStoreTiles();
            recordCurrentLayout();
            positionTilePostDrop(newTile, event.target as ITileDropIndicator);

            combine.addElement(newTile);
            invalidateCombine();

            cleanUpPostDrag();
        }

        private function pushAllUp(draggedTile: ICombineElement, combine: ICombine): void{

            transformAndScale(draggedTile, combine, 0, 0, 1.0, 0.5);
        }

        private function pushAllDown(draggedTile: ICombineElement, combine: ICombine): void{

            transformAndScale(draggedTile, combine, 0, 50, 1.0, 0.5);
        }

        private function pushAllLeft(draggedTile: ICombineElement, combine: ICombine): void{

            transformAndScale(draggedTile, combine, 0, 0, 0.5, 1.0);
        }

        private function pushAllRight(draggedTile: ICombineElement, combine: ICombine): void{

            transformAndScale(draggedTile, combine, 50, 0, 0.5, 1.0);
        }

        /**
         * Resizes every tile element (except draggedTile) by the scale factor given by scaleX and scaleY
         * And then translates the element as given by translateX and translateY
         * @param draggedTile
         * @param translateX
         * @param translateY
         * @param reducedScaleX
         * @param reducedScaleY
         */
        private function transformAndScale(draggedTile: ICombineElement, combine: ICombine,
                                           translateX: Number, translateY: Number, reducedScaleX: Number, reducedScaleY: Number ): void{

            for( var i: int = 0; i<combine.numElements; ++i ){
                var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
                if(element && element != draggedTile){

                    element.reducedX = translateX + element.reducedX*reducedScaleX;
                    element.reducedY = translateY + element.reducedY*reducedScaleY;
                    element.reducedWidth *= reducedScaleX;
                    element.reducedHeight *= reducedScaleY;
                }
            }
        }

		/**
		 * 
		 * @param event
		 * 
		 */
        private function handleCombineDropOnPlaceHolder(event: CombineDropEvent): void {

            reStoreTiles();
			recordCurrentLayout();
            var incomingCombine: ICombine = event.draggedCombine as ICombine;
            if(incomingCombine){

                switch(event.dropAction)
                {
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        pushAllDown(null, incomingCombine );
                        pushAllUp(null, combine);
                        break;
                    case TileDropAction.TOP_DROP_ACTION:
                        pushAllUp(null, incomingCombine );
                        pushAllDown(null, combine);
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        pushAllRight(null, incomingCombine );
                        pushAllLeft(null, combine);
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        pushAllLeft(null, incomingCombine );
                        pushAllRight(null, combine);
                        break;
                }
				
				// Rip off all the ICombineElements from incomingCombine and add to this combine.
                for( var i: int = incomingCombine.numElements - 1; i >= 0; i-- )
                {
                    var element: ICombineElement = incomingCombine.removeElementAt(i) as ICombineElement;
					if(element)
                    	combine.addElement(element);
                }
				invalidateCombine();
            }
        }

        private function recordEmptySpace(element: ICombineElement): void{

            emptySpace = new Rectangle(element.reducedX,
                    element.reducedY, element.reducedWidth, element.reducedHeight);
        }

        /**
         * This function handles the case of Tile(from the same combine or another combine) being
         * dropped on a place holder
         * @param event
         */
        private function handleTileDropOnPlaceHolder(event: TileDropEvent): void {

            reStoreTiles();
			recordCurrentLayout();
			
            var draggedTile: ICombineElement = event.draggedTile;
            var droppedTile: ICombineElement = event.droppedTile;

            //if the draggedTile was from another combine... clone a new Tile
            if(draggedTile.owner != combine ){

                var newDesc: TileContainerDescriptor = (draggedTile as TileContainer).tileDescriptor.clone();
                var newTile: TileContainer = TileContainerFactory.createTileContainer(newDesc);
                draggedTile = newTile;
                combine.addElement(newTile);
            }else{

                //This will make sure the dragged tile will be in front during animation
                combine.setElementIndex(draggedTile, combine.numElements-1);
            }

            if(!droppedTile){
                //Dropped at the perimeter level
                switch(event.dropAction) {
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        pushAllUp(null, combine);
                        break;
                    case TileDropAction.TOP_DROP_ACTION:
                        pushAllDown(null, combine);
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        pushAllLeft(null, combine);
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        pushAllRight(null, combine);
                        break;
                }
                // There may be no empty space in this scenario

                if(!newTile)
                    recordEmptySpace(draggedTile);

                // now resize the draggedTile
                draggedTile.reducedX = draggedTile.reducedY = 0;
                draggedTile.reducedHeight = draggedTile.reducedWidth = 100;
                switch(event.dropAction) {
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        draggedTile.reducedHeight = 50;
                        draggedTile.reducedY = 50;
                        break;
                    case TileDropAction.TOP_DROP_ACTION:
                        draggedTile.reducedHeight = 50;
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        draggedTile.reducedWidth = 50;
                        draggedTile.reducedX = 50;
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        draggedTile.reducedWidth = 50;
                        break;
                }
            }
            else
            {
                if(!newTile)
                    recordEmptySpace(draggedTile);

                switch (event.dropAction) {
                    case TileDropAction.TOP_DROP_ACTION:
                        draggedTile.reducedX = droppedTile.reducedX;
                        draggedTile.reducedY = droppedTile.reducedY;
                        draggedTile.reducedWidth = droppedTile.reducedWidth;
                        draggedTile.reducedHeight = droppedTile.reducedHeight = droppedTile.reducedHeight / 2;
                        droppedTile.reducedY += droppedTile.reducedHeight;
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        draggedTile.reducedX = droppedTile.reducedX;
                        draggedTile.reducedY = droppedTile.reducedY;
                        draggedTile.reducedHeight = droppedTile.reducedHeight;
                        draggedTile.reducedWidth = droppedTile.reducedWidth = droppedTile.reducedWidth / 2;
                        droppedTile.reducedX += droppedTile.reducedWidth;
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        draggedTile.reducedX = droppedTile.reducedX + droppedTile.reducedWidth / 2;
                        draggedTile.reducedY = droppedTile.reducedY;
                        draggedTile.reducedHeight = droppedTile.reducedHeight;
                        draggedTile.reducedWidth = droppedTile.reducedWidth = droppedTile.reducedWidth / 2;
                        break;
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        draggedTile.reducedX = droppedTile.reducedX;
                        draggedTile.reducedY = droppedTile.reducedY + droppedTile.reducedHeight / 2;
                        draggedTile.reducedWidth = droppedTile.reducedWidth;
                        draggedTile.reducedHeight = droppedTile.reducedHeight = droppedTile.reducedHeight / 2;
                        break;
                }
            }

            if(emptySpace)
                fillStrategy.consumeEmptySpace(emptySpace);
			// REVISIT: Define an interface for this
			traceUtil("Invalidation after tile drop");
			invalidateCombine();
        }

        /**
         * Eliminate all adjacent placeholders which have same width / height.
         * In effect you are eliminating adjacent placeholders which have no value in dropping because it
         * results in the same arrangement as it was before the drag started.
         * @param element
         * @param placeHoldersList
         */
        private function removeRedundantPlaceHolders(element: ICombineElement, placeHoldersList: ArrayList): void {

            var redundantList: ArrayList = new ArrayList();
            for each( var placeHolder: ITileDropIndicator in placeHoldersList.source){

                if(placeHolder.side == TileSide.TOP_SIDE){
                    if(element.reducedWidth == placeHolder.reducedWidth &&
                            element.reducedY + element.reducedHeight == placeHolder.reducedY){
                        redundantList.addItem(placeHolder);
                    }
                }
                else if(element.reducedWidth == placeHolder.reducedWidth &&
                        placeHolder.side == TileSide.BOTTOM_SIDE){
                    if(element.reducedY == placeHolder.reducedY + placeHolder.reducedHeight){
                        redundantList.addItem(placeHolder);
                    }
                }
                else if(element.reducedHeight == placeHolder.reducedHeight &&
                        placeHolder.side == TileSide.LEFT_SIDE){
                    if(element.reducedX + element.reducedWidth == placeHolder.reducedX){
                        redundantList.addItem(placeHolder);
                    }
                }
                else{
                    if(element.reducedHeight == placeHolder.reducedHeight &&
                            element.reducedX == placeHolder.reducedX + placeHolder.reducedWidth){
                        redundantList.addItem(placeHolder);
                    }
                }
            }

            for each(var redundantPlaceHolder: ITileDropIndicator in redundantList.source)
                placeHoldersList.removeItem(redundantPlaceHolder);
        }

        /**
         * Looks into the placeHolders list and figures out if a place holder has exact dimension place holder on the
         * left/right or top/bottom.
         * If it does than the placeholders are merged into one
         *
         * @param placeHoldersList
         */
        private function removeAdjoiningDuplicates(placeHoldersList: ArrayList): void {

            var duplicatesList: ArrayList = new ArrayList();
            for each( var placeHolder: ITileDropIndicator in placeHoldersList.source){

                if(duplicatesList.getItemIndex(placeHolder) >= 0)
                    continue;

                var adjoiningPlaceHolder: ITileDropIndicator =
                        findAdjoiningPlaceHolder(placeHolder, placeHoldersList);
                if(adjoiningPlaceHolder){
                    if( (adjoiningPlaceHolder.side == TileSide.TOP_SIDE ||
                            adjoiningPlaceHolder.side == TileSide.BOTTOM_SIDE ) &&
                            adjoiningPlaceHolder.reducedWidth == placeHolder.reducedWidth ){
                        duplicatesList.addItem( merge(placeHolder, adjoiningPlaceHolder) );

                    }
                    else if( (adjoiningPlaceHolder.side == TileSide.RIGHT_SIDE ||
                            adjoiningPlaceHolder.side == TileSide.LEFT_SIDE ) &&
                            adjoiningPlaceHolder.reducedHeight == placeHolder.reducedHeight ){
                        duplicatesList.addItem( merge(placeHolder, adjoiningPlaceHolder) );
                    }
                }
            }

            for each( var duplicatePlaceHolder: ITileDropIndicator in duplicatesList.source)
                placeHoldersList.removeItem(duplicatePlaceHolder);
        }

        /**
         * This function creates one place holder from two adjoining placeholders and
         * returns the matchingPlaceHolder to be marked for removal
         * @param placeHolder
         * @param matchingPlaceHolder
         */
        private function merge(placeHolder: ITileDropIndicator,
                               matchingPlaceHolder: ITileDropIndicator): ITileDropIndicator{

            switch(placeHolder.side){
                case TileSide.TOP_SIDE:
                    placeHolder.reducedY = matchingPlaceHolder.reducedY;
                    placeHolder.reducedHeight -= matchingPlaceHolder.reducedHeight;
                    break;
                case TileSide.LEFT_SIDE:
                    placeHolder.reducedX -= matchingPlaceHolder.reducedX;
                    placeHolder.reducedWidth += matchingPlaceHolder.reducedWidth;
                    break;
                case TileSide.BOTTOM_SIDE:
                    placeHolder.reducedHeight += matchingPlaceHolder.reducedHeight;
                    break;
                case TileSide.RIGHT_SIDE:
                    placeHolder.reducedWidth += matchingPlaceHolder.reducedWidth;
                    break;
            }
            return matchingPlaceHolder;
        }

        private function findAdjoiningPlaceHolder(placeHolder: ITileDropIndicator,
                                                  placeHoldersList: ArrayList): ITileDropIndicator {

            var found: Boolean;
            var sideOfInterest: String;
            switch (placeHolder.side){

                case TileSide.TOP_SIDE:
                    sideOfInterest = TileSide.BOTTOM_SIDE;
                    break;
                case TileSide.LEFT_SIDE:
                    sideOfInterest = TileSide.RIGHT_SIDE;
                    break;
                case TileSide.BOTTOM_SIDE:
                    sideOfInterest = TileSide.TOP_SIDE;
                    break;
                case TileSide.RIGHT_SIDE:
                    sideOfInterest = TileSide.LEFT_SIDE;
                    break;
            }
            for each( var adjoiningPlaceHolder: ITileDropIndicator in placeHoldersList.source){

                if(adjoiningPlaceHolder.side == sideOfInterest){
                    if(sideOfInterest == TileSide.TOP_SIDE){
                        if(placeHolder.reducedY + placeHolder.reducedHeight == adjoiningPlaceHolder.reducedY){
                            found = true;
                            break;
                        }
                    }
                    else if(sideOfInterest == TileSide.BOTTOM_SIDE){
                        if(placeHolder.reducedY == adjoiningPlaceHolder.reducedY + adjoiningPlaceHolder.reducedHeight){
                            found = true;
                            break;
                        }
                    }
                    else if(sideOfInterest == TileSide.LEFT_SIDE){
                        if(placeHolder.reducedX + placeHolder.reducedWidth == adjoiningPlaceHolder.reducedX){
                            found = true;
                            break;
                        }
                    }
                    else{
                        if(placeHolder.reducedX == adjoiningPlaceHolder.reducedX + adjoiningPlaceHolder.reducedWidth){
                            found = true;
                            break;
                        }
                    }
                }
            }

            return found ? adjoiningPlaceHolder : null;
        }

        private function createPlaceHolders(element: ICombineElement): ArrayList {

            var placeHolders: ArrayList = new ArrayList();
            //var tallerDimension: Boolean = element.reducedHeight > element.reducedWidth;

            var leftPlaceHolder: ITileDropIndicator = createTallerPlaceholder(element, TileSide.LEFT_SIDE);
            leftPlaceHolder.reducedX = element.reducedX;
            placeHolders.addItem(leftPlaceHolder);

            var rightPlaceHolder: ITileDropIndicator = createTallerPlaceholder(element, TileSide.RIGHT_SIDE);
            rightPlaceHolder.reducedX = element.reducedX + element.reducedWidth - PLACE_HOLDER_GIRTH;
            placeHolders.addItem(rightPlaceHolder);

            var topPlaceHolder: ITileDropIndicator = createWiderPlaceholder(element, TileSide.TOP_SIDE);
            topPlaceHolder.reducedY = element.reducedY;
            placeHolders.addItem(topPlaceHolder);

            var bottomPlaceHolder: ITileDropIndicator = createWiderPlaceholder(element, TileSide.BOTTOM_SIDE);
            bottomPlaceHolder.reducedY = element.reducedY + element.reducedHeight - (PLACE_HOLDER_GIRTH*aspectRatio);
            placeHolders.addItem(bottomPlaceHolder);

            checkAndAdjustBounds(topPlaceHolder);
            checkAndAdjustBounds(leftPlaceHolder);
            checkAndAdjustBounds(rightPlaceHolder);
            checkAndAdjustBounds(bottomPlaceHolder);
            return placeHolders;
        }

        /**
         * This method will find if the tileElement is sitting on the perimeter of this Combine,
         * If it is and also if there is an ecompassing placeHolder on the same side, the tileElement
         * will be shifted inwards and re-sized accordingly
         * @param tileElement
         */
        private function checkAndAdjustBounds(tileElement: ICombineElement): void {

            //return;

            // touching the left boundary?
            if(canDropOnLeftBoundary && tileElement.reducedX == 0){

                tileElement.reducedX = PLACE_HOLDER_GIRTH;
                tileElement.reducedWidth -= PLACE_HOLDER_GIRTH;
            }
            // touching the right boundary?
            if(canDropOnRightBoundary && Math.ceil(tileElement.reducedWidth+tileElement.reducedX) == 100){
                tileElement.reducedWidth -= PLACE_HOLDER_GIRTH;
            }
            // touching the top boundary?
            if(canDropOnTopBoundary && tileElement.reducedY == 0){
                tileElement.reducedY = (PLACE_HOLDER_GIRTH*aspectRatio);
                tileElement.reducedHeight -= (PLACE_HOLDER_GIRTH*aspectRatio);
            }
            // touching the bottom boundary?
            if(canDropOnBottomBoundary && Math.ceil(tileElement.reducedY+tileElement.reducedHeight) == 100){
                tileElement.reducedHeight -= (PLACE_HOLDER_GIRTH*aspectRatio);
            }
        }

        private function createWiderPlaceholder(element: ICombineElement, side: String): ITileDropIndicator {

            var placeHolder: ITileDropIndicator = getPlaceHolder(side);
            placeHolder.reducedX = element.reducedX;
            placeHolder.reducedHeight = (PLACE_HOLDER_GIRTH*aspectRatio);
            placeHolder.reducedWidth = element.reducedWidth;
            placeHolder.relatedTile = element;
            return placeHolder;
        }

        private function createNarrowerPlaceholder(element: ICombineElement, side: String): ITileDropIndicator {

            var placeHolder: ITileDropIndicator = getPlaceHolder(side);
            placeHolder.reducedX = element.reducedX + PLACE_HOLDER_GIRTH;
            placeHolder.reducedHeight = (PLACE_HOLDER_GIRTH*aspectRatio);
            placeHolder.reducedWidth = element.reducedWidth - 2*PLACE_HOLDER_GIRTH;
            placeHolder.relatedTile = element;
            return placeHolder;
        }

        private function createTallerPlaceholder(element: ICombineElement, side: String): ITileDropIndicator {

            var placeHolder: ITileDropIndicator = getPlaceHolder(side);
            placeHolder.reducedY = element.reducedY;
            placeHolder.reducedHeight = element.reducedHeight;
            placeHolder.reducedWidth = PLACE_HOLDER_GIRTH;
            placeHolder.relatedTile = element;
            return placeHolder;

        }

        private function createShorterPlaceholder(element: ICombineElement, side: String): ITileDropIndicator {

            var placeHolder: ITileDropIndicator = getPlaceHolder(side);
            placeHolder.reducedY = element.reducedY + PLACE_HOLDER_GIRTH;
            placeHolder.reducedHeight = element.reducedHeight - 2*PLACE_HOLDER_GIRTH;
            placeHolder.reducedWidth = PLACE_HOLDER_GIRTH;
            placeHolder.relatedTile = element;
            return placeHolder;

        }

        private function getPlaceHolder(side: String): ITileDropIndicator {

            var placeHolder: ITileDropIndicator = combine.dropIndicatorFactory.newInstance();
            placeHolder.side = side;

            placeHolder.addEventListener(TileDropEvent.DROP, handleTileDropOnPlaceHolder, false, 0, true);
            placeHolder.addEventListener(CombineDropEvent.DROP, handleCombineDropOnPlaceHolder, false, 0, true);
            placeHolder.addEventListener(PlaceHolderDropEvent.DROP, handleResourceDropOnPlaceHolder, false, 0, true);

            return placeHolder;
        }

        private function restoreSize(element: ICombineElement, placeHoldersList: Array): void {

            var placeHoldersMap: Dictionary = getPlaceHoldersFor(element, placeHoldersList);
            if(placeHoldersMap[TileSide.LEFT_SIDE]){
                element.reducedX -= PLACE_HOLDER_GIRTH;
                element.reducedWidth += PLACE_HOLDER_GIRTH;
            }
            if(placeHoldersMap[TileSide.TOP_SIDE]){
                element.reducedY -= (PLACE_HOLDER_GIRTH*aspectRatio);
                element.reducedHeight += (PLACE_HOLDER_GIRTH*aspectRatio);
            }
            if(placeHoldersMap[TileSide.RIGHT_SIDE]){
                element.reducedWidth += PLACE_HOLDER_GIRTH;
            }
            if(placeHoldersMap[TileSide.BOTTOM_SIDE]){
                element.reducedHeight += (PLACE_HOLDER_GIRTH*aspectRatio);
            }
        }

        /*private function restoreSize(element: ICombineElement): void {

         element.reducedX -= PLACE_HOLDER_GIRTH;
         element.reducedY -= PLACE_HOLDER_GIRTH;

         element.reducedWidth += 2 * PLACE_HOLDER_GIRTH;
         element.reducedHeight += 2 * PLACE_HOLDER_GIRTH;
         }*/

        private function checkAndAdjustBoundsInReverse(element: ICombineElement): void {

            // touching the left boundary?
            if(canDropOnLeftBoundary && element.reducedX == PLACE_HOLDER_GIRTH){

                element.reducedX = 0;
                element.reducedWidth += PLACE_HOLDER_GIRTH;
            }
            // touching the right boundary?
            if(canDropOnRightBoundary && Math.ceil(element.reducedWidth+element.reducedX+PLACE_HOLDER_GIRTH) == 100){
                element.reducedWidth += PLACE_HOLDER_GIRTH;
            }
            // touching the top boundary?
            if(canDropOnTopBoundary && element.reducedY == (PLACE_HOLDER_GIRTH*aspectRatio)){
                element.reducedY = 0;
                element.reducedHeight += (PLACE_HOLDER_GIRTH*aspectRatio);
            }
            // touching the bottom boundary?
            if(canDropOnBottomBoundary && Math.ceil(element.reducedY+element.reducedHeight+(PLACE_HOLDER_GIRTH*aspectRatio)) == 100){
                element.reducedHeight += (PLACE_HOLDER_GIRTH*aspectRatio);
            }


        }



        /**
         * returns all place holders of the element as a dictionary.
         * The related place holders are mapped in the direction with side as the key
         * @param element
         * @return
         */
        private function getPlaceHoldersFor(element: ICombineElement, placeHolderList: Array): Dictionary {

            var dictionary: Dictionary = new Dictionary(true);
            for each(var placeHolder: ITileDropIndicator in placeHolderList){

                if(placeHolder.relatedTile == element ){
                    dictionary[placeHolder.side] = placeHolder;
                }
            }
            return dictionary;
        }

        private function shrink(element: ICombineElement, placeHoldersList: Array): void {

            var placeHoldersMap: Dictionary = getPlaceHoldersFor(element, placeHoldersList);
            var topPlaceHolder: ITileDropIndicator = placeHoldersMap[TileSide.TOP_SIDE];
            var leftPlaceHolder: ITileDropIndicator = placeHoldersMap[TileSide.LEFT_SIDE];
            var rightPlaceHolder: ITileDropIndicator = placeHoldersMap[TileSide.RIGHT_SIDE];

            var bottomPlaceHolder: ITileDropIndicator = placeHoldersMap[TileSide.BOTTOM_SIDE];
            if(leftPlaceHolder){
                element.reducedX += PLACE_HOLDER_GIRTH;
                element.reducedWidth -= PLACE_HOLDER_GIRTH;
            }
            if(topPlaceHolder){
                element.reducedY += (PLACE_HOLDER_GIRTH*aspectRatio);
                element.reducedHeight -= (PLACE_HOLDER_GIRTH*aspectRatio);
            }
            if(rightPlaceHolder){
                element.reducedWidth -= PLACE_HOLDER_GIRTH;
            }
            if(bottomPlaceHolder){
                element.reducedHeight -= (PLACE_HOLDER_GIRTH*aspectRatio);
            }

            // Resize the top and bottom placeholders depending on
            // the existence of left and right place holders.
            // The left and right place holders always takes the full height of the tile
            if(topPlaceHolder){

                if(leftPlaceHolder){
                    topPlaceHolder.reducedX += PLACE_HOLDER_GIRTH;
                    topPlaceHolder.reducedWidth -= PLACE_HOLDER_GIRTH;
                }
                if(rightPlaceHolder){
                    topPlaceHolder.reducedWidth -= PLACE_HOLDER_GIRTH;
                }
            }

            if(bottomPlaceHolder){

                if(leftPlaceHolder){
                    bottomPlaceHolder.reducedX += PLACE_HOLDER_GIRTH;
                    bottomPlaceHolder.reducedWidth -= PLACE_HOLDER_GIRTH;
                }
                if(rightPlaceHolder){
                    bottomPlaceHolder.reducedWidth -= PLACE_HOLDER_GIRTH;
                }
            }
        }

        private function createPerimeterPlaceHolders(placeHoldersList: IList): void {

            if(canDropOnLeftBoundary){

                var boundaryPlaceHolder: ITileDropIndicator = getPlaceHolder(TileSide.LEFT_SIDE);
                boundaryPlaceHolder.reducedY = canDropOnTopBoundary ? (PLACE_HOLDER_GIRTH*aspectRatio) : 0;
                boundaryPlaceHolder.reducedX = 0;
                boundaryPlaceHolder.reducedWidth = PLACE_HOLDER_GIRTH;
                boundaryPlaceHolder.reducedHeight = 100;
                boundaryPlaceHolder.reducedHeight -= (canDropOnTopBoundary ? (PLACE_HOLDER_GIRTH*aspectRatio) : 0);
                boundaryPlaceHolder.reducedHeight -= (canDropOnBottomBoundary ? (PLACE_HOLDER_GIRTH*aspectRatio) : 0);
                placeHoldersList.addItem(boundaryPlaceHolder);
            }

            if(canDropOnRightBoundary){

                boundaryPlaceHolder = getPlaceHolder(TileSide.RIGHT_SIDE);
                boundaryPlaceHolder.reducedY = canDropOnTopBoundary ? (PLACE_HOLDER_GIRTH*aspectRatio) : 0;
                boundaryPlaceHolder.reducedX = 100-PLACE_HOLDER_GIRTH;
                boundaryPlaceHolder.reducedWidth = PLACE_HOLDER_GIRTH;
				boundaryPlaceHolder.reducedHeight = 100;
				boundaryPlaceHolder.reducedHeight -= (canDropOnTopBoundary ? (PLACE_HOLDER_GIRTH*aspectRatio) : 0);
				boundaryPlaceHolder.reducedHeight -= (canDropOnBottomBoundary ? (PLACE_HOLDER_GIRTH*aspectRatio) : 0);
                placeHoldersList.addItem(boundaryPlaceHolder);
            }

            if(canDropOnTopBoundary){

                boundaryPlaceHolder = getPlaceHolder(TileSide.TOP_SIDE);
                boundaryPlaceHolder.reducedY = boundaryPlaceHolder.reducedX = 0;
                boundaryPlaceHolder.reducedWidth = 100;
                boundaryPlaceHolder.reducedHeight = (PLACE_HOLDER_GIRTH*aspectRatio);
                placeHoldersList.addItem(boundaryPlaceHolder);
            }

            if(canDropOnBottomBoundary){

                boundaryPlaceHolder = getPlaceHolder(TileSide.BOTTOM_SIDE);
                boundaryPlaceHolder.reducedX = 0;
                boundaryPlaceHolder.reducedY = 100 - (PLACE_HOLDER_GIRTH*aspectRatio);
                boundaryPlaceHolder.reducedWidth = 100;
                boundaryPlaceHolder.reducedHeight = (PLACE_HOLDER_GIRTH*aspectRatio);
                placeHoldersList.addItem(boundaryPlaceHolder);
            }
        }


        private function swapProperties(elementA: ICombineElement, elementB: ICombineElement): void{

            var reducedXCopy: Number = elementA.reducedX;
            var reducedYCopy: Number = elementA.reducedY;
            var reducedWidthCopy: Number = elementA.reducedWidth;
            var reducedHeightCopy: Number = elementA.reducedHeight;

            copyProperties(elementA, elementB);

            elementB.reducedX = reducedXCopy;
            elementB.reducedY = reducedYCopy;
            elementB.reducedWidth = reducedWidthCopy;
            elementB.reducedHeight = reducedHeightCopy;



        }

        private function copyProperties(destination: ICombineElement, source: ICombineElement): void{

            destination.reducedX = source.reducedX;
            destination.reducedY = source.reducedY;
            destination.reducedWidth = source.reducedWidth;
            destination.reducedHeight = source.reducedHeight;
        }

        
		private var previousLayouts: Array = [];
		private function recordCurrentLayout(): void {
						
			var savedLayoutInfo: Dictionary  = new Dictionary(true);
			for(var i: int = 0; i<combine.numElements; ++i){
				var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
				if(!element) continue;
				savedLayoutInfo[element] = getDimensions(element);
			}
			previousLayouts.push(savedLayoutInfo);
		}
		
		private function getDimensions(element: ICombineElement): Object{
			var obj: Object = new Object();
			obj.reducedX = element.reducedX;
			obj.reducedY = element.reducedY;
			obj.reducedWidth = element.reducedWidth;
			obj.reducedHeight = element.reducedHeight;
			return obj;
		}
		
		private function onMenuItemSelect(ev: FlexNativeMenuEvent): void{
			
			if(ev.label == "Undo"){
				revertToSavedLayout();
			}
		}

		private function revertToSavedLayout(): void {
			
			if(!combine.visible)
				return;
			if(previousLayouts.length == 0)
				return;
			
			var savedLayoutInfo: Dictionary = previousLayouts.pop();
			var elementsToRemove: Array = [];
			for(var i: int = 0; i<combine.numElements; ++i){
				
				var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
				if(!element) continue;
				var savedInfo: Object = savedLayoutInfo[element];
				if(savedInfo){
					element.reducedHeight = savedInfo.reducedHeight;	
					element.reducedWidth = savedInfo.reducedWidth;	
					element.reducedX = savedInfo.reducedX;	
					element.reducedY = savedInfo.reducedY;
				}
				else{
					// this element was newly added, so remove it
					elementsToRemove.push(element);
				}
			}
			
			for each(element in elementsToRemove)
				combine.removeElement(element);
			
			// This smells!! Skinnable container hasn't implemented invalidateDisplayList()
			// and allowed for layout and contentGroup to be invalidated
			invalidateCombine();
		}
		
		private static const LOG_ENABLED: Boolean = false;
		
		private function traceUtil(msg: String): void {
			
			if(LOG_ENABLED)
				trace("LOHITH: " + combine.name + " --> " + msg);
		}

        private function invalidateCombine(): void {

            traceUtil("INVALIDATING COMBINE");
            (combine as SkinnableContainer).invalidateProperties();
            (combine as SkinnableContainer).contentGroup.invalidateDisplayList();
        }


















        //--------------------------------------------------------------------------
        //
        //  Operating System to Affect Drag handling
        //
        //--------------------------------------------------------------------------

        // TODO: Employ separation of concerns and mark out which
        // components handle what part of the drag and drop drama

        private function isResourceDraggedFromOutside(event: NativeDragEvent): Boolean{

            var clipboard: Clipboard = event.clipboard;

            if( clipboard.hasFormat(ClipboardFormats.URL_FORMAT) ||
                    clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT) ){

                return true;
            }

            return false;
        }


        // Native drag and drop

        /**
         *
         * @param event
         *
         */
        private function onNativeDragIn(event: NativeDragEvent): void {

            if(!isResourceDraggedFromOutside(event))
                return;

            if(event.target is ICombine && !isNativeDragging){

                combine.resizeGroup.visible = false;
                aspectRatio = (combine.width/combine.height);

                prepContainerForNewTile();
                combine.animate = true;

                isNativeDragging = true;
            }
            else if(isNativeDragging && event.target is ITileDropIndicator){

                var clipboard: Clipboard = event.clipboard;

                if( clipboard.hasFormat(ClipboardFormats.URL_FORMAT) ||
                        clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT) ){

                    var dropTarget: InteractiveObject = event.target as InteractiveObject;
                    dropTarget.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onNativeDragDrop);
                    dropTarget.addEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onNativeDragExit);

                    NativeDragManager.dropAction = NativeDragActions.COPY;
                    NativeDragManager.acceptDragDrop(dropTarget);
                }
                else {

                    //trace("Unrecognized format");
                    return;
                }
            }
        }

        private function onNativeDragExit(event: NativeDragEvent): void {

            if(!isResourceDraggedFromOutside(event))
                return;

            if((event.target is ICombine)){

                cleanUpPostDrag();
            }
            else if(event.target is ITileDropIndicator){

                var dropTarget: InteractiveObject = event.target as InteractiveObject;
                dropTarget.removeEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onNativeDragDrop);
                dropTarget.removeEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onNativeDragExit);
            }
        }

        private function onNativeDragDrop(event: NativeDragEvent): void {

            if(!isResourceDraggedFromOutside(event))
                return;

            var dropIndicator: ITileDropIndicator = event.target as ITileDropIndicator;

            if(isNativeDragging && dropIndicator){

                var fileUrl: String = event.clipboard.getData(ClipboardFormats.URL_FORMAT) as String;

                var tileDescriptor: TileContainerDescriptor = TileContainerFactory.createTileDescriptorForAssetURL(fileUrl);
                var newTile: TileContainer = TileContainerFactory.createTileContainer(tileDescriptor);

                // setting x and y helps in properly animating the drop
                var stagePoint: Point = new Point(event.stageX, event.stageY);
                var localPoint: Point = combine.globalToLocal(stagePoint);
                newTile.x = localPoint.x;
                newTile.y = localPoint.y;
                newTile.width = 80;
                newTile.height = 60;

                reStoreTiles();
                recordCurrentLayout();
                positionTilePostDrop(newTile, dropIndicator);

                combine.addElement(newTile);
                invalidateCombine();

                dropIndicator.removeEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onNativeDragDrop);
                dropIndicator.removeEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onNativeDragExit);

                cleanUpPostDrag();
            }
        }


        private function positionTilePostDrop(newTile: TileContainer, dropIndicator: ITileDropIndicator): void{

            var dropAction: String = dropIndicator.getDropAction();
            var droppedTile: ICombineElement = dropIndicator.relatedTile;
            if(!droppedTile){

                //Dropped at the perimeter level
                switch(dropAction) {
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        pushAllUp(null, combine);
                        break;
                    case TileDropAction.TOP_DROP_ACTION:
                        pushAllDown(null, combine);
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        pushAllLeft(null, combine);
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        pushAllRight(null, combine);
                        break;
                }
                // There may be no empty space in this scenario
                // now resize the draggedTile
                newTile.reducedX = newTile.reducedY = 0;
                newTile.reducedHeight = newTile.reducedWidth = 100;
                switch(dropAction) {
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        newTile.reducedHeight = 50;
                        newTile.reducedY = 50;
                        break;
                    case TileDropAction.TOP_DROP_ACTION:
                        newTile.reducedHeight = 50;
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        newTile.reducedWidth = 50;
                        newTile.reducedX = 50;
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        newTile.reducedWidth = 50;
                        break;
                }
            }
            else {
                switch (dropAction) {

                    case TileDropAction.TOP_DROP_ACTION:
                        newTile.reducedX = droppedTile.reducedX;
                        newTile.reducedY = droppedTile.reducedY;
                        newTile.reducedWidth = droppedTile.reducedWidth;
                        newTile.reducedHeight = droppedTile.reducedHeight = droppedTile.reducedHeight / 2;
                        droppedTile.reducedY += droppedTile.reducedHeight;
                        break;
                    case TileDropAction.LEFT_DROP_ACTION:
                        newTile.reducedX = droppedTile.reducedX;
                        newTile.reducedY = droppedTile.reducedY;
                        newTile.reducedHeight = droppedTile.reducedHeight;
                        newTile.reducedWidth = droppedTile.reducedWidth = droppedTile.reducedWidth / 2;
                        droppedTile.reducedX += droppedTile.reducedWidth;
                        break;
                    case TileDropAction.RIGHT_DROP_ACTION:
                        newTile.reducedX = droppedTile.reducedX + droppedTile.reducedWidth / 2;
                        newTile.reducedY = droppedTile.reducedY;
                        newTile.reducedHeight = droppedTile.reducedHeight;
                        newTile.reducedWidth = droppedTile.reducedWidth = droppedTile.reducedWidth / 2;
                        break;
                    case TileDropAction.BOTTOM_DROP_ACTION:
                        newTile.reducedX = droppedTile.reducedX;
                        newTile.reducedY = droppedTile.reducedY + droppedTile.reducedHeight / 2;
                        newTile.reducedWidth = droppedTile.reducedWidth;
                        newTile.reducedHeight = droppedTile.reducedHeight = droppedTile.reducedHeight / 2;
                        break;
                }
            }
        }

	}
}
