/**
 * Created by synesis on 23/04/2015.
 */
package com.al.combine
{

    import com.al.containers.CombineContainer;
    import com.al.containers.TileContainer;
    import com.al.events.TileFullScreenEvent;
    import com.al.util.EventUtils;

    import flash.display.StageDisplayState;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.FullScreenEvent;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;

    import mx.core.FlexGlobals;
    import mx.core.UIComponent;

    import spark.components.Group;
    import spark.components.SkinnableContainer;

    public class CombineFullScreenManager
    {
        // the combine which is currently being managed
        private var combine: CombineContainer;

        public function CombineFullScreenManager() {
        }

        public function manageCombine(combineContainer: CombineContainer): void{

            combine = combineContainer;
            combine.addEventListener(TileFullScreenEvent.FULL_SCREEN, onFullScreen);
        }

        protected var tiles: Array = [];

        private function onFullScreen(event: TileFullScreenEvent): void {

            event.stopImmediatePropagation();

            if(combine.stage.displayState == StageDisplayState.NORMAL){

                goFullScreen(event.tile);
            }
        }

        private var widthBeforeFS: Number;
        private var heightBeforeFS: Number;

        private function revertFullScreen(event: Event = null): void{

            var application: SkinnableContainer = FlexGlobals.topLevelApplication as SkinnableContainer;
            if(combine.fullScreenGroup && application.contains(combine.fullScreenGroup)) {

                combine.fullScreenGroup.removeAllElements();
                application.removeElement(combine.fullScreenGroup);

                while(tiles.length > 0){
                    var element: TileContainer = tiles.pop() as TileContainer;
                    if(element)
                    {
                        element.percentHeight = element.percentWidth = NaN;
                        combine.addElement(element);
                    }
                }
            }

            fullScreenTile.dispatchEvent(
                    new TileFullScreenEvent(fullScreenTile, TileFullScreenEvent.FULL_SCREEN_EXITED));

            combine.resizeGroup.visible = true;
            combine.resizeGroup.enabled = true;

            combine.stage.nativeWindow.width = widthBeforeFS;
            combine.stage.nativeWindow.height = heightBeforeFS;

            combine.stage.scaleMode = StageScaleMode.NO_SCALE;
            combine.stage.displayState = StageDisplayState.NORMAL;

            combine.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
            combine.stage.removeEventListener(FullScreenEvent.FULL_SCREEN, onFullScreenChange);
        }


        // tile which initiated full screen
        private var fullScreenTile: TileContainer;

        private function goFullScreen(initiatedTile: TileContainer): void {

            var application: SkinnableContainer = FlexGlobals.topLevelApplication as SkinnableContainer;

            if(!combine.fullScreenGroup)
                combine.fullScreenGroup = new Group();
            combine.fullScreenGroup.percentWidth = 100;
            combine.fullScreenGroup.percentHeight = 100;

            tiles = [];
            for(var i: int = 0 ; i<combine.numElements; ++i){

                tiles.push(combine.getElementAt(i));
            }

            for each(var tile: TileContainer in tiles){

                if(tile){

                    combine.removeElement(tile);
                    tile.x = tile.y = 0;
                    tile.percentWidth = tile.percentHeight = 100;

                    if(tile == initiatedTile){

                        combine.fullScreenGroup.addElement(tile);
                        combine.fullScreenGroup.invalidateDisplayList();
                    }
                }
            }

            //save current width and height
            widthBeforeFS = combine.stage.nativeWindow.width;
            heightBeforeFS = combine.stage.nativeWindow.height;

            if(!application.contains(combine.fullScreenGroup))
                application.addElement(combine.fullScreenGroup);

            combine.invalidateDisplayList();
            combine.resizeGroup.visible = false;
            combine.resizeGroup.enabled = false;

            application.height = application.stage.fullScreenHeight;
            application.width = application.stage.fullScreenWidth;

            //combine.stage.scaleMode = StageScaleMode.EXACT_FIT;

            combine.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
            combine.stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreenChange);

            combine.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;

            fullScreenTile = initiatedTile;
            fullScreenTile.addEventListener(TileFullScreenEvent.EXIT_FULL_SCREEN, revertFullScreen);
        }

        private function onFullScreenChange(event: FullScreenEvent): void{

            var application: UIComponent = FlexGlobals.topLevelApplication as UIComponent;

            if(!event.fullScreen){

                revertFullScreen(event);
            }
        }

        private function onKeyDown(event: KeyboardEvent): void{

            if(event.keyCode == Keyboard.LEFT){

                var tile: TileContainer = combine.fullScreenGroup.getElementAt(0) as TileContainer;
               var previousTile: TileContainer = getTileWithVisualIndex(tile.visualIndex-1);
                if(previousTile){

                    combine.fullScreenGroup.removeAllElements();
                    combine.fullScreenGroup.addElement(previousTile);
                    combine.fullScreenGroup.invalidateDisplayList();
                }
            }else if(event.keyCode == Keyboard.RIGHT){

                tile = combine.fullScreenGroup.getElementAt(0) as TileContainer;
                var nextTile: TileContainer = getTileWithVisualIndex(tile.visualIndex+1);
                if(nextTile){

                    combine.fullScreenGroup.removeAllElements();
                    combine.fullScreenGroup.addElement(nextTile);
                    combine.fullScreenGroup.invalidateDisplayList();
                }
            }
        }

        private function getTileWithVisualIndex(visualIndex: int): TileContainer {

            for each(var tile: TileContainer in tiles){
                if(tile.visualIndex == visualIndex)
                    return tile;
            }
            return null;
        }
    }
}
