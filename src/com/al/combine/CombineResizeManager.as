/**
 * Created by lram on 27/02/2015.
 */
package com.al.combine
{
	import com.al.application.controls.GutterLane;
	import com.al.application.controls.TileResizeHandle;
	import com.al.dragging.StartDraggingStrategy;
	import com.al.enums.HandleOrientation;
	import com.al.enums.TileSide;
	import com.al.events.StartDraggingEvent;
	import com.al.layouts.CombineLayout;
	import com.al.tile.ICombineElement;
	import com.al.tile.IResizeHandle;
    import com.al.tile.ITileContainer;
    import com.al.util.EventUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import mx.core.IUIComponent;
	import mx.core.IVisualElementContainer;
	import mx.core.UIComponent;
	
	import spark.components.Group;
	import spark.components.SkinnableContainer;
	
	public class CombineResizeManager
	{
		// the combine which is currently being managed
		private var combine: ICombine;
		
		private var edgesDictionary: Dictionary;

		private var cornersDictionary: Dictionary;
		
		private var splittersDictionary: Dictionary;
		
		private var gutterLanes: Array;
		private var resizeHandles: Array;
		private var splitterHandles: Array;
		
		private static const MINIMUM_WIDTH: Number = 10;
		private static const MINIMUM_HEIGHT: Number = 10;
		
		public function manageCombine(combineContainer: ICombine): void{
			
			combine = combineContainer;
		}

        private var tiles: Array;
		public function commitProperties(): void{

			// If resizeGroup not visible no need to re-compute(may be we are in the process of dragging here..)
			if(!combine.resizeGroup.visible) return;

            tiles = getTiles();
			traceUtil("Resize Handles... COMMITPROPERTIES. Tiles - ", tiles.length);
			traceUtil("Resize Handles... COMMITPROPERTIES. Combine Elements - ", combine.numElements);

			combine.resizeGroup.removeAllElements();
			
			cornersDictionary = new Dictionary(true);
			// pass 1: Determines corners	
			for(var i: int = 0; i<tiles.length; ++i) {
				
				var element: ICombineElement = tiles[i] as ICombineElement;
				if(!element) 
					continue;
				
				createOrUpdateCornersFor(element);
			}
			
			// pass 2: Determine vertical and horizontal resizable edges. 
			// i,e Adjoining tiles with equal length side
			edgesDictionary = new Dictionary(true);
			for( i = 0; i<tiles.length; ++i) {
				
				element = tiles[i] as ICombineElement;
				if(!element) 
					continue;
				createOrUpdateVerticalOrHorizontalEdge(element);
			}
			
			// pass 3: Determine splitters
			//splittersDictionary = new Dictionary(true);
			splitterHandles = [];
			for( i = 0; i<tiles.length; ++i) {
				
				element = tiles[i] as ICombineElement;
				if(!element) 
					continue;
				createSplitters(element);
			}
			
			// Pass 4: Create all the resize handle for corners, edges and splitters
			
			// Corners - we are interested in the ones
			// where more than 1 tile containers meet
			for each( var corner: Corner in cornersDictionary){
				if(corner.elements.length > 1 ){
					combine.resizeGroup.addElement(getCornerResizeHandle(corner));	
				}
			}
			
			
			// Edges 
			for each( corner in edgesDictionary){
				
				if(corner.elements.length > 1 ){
					combine.resizeGroup.addElement(getHorizontalOrVerticalResizeHandle(corner));	
				}
			}
			
			// Splitters
			for each( var handle: IResizeHandle in splitterHandles){
				
				combine.resizeGroup.addElement(handle);	
			}
			
			resizeHandles = getAllHandles();
			//traceUtil("Handles created..." + resizeHandles.length);
			
			// Last pass: Determine gutters running vertically / horizontally
			gutterLanes = [];
			dividingVerticalLines = new Dictionary(true);
			dividingHorizontalLines = new Dictionary(true);
			for each(handle in resizeHandles){
				
				if(handle.corner.elements.length > 1 )
					buildGutter(handle);
			}
			
			for each( var line: Rectangle in dividingVerticalLines){
				
				var gutterLane: GutterLane = createGutterLane(line);
				combine.resizeGroup.addElementAt(gutterLane,0);
				gutterLanes.push(gutterLane);
			}
			for each( line in dividingHorizontalLines){
				
				gutterLane = createGutterLane(line);
				combine.resizeGroup.addElementAt(gutterLane,0);
				gutterLanes.push(gutterLane);
			}
			
		}

        /**
         * All tiles in this combine
         * @return
         */

        private function getTiles(): Array {

            var tiles: Array = [];
            for ( var i: int = 0; i<combine.numElements; ++i) {

                var element: ITileContainer = combine.getElementAt(i) as ITileContainer;
                if(element)
                    tiles.push(element);
            }
            return tiles;
        }
		
		/**
		 * 
		 * @param line
		 * @return 
		 * 
		 */
		private function createGutterLane(line: Rectangle): GutterLane {
			
			var gutterLane: GutterLane = new GutterLane(line);
			gutterLane.reducedX = line.left;
			gutterLane.reducedY = line.top;
			gutterLane.reducedWidth = line.width;
			gutterLane.reducedHeight = line.height;
			
			gutterLane.addEventListener(StartDraggingEvent.START_DRAGGING, onGutterLineDragStart);
			StartDraggingStrategy.dragWatch(gutterLane, 0);
			
			return gutterLane;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event handlers
		//
		//--------------------------------------------------------------------------
		private var currentHandle: IResizeHandle;
		private var affectedElements: Array;
		private static const SNAP_PROXIMITY: int = 1.6;
		
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onStartDragging(event: StartDraggingEvent): void {
			
			currentHandle = (event.target as IResizeHandle);
			combine.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			combine.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			
			var deltaXTransform: Number = event.deltaX*(100/combine.width);
			var deltaYTransform: Number = event.deltaY*(100/combine.height);
			
			determineVerticalAndHorizontalDividingLine(currentHandle);
			affectedElements = findAffectedElements(currentHandle);
			
			var transformationApplied: Boolean = 
				performXTransformationOnElements(deltaXTransform, affectedElements);
			
			if(transformationApplied){
				currentHandle.reducedX += deltaXTransform;
				currentHandle.corner.x += deltaXTransform;
			}
			transformationApplied = 
				performYTransformationOnElements(deltaYTransform, affectedElements);
			
			if(transformationApplied){
				currentHandle.reducedY += deltaYTransform;
				currentHandle.corner.y += deltaYTransform;
			}
			
			(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
			combine.resizeGroup.invalidateDisplayList();
			event.mouseEvent.updateAfterEvent();
			
			gutterLanes.forEach(removeCombineElement);
			resizeHandles.forEach(hideAllOtherHandles);
		}
		
		
		private var currentSplitter: IResizeHandle;
		private var splitterSnapped: Boolean;
		
		private var elementDimension: Rectangle;
		private var splitElementDimension: Rectangle;
		
		/**
		 *  
		 * @param event
		 * 
		 */
		private function onSplitterDragStart(event: StartDraggingEvent): void{
		
			currentSplitter = (event.target as IResizeHandle);
			
			if(currentSplitter) {
				
				var line: Rectangle;
				var element: ICombineElement;
				var elementToSplit: ICombineElement;
				var corner: Corner = currentSplitter.corner;
				
				combine.stage.addEventListener(MouseEvent.MOUSE_MOVE, onSplitterMove);
				combine.stage.addEventListener(MouseEvent.MOUSE_UP, onSplitterDragEnd);
				
				if(corner.verticalLine){
					
					line = corner.verticalLine;
					elementToSplit = 
						corner.elements[0].reducedHeight > corner.elements[1].reducedHeight ?
						corner.elements[0] as ICombineElement : corner.elements[1] as ICombineElement;
					element = (corner.elements[0] == elementToSplit) ?
								corner.elements[1] as ICombineElement : corner.elements[0] as ICombineElement;
					
					//save the original Dimensions
					elementDimension = getBoundsRectangle(element);
					splitElementDimension = getBoundsRectangle(elementToSplit);
					
					if(normalize(elementToSplit.reducedY) == normalize(element.reducedY)){
						elementToSplit.reducedY += element.reducedHeight;
					}
					elementToSplit.reducedHeight -= element.reducedHeight;
					affectedElements = [element];
					verticalAlignedElements = new Dictionary(true);
					verticalAlignedElements[element] = isElementOnVerticalLine(element, line);
				}
				else{
				
					line = corner.horizontalLine;
					elementToSplit = 
						corner.elements[0].reducedWidth > corner.elements[1].reducedWidth ?
						corner.elements[0] as ICombineElement : corner.elements[1] as ICombineElement;
					element = (corner.elements[0] == elementToSplit) ?
						corner.elements[1] as ICombineElement : corner.elements[0] as ICombineElement;
					
					//save the original Dimensions
					elementDimension = getBoundsRectangle(element);
					splitElementDimension = getBoundsRectangle(elementToSplit);
					
					if(normalize(elementToSplit.reducedX) == normalize(element.reducedX)){
						elementToSplit.reducedX += element.reducedWidth;
					}
					elementToSplit.reducedWidth -= element.reducedWidth;
					
					affectedElements = [element];
					horizontalAlignedElements = new Dictionary(true);
					horizontalAlignedElements[element] = isElementOnHorizontalLine(element, line);
				}
				
				splitterSnapped = false;
				
				gutterLanes.forEach(removeCombineElement);
				resizeHandles.forEach(removeCombineElement);
				combine.resizeGroup.addElement(currentSplitter);
				//splitterHandles.forEach(hideAllOtherSplitters);				
				
				(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
				combine.resizeGroup.invalidateDisplayList();
				event.mouseEvent.updateAfterEvent();
			}
		}
		
		private static const SNAP_DISTANCE: Number = 8;
		
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onSplitterMove(event: MouseEvent): void {
			
			if(!currentSplitter) return;
			
			var combinePoint: Point = combine.globalToLocal(new Point(event.stageX, event.stageY));
			
			var smallerElement: ICombineElement = affectedElements[0] as ICombineElement;
			
			if(currentSplitter.corner.verticalLine){
				
				var newReducedX: Number = (combinePoint.x)*(100/combine.width);
				var tileSide: String = verticalAlignedElements[smallerElement];
				var widthDisplaced: Number = (tileSide == TileSide.RIGHT_SIDE) ?
												(newReducedX - elementDimension.right):
												(newReducedX - elementDimension.left);
												
				// Allowed resizing in one direction(i,e. towards the splitting element)
				if(tileSide == TileSide.RIGHT_SIDE  && (newReducedX-elementDimension.right) < 0) return;
				if(tileSide == TileSide.LEFT_SIDE  && (newReducedX-elementDimension.left) > 0) return;
				
				var deltaXTransform: Number = (tileSide == TileSide.RIGHT_SIDE) ?
															(newReducedX - (smallerElement.reducedX+smallerElement.reducedWidth)):
															(newReducedX - smallerElement.reducedX);
				//Snapping
				if(Math.abs(widthDisplaced) > SNAP_DISTANCE){
					
					splitterSnapped = true;
					//deltaXTransform = (elementDimension.width+splitElementDimension.width) - smallerElement.reducedWidth;
					//direction
					//deltaXTransform *= (tileSide == TileSide.RIGHT_SIDE ? 1 : -1);
                    smallerElement.reducedX = Math.min(elementDimension.x, splitElementDimension.x);
                    smallerElement.reducedWidth = elementDimension.width + splitElementDimension.width;
				}
				else{
					splitterSnapped = false;
					currentSplitter.reducedX = newReducedX;
				    performXTransformationOnElements(deltaXTransform, affectedElements);
				}

				(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
				combine.resizeGroup.invalidateDisplayList();
				event.updateAfterEvent();
			}
			else if(currentSplitter.corner.horizontalLine){
				
				var newReducedY: Number = (combinePoint.y)*(100/combine.height);
				tileSide = horizontalAlignedElements[smallerElement];
				widthDisplaced = (tileSide == TileSide.BOTTOM_SIDE) ?
												(newReducedY - elementDimension.bottom):
												(newReducedY - elementDimension.top);
				
				// Allowed resizing in one direction(i,e. towards the splitting element)
				if(tileSide == TileSide.BOTTOM_SIDE  && (newReducedY-elementDimension.bottom) < 0) return;
				if(tileSide == TileSide.TOP_SIDE  && (newReducedY-elementDimension.top) > 0) return;
				
				var deltaYTransform: Number = (tileSide == TileSide.BOTTOM_SIDE) ?
												(newReducedY - (smallerElement.reducedY+smallerElement.reducedHeight)):
												(newReducedY - smallerElement.reducedY);
				//Snapping
				if(Math.abs(widthDisplaced) > SNAP_DISTANCE){
					
					splitterSnapped = true;
					//deltaYTransform = (elementDimension.height+splitElementDimension.height) - smallerElement.reducedHeight;
					//direction
					//deltaYTransform *= (tileSide == TileSide.BOTTOM_SIDE ? 1 : -1);
                    smallerElement.reducedY = Math.min(elementDimension.y, splitElementDimension.y);
                    smallerElement.reducedHeight = elementDimension.height + splitElementDimension.height;
				}
				else{
					splitterSnapped = false;	
					currentSplitter.reducedY = newReducedY;
				    performYTransformationOnElements(deltaYTransform, affectedElements);
				}

				(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
				combine.resizeGroup.invalidateDisplayList();
				event.updateAfterEvent();
			}
			
			currentSplitter.visible = !splitterSnapped;
			event.stopImmediatePropagation();	
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onSplitterDragEnd(event: MouseEvent): void {
		
			if(!currentSplitter) return;
			
				
			if(!splitterSnapped){
				
				var corner: Corner = currentSplitter.corner;
				var element: ICombineElement = affectedElements[0] as ICombineElement;
				var splitElement: ICombineElement = corner.elements[0] == element ? 
													corner.elements[1] as ICombineElement : 
													corner.elements[0] as ICombineElement;
					
				applyDimension(element, elementDimension);
				applyDimension(splitElement, splitElementDimension);
			}
			
			//clean up
			onMouseUp(event);
			EventUtils.removeListener(event, onSplitterDragEnd);
		}
		
		/**
		 * 
		 * @param element
		 * @return 
		 * 
		 */
		private function getBoundsRectangle(element: ICombineElement): Rectangle{
			
			return new Rectangle(element.reducedX, element.reducedY, element.reducedWidth, element.reducedHeight); 
		}
		
		/**
		 * 
		 * @param element
		 * @param dimension
		 * 
		 */
		private function applyDimension(element: ICombineElement, dimension: Rectangle): void{
			
			element.reducedX = dimension.left;
			element.reducedY = dimension.top;
			element.reducedWidth = dimension.width;
			element.reducedHeight = dimension.height; 
		}
		
		private var currentGutterLane: GutterLane;
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onGutterLineDragStart(event: StartDraggingEvent): void {
		
			var gutter: GutterLane = (event.target as GutterLane);
			
			if(gutter){
				
				currentGutterLane = gutter;
				currentGutterLane.selected = true;
				combine.stage.addEventListener(MouseEvent.MOUSE_MOVE, onGutterLineMove);
				combine.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				
				verticalLine = gutter.line.width == 1 ? gutter.line : null;
				horizontalLine = gutter.line.height == 1 ? gutter.line : null;
				affectedElements = findAffectedElements(null);
				
				var deltaXTransform: Number = event.deltaX*(100/combine.width);
				var deltaYTransform: Number = event.deltaY*(100/combine.height);
				
				if(verticalLine){
					var transformationApplied: Boolean = 
							performXTransformationOnElements(deltaXTransform, affectedElements);
					
					if(transformationApplied){
						currentGutterLane.reducedX += deltaXTransform;
						//currentGutterLane.line.x += deltaXTransform;
					}
				}
				
				if(horizontalLine){
					transformationApplied = 
						performYTransformationOnElements(deltaYTransform, affectedElements);
					
					if(transformationApplied){
						currentGutterLane.reducedY += deltaYTransform;
						//currentGutterLane.line.y += deltaYTransform;
					}
				}
				
				resizeHandles.forEach(removeCombineElement);
				gutterLanes.forEach(hideAllOtherGutterLines);
				//splitterHandles.forEach(removeCombineElement);
				
				(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
				combine.resizeGroup.invalidateDisplayList();
				event.mouseEvent.updateAfterEvent();
			}
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onGutterLineMove(event: MouseEvent): void {
			
			if(!currentGutterLane) return;
			
			var combinePoint: Point = combine.globalToLocal(new Point(event.stageX, event.stageY));
			
			if(verticalLine){
				
				var newReducedX: Number = (combinePoint.x)*(100/combine.width);
				newReducedX = snapHorizontal(newReducedX);
				
				var deltaXTransform: Number = newReducedX - currentGutterLane.reducedX;
				var transformationApplied: Boolean = 
					performXTransformationOnElements(deltaXTransform, affectedElements);
				
				if(transformationApplied){
					currentGutterLane.reducedX += deltaXTransform;
					//currentGutterLane.line.x += deltaXTransform;
					
					(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
					combine.resizeGroup.invalidateDisplayList();
					event.updateAfterEvent();
				}
			}
			
			if(horizontalLine){
				
				var newReducedY: Number = (combinePoint.y)*(100/combine.height);
				newReducedY = snapVertical(newReducedY);
				
				var deltaYTransform: Number = newReducedY - currentGutterLane.reducedY;
				transformationApplied = 
					performYTransformationOnElements(deltaYTransform, affectedElements);
				
				if(transformationApplied){
					currentGutterLane.reducedY += deltaYTransform;
					//currentGutterLane.line.y += deltaYTransform;
					
					(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
					combine.resizeGroup.invalidateDisplayList();
					event.updateAfterEvent();
				}
			}
			
			event.stopImmediatePropagation();	
		}
		
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onMouseMove(event: MouseEvent): void {
			
			if(!currentHandle) return;
			
			var combinePoint: Point = combine.globalToLocal(new Point(event.stageX, event.stageY));
			var newReducedX: Number = (combinePoint.x)*(100/combine.width);
			var newReducedY: Number = (combinePoint.y)*(100/combine.height);
			
			newReducedX = snapHorizontal(newReducedX);
			newReducedY = snapVertical(newReducedY);
			
			var deltaXTransform: Number = newReducedX - currentHandle.reducedX;
			var deltaYTransform: Number = newReducedY - currentHandle.reducedY;
			
			var transformationApplied: Boolean = 
				performXTransformationOnElements(deltaXTransform, affectedElements);
			
			if(transformationApplied){
				currentHandle.reducedX += deltaXTransform;
				currentHandle.corner.x += deltaXTransform;
				
				(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
				combine.resizeGroup.invalidateDisplayList();
				event.updateAfterEvent();
			}
			
			transformationApplied = 
				performYTransformationOnElements(deltaYTransform, affectedElements);
			if(transformationApplied){
				currentHandle.reducedY += deltaYTransform;
				currentHandle.corner.y += deltaYTransform;
				
				(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
				combine.resizeGroup.invalidateDisplayList();
				event.updateAfterEvent();
			}
			
			event.stopImmediatePropagation();
		}
		
		/**
		 * Clean up - remove event listeners, clear context
		 * @param event
		 * 
		 */
		private function onMouseUp(event: MouseEvent): void {
			
			currentHandle = null;
			affectedElements = null;
			
			if(currentGutterLane)
				currentGutterLane.selected = false;
			
			currentSplitter = null;
			currentGutterLane = null;
			verticalAlignedElements = null;
			horizontalAlignedElements = null;
			
			combine.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);   
			combine.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			combine.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onSplitterMove);
			combine.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onGutterLineMove);
			
			combine.resizeGroup.removeAllElements();
			(combine as UIComponent).invalidateProperties();
			(combine as SkinnableContainer).contentGroup.invalidateDisplayList();
		}
		
		
		//--------------------------------------------------------------------------
		//
		//  Private methods
		//
		//--------------------------------------------------------------------------		
		
		private var dividingVerticalLines: Dictionary;
		private var dividingHorizontalLines: Dictionary;
		/**
		 * 
		 * @param corner
		 * 
		 */
		private function buildGutter(handle: IResizeHandle): void{

			var corner: Corner = handle.corner;

			var key: String;
            var vLine: Rectangle;
            var hLine: Rectangle

            if( handle.orientation == HandleOrientation.HORIZONTAL_SPLIT &&
                    corner.elements[0].roundedY == 0 && corner.elements[1].roundedY == 0 &&
                    corner.elements[0].roundedBottom == 100 && corner.elements[1].roundedBottom == 100){

                vLine = new Rectangle(corner.x, 0, 1, 100);
            }
            else if(handle.orientation == HandleOrientation.VERTICAL_SPLIT &&
                    corner.elements[0].roundedX == 0 && corner.elements[1].roundedX == 0 &&
                    corner.elements[0].roundedRight == 100 && corner.elements[1].roundedRight == 100){

                hLine = new Rectangle(0, corner.y, 100, 1);
            }
            else if( handle.orientation != HandleOrientation.HORIZONTAL_SPLIT &&
                    handle.orientation != HandleOrientation.VERTICAL_SPLIT )
            {

                var left: Number = 0;
                var right: Number = 100;
                var top: Number = 0;
                var bottom: Number = 100;

                switch(handle.orientation){
                    case HandleOrientation.RIGHT_SPLIT:
                        left = corner.x;
                        break;
                    case HandleOrientation.LEFT_SPLIT:
                        right = corner.x;
                        break;
                    case HandleOrientation.BOTTOM_SPLIT:
                        top = corner.y;
                        break;
                    case HandleOrientation.TOP_SPLIT:
                        bottom = corner.y;
                        break;
                }

                var bounds:Rectangle = getLargerBoundingRectangle(handle, left, right, top, bottom);
                vLine = new Rectangle(corner.x, bounds.top, 1, bounds.height);
                hLine = new Rectangle(bounds.left, corner.y, bounds.width, 1);
            }
			
			if(vLine)
            {
                key = normalize(vLine.left) + ":" + normalize(vLine.top) + ":" + normalize(vLine.bottom);
                if (!dividingVerticalLines[key])
                {
                    dividingVerticalLines[key] = vLine;
                }
            }

            if(hLine)
            {
                key = normalize(hLine.top) + ":" + normalize(hLine.left) + ":" + normalize(hLine.right);
                if (!dividingHorizontalLines[key])
                {
                    dividingHorizontalLines[key] = hLine;
                }
            }
		}
		
		/**
		 * TODO: OPTIMIZE - Use visualIndex to limit the handles checked during iteration
		 *  
		 * @param handle
		 * @param left
		 * @param right
		 * @param top
		 * @param bottom
		 * @return 
		 * 
		 */
		private function getLargerBoundingRectangle(fromHandle: IResizeHandle, left: Number, right: Number, top: Number, bottom: Number): Rectangle{
			
			var corner: Corner = fromHandle.corner;
			for each( var handle: IResizeHandle in resizeHandles ){
				
				if(handle.corner.elements.length > 2  && handle != fromHandle){
					
					if(handle.corner.roundedY == corner.roundedY){
						
						if(handle.corner.x < corner.x && handle.orientation == HandleOrientation.RIGHT_SPLIT)
							left = Math.max(left, handle.corner.x);
						else if(handle.corner.x > corner.x && handle.orientation == HandleOrientation.LEFT_SPLIT)
							right = Math.min(right, handle.corner.x);
					}
					else if(handle.corner.roundedX == corner.roundedX){
						
						if(handle.corner.y < corner.y && handle.orientation == HandleOrientation.BOTTOM_SPLIT)
							top = Math.max(top, handle.corner.y);
						else if(handle.corner.y > corner.y && handle.orientation == HandleOrientation.TOP_SPLIT)
							bottom = Math.min(bottom, handle.corner.y);
					}
				}
			}
			return new Rectangle(left, top, right-left, bottom-top);		
		}
		
		private function snapVertical(newReducedY: Number): Number {
			
			if(!horizontalLine)
				return newReducedY;
			
			for each(var corner: Corner in cornersDictionary){
				
				if(corner.y != horizontalLine.top){// && (!currentHandle || corner != currentHandle.corner)){
					
					if(newReducedY < corner.y && (newReducedY + SNAP_PROXIMITY) > corner.y){
						
						return corner.y;
					}else if(newReducedY > corner.y &&  (newReducedY - SNAP_PROXIMITY) < corner.y){
						
						return corner.y;
					}
				}
			}
			return newReducedY;
		}
		
		private function snapHorizontal(newReducedX: Number): Number {
			
			if(!verticalLine)
				return newReducedX;
			
			for each(var corner: Corner in cornersDictionary){
				
				if(corner.x != verticalLine.left){// && (!currentHandle || corner != currentHandle.corner)){
					
					if(newReducedX < corner.x && (newReducedX + SNAP_PROXIMITY) > corner.x){
						
						return corner.x;
					}else if(newReducedX > corner.x &&  (newReducedX - SNAP_PROXIMITY) < corner.x){
						
						return corner.x;
					}
				}
			}
			return newReducedX;
		}
		
		/**
		 * Hides the rest of the resizing handles
		 */ 
		private function getAllHandles(): Array {
			
			var handles: Array = [];
			for( var i: int = 0; i< combine.resizeGroup.numElements; ++i) {
				
				var element: IResizeHandle = combine.resizeGroup.getElementAt(i) as IResizeHandle;
				if(element)
					handles.push(element);	
			}
			return handles;
		}
		
		/**
		 * Removes the handle
		 */ 
		private function removeCombineElement(handle: ICombineElement, index:int, arr:Array): void {
			
			if(handle) 
				combine.resizeGroup.removeElement(handle);
		}
		
		/**
		 * Hides the rest of the resizing handles
		 */ 
		private function showAllOtherHandles(handle: IResizeHandle, index:int, arr:Array): void {
			
			if(handle != currentHandle) 
				handle.visible = true;
		}
		
		/**
		 * Hides the rest of the resizing handles
		 */ 
		private function hideAllOtherHandles(handle: IResizeHandle, index:int, arr:Array): void {
			
			if(handle != currentHandle) 
				handle.visible = false;
		}
		
		/**
		 * Hides the rest of the gutter lines
		 */ 
		private function hideAllOtherGutterLines(gutter: GutterLane, index:int, arr:Array): void {
			
			if(gutter != currentGutterLane) 
				gutter.visible = false;
		}
		
		/**
		 * Hides the rest of the gutter lines
		 */ 
		private function hideAllOtherSplitters(splitter: IResizeHandle, index:int, arr:Array): void {
			
			if(splitter != currentSplitter) 
				splitter.visible = false;
		}
		
		private var verticalLine: Rectangle;
		private var horizontalLine: Rectangle;
		/**
		 * Finds the vertical and horizontal range along which elements will be re-sized
		 * @param corner
		 * 
		 */
		private function determineVerticalAndHorizontalDividingLine(handle: IResizeHandle): void {
			
			var corner: Corner = handle.corner;
		
			if(corner.elements.length == 2){
				
				var element: ICombineElement = corner.elements[0] as ICombineElement;
				if(handle.orientation == HandleOrientation.VERTICAL_SPLIT)
					horizontalLine = 
						new Rectangle(element.reducedX, corner.y, element.reducedWidth, 1);	
				else if(handle.orientation == HandleOrientation.HORIZONTAL_SPLIT)
					verticalLine = 
						new Rectangle(corner.x, element.reducedY, 1, element.reducedHeight);	
			}
			else {
				// 3 or 4 elements
				var left: Number = 0;
				var right: Number = 100;
				var top: Number = 0;
				var bottom: Number = 100;
				
				switch(handle.orientation){
					case HandleOrientation.RIGHT_SPLIT:			
						left = corner.x;
						break;
					case HandleOrientation.LEFT_SPLIT:
						right = corner.x;
						break;
					case HandleOrientation.BOTTOM_SPLIT:
						top = corner.y;
						break;
					case HandleOrientation.TOP_SPLIT:
						bottom = corner.y;
						break;
				}
				
				var bounds: Rectangle = getConservativeBoundingRectangle(corner, left, right, top, bottom);
				
				verticalLine = new Rectangle(corner.x, bounds.top, 1, bounds.height);	  
				horizontalLine = new Rectangle(bounds.left, corner.y, bounds.width, 1);	
				//traceUtil("left - " + bounds.left + " right - " + bounds.right + " top - " + bounds.top + " bottom - " + bounds.bottom);
			}
		}
		
		/**
		 * This method returns the rectangle outside of which no tiles will be affected by resizing.
		 * Needless to say all elements within the bounding rectangle are subjected to be affected by resizing. 
		 */
		private function getConservativeBoundingRectangle(corner: Corner, left: Number, right: Number, top: Number, bottom: Number): Rectangle{
			
			for each( var handle: IResizeHandle in resizeHandles ){
				
				if(handle.corner.elements.length > 2 && handle != currentHandle){
					
					if(handle.corner.roundedY == corner.roundedY){
						
						if(handle.corner.x < corner.x && 
							(handle.orientation == HandleOrientation.FOUR_WAY_SPLIT || handle.orientation == HandleOrientation.RIGHT_SPLIT))
							left = Math.max(left, handle.corner.x);
						else if(handle.corner.x > corner.x &&
							(handle.orientation == HandleOrientation.FOUR_WAY_SPLIT || handle.orientation == HandleOrientation.LEFT_SPLIT))
							right = Math.min(right,handle.corner.x);
					}
					else if(handle.corner.roundedX == corner.roundedX){
						
						if(handle.corner.y < corner.y && 
							(handle.orientation == HandleOrientation.FOUR_WAY_SPLIT || handle.orientation == HandleOrientation.BOTTOM_SPLIT))
							top = Math.max(top, handle.corner.y);
						else if(handle.corner.y > corner.y &&
							(handle.orientation == HandleOrientation.FOUR_WAY_SPLIT || handle.orientation == HandleOrientation.TOP_SPLIT))
							bottom = Math.min(bottom,handle.corner.y);
					}
				}
			}
			return new Rectangle(left, top, right-left, bottom-top);
		}
		
		/**
		 * Corners on the perimeter of the combine are ignored.
		 * 
		 * @param x
		 * @param y
		 * @return 
		 * 
		 */
		private function getCornerAt(x: Number, y: Number): Corner {
			
			if(x==0 || y==0 || Math.ceil(x)==100 || Math.ceil(y)==100)
				return null;
			
			var key: String = normalize(x)+":"+normalize(y);
			if(cornersDictionary[key] == null){
				
				//traceUtil("Corner - " + key);
				cornersDictionary[key] = new Corner(x,y);
			}
			return cornersDictionary[key];
		}
		
		/**
		 * Edges on the perimeter of the combine are ignored.
		 * 
		 * @param x
		 * @param y
		 * @return 
		 * 
		 */
		private function getEdgeAt(x: Number, y: Number): Corner {
			
			if(x==0 || y==0 || Math.ceil(x)==100 || Math.ceil(y)==100)
				return null;   
			
			var key: String = normalize(x)+":"+normalize(y);
			if(edgesDictionary[key] == null){
				
				traceUtil("Edge - " + key);
				edgesDictionary[key] = new Corner(x,y);
			}
			return edgesDictionary[key];
		}
		
		/**
		 * 
		 * @param element
		 * @param side
		 * @return 
		 * 
		 */
		private function createSplittersOnSide(element: ICombineElement, side: String): void {
			
			var line: Rectangle;
			var cornerX: Number ;
			var cornerY: Number;
			
			switch(side){
				case TileSide.TOP_SIDE:
					line = new Rectangle(element.reducedX, element.reducedY, element.reducedWidth, 1);
					break;
				
				case TileSide.BOTTOM_SIDE:
					line = new Rectangle(element.reducedX, element.reducedY+element.reducedHeight, element.reducedWidth, 1);
					break;
				
				case TileSide.LEFT_SIDE:
					line = new Rectangle(element.reducedX, element.reducedY, 1, element.reducedHeight);
					break;
				
				case TileSide.RIGHT_SIDE:
					line = new Rectangle(element.reducedX+element.reducedWidth, element.reducedY, 1, element.reducedHeight);
					break;
			}

			var key: String;
			var horizontalLine: Boolean = (line.height == 1);
			var verticalLine: Boolean = (line.width == 1);
			
			for( var i: int =0; i<tiles.length; ++i){
				
				var adjacentElement: ICombineElement = tiles[i] as ICombineElement;
				if(!adjacentElement || adjacentElement == element) continue;
				
				if(verticalLine && adjacentElement.reducedHeight >= element.reducedHeight ) continue;
				if(horizontalLine && adjacentElement.reducedWidth >= element.reducedWidth ) continue;
				
				//If the element is not on the same left or right perimeter do not consider
				if(horizontalLine && 
					(normalize(adjacentElement.reducedX) != normalize(line.left) &&
					 normalize(adjacentElement.reducedX+adjacentElement.reducedWidth) != normalize(line.right)) ) continue;
				//If the element is not on the same top or bottom perimeter do not consider
				if(verticalLine && 
					(normalize(adjacentElement.reducedY) != normalize(line.top) &&
					 normalize(adjacentElement.reducedY+adjacentElement.reducedHeight) != normalize(line.bottom)) ) continue;
				
				var onSide: String = verticalLine ? 
										isElementOnVerticalLine(adjacentElement, line) : isElementOnHorizontalLine(adjacentElement, line);
				
				if(onSide != null){
					
					cornerY = line.top;
					cornerX = line.left;
					cornerY = verticalLine ? adjacentElement.reducedY+adjacentElement.reducedHeight/2 : cornerY;
					cornerX = horizontalLine ? adjacentElement.reducedX+adjacentElement.reducedWidth/2 : cornerX;
					
					key = normalize(cornerX)+":"+normalize(cornerY);
					var corner: Corner = new Corner(cornerX,cornerY);
					corner.verticalLine = verticalLine ? line : null;
					corner.horizontalLine = horizontalLine ? line : null;
					corner.elements.push(element);
					corner.elements.push(adjacentElement);

					traceUtil("Splitter - " + key);
					
					var handle: IResizeHandle = new TileResizeHandle(corner);
					handle.reducedX = corner.x;
					handle.reducedY = corner.y;
					handle.orientation = 
						horizontalLine ? HandleOrientation.VERTICAL_SPLIT : HandleOrientation.HORIZONTAL_SPLIT;
					StartDraggingStrategy.dragWatch(handle, 0);
					handle.addEventListener(StartDraggingEvent.START_DRAGGING, onSplitterDragStart);
					splitterHandles.push(handle);
				}
				
			}			
		}
		
		
		/**
		 * 
		 * @param element
		 * 
		 */
		private function createSplitters(element: ICombineElement): void{
			
			//left edge
			createSplittersOnSide(element, TileSide.LEFT_SIDE);

			
			//right edge
			createSplittersOnSide(element, TileSide.RIGHT_SIDE);
			
			//top edge
			createSplittersOnSide(element, TileSide.TOP_SIDE);

			
			//bottom edge
			createSplittersOnSide(element, TileSide.BOTTOM_SIDE);

		}
		
		/**
		 * 
		 * @param element
		 * 
		 */
		private function createOrUpdateCornersFor(element: ICombineElement): void{
			
			//top left corner
			var corner: Corner = getCornerAt(element.reducedX, element.reducedY);
			if(corner)
				corner.elements.push(element);
			
			//top right corner
			corner = getCornerAt(element.reducedX+element.reducedWidth, element.reducedY);
			if(corner)
				corner.elements.push(element);
			
			//bottom left corner
			corner = getCornerAt(element.reducedX, element.reducedY+element.reducedHeight);
			if(corner)
				corner.elements.push(element);
			
			//bottom right
			corner = getCornerAt(element.reducedX+element.reducedWidth, element.reducedY+element.reducedHeight);
			if(corner)
				corner.elements.push(element);
			}
		
		/**
		 * 
		 * @param element
		 * 
		 */
		private function createOrUpdateVerticalOrHorizontalEdge(element: ICombineElement): void{
			
			//left edge
			var corner: Corner = getEdgeAt(element.reducedX, element.reducedY+(element.reducedHeight/2));
			if(corner)
				corner.elements.push(element);
			
			//right edge
			corner = getEdgeAt(element.reducedX+element.reducedWidth, element.reducedY+(element.reducedHeight/2));
			if(corner)
				corner.elements.push(element);
			
			//top edge
			corner = getEdgeAt(element.reducedX+(element.reducedWidth/2), element.reducedY);
			if(corner)
				corner.elements.push(element);
			
			//bottom edge
			corner = getEdgeAt(element.reducedX+(element.reducedWidth/2), element.reducedY+element.reducedHeight);
			if(corner)
				corner.elements.push(element);
		}
		
		
		/**
		 * 
		 * 
		 * 
		 * TODO: Re-use handles instead of creating them 
		 * @param corner
		 * @return 
		 * 
		 */
		private function getCornerResizeHandle(corner: Corner): IResizeHandle {
			
			var handle: IResizeHandle = new TileResizeHandle(corner);
			handle.reducedX = corner.x;
			handle.reducedY = corner.y;
			
			handle.addEventListener(StartDraggingEvent.START_DRAGGING, onStartDragging);
			StartDraggingStrategy.dragWatch(handle, 0);
			
			
			var cornerX: Number = normalize(corner.x);
			var cornerY: Number = normalize(corner.y);
			
			for each( var element: ICombineElement in corner.elements ){

				var reducedLeft: Number = normalize(element.reducedX);//Math.ceil(element.reducedX);
				var reducedTop: Number = normalize(element.reducedY); //Math.ceil(element.reducedY);
				var reducedBottom: Number = normalize(element.reducedY+element.reducedHeight);//Math.ceil(element.reducedHeight);
				var reducedRight: Number = normalize(element.reducedX+element.reducedWidth);//Math.ceil(element.reducedHeight);

				if(reducedLeft == cornerX && reducedTop == cornerY){
					
					handle.adjoiningTiles[TileSide.BOTTOM_RIGHT] = element;	    	   
				}
				else if(reducedRight == cornerX && reducedTop == cornerY){
					
					handle.adjoiningTiles[TileSide.BOTTOM_LEFT] = element;	    
				}
				else if(reducedRight == cornerX && reducedBottom == cornerY){
					
					handle.adjoiningTiles[TileSide.TOP_LEFT] = element;	   
				}
				else if(reducedLeft == cornerX && reducedBottom == cornerY){
					handle.adjoiningTiles[TileSide.TOP_RIGHT] = element;   
				}
				else{
					//traceUtil("getResizeHandle: FAIL");
				}
			}
			if(corner.elements.length == 2 ){
				// This is a case of T junction.
				// We need to find the element whose edge is meeting the 2 corners
				completeTheCorner(corner, handle);
			}
			handle.orientation = getOrientation(handle);
			
			return handle;
		}
		
		/**
		 * 
		 * @param corner
		 * @return 
		 * 
		 */
		private function getHorizontalOrVerticalResizeHandle(corner: Corner): IResizeHandle {
			
			var handle: IResizeHandle = new TileResizeHandle(corner);
			handle.reducedX = corner.x;
			handle.reducedY = corner.y;
			
			handle.addEventListener(StartDraggingEvent.START_DRAGGING, onStartDragging);
			StartDraggingStrategy.dragWatch(handle, 0);
			
			
			var cornerX: Number = normalize(corner.x);
			var cornerY: Number = normalize(corner.y);
			
			for each( var element: ICombineElement in corner.elements ){
				
				var midHeight: Number = normalize(element.reducedY+element.reducedHeight/2);
				var midWidth: Number = normalize(element.reducedX+element.reducedWidth/2); 
				var reducedX: Number = normalize(element.reducedX);
				var reducedY: Number = normalize(element.reducedY);
				var reducedBottom: Number = normalize(element.reducedY+element.reducedHeight);
				var reducedRight: Number = normalize(element.reducedX+element.reducedWidth); 
				
				if(reducedBottom == cornerY && midWidth == cornerX){
					
					handle.adjoiningTiles[TileSide.TOP_LEFT] = element;	    	   
				}
				else if( reducedY == cornerY && midWidth == cornerX){
					
					handle.adjoiningTiles[TileSide.BOTTOM_LEFT] = element;	    
				}
				else if(reducedRight == cornerX && midHeight == cornerY){
					
					handle.adjoiningTiles[TileSide.TOP_LEFT] = element;	   
				}
				else if(reducedX == cornerX && midHeight == cornerY){
					handle.adjoiningTiles[TileSide.TOP_RIGHT] = element;   
				}
				else{
					//traceUtil("getResizeHandle: FAIL");
				}
			}
			handle.orientation = getOrientation(handle);
			
			return handle;
		}
		
		/**
		 * Finds the third element whose edge sits on this corner and adds it to the handle's
		 * adjoining dictionary. 
		 * @param corner
		 * @param handle
		 * 
		 */
		private function completeTheCorner(corner: Corner, handle: IResizeHandle): void {
			
			var found: Boolean;
			for(var i: int = 0; i<tiles.length; ++i) {
				
				var element: ICombineElement = tiles[i] as ICombineElement;
				if(!element) continue;
				if(corner.elements.indexOf(element) >= 0) continue;
				
				var side: String = isPointOnElment(corner.x, corner.y, element);  
				if(side){
					
					handle.adjoiningTiles[side] = element;   
					corner.elements.push(element);
					found = true;
					break;
				}
			}
			if(!found)
				traceUtil("completeTheCorner: FAIL");
		}
		
		/**
		 * Determines the element which has the point represented by x and y on its boundary.
		 * 
		 * @param x
		 * @param y
		 * @param element
		 * @return The side of the element which contains the point
		 * 
		 */
		private function isPointOnElment(x:Number, y:Number, element: ICombineElement): String{
			
			x = normalize(x);
			y = normalize(y);
			var reducedX: Number = normalize(element.reducedX);//Math.ceil(element.reducedX);
			var reducedY: Number = normalize(element.reducedY); //Math.ceil(element.reducedY);
			var reducedWidth: Number = normalize(element.reducedWidth);//Math.ceil(element.reducedWidth);
			var reducedHeight: Number = normalize(element.reducedHeight);
			var reducedBottom: Number = normalize(element.reducedY+element.reducedHeight);//Math.ceil(element.reducedHeight);
			var reducedRight: Number = normalize(element.reducedX+element.reducedWidth);//Math.ceil(element.reducedHeight);
			
			if(reducedX == x && 
				testInRange(y, reducedY, reducedHeight) ){
				
				return TileSide.TOP_RIGHT;	    	
			}
			else if(reducedY == y && 
				testInRange(x, reducedX, reducedWidth) ){
				
				return TileSide.BOTTOM_SIDE;	    
			}
			else if(reducedBottom == y &&
				testInRange(x, reducedX, reducedWidth) ){
				
				return TileSide.TOP_LEFT;	   
			}
			else if(reducedRight == x && 
				testInRange(y, reducedY, reducedHeight) ){
				
				return TileSide.TOP_LEFT;   
			}
			return null;
		}
		
		/**
		 * Maps the affected elements to the side they are aligned to the dividing horizontal line 
		 */
		private var horizontalAlignedElements: Dictionary;
		/**
		 * Maps the affected elements to the side they are aligned to the dividing vertical line 
		 */
		private var verticalAlignedElements: Dictionary;
		
		/**
		 * Find all the tiles which will be affected(resized and re-laid out) if user
		 * starts dragging the handle. 
		 * @param handle
		 * @return 
		 * 
		 */
		private function findAffectedElements(handle: IResizeHandle): Array {
			
			var elements: Array = [];
			horizontalAlignedElements = new Dictionary(true);
			verticalAlignedElements = new Dictionary(true);
			var element: ICombineElement;
			
			if( handle && handle.corner.elements.length == 2){
				
				elements = handle.corner.elements;	
				
				if(handle.orientation == HandleOrientation.VERTICAL_SPLIT)
				{
					element = elements[0];
					var side: String = isElementOnHorizontalLine(element, horizontalLine);
					horizontalAlignedElements[element] = side;
					element = elements[1];
					side = isElementOnHorizontalLine(element, horizontalLine);
					horizontalAlignedElements[element] = side;
						
				}
				else{
					element = elements[0];
					side = isElementOnVerticalLine(element, verticalLine);
					verticalAlignedElements[element] = side;
					element = elements[1];
					side = isElementOnVerticalLine(element, verticalLine);
					verticalAlignedElements[element] = side;
				}
			}
			else
			{
			
				for(var i: int = 0; i<tiles.length; ++i) {
					
					element = tiles[i] as ICombineElement;
					if(element){
						
						var horizontalSide: String = isElementOnHorizontalLine(element, horizontalLine);
						var verticalSide: String = isElementOnVerticalLine(element, verticalLine);
						if(horizontalSide)
							horizontalAlignedElements[element] = horizontalSide;
						if(verticalSide)
							verticalAlignedElements[element] = verticalSide;
						
						if(horizontalSide || verticalSide)
							elements.push(element);
					}
				}
			}
			return elements;
		}
		
		/**
		 * Check if after applying delta transformations the minimum width and height is respected.
		 * If it is not then revert back transformations and return.
		 * 
		 * 
		 * @param deltaXTransform
		 * @param deltaYTransform
		 * @param elements
		 * @return true if tansformation was applied
		 * 
		 */
		private function performXTransformationOnElements(deltaXTransform: Number, elements: Array): Boolean{
			
			var minimumDimensionBreached: Boolean = false;
			var transformedElements: Array = [];
			
			for each(var element: ICombineElement in elements) {
				
				if(element){
					
					transformedElements.push(element);
					var verticalSide: String = verticalAlignedElements[element];
					
					if(verticalSide){
						if(verticalSide == TileSide.RIGHT_SIDE){
							element.reducedWidth += deltaXTransform;			
						}else {//if(side == TileSide.LEFT_SIDE){
							element.reducedWidth -= deltaXTransform;
							element.reducedX += deltaXTransform;
						}	
					}
					if(element.reducedWidth < MINIMUM_WIDTH){
						minimumDimensionBreached = true;
						break;
					}
				}
			}
			
			if(minimumDimensionBreached){
			
				for each(element in transformedElements){
					
					verticalSide = verticalAlignedElements[element];
					
					if(verticalSide){
						if(verticalSide == TileSide.RIGHT_SIDE){
							element.reducedWidth -= deltaXTransform;			
						}else {//if(side == TileSide.LEFT_SIDE){
							element.reducedWidth += deltaXTransform;
							element.reducedX -= deltaXTransform;
						}	
					}	
				}
			}
			
			return !minimumDimensionBreached;
		}
		
		private function performYTransformationOnElements(deltaYTransform: Number, elements: Array): Boolean{
			
			var minimumDimensionBreached: Boolean = false;
			var transformedElements: Array = [];
			
			for each(var element: ICombineElement in elements) {
				
				if(element){
					
					transformedElements.push(element);
					var horizontalSide: String = horizontalAlignedElements[element];
					
					if(horizontalSide){
						if(horizontalSide == TileSide.BOTTOM_SIDE){
							element.reducedHeight += deltaYTransform;			
						}else {//if(side == TileSide.TOP_SIDE){
							element.reducedHeight -= deltaYTransform;
							element.reducedY += deltaYTransform;
						}
					}
					if(element.reducedHeight < MINIMUM_HEIGHT){
						minimumDimensionBreached = true;
						break;
					}
				}
			}
			
			if(minimumDimensionBreached){
			
				for each(element in transformedElements){
					
					horizontalSide = horizontalAlignedElements[element];
					
					if(horizontalSide){
						if(horizontalSide == TileSide.BOTTOM_SIDE){
							element.reducedHeight -= deltaYTransform;			
						}else {//if(side == TileSide.TOP_SIDE){
							element.reducedHeight += deltaYTransform;
							element.reducedY -= deltaYTransform;
						}
					}
				}
			}
			
			return !minimumDimensionBreached;
		}
		
		/**
		 * Returns the side of the element which falls on the line 
		 * @param element
		 * @param line
		 * @return 
		 * 
		 */	   
		private function isElementOnVerticalLine(element: ICombineElement, line: Rectangle): String {
			
			if(!line) return null;
			
			var bounds: Rectangle = new Rectangle(element.reducedX, element.reducedY, element.reducedWidth, element.reducedHeight);
			
			if(isPointOnLine(bounds.left, bounds.top, line) && isPointOnLine(bounds.left, bounds.bottom, line) ){
				return TileSide.LEFT_SIDE;
			}
			else if(isPointOnLine(bounds.right, bounds.top, line) && isPointOnLine(bounds.right, bounds.bottom, line) ){
				return TileSide.RIGHT_SIDE;   
			}
			return null;
		}
		
		/**
		 * Returns the side of the element which falls on the line 
		 * @param element
		 * @param line
		 * @return 
		 * 
		 */	   
		private function isElementOnHorizontalLine(element: ICombineElement, line: Rectangle): String {
			
			if(!line) return null;
			
			var bounds: Rectangle = new Rectangle(element.reducedX, element.reducedY, element.reducedWidth, element.reducedHeight);
			if(isPointOnLine(bounds.left, bounds.top, line) && isPointOnLine(bounds.right, bounds.top, line) ){
				return TileSide.TOP_SIDE;
			}
			else if(isPointOnLine(bounds.left,bounds.bottom, line) && isPointOnLine(bounds.right, bounds.bottom, line) ){
				return TileSide.BOTTOM_SIDE;   
			}
			
			return null;
		}
		
		/**
		 * 
		 * @param x
		 * @param y
		 * @param line
		 * @return 
		 * 
		 */
		private function isPointOnLine(x: Number, y: Number, line: Rectangle): Boolean{
			
			x = normalize(x);
			y = normalize(y);
			var lineX: Number = normalize(line.x);
			var lineY: Number = normalize(line.y);
			var lineBottom: Number = normalize(line.bottom);
			var lineRight: Number = normalize(line.right);
			
			if(x == lineX && testInRange(y, lineY, lineBottom-lineY) )
				return true;
			else if(y == lineY && testInRange(x, lineX, lineRight-lineX) )
				return true;
			
			return false;
		}
		
		private function testInRange(n:Number, fromN: Number, byN: Number): Boolean {
			
			return (n >= fromN && n <= fromN+byN);
		}
		
		private function getOrientation(handle: IResizeHandle): String {
			
			var corner: Corner = handle.corner;
			var topLeftTile: ICombineElement = handle.adjoiningTiles[TileSide.TOP_LEFT] as ICombineElement;
			var topRightTile: ICombineElement = handle.adjoiningTiles[TileSide.TOP_RIGHT] as ICombineElement;
			var bottomLeftTile: ICombineElement = handle.adjoiningTiles[TileSide.BOTTOM_LEFT] as ICombineElement;
			var bottomRightTile: ICombineElement = handle.adjoiningTiles[TileSide.BOTTOM_RIGHT] as ICombineElement;
			var bottomTile: ICombineElement = handle.adjoiningTiles[TileSide.BOTTOM_SIDE] as ICombineElement;
			
			//   o
			//   o o
			//   o
			if(topLeftTile && topRightTile && bottomRightTile && !bottomLeftTile){
				
				return HandleOrientation.RIGHT_SPLIT;
			}
			//    o
			//  o o
			//    o
			else if(topLeftTile && topRightTile && bottomLeftTile && !bottomRightTile){
				
				return HandleOrientation.LEFT_SPLIT;
			}
			//   o o o
			//     o
			else if(topLeftTile && !topRightTile && bottomLeftTile && bottomRightTile){
				
				return HandleOrientation.BOTTOM_SPLIT;
			}
			//    o 
			//  o o o
			else if(topLeftTile && topRightTile && bottomTile && !bottomRightTile){
				
				return HandleOrientation.TOP_SPLIT;
			}
			//  o
			//  o
			//  o
			else if(topLeftTile && topRightTile && !bottomLeftTile && !bottomRightTile && !bottomTile){
				
				return HandleOrientation.HORIZONTAL_SPLIT;	
			}
			//  o o o
			else if(topLeftTile && !topRightTile && bottomLeftTile && !bottomRightTile){
				
				return HandleOrientation.VERTICAL_SPLIT;
			}
			//    o
			//  o o o
			//    o
			else if(topLeftTile && topRightTile && bottomLeftTile && bottomRightTile){
				
				return HandleOrientation.FOUR_WAY_SPLIT;
			}
			
			return "";
		}
		
				
		/**
		 *
		 * This method approximates to the given preicision
		 *  
		 * @param n
		 * @param precision
		 * @return 
		 * 
		 */
		private function normalize(n: Number, precision: int = 6): Number{
		
			// sanity check
			n = (Math.ceil(n) == 100) ? 100 : n;
			return(Math.round(n*Math.pow(10,precision)));
		}
		
		private static const LOGENABLED: Boolean = false;
		
		private function traceUtil(msg: String, ... args): void {
			
			if(LOGENABLED)
				trace(combine.name + " --> " + msg, args);
		}
	}
}
