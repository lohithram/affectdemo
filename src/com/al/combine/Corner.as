package com.al.combine
{
	import com.al.tile.ICombineElement;
	
	import flash.geom.Rectangle;
	
	import spark.primitives.Line;
	
	public class Corner {
		
		public static const PRECISSION: int = 6;
		public var elements: Array = [];	
		
		/**
		 * Rectangle representing the line along which
		 * combine elements are resized 
		 */
		public var verticalLine: Rectangle;
		public var horizontalLine: Rectangle; 
		
		public function Corner(x: Number, y: Number) {
			
			_x = x;
			_y = y;
			
			_roundedX = Math.round(x*Math.pow(10,PRECISSION));
			_roundedY = Math.round(y*Math.pow(10,PRECISSION));
		}
		
		private var _roundedX: Number;
		public function get roundedX(): Number {
			return _roundedX;
		}
		
		private var _roundedY: Number;
		public function get roundedY(): Number {
			return _roundedY;
		}
		
		private var _x: Number;
		public function get x(): Number {
			return _x;
		}
		public function set x(value: Number): void {
			_x = value;
			_roundedX = Math.ceil(value);
		}
		
		private var _y: Number;
		public function get y(): Number {
			return _y;
		}
		public function set y(value: Number): void {
			_y = value;
			_roundedY = Math.ceil(value);
		}
		
	}

}