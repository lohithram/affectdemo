package com.al.combine
{
	import com.al.enums.TileSide;
	import com.al.tile.ICombineElement;
	import com.al.tile.ITileDropIndicator;
	
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	/**
	 * 
	 * @author lram
	 * 
	 */
	public class EmptySpaceFillStrategy
	{
		
		// the combine which is currently being managed
		private var combine: ICombine;
		
		public function EmptySpaceFillStrategy(combineContainer: ICombine){
			
			combine = combineContainer;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Public methods
		//
		//--------------------------------------------------------------------------

		/**
		 *
		 * REVISIT: To optimize, recursive within the previously determined candidates instead of 
		 * re-evaluating the candidates to fill the space..
		 * Refining the algo - Give those candidates which can fill the space entirely the preference ? 
		 * @param emptySpace
		 * @param ignoreElement - Ignores this element from the list of candidates eligible to fill the gap
		 */
		public function consumeEmptySpace(emptySpace: Rectangle, ignoreElement: ICombineElement = null): void{

			reCalculateVisualIndices();
			// find adjoining tiles which share their full border with the empty one
			var candidates: Dictionary = findAdjoiningTiles(emptySpace);

			var sortedCandidates:Array = [];
			for (var element: * in candidates){

				if(ignoreElement != element)
					sortedCandidates.push(element);
			}
			
			if(sortedCandidates.length == 0){
				//trace("Ykes! WE HAVE A WHITE SPACE");
				//Alert.show("This is where the Algo fails to fill up all the empty space");
				return;
			}
			
			// resolve contention by ordering them according to visual Index
			// The element with the smallest visual index gets preference over others.
			if(sortedCandidates.length>1)
				sortedCandidates.sortOn("visualIndex", Array.NUMERIC);
			
			element = sortedCandidates[0];
			var isGapFilled: Boolean = fillGap(emptySpace, element, candidates[element] as String);
			if(!isGapFilled){
				consumeEmptySpace(emptySpace);		
			}
			else{
				return;
			}
		}
		
		//--------------------------------------------------------------------------
		//
		//  Private methods
		//
		//--------------------------------------------------------------------------
		
		private function findAdjoiningTiles(emptySpace: Rectangle): Dictionary {
			
			var candidatesForFillingEmptySpace: Dictionary = new Dictionary(true);
			for(var i: int = 0; i < combine.numElements; ++i){
				
				var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
				if(!element) continue;
				var side: String = sharesFullSide(emptySpace, element);
				if(side) {
					candidatesForFillingEmptySpace[element] = side;
				}
			}
			return candidatesForFillingEmptySpace;
		}
		
		/**
		 * If the testElement shares full length of one of its side with the rectangle
		 * represented emptySpace, this method returns the side of the intersection.
		 * The side is from emptySpace perspective.
		 * False otherwise
		 * @param draggedTIle
		 * @param testElement
		 * @return
		 */
		private function sharesFullSide(emptySpace: Rectangle, testElement: ICombineElement): String{
			
			// inflating is a stroke of genius ;)!
			var spaceToFill: Rectangle = emptySpace.clone();
			spaceToFill.inflate(0.1,0.1);
			
			var side: String;
			var topLeftIntersect: Boolean;
			var topRightIntersect: Boolean;
			var bottomRightIntersect: Boolean;
			var bottomLeftIntersect: Boolean;
			
			var cornersOnSharedBoundary: int=0;
			//topLeft corner of the testElement
			if(spaceToFill.contains(testElement.reducedX, testElement.reducedY)){
				topLeftIntersect = true;
				++cornersOnSharedBoundary;
			}
			//topRight corner
			if(spaceToFill.contains(testElement.reducedX+testElement.reducedWidth, testElement.reducedY)){
				topRightIntersect = true;
				++cornersOnSharedBoundary;
			}
			//bottomRight corner
			if(cornersOnSharedBoundary < 2 &&
				spaceToFill.contains(testElement.reducedX+testElement.reducedWidth, testElement.reducedY+testElement.reducedHeight)){
				bottomRightIntersect = true;
				++cornersOnSharedBoundary;
			}
			//bottomLeft corner
			if(cornersOnSharedBoundary < 2 &&
				spaceToFill.contains(testElement.reducedX, testElement.reducedY+testElement.reducedHeight)){
				bottomLeftIntersect = true;
				++cornersOnSharedBoundary;
			}
			
			//determine which edge does the element share with rectangle ?
			if(cornersOnSharedBoundary == 2){
				
				if(topLeftIntersect && topRightIntersect){
					side = TileSide.BOTTOM_SIDE;
				}else if(topLeftIntersect && bottomLeftIntersect){
					side = TileSide.RIGHT_SIDE;
				}else if(bottomLeftIntersect && bottomRightIntersect){
					side = TileSide.TOP_SIDE;
				}else{
					side = TileSide.LEFT_SIDE;
				}
			}
			return side;
		}
		
		/**
		 * Returns true if the entire space was filled
		 * @param emptySpace
		 * @param element
		 * @return
		 */
		private function fillGap(emptySpace: Rectangle, element: ICombineElement, side: String): Boolean {
			
			switch(side){
				case TileSide.TOP_SIDE:
					element.reducedHeight += emptySpace.height;
					emptySpace.width -= element.reducedWidth;
					emptySpace.x += (element.reducedX == emptySpace.x) ? element.reducedWidth : 0;
					break;
				case TileSide.BOTTOM_SIDE:
					element.reducedY = emptySpace.y;
					element.reducedHeight += emptySpace.height;
					emptySpace.width -= element.reducedWidth;
					emptySpace.x += (element.reducedX == emptySpace.x) ? element.reducedWidth : 0;
					break;
				case TileSide.LEFT_SIDE:
					element.reducedWidth += emptySpace.width;
					emptySpace.height -= element.reducedHeight;
					emptySpace.y += (element.reducedY == emptySpace.y) ? element.reducedHeight : 0;
					break;
				case TileSide.RIGHT_SIDE:
					element.reducedX = emptySpace.x;
					element.reducedWidth += emptySpace.width;
					emptySpace.height -= element.reducedHeight;
					emptySpace.y += (element.reducedY == emptySpace.y) ? element.reducedHeight : 0;
					break;
			}
			return emptySpace.width <= 0 || emptySpace.height <= 0;
		}
		
		private function reCalculateVisualIndices(): void {
			
			combine.calculateVisualIndices();
		}
		
		private function visualHierarchyFunction(elementA: ICombineElement, elementB: ICombineElement): int{
			
			if(elementA.reducedY > elementB.reducedY){
				return 1;
			}
			else if(elementA.reducedY < elementB.reducedY){
				return -1;
			}
			else{
				//both elements reducedY are equal
				if(elementA.reducedX > elementB.reducedX)
					return 1;
				else if(elementA.reducedX < elementB.reducedX)
					return -1;
				else
					return 0;
			}
		}
	}
}