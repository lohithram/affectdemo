/**
 * Created by lram on 27/02/2015.
 */
package com.al.combine
{

    import com.al.descriptors.CombineDescriptor;
    import com.al.descriptors.ICombineDescriptor;
    
    import mx.core.ClassFactory;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;
    import mx.core.IVisualElementContainer;
    
    import spark.components.Group;

    public interface ICombine extends IVisualElementContainer, IUIComponent
    {
        function get dropIndicatorFactory(): ClassFactory;

        function set combineDescriptor(value: ICombineDescriptor): void;

        function get combineDescriptor(): ICombineDescriptor;

		function get resizeGroup(): Group;
		
		function calculateVisualIndices(): Array;

        function get animate(): Boolean;

        function set animate(value: Boolean): void;
		
    }
}
