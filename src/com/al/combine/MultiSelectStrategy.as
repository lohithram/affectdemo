package com.al.combine
{
	import com.al.enums.TileSide;
	import com.al.events.StartDraggingEvent;
	import com.al.events.TileDropEvent;
    import com.al.events.TileFocusedEvent;
    import com.al.tile.ICombineElement;
	import com.al.tile.ITileDropIndicator;

    import flash.events.Event;

    import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import spark.events.ElementExistenceEvent;
	
	/**
	 * 
	 * @author lram
	 * 
	 */
	public class MultiSelectStrategy
	{
		
		// the combine which is currently being managed
		private var combine: ICombine;
		
		public function MultiSelectStrategy(combineContainer: ICombine){
			
			combine = combineContainer;
			combine.addEventListener(ElementExistenceEvent.ELEMENT_ADD, onElementAdd);
			combine.addEventListener(ElementExistenceEvent.ELEMENT_REMOVE, onElementRemove);
		}
		
		//--------------------------------------------------------------------------
		//
		//  Public methods
		//
		//--------------------------------------------------------------------------
		public function get selectedElements(): Array{
		
			var selectedList: Array = [];
			var sortedElements: Array = combine.calculateVisualIndices();
			
			for each(var element: ICombineElement in sortedElements){
				
				if(element.selected){
					selectedList.push(element);
				}
			}
			return selectedList;
		}
		
		/**
		 * Selects only this element and unselects the rest.
		 * @param element
		 * 
		 */		
		public function selectElement(elementToSelect: ICombineElement): void{
		
			for(var i: int = 0; i < combine.numElements; ++i){
				
				var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
				if(element)
					element.selected = (element == elementToSelect);
			}
			elementToSelect.dispatchEvent(new TileFocusedEvent());
		}
		
		//--------------------------------------------------------------------------
		//
		//  Private methods
		//
		//--------------------------------------------------------------------------
		private var startIndex: int = -1;
		/**
		 * 
		 * @param event
		 * 
		 */
		private function onElementAdd(event: ElementExistenceEvent): void {
			
			if(event.element is ICombineElement && !(event.element is ITileDropIndicator)){
				
				event.element.addEventListener(MouseEvent.CLICK, onClick);
			}
		}
		
		private function onElementRemove(event:ElementExistenceEvent):void {
			
			if(event.element is ICombineElement && !(event.element is ITileDropIndicator)){
				
				event.element.removeEventListener(MouseEvent.CLICK, onClick);
			}
		}
		
		private function onClick(event: MouseEvent):void {
		
			var clickedElement: ICombineElement = event.currentTarget as ICombineElement;
			if(event.commandKey){
				
				clickedElement.selected = !clickedElement.selected;	
			}
			else if(event.shiftKey){
				
				for(var i: int = 0; i < combine.numElements; ++i){
					
					var element: ICombineElement = combine.getElementAt(i) as ICombineElement;
					if(!element) continue;
					var minIndex: int = Math.min(startIndex, clickedElement.visualIndex);
					var maxIndex: int = Math.max(startIndex, clickedElement.visualIndex);
					element.selected = (element.visualIndex >= minIndex) && (element.visualIndex <= maxIndex);
				}
			}
			else{
				selectElement(clickedElement);
				startIndex = clickedElement.visualIndex;
			}
		}
	}
}
