/**
 * Created by lram on 12/02/2015.
 */
package com.al.components
{

    import com.al.model.ImageInfo;
    import themes.affect.PreviewSkin;

    import flash.events.Event;

    import flash.events.MouseEvent;

    import mx.binding.utils.BindingUtils;

    import mx.core.UIComponent;
    import mx.events.FlexEvent;

    import spark.components.Image;
    import spark.components.supportClasses.SkinnableComponent;
    import spark.core.IContentLoader;

    public class AssetPreview extends SkinnableComponent
    {

        private static const DESIRED_WIDTH: Number = 112;

        [SkinPart(required=true)]
        public var imageControl: Image;

        [SkinPart]
        public var nextControl: UIComponent;

        [SkinPart]
        public var prevControl: UIComponent;

        [Bindable]
        public var desiredWidth: Number = DESIRED_WIDTH;

        [Bindable]
        public var contentCache: IContentLoader;

        private var imageIndex: int;

        public function AssetPreview(){
        }

        //-----------------------------------------------------------------
        //
        //              Properties
        //
        //-----------------------------------------------------------------

        //-----------------------------------------------------------------
        // previewData
        //-----------------------------------------------------------------
        private var _previewData: Array;
        private var previewDataChanged: Boolean;

        [Bindable]
        public function get previewData(): Array{
            return _previewData;
        }

        public function set previewData(value: Array): void{

            if(value != _previewData){
                _previewData = value;
                previewDataChanged = true;
                invalidateProperties();
            }
        }

        //-----------------------------------------------------------------
        // imageNumber
        //-----------------------------------------------------------------
        [Bindable("imageIndexChanged")]
        public function get imageNumber(): int{
            return imageIndex+1;
        }


        //-----------------------------------------------------------------
        //
        //              Overrides
        //
        //-----------------------------------------------------------------
        override protected function childrenCreated(): void{

            BindingUtils.bindSetter(onSkinChanged, this, 'skin');
        }

        override protected function commitProperties(): void{

            super.commitProperties();
            if(previewDataChanged){

                imageIndex = (previewData && previewData.length > 0) ? 0 : -1;
                dispatchEvent(new Event("imageIndexChanged"));
                updateImageSource();
                updateImageControlsVisibility();
                previewDataChanged = false;
            }
        }

        override protected function partAdded(partName: String, instance: Object): void{

            if(instance == nextControl){

                nextControl.addEventListener(MouseEvent.CLICK, onNext);
            }
            else if(instance == prevControl){

                prevControl.addEventListener(MouseEvent.CLICK, onPrev);
            }
        }

        override protected function partRemoved(partName: String, instance: Object): void{

            if(instance == nextControl){
                nextControl.removeEventListener(MouseEvent.CLICK, onNext);
            }
            else if(instance == prevControl){

                prevControl.removeEventListener(MouseEvent.CLICK, onPrev);
            }
        }

        //-----------------------------------------------------------------
        //
        //              Private methods and event handlers
        //
        //-----------------------------------------------------------------
        private var showBrowseControls: Boolean;

        private function updateImageControlsVisibility(): void{

            showBrowseControls = (previewData && previewData.length > 1);
        }

        private function onSkinChanged(value: Object): void{

            skin.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut, false, 0, true);
            skin.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver, false, 0, true);
        }

        private function onMouseOver(event: Event): void{

            if(prevControl)
                prevControl.visible = showBrowseControls;
            if(nextControl)
                nextControl.visible = showBrowseControls;
        }

        private function onMouseOut(event: Event): void{

            if(prevControl)
                prevControl.visible = false;
            if(nextControl)
                nextControl.visible = false;
        }

        private function updateImageSource(): void{

            if(imageIndex >= 0) {

                var imageInfo: ImageInfo = previewData[imageIndex] as ImageInfo;
                imageControl.source = (imageInfo.imageData != null) ? imageInfo.imageData : imageInfo.url;
            }
            else {
                imageControl.source = null;
            }
        }

        private function onPrev(event: MouseEvent): void{

            if(imageIndex > 0){
                --imageIndex;
                dispatchEvent(new Event("imageIndexChanged"));
            }
            updateImageSource();
        }

        private function onNext(event: MouseEvent): void{

            if(imageIndex < previewData.length-1)
            {
                ++imageIndex;
                dispatchEvent(new Event("imageIndexChanged"));
            }
            updateImageSource();
        }
    }
}
