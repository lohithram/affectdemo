/**
 * Created by abhisekpaul on 29/05/15.
 */
package com.al.components
{
    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.core.UIComponent;

    import spark.components.SkinnableContainer;
    import spark.components.supportClasses.ButtonBase;
    import spark.events.TextOperationEvent;

    [SkinState("normal")]
    [SkinState("expanded")]

    [Event(name="delete", type="flash.events.Event")]
    public class CollapsiblePanel extends SkinnableContainer
    {
        public function CollapsiblePanel()
        {
            super();
        }
        private static const STATE_NORMAL:String = "normal";
        private static const STATE_EXPANDED:String = "expanded";

        [Bindable]
        public var titleEditable:Boolean = true;

        private var titleChanged:Boolean;
        private var _title:String;

        [Bindable("titleChange")]
        public function get title():String
        {
            return _title;
        }

        public function set title(value:String):void
        {
            if(_title != value)
            {
                _title = value;
                titleChanged = true;
                dispatchEvent(new Event("titleChange"));
                invalidateProperties();
            }
        }

        private var _open: Boolean;
        public function get open(): Boolean{

            return _open;
        }

        public function set open(value: Boolean): void{

            if(value != _open){

                _open = value;
                invalidateSkinState();
            }
        }


        [SkinPart(required="false")]
        public var expandTrigger:UIComponent;

        [Bindable]
        public var count:String = "";

        [SkinPart(required="false")]
        public var titleDisplay:EditableLabel;

        [SkinPart(required="false")]
        public var deleteBtn:ButtonBase;

        public function toggle():void
        {
            open = !open;
            invalidateSkinState();
        }

        public function expand():void
        {
            open = true;
            invalidateSkinState();
        }

        public function collapse():void
        {
            open = false;
            invalidateSkinState();
        }

        protected function onCollapseClick(event:MouseEvent):void
        {
            toggle();
        }

        protected function onDeleteClick(event:MouseEvent):void
        {
            dispatchEvent(new Event("delete"));
        }

        protected function onTitleChange(event:TextOperationEvent):void
        {
            title = titleDisplay.text;
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName,instance);
            if(instance == expandTrigger)
            {
                expandTrigger.addEventListener(MouseEvent.CLICK, onCollapseClick);
            }
            else if(instance == titleDisplay)
            {
                titleDisplay.text = _title;
                titleDisplay.editable = titleEditable;
                titleDisplay.addEventListener(TextOperationEvent.CHANGE, onTitleChange);
            }
            else if(instance == deleteBtn)
            {
                deleteBtn.addEventListener(MouseEvent.CLICK, onDeleteClick);
            }
        }

        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName,instance);
            if(instance == expandTrigger)
            {
                expandTrigger.removeEventListener(MouseEvent.CLICK, onCollapseClick);
            }
            else if(instance == titleDisplay)
            {
                titleDisplay.removeEventListener(TextOperationEvent.CHANGE, onTitleChange);
            }
            else if(instance == deleteBtn)
            {
                deleteBtn.removeEventListener(MouseEvent.CLICK, onDeleteClick);
            }
        }

        override protected function commitProperties():void
        {
            super.commitProperties();

            if(titleChanged)
            {
                titleChanged = false;
                if(titleDisplay)
                {
                    titleDisplay.text = _title;
                    titleDisplay.editable = titleEditable;
                }
            }
        }

        override protected function getCurrentSkinState():String
        {
            return open ? STATE_EXPANDED : STATE_NORMAL;
        }
    }
}
