/**
 * Created by lram on 24/02/2015.
 */
package com.al.components
{

    import com.al.combine.ICombine;
    import com.al.enums.DragSourceFormats;
    import com.al.enums.TileDropAction;
    import com.al.enums.TileSide;
    import com.al.events.CombineDropEvent;
    import com.al.events.PlaceHolderDropEvent;
    import com.al.events.TileDropEvent;
    import com.al.tile.ICombineElement;
    import com.al.tile.ITileDropIndicator;
    import com.al.util.EventUtils;

    import flash.display.Graphics;
    import flash.geom.Rectangle;

    import mx.core.DragSource;
    import mx.events.DragEvent;
    import mx.managers.DragManager;

    import spark.components.supportClasses.SkinnableComponent;

    [SkinState("normal")]
	[SkinState("dragOver")]
	
    public class CombineDropPlaceHolder extends SkinnableComponent implements ITileDropIndicator
    {
		public static const GAP: Number = 3;
		public static const DASH_WIDTH: Number = 2;
		
		[Bindable]
        public var active: Boolean;

        public function CombineDropPlaceHolder(side: String="") {

            super();
            _side = side;

            addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
        }

        //--------------------------------------------------------------------------
        //
        //  Properties
        //
        //--------------------------------------------------------------------------
        //---------------------------------------------------
        // visualIndex
        //---------------------------------------------------
        private var _visualIndex: int;
        public function get visualIndex(): int {
            return _visualIndex;
        }

        public function set visualIndex(value: int): void {
            _visualIndex = value;
        }

        //---------------------------------------------------
        // side
        //---------------------------------------------------
        private var _relatedTile: ICombineElement;
        public function get relatedTile(): ICombineElement {
            return _relatedTile;
        }

        public function set relatedTile(value: ICombineElement): void {
            _relatedTile = value;
        }

        //---------------------------------------------------
        // side
        //---------------------------------------------------
        private var _side: String;
        public function get side(): String {
            return _side;
        }

        public function set side(value: String): void {
            _side = value;
        }

        //---------------------------------------------------
        // reducedX
        //---------------------------------------------------
        private var _reducedX: Number;
        public function get reducedX(): Number {
            return _reducedX;
        }

        public function set reducedX(value: Number): void{
            _reducedX = value;
        }

        //---------------------------------------------------
        // reducedY
        //---------------------------------------------------
        private var _reducedY: Number;
        public function get reducedY(): Number{
            return _reducedY;
        }
        public function set reducedY(value: Number): void{
            _reducedY = value;
        }

        //---------------------------------------------------
        // reducedWidth
        //---------------------------------------------------
        private var _reducedWidth: Number;
        public function get reducedWidth(): Number{
            return _reducedWidth;
        }
        public function set reducedWidth(value: Number): void{
            _reducedWidth = value;
        }

        //---------------------------------------------------
        // reducedX
        //---------------------------------------------------
        private var _reducedHeight: Number;
        public function get reducedHeight(): Number{
            return _reducedHeight;
        }
        public function set reducedHeight(value: Number): void{
            _reducedHeight = value;
        }
		
		//---------------------------------------------------
		// roundedX
		//---------------------------------------------------
		private var _roundedX: Number;
		public function get roundedX(): Number {
			return _roundedX;
		}
		
		//---------------------------------------------------
		// roundedY
		//---------------------------------------------------
		private var _roundedY: Number;
		public function get roundedY(): Number {
			return _roundedY;
		}
		
		//---------------------------------------------------
		// roundedWidth
		//---------------------------------------------------
		private var _roundedWidth: Number;
		public function get roundedRight(): Number {
			return _roundedWidth;
		}
		
		//---------------------------------------------------
		// roundedHeight
		//---------------------------------------------------
		private var _roundedHeight: Number;
		public function get roundedBottom(): Number {
			return _roundedHeight;
		}
		
        //---------------------------------------------------
        // selected
        //---------------------------------------------------
		public function set selected(value: Boolean): void{
			
		}
		public function get selected(): Boolean{
			return false;
		}

		//---------------------------------------------------
		// includeInSizing
		//---------------------------------------------------
		public function get includeInSizing(): Boolean {
			return true;
		}
		
        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //
        //  Privates
        //
        //--------------------------------------------------------------------------
		/**
		 * TODO: Not used but kept for future re-use. 
		 * @param rect
		 * 
		 */
		private function drawDottedRectangle(rect: Rectangle): void {
			
			drawHDottedLine(rect.left, rect.top, width);
			drawHDottedLine(rect.left, rect.bottom, width);
			drawVDottedLine(rect.left, rect.top, height);
			drawVDottedLine(rect.right, rect.top, height);
			
		}
		
		private function drawHDottedLine(x1: Number, y1: Number, lineWidth: Number, dash: Number = DASH_WIDTH, gap: Number = GAP): void{
		
			var g: Graphics = graphics;
			var pos: Number = x1;
			
			while ((lineWidth-dash-gap) > 0) {
				g.moveTo( pos, y1 );
				pos += dash;
				g.lineTo( pos, y1 );
				pos += gap;
				lineWidth -= (dash+gap);
			}
		}
		
		private function drawVDottedLine(x1: Number, y1: Number, lineHeight: Number, dash: Number = DASH_WIDTH, gap: Number = GAP): void{
		
			var g: Graphics = graphics;
			var pos: Number = y1;

			while ((lineHeight-dash) > 0) {
				g.moveTo( x1, pos );
				pos += dash;
				g.lineTo( x1, pos );
				pos += gap;
				lineHeight -= (dash+gap);
			}
		}
		
        private function onDragEnter(event: DragEvent): void{

            active = true;
            DragManager.acceptDragDrop(this);
            if(!event.dragSource.hasFormat(DragSourceFormats.ASSET_CELL))
            {
                DragManager.showFeedback(DragManager.MOVE);
            }

            invalidateDisplayList();
            addEventListener(DragEvent.DRAG_DROP, onDragDrop);
            addEventListener(DragEvent.DRAG_EXIT, onDragExit);
        }

        private function onDragDrop(event: DragEvent): void{

            active = false;
            invalidateDisplayList();
            EventUtils.removeListener(event, onDragDrop);
            removeEventListener(DragEvent.DRAG_EXIT, onDragExit);

            var dropAction:String = getDropAction();
            var dragSource: DragSource = event.dragSource;

            if(event.dragSource.hasFormat(DragSourceFormats.TILE)){

                dispatchEvent(
                        new TileDropEvent(event.dragSource.dataForFormat(DragSourceFormats.TILE) as ICombineElement, relatedTile, dropAction));
            }
            else if(event.dragSource.hasFormat(DragSourceFormats.COMBINE)){

                dispatchEvent(
                        new CombineDropEvent(
                                event.dragSource.dataForFormat(DragSourceFormats.COMBINE) as ICombine, dropAction));
            }
            else{
                dispatchEvent(new PlaceHolderDropEvent(event.dragSource, dropAction));
            }
        }

        private function onDragExit(event: DragEvent): void{

            active = false;
            invalidateDisplayList();
            EventUtils.removeListener(event, onDragExit);
            removeEventListener(DragEvent.DRAG_DROP, onDragDrop);
        }

        public function getDropAction(): String {

            switch (side){
                case TileSide.TOP_SIDE:
                    return TileDropAction.TOP_DROP_ACTION;
                case TileSide.LEFT_SIDE:
                    return TileDropAction.LEFT_DROP_ACTION;
                case TileSide.RIGHT_SIDE:
                    return TileDropAction.RIGHT_DROP_ACTION;
                case TileSide.BOTTOM_SIDE:
                    return TileDropAction.BOTTOM_DROP_ACTION;
            }
            return "";
        }
    }
}
