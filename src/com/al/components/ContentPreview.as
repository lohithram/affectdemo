/**
 * Created by synesis on 20/04/2015.
 */
package com.al.components
{

    import spark.components.supportClasses.SkinnableComponent;
    import spark.core.IContentLoader;

    public class ContentPreview extends SkinnableComponent
    {
        [Bindable]
        public var contentCache: IContentLoader;

        [Bindable]
        public var imageUrl: Object;

        [Bindable]
        public var title: String;

        public function ContentPreview() {
            super();
        }
    }
}
