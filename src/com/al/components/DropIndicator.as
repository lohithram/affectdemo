package com.al.components
{
	import com.al.enums.ViewState;
	
	import mx.events.DragEvent;
	
	import spark.components.supportClasses.SkinnableComponent;
	
	[SkinState("normal")]
	[SkinState("dragOver")]
	
	public class DropIndicator extends SkinnableComponent
	{
		protected var skinState: String;
		public function DropIndicator()
		{
			super();
			skinState = ViewState.NORMAL;
			addEventListener(DragEvent.DRAG_EXIT, onDragExit);
			addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
		}
		
		override protected function getCurrentSkinState():String{
			
			return skinState;
		}
		
		private function onDragExit(ev: DragEvent): void{
			
			skinState = ViewState.NORMAL;
			invalidateSkinState();
		}
		
		private function onDragEnter(ev: DragEvent): void{
			
			skinState = ViewState.DRAG_OVER;
			invalidateSkinState();
		}
		
	}
}