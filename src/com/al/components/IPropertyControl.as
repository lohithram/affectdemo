/**
 * Created by lohithram on 01/05/15.
 */
package com.al.components {

    import mx.core.IVisualElement;

    public interface IPropertyControl extends IVisualElement{

        function get propertyName(): String;

        function get propertyValue(): Object;

        function set propertyValue(value: Object): void;
    }
}
