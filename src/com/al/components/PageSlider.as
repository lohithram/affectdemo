/**
 * Created by synesis on 20/04/2015.
 */
package com.al.components
{

    import com.al.events.HidePreviewEvent;
    import com.al.events.ShowPreviewEvent;

    import flash.events.MouseEvent;
    import flash.geom.Point;

    import spark.components.HSlider;

    [Event(name="showPreview", type="com.al.events.ShowPreviewEvent")]
    [Event(name="hidePreview", type="com.al.events.HidePreviewEvent")]

    public class PageSlider extends HSlider
    {
        public function PageSlider() {
            super();
        }

        private var recentPreviewIndex: Number = 0;

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName, instance);

            if (instance == track)
            {
                track.addEventListener(MouseEvent.MOUSE_OUT, trackMouseOutHandler);
                track.addEventListener(MouseEvent.MOUSE_MOVE, trackMouseMoveHandler);
                track.addEventListener(MouseEvent.MOUSE_OVER, trackMouseOverHandler);
            }
        }


        protected function trackMouseOutHandler(event: MouseEvent): void {

            // remove preview
            dispatchEvent(new HidePreviewEvent());
        }

        protected function trackMouseOverHandler(event: MouseEvent): void{

            var thumbW:Number = (thumb) ? thumb.width : 0;
            var thumbH:Number = (thumb) ? thumb.height : 0;
            var offsetX:Number = event.stageX - (thumbW / 2);
            var offsetY:Number = event.stageY - (thumbH / 2);
            var p:Point = track.globalToLocal(new Point(offsetX, offsetY));
            var index: Number = pointToValue(p.x, p.y);
            recentPreviewIndex = index;

            dispatchEvent(new ShowPreviewEvent(index, event));
        }

        protected function trackMouseMoveHandler(event: MouseEvent): void{

            var thumbW:Number = (thumb) ? thumb.width : 0;
            var thumbH:Number = (thumb) ? thumb.height : 0;
            var offsetX:Number = event.stageX - (thumbW / 2);
            var offsetY:Number = event.stageY - (thumbH / 2);
            var p:Point = track.globalToLocal(new Point(offsetX, offsetY));
            var index: Number = pointToValue(p.x, p.y);

            if(recentPreviewIndex != index){

                recentPreviewIndex = index;
                dispatchEvent(new ShowPreviewEvent(index, event));
            }
        }

    }
}
