package com.al.components
{
	import com.al.tile.ICombineElement;
	
	import spark.components.Group;
	
	public class ReducedElement extends Group implements ICombineElement
	{
		public function ReducedElement()
		{
			super();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Properties
		//
		//--------------------------------------------------------------------------
		//---------------------------------------------------
		// visualIndex
		//---------------------------------------------------
		private var _visualIndex: int;
		public function get visualIndex(): int {
			return _visualIndex;
		}
		
		public function set visualIndex(value: int): void {
			_visualIndex = value;
			invalidateProperties();
		}
		
		//---------------------------------------------------
		// reducedX
		//---------------------------------------------------
		private var _reducedX: Number = 0;
		public function get reducedX(): Number {
			return _reducedX;
		}
		
		public function set reducedX(value: Number): void{
			_reducedX = value;
		}
		
		//---------------------------------------------------
		// reducedY
		//---------------------------------------------------
		private var _reducedY: Number = 0;
		public function get reducedY(): Number{
			return _reducedY;
		}
		public function set reducedY(value: Number): void{
			_reducedY = value;
		}
		
		//---------------------------------------------------
		// reducedWidth
		//---------------------------------------------------
		private var _reducedWidth: Number;
		public function get reducedWidth(): Number{
			return _reducedWidth;
		}
		public function set reducedWidth(value: Number): void{
			_reducedWidth = value;
		}
		
		//---------------------------------------------------
		// reducedHeight
		//---------------------------------------------------
		private var _reducedHeight: Number;
		public function get reducedHeight(): Number{
			return _reducedHeight;
		}
		public function set reducedHeight(value: Number): void{
			_reducedHeight = value;
		}
		
		//---------------------------------------------------
		// reducedX
		//---------------------------------------------------
		private var _selected: Boolean;
		private var selectionChanged: Boolean;
		public function set selected(value: Boolean): void{
			
			if(_selected != value){
				
				_selected = value;
				selectionChanged = true;
				invalidateProperties();
			}
		}
		
		public function get selected(): Boolean{
			return _selected;
		}
		
		
		//---------------------------------------------------
		// includeInSizing
		//---------------------------------------------------
		public function get includeInSizing(): Boolean {
			return true;
		}
	}
}