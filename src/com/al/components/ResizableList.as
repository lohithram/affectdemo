/**
 * Created by synesis on 21/04/2015.
 */
package com.al.components
{

    import ardisia.components.timebarContainer.TimebarContainerHScrollBar;
    import ardisia.managers.cursorManager.CursorManager;
    import ardisia.managers.cursorManager.CursorPriority;
    import ardisia.managers.cursorManager.DefaultCursors;

    import com.al.util.SnapshotUtil;

    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;

    import mx.controls.Image;
    import mx.core.DragSource;

    import mx.core.IFlexDisplayObject;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;

    import mx.core.mx_internal;
    import mx.events.DragEvent;

    import mx.events.FlexEvent;
    import mx.events.SandboxMouseEvent;
    import mx.managers.DragManager;

    import spark.components.BorderContainer;
    import spark.components.IItemRenderer;

    import spark.components.List;
    import spark.components.supportClasses.ButtonBase;

    use namespace mx_internal;

    public class ResizableList extends List
    {
        [SkinPart]
        public var horizontalScrollBar: TimebarContainerHScrollBar;

        [Bindable]
        public var thumbnailWidth: Number;

        [Bindable]
        public var keyEnabled: Boolean = true;


        override protected function keyDownHandler(event: KeyboardEvent):void
        {
            if(keyEnabled)
                super.keyDownHandler(event);
        }

        override protected function dragStartHandler(event:DragEvent):void
        {
            if (event.isDefaultPrevented())
                return;

            var dragSource:DragSource = new DragSource();
            addDragData(dragSource);
            DragManager.doDrag(this,
                    dragSource,
                    event,
                    createDragIndicator(),
                    0 /*xOffset*/,
                    0 /*yOffset*/,
                    1.0,
                    dragMoveEnabled);

            if(mouseDownObject)
                mouseDownObject.alpha = 0.6;
        }

        override protected function dragCompleteHandler(event:DragEvent):void
        {
            super.dragCompleteHandler(event);

            if(mouseDownObject)
                mouseDownObject.alpha = 1.0;
        }

        override protected function partAdded(partName: String, instance: Object): void {

            super.partAdded(partName, instance);

            if(instance == horizontalScrollBar){

                horizontalScrollBar.viewport = dataGroup;
                //horizontalScrollBar.validateNow(); // get resize buttons initialized
                //horizontalScrollBar.includeInLayout = false;
                horizontalScrollBar.addEventListener(Event.CHANGE,
                        horizontalScrollBarHandler);

                // add end handler to handle bug with scrollbar stepping animations
                horizontalScrollBar.addEventListener(FlexEvent.CHANGE_END,
                        horizontalScrollBarHandler);
                if (horizontalScrollBar.resizeLeftPart)
                {
                    horizontalScrollBar.resizeLeftPart.addEventListener(MouseEvent.ROLL_OVER,
                            horizontalScrollBarHandler);
                    horizontalScrollBar.resizeLeftPart.addEventListener(MouseEvent.MOUSE_DOWN,
                            horizontalScrollBarHandler);
                    horizontalScrollBar.resizeLeftPart.addEventListener(MouseEvent.ROLL_OUT,
                            horizontalScrollBarHandler);
                }

                if (horizontalScrollBar.resizeRightPart)
                {
                    horizontalScrollBar.resizeRightPart.addEventListener(MouseEvent.ROLL_OVER,
                            horizontalScrollBarHandler);
                    horizontalScrollBar.resizeRightPart.addEventListener(MouseEvent.MOUSE_DOWN,
                            horizontalScrollBarHandler);
                    horizontalScrollBar.resizeRightPart.addEventListener(MouseEvent.ROLL_OUT,
                            horizontalScrollBarHandler);
                }
            }
            else if(instance == dataGroup && horizontalScrollBar){

                horizontalScrollBar.viewport = dataGroup;
            }
        }

        //-----------------------------------------------------------------------------
        //
        //     ScrollBar event handling
        //
        //-----------------------------------------------------------------------------

        /**
         *  @private
         *
         * 	Stores which resize button on the scrollbar was depressed.
         */
        protected var resizeButtonMouseDown: ButtonBase;

        /**
         *  @private
         *
         * 	Initial scroll thumb x coordinate. Useful for drags.
         */
        protected var initialThumbX:Number;

        /**
         *  @private
         *
         * 	Initial thumb width. Useful for drags.
         */
        protected var initialThumbWidth:Number;

        /**
         *  @private
         *
         * 	Initial mouse down x coordinate. Useful for drags.
         */
        protected var initialMouseDownX:Number;

        /**
         *  @private
         *
         * 	Convenience variable: the total duration of the period in milliseconds.
         */
        protected var duration:Number

        protected function horizontalScrollBarHandler(event:Event):void
        {
            var mouseEvt:MouseEvent = event as MouseEvent;

            switch (event.type)
            {

                case Event.CHANGE:
                case FlexEvent.CHANGE_END:
                    //setVisiblePeriod(horizontalScrollBar.value,
                      //      horizontalScrollBar.value + horizontalScrollBar.pageSize, true);

                    break;

                case MouseEvent.ROLL_OVER:
                    CursorManager.setCursor(DefaultCursors.E_W_RESIZE,
                            CursorPriority.HIGH, "timebarCursorGroupRollOver");

                    break;

                case MouseEvent.ROLL_OUT:
                    CursorManager.removeGroup("timebarCursorGroupRollOver");

                    break;

                case MouseEvent.MOUSE_DOWN:
                    // swallow event to prevent it from reaching the thumb
                    event.stopImmediatePropagation();

                    CursorManager.setCursor(DefaultCursors.E_W_RESIZE,
                            CursorPriority.HIGH, "timebarCursorGroupDown");

                    // store which button is moused down
                    resizeButtonMouseDown = event.currentTarget as ButtonBase;
                    resizeButtonMouseDown.keepDown(true);

                    initialMouseDownX = mouseX;
                    initialThumbX = resizeButtonMouseDown.getLayoutBoundsX();
                    initialThumbWidth = horizontalScrollBar.thumb.getLayoutBoundsWidth();

                    // add listeners
                    systemManager.addEventListener(MouseEvent.MOUSE_MOVE,
                            horizontalScrollBarHandler);
                    systemManager.addEventListener(MouseEvent.MOUSE_UP,
                            horizontalScrollBarHandler, true);
                    systemManager.getSandboxRoot().addEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE,
                            horizontalScrollBarHandler, true);

                    break;

                case MouseEvent.MOUSE_MOVE:
                    var delta:Number = mouseX - initialMouseDownX;
                    var range:Number = horizontalScrollBar.track.getLayoutBoundsWidth();
                    var trackX:Number = horizontalScrollBar.track.getLayoutBoundsX();
                    var thumbWidth:Number = horizontalScrollBar.thumb.getLayoutBoundsWidth();

                    var thumbX:Number = resizeButtonMouseDown.getLayoutBoundsX();

                    if (resizeButtonMouseDown == horizontalScrollBar.resizeLeftPart)
                    {
                        var newStartX:Number = initialThumbX + delta - trackX;
                        newStartX = Math.max(0, newStartX);

                        var variance: Number = horizontalScrollBar.thumb.getLayoutBoundsX() - newStartX;
                    }
                    else
                    {
                        var buttonWidth:Number = resizeButtonMouseDown.getLayoutBoundsWidth();
                        var newEndX:Number = initialThumbX + buttonWidth + delta - trackX;
                        newEndX = Math.min(newEndX, range);

                        variance = newEndX - (horizontalScrollBar.thumb.getLayoutBoundsX() + thumbWidth);
                    }
                    // TODO: This is where i have to change pre-determine parameter value
                    addVariance(variance);

                    // validate immediately to avoid scrollbar flicker
                    validateNow();

                    break;

                case MouseEvent.MOUSE_UP:
                    CursorManager.removeGroup("timebarCursorGroupDown");
                    resizeButtonMouseDown.keepDown(false);

                    // remove listeners
                    systemManager.removeEventListener(MouseEvent.MOUSE_MOVE,
                            horizontalScrollBarHandler);
                    systemManager.removeEventListener(MouseEvent.MOUSE_UP,
                            horizontalScrollBarHandler, true);
                    systemManager.getSandboxRoot().removeEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE,
                            horizontalScrollBarHandler, true);

                    break;

            }
        }


        const MIN: Number = 80;
        const MAX: Number = 160;
        protected function addVariance(delta: Number):void
        {
            var thumbWidth: Number = horizontalScrollBar.thumb.getLayoutBoundsWidth();
            var newWidth: Number = thumbWidth / (thumbWidth + delta) * thumbnailWidth;
            if(newWidth >= MIN && newWidth <= MAX)
                thumbnailWidth = newWidth;
            ////trace("New width - " + thumbnailWidth);
        }
    }
}
