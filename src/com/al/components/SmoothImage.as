/**
 * Created by lohithram on 17/07/15.
 */
package com.al.components {

    import mx.graphics.BitmapSmoothingQuality;

    import spark.components.Image;

    public class SmoothImage extends Image {

        public function SmoothImage() {

            super();
            smooth = true;
            setStyle("smoothingQuality", BitmapSmoothingQuality.HIGH);
        }

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {

            super.updateDisplayList(unscaledWidth, unscaledHeight);
            smooth = (unscaledWidth > 80 && unscaledHeight > 60);
            //trace("SMOOTHING ENABLED: "+ smooth);
        }
    }
}
