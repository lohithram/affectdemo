/**
 * Created by abhisekpaul on 03/06/15.
 */
package com.al.components
{
    import spark.components.VScrollBar;
    import spark.components.supportClasses.ButtonBase;

    public class ZoomVScrollBar extends VScrollBar
    {
        [SkinPart(required="true")]
        public var resizeUpPart:ButtonBase;

        [SkinPart(required="true")]
        public var resizeDownPart:ButtonBase;

        override protected function updateSkinDisplayList():void
        {
            super.updateSkinDisplayList();

            if (thumb)
            {
                thumb.visible = true;
                var layoutX:Number = thumb.getLayoutBoundsX();
                var layoutY:Number = thumb.getLayoutBoundsY();
            }

            if (resizeUpPart)
            {
                resizeUpPart.setLayoutBoundsPosition(layoutX, layoutY);
                resizeUpPart.visible = true;
            }
            if (resizeDownPart && thumb)
            {
                // round to avoid subpixels
                var yPosition:Number = Math.round(layoutY + thumb.getLayoutBoundsHeight() -
                        resizeDownPart.getLayoutBoundsHeight());
                resizeDownPart.setLayoutBoundsPosition(layoutX,yPosition);
                resizeDownPart.visible = true;
            }
        }
    }
}
