/**
 * Created by abhisekpaul on 03/06/15.
 */
package com.al.components{
    import ardisia.managers.cursorManager.CursorManager;
    import ardisia.managers.cursorManager.CursorPriority;
    import ardisia.managers.cursorManager.DefaultCursors;

    import com.al.events.ItemEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.core.mx_internal;

    import mx.events.FlexEvent;
    import mx.events.SandboxMouseEvent;

    import spark.components.SkinnableContainer;
    import spark.components.supportClasses.ButtonBase;

    use namespace mx_internal;

    [Event(name='zoomFactorChange', type='com.al.events.ItemEvent')]
    public class ZoomableContainer extends SkinnableContainer
    {

        [SkinPart]
        public var vScrollBar: ZoomVScrollBar;

        [Bindable]
        public var zoomFactor: Number = 1;

        [Bindable]
        public var keyEnabled: Boolean = true;

        protected var resizeButtonMouseDown: ButtonBase;

        protected var initialThumbY:Number;

        protected var initialThumbHeight:Number;

        protected var initialMouseDownY:Number;

        protected var duration:Number;

        private const CURSOR_GROUP:String = "resizeCursorGroup";

        protected function vScrollBarHandler(event:Event):void
        {
            var mouseEvt:MouseEvent = event as MouseEvent;

            switch (event.type)
            {

                case Event.CHANGE:
                case FlexEvent.CHANGE_END:
                    //setVisiblePeriod(horizontalScrollBar.value,
                    //      horizontalScrollBar.value + horizontalScrollBar.pageSize, true);

                    break;

                case MouseEvent.ROLL_OVER:
                    CursorManager.setCursor(DefaultCursors.ROW_RESIZE,
                            CursorPriority.HIGH, CURSOR_GROUP);

                    break;

                case MouseEvent.ROLL_OUT:
                    CursorManager.removeGroup(CURSOR_GROUP);

                    break;

                case MouseEvent.MOUSE_DOWN:
                    // swallow event to prevent it from reaching the thumb
                    event.stopImmediatePropagation();

                    CursorManager.setCursor(DefaultCursors.ROW_RESIZE,
                            CursorPriority.HIGH, CURSOR_GROUP);

                    // store which button is moused down
                    resizeButtonMouseDown = event.currentTarget as ButtonBase;
                    resizeButtonMouseDown.keepDown(true);

                    initialMouseDownY = mouseY;
                    initialThumbY = resizeButtonMouseDown.getLayoutBoundsY();
                    initialThumbHeight = vScrollBar.thumb.getLayoutBoundsHeight();

                    // add listeners
                    systemManager.addEventListener(MouseEvent.MOUSE_MOVE,
                            vScrollBarHandler);
                    systemManager.addEventListener(MouseEvent.MOUSE_UP,
                            vScrollBarHandler, true);
                    systemManager.getSandboxRoot().addEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE,
                            vScrollBarHandler, true);

                    break;

                case MouseEvent.MOUSE_MOVE:
                    var delta:Number = 0;
                    if (resizeButtonMouseDown == vScrollBar.resizeUpPart)
                    {
                        delta=mouseY - initialMouseDownY;
                    }
                    else
                    {
                        delta=initialMouseDownY - mouseY;
                    }
                    updateZoomFactor(delta);

                    // validate immediately to avoid scrollbar flicker
                    validateNow();

                    break;

                case MouseEvent.MOUSE_UP:
                    CursorManager.removeGroup(CURSOR_GROUP);
                    resizeButtonMouseDown.keepDown(false);

                    // remove listeners
                    systemManager.removeEventListener(MouseEvent.MOUSE_MOVE,
                            vScrollBarHandler);
                    systemManager.removeEventListener(MouseEvent.MOUSE_UP,
                            vScrollBarHandler, true);
                    systemManager.getSandboxRoot().removeEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE,
                            vScrollBarHandler, true);

                    break;

            }
        }


        override protected function partAdded(partName: String, instance: Object): void
        {

            super.partAdded(partName, instance);

            if(instance == vScrollBar){

                vScrollBar.viewport = contentGroup;
                //horizontalScrollBar.validateNow(); // get resize buttons initialized
                //horizontalScrollBar.includeInLayout = false;
                vScrollBar.addEventListener(Event.CHANGE,
                        vScrollBarHandler);

                // add end handler to handle bug with scrollbar stepping animations
                vScrollBar.addEventListener(FlexEvent.CHANGE_END,
                        vScrollBarHandler);
                if (vScrollBar.resizeUpPart)
                {
                    vScrollBar.resizeUpPart.addEventListener(MouseEvent.ROLL_OVER,
                            vScrollBarHandler);
                    vScrollBar.resizeUpPart.addEventListener(MouseEvent.MOUSE_DOWN,
                            vScrollBarHandler);
                    vScrollBar.resizeUpPart.addEventListener(MouseEvent.ROLL_OUT,
                            vScrollBarHandler);
                }

                if (vScrollBar.resizeDownPart)
                {
                    vScrollBar.resizeDownPart.addEventListener(MouseEvent.ROLL_OVER,
                            vScrollBarHandler);
                    vScrollBar.resizeDownPart.addEventListener(MouseEvent.MOUSE_DOWN,
                            vScrollBarHandler);
                    vScrollBar.resizeDownPart.addEventListener(MouseEvent.ROLL_OUT,
                            vScrollBarHandler);
                }
            }
            else if(instance == contentGroup && vScrollBar){

                vScrollBar.viewport = contentGroup;
            }
        }

        [Bindable]
        //between 0 to 1 must be the item
        public var sensitivity:Number = 0.5;

        protected function updateZoomFactor(delta: Number):void
        {
            //trace(zoomFactor);
            if(sensitivity<=0) sensitivity = 0.1
            var newFactor: Number = zoomFactor + (delta*sensitivity/10);

            if(newFactor<0)
            {
                newFactor = 0;
            }
            else if(newFactor > 100 )
            {
                newFactor = 100;
            }

            if(zoomFactor != newFactor)
            {
                zoomFactor = newFactor;
                //trace("zoomchanged: " + zoomFactor);
                dispatchEvent(new ItemEvent(ItemEvent.ZOOM_FACTOR_CHANGE,zoomFactor));
            }
        }
    }
}
