////////////////////////////////////////////////////////////////////////////////
//	
//	Copyright 2014 Ardisia Labs LLC. All Rights Reserved.
//
//	This file is licensed under the Ardisia Component Library License. 
//
//	Only license holders are entitled to use this file subject to the  
//	conditions of the license. All other uses are expressly forbidden. Visit 
//	http://www.ardisialabs.com to view and purchase a license.
//
//	Apache Flex's source code notices are reproduced below.
//
// 	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//
//  Licensed to the Apache Software Foundation (ASF) under one or more
//  contributor license agreements.  See the NOTICE file distributed with
//  this work for additional information regarding copyright ownership.
//  The ASF licenses this file to You under the Apache License, Version 2.0
//  (the "License"); you may not use this file except in compliance with
//  the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////

package com.al.components.calendarList
{
	
import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.core.IVisualElement;
import mx.core.IVisualElementContainer;
import mx.events.CollectionEvent;

import spark.components.Label;
import spark.components.List;
import spark.events.IndexChangeEvent;

import ardisia.scheduling.calendar.dataTypes.InfiniteRecurrenceData;
import ardisia.scheduling.calendar.dataTypes.SelectedCalendarData;
import ardisia.scheduling.calendar.events.CalendarEvent;
import ardisia.scheduling.dataTypes.SchedulingData;
import ardisia.scheduling.iCalendarRecurrence.iCalendarRecurrence;
import ardisia.utils.DateUtils;

import com.al.components.calendarList.dataTypes.StoreData;
import com.al.components.calendarList.interfaces.ICalendarListItemRenderer;

//--------------------------------------
//  Events
//--------------------------------------

/**
 *  Dispatched when the selected data changes. See the "selectedData" property.
 *
 *  @eventType ardisia.scheduling.calendar.events.CalendarEvent
 */
[Event(name="selectedDataChange", type="ardisia.scheduling.calendar.events.CalendarEvent")]

//--------------------------------------
//  Skin states
//--------------------------------------

[SkinState("normal")]

[SkinState("disabled")]

//--------------------------------------
//  Excluded APIs
//--------------------------------------

[Exclude(name="dataProvider", kind="property")]

/**
 * 	List designed to display SchedulingData objects. Created for use with the
 * 	Ardisia Calendar examples. 
 * 
 * 	<p>To use, reference a collection of SchedulingData objects in the 
 * 	schedulingDataProvider property.</p>
 * 
 * 	<p>Item renderers must implement the ISchedulingDataListItemRenderer 
 * 	interface.</p>
 */ 
public class CalendarList extends List
{

	//--------------------------------------------------------------------------
	//
	// 	Variables
	//
	//--------------------------------------------------------------------------
	
	/**
	 *  @private
	 */
	protected var emptySetLabel:Label;
	
	/**
	 *  @private
	 */
	protected var swallowCollection:Boolean;
	
	/**
	 *  @private
	 */
	protected var configDirty:Boolean;
	
	/**
	 *  @private
	 */
	protected var infiniteRecurrenceEnd:Date;
	
	/**
	 *  @private
	 */
	protected var infiniteRecurrenceVector:Vector.<InfiniteRecurrenceData>;
	
	/**
	*  @private
	*/
	protected var store:Vector.<StoreData>;
	
	//--------------------------------------------------------------------------
	//
	//  Overridden Properties
	//
	//--------------------------------------------------------------------------
	
	//----------------------------------
	//  dataProvider
	//----------------------------------
	
	/**
	 * 	@private
	 */
	protected var dataProviderDirty:Boolean;
	
	/**
	 *  @private
	 */
	override public function set dataProvider(value:IList):void
	{
		super.dataProvider = value;
		
		dataProviderDirty = true;
		invalidateProperties();
	}
	
	//--------------------------------------------------------------------------
	//
	//  Properties
	//
	//--------------------------------------------------------------------------
	
	//----------------------------------
	//  schedulingDataProvider
	//----------------------------------
	
	/**
	 * 	@private
	 */
	protected var schedulingDataProviderDirty:Boolean;
	
	private var _schedulingDataProvider:IList;
	
	/**
	 *  @private
	 */
	public function get schedulingDataProvider():IList
	{
		return _schedulingDataProvider;
	}
	
	public function set schedulingDataProvider(value:IList):void 
	{
		if (value == _schedulingDataProvider)
			return;
		
		if (_schedulingDataProvider)
			_schedulingDataProvider.removeEventListener(CollectionEvent.COLLECTION_CHANGE, 
				eventHandler);
		_schedulingDataProvider = value;
		
		if (_schedulingDataProvider)
			_schedulingDataProvider.addEventListener(CollectionEvent.COLLECTION_CHANGE, 
				eventHandler);
		
		schedulingDataProviderDirty = true;
		invalidateProperties();
	}
	
	//----------------------------------
	//  selectedData
	//----------------------------------
	
	/**
	 * 	@private
	 */
	protected var selectedDataDirty:Boolean;
	
	private var _selectedData:Vector.<SelectedCalendarData>;
	
	/**
	 *  @private
	 */
	public function get selectedData():Vector.<SelectedCalendarData> 
	{
		return _selectedData;
	}
	
	public function set selectedData(value:Vector.<SelectedCalendarData>):void 
	{
		if (value == _selectedData)
			return;
		_selectedData = value;
		
		selectedDataDirty = true;
		invalidateProperties();
	}
	
	//----------------------------------
	//  startDate
	//----------------------------------
	
	/**
	 * 	@private
	 */
	private var _startDate:Date;
	
	/**
	 *  @private
	 */
	public function get startDate():Date 
	{
		return _startDate;
	}
	
	public function set startDate(value:Date):void 
	{
		_startDate = value;
		
		configDirty = true;
		invalidateProperties();
	}
	
	//----------------------------------
	//  endDate
	//----------------------------------
	
	/**
	 * 	@private
	 */
	private var _endDate:Date;
	
	/**
	 *  @private
	 */
	public function get endDate():Date 
	{
		return _endDate;
	}
	
	public function set endDate(value:Date):void 
	{
		_endDate = value;
		
		configDirty = true;
		invalidateProperties();
	}
	
	//--------------------------------------------------------------------------
	//
	//  Overridden methods
	//
	//--------------------------------------------------------------------------
	
	//----------------------------------
	//  interface
	//----------------------------------
	
	/**
	 * 	@private
	 */
	override public function updateRenderer(renderer:IVisualElement, 
											itemIndex:int, 
											data:Object):void
	{
		var itemRenderer:ICalendarListItemRenderer = renderer as ICalendarListItemRenderer;
		
		if (!itemRenderer)
			return;
		
		// determine if the item is the first item in a day and mark it 
		// for special formatting by comparing start date to the previous
		// start date in the dataProvider (assuming dataprovider is sorted
		// by date
		var previousIndex:int = itemIndex - 1;
		
		if (previousIndex > -1)
		{
			var lastDt:Date = dataProvider[previousIndex].dtStart as Date;
			var currentDt:Date = data.dtStart as Date;
			itemRenderer.isFirst = lastDt.fullYear == currentDt.fullYear && lastDt.month == currentDt.month 
				&& lastDt.date == currentDt.date ? false : true;
		}
		
		super.updateRenderer(renderer, itemIndex, data);
	}
	
	//----------------------------------
	//  private / protected
	//----------------------------------
	
	/**
	 * 	@private
	 */
	override protected function createChildren():void
	{
		super.createChildren();
		
		// ensure that a start date is set
		if (!_startDate)
			_startDate = DateUtils.scrubTime(new Date());
		
		if (!_endDate)
			_endDate = DateUtils.addMonths(_startDate, 1);
		
		if (!dataProvider)
			dataProvider = new ArrayCollection();
		
		addEventListener(IndexChangeEvent.CHANGE, eventHandler);
		
		
		emptySetLabel = new Label();
		emptySetLabel.setStyle('fontSize', 18);
		emptySetLabel.setStyle('fontStyle', 'italic');
		emptySetLabel.setStyle('color', 0x777777);
		emptySetLabel.setStyle('paddingTop', 19);
		emptySetLabel.setStyle('paddingLeft', 17);
		emptySetLabel.text = "No Items";
		emptySetLabel.visible = emptySetLabel.includeInLayout = false;
		
		IVisualElementContainer(skin).addElement(emptySetLabel);
	}
	
	/**
	 * 	@private
	 */
	override protected function commitProperties():void 
	{
		
		//----------------------------------
		//  property flags
		//----------------------------------
		
		if (configDirty)
		{
			configDirty = false;
			
			var checkRecurrenceEnd:Boolean = dataProviderDirty = true;
		}
		
		if (schedulingDataProviderDirty)
		{
			schedulingDataProviderDirty = false;
			
			// setup a new store and reset the infiniteRecurrenceArray
			store = new Vector.<StoreData>();
			infiniteRecurrenceVector = new Vector.<InfiniteRecurrenceData>();
			
			// set a date in the future for the infinite recurrence objects to 
			// be created, default to the end of the view
			infiniteRecurrenceEnd = _endDate;
			iCalendarRecurrence.defaultUntil = infiniteRecurrenceEnd;
			
			// when rebuilding the entire store, it is MUCH faster
			// to skip individual sorting and sort the entire store at once
			// using the efficient native sort function
			for each (var item:SchedulingData in schedulingDataProvider)
			{
				// skip if the calendar is not enabled
				if (!item.calendar.enabled)
					continue;
				
				// skip individual sorting;
				addToStore(item, false);
			}
			store.sort(sortStore); // now sort the entire collection
			
			dataProviderDirty = true;
		}
		
		// if flagged, determine whether or not to update the infinite
		// recurrence data.
		if (checkRecurrenceEnd)
		{
			checkRecurrenceEnd = false;
			
			// check if the view's end date will exceed the recurrence end
			if (infiniteRecurrenceEnd && _endDate.time > infiniteRecurrenceEnd.time)
			{
				updateRecurrenceSet(_endDate);
			}
		}
		
		if (dataProviderDirty)
		{
			swallowCollection = true;
			dataProviderDirty = false;
			dataProvider.removeAll();
			
			// filter out the data in the displayed date range
			var len:int = store ? store.length : 0;
			for (var i:int = 0; i < len; i++)
			{
				var storeData:StoreData = store[i];
				
				// since the store is sorted, you can break out once the start time
				// is greater than the end of the displayed range
				if (storeData.dtStart.time >= _endDate.time)
					break;
					
				else if (storeData.dtStart.time < _endDate.time && storeData.dtEnd.time > _startDate.time)
				{
					dataProvider.addItem(storeData);
				}
			}
			swallowCollection = false;
			
			// if still empty display empty set label
			emptySetLabel.visible = emptySetLabel.includeInLayout = dataProvider.length == 0;
		}
		
		if (selectedDataDirty)
		{
			selectedDataDirty = false;
			
			var selIndices:Vector.<int> = new Vector.<int>();
			// get the selected indices
			if (_selectedData)
			{
				len = dataProvider.length;
				for (i = 0; i < len; i++)
				{
					var data:StoreData = dataProvider[i];
					for each (var selData:SelectedCalendarData in _selectedData)
					{
						if (selData.dtStart.time == data.dtStart.time && selData.dtEnd.time == data.dtEnd.time 
							&& selData.schedulingData == data.schedulingData)
						{
							selIndices.push(i);
						}
					}
				}
				selectedIndices = selIndices;
			}
			else
			{
				selectedIndices = null;
			}
		}
		
		super.commitProperties();
	}
	
	/**
	 * 	@private
	 */
	override protected function getCurrentSkinState():String
	{
		return !enabled ? "disabled" : "normal";
	}
	
	//--------------------------------------------------------------------------
	//  
	// 	Methods
	//
	//--------------------------------------------------------------------------
	
	//----------------------------------
	//  private / protected
	//----------------------------------
	
	/**
	 * 	@private
	 */
	protected function sortStore(x:StoreData, 
								 y:StoreData):Number
	{
		var itemXStartTime:Number = x.dtStart.time;
		var itemXEndTime:Number = x.dtEnd.time;
		
		var itemYStartTime:Number = y.dtStart.time;
		var itemYEndTime:Number = y.dtEnd.time;
		
		// if earlier
		if (itemXStartTime < itemYStartTime)
			return -1;
			
			// if later
		else if (itemXStartTime > itemYStartTime)
			return 1;
		
		// all day
		if (x.schedulingData.allDay && !y.schedulingData.allDay)
			return -1;
		else if (!x.schedulingData.allDay && y.schedulingData.allDay)
			return 1;
		
		// if start times are the same, sort by endtime and if they are the 
		// same, sort by the summary
		if (itemXEndTime < itemYEndTime)
			return 1;
		else if (itemXEndTime > itemYEndTime)
			return -1;
		
		if (x.schedulingData.summary < y.schedulingData.summary)
			return -1;
		else if (x.schedulingData.summary > y.schedulingData.summary)
			return 1;
		
		// finally, sort by UID 
		if (x.schedulingData.uid < y.schedulingData.uid)
			return -1;
		else
			return 1;
		
		return undefined;
	}
	
	/**
	 * 	@private
	 */
	protected function updateRecurrenceSet(end:Date):void
	{
		// update the recurrence end date
		infiniteRecurrenceEnd = end;
		iCalendarRecurrence.defaultUntil = infiniteRecurrenceEnd;
		
		// loop through the infiniteRecurrenceVector array and add 
		// recurrence items
		for each (var recurItem:InfiniteRecurrenceData in infiniteRecurrenceVector)
		{
			var calendarItemData:SchedulingData = recurItem.schedulingData;
			var lastRecurDate:Date = recurItem.lastRecurDate;
			
			// get duration
			var duration:Number = calendarItemData.dtEnd.time - 
				calendarItemData.dtStart.time;
			
			// recurDates times are scrubbed, must add them back in
			var startHours:Number = calendarItemData.dtStart.hours;
			var startMinutes:Number = calendarItemData.dtStart.minutes;
			
			// get the recurrence items
			var recurDates:Array = iCalendarRecurrence.generateRecurrenceDates(calendarItemData, 
				lastRecurDate);
			
			if (recurDates.length < 1)
				continue;
			
			// add the new recurred items to the store
			for each (var recurDate:Date in recurDates)
			{
				var newData:StoreData = new StoreData();
				newData.schedulingData = calendarItemData;
				newData.dtStart = new Date(recurDate.fullYear, recurDate.month, 
					recurDate.date, startHours, startMinutes);
				newData.dtEnd = new Date(newData.dtStart.time + duration);
				
				// fix DST issues
				if (newData.schedulingData.allDay)
					newData.dtEnd = DateUtils.getClosestDay(newData.dtEnd);
				
				// insert at the proper index
				var newIndex:int = returnSortedIndex(newData);
				store.splice(newIndex, 0, newData);
			}
			
			// update the infiniteRecurrenceArray
			recurItem.lastRecurDate = recurDates[recurDates.length - 1];
		}
	}
	
	/**
	 * 	@private
	 */
	protected function returnSortedIndex(item:StoreData):int
	{
		var len:int = store.length;
		
		for (var i:int = 0; i < len; i++)
		{
			var storeItem:StoreData = store[i];
			var position:Number = sortStore(item, storeItem);
			
			if (position == -1)
				return i;
		}
		
		return i;
	}
	
	/**
	 * 	@private
	 */
	protected function addToStore(item:SchedulingData, 
								  sort:Boolean = true):void
	{
		// skip if the calendar is not enabled
		if (!item.calendar.enabled)
			return;
		 
		// if allday, make sure the times are scrubbed
		if (item.allDay)
		{
			item.dtStart = DateUtils.scrubTime(item.dtStart, false);
			item.dtEnd = DateUtils.scrubTime(item.dtEnd, false);
		}
		
		// non-recurrence items
		if (!item.rDate && !item.freq)
		{
			var newData:StoreData = new StoreData();
			newData.schedulingData = item;
			newData.dtStart = item.dtStart;
			newData.dtEnd = item.dtEnd;
			
			// if sorted, insert in the proper sorted index
			if (sort)
			{
				var newIndex:int = returnSortedIndex(newData);
				store.splice(newIndex, 0, newData);
			}
			else
			{
				store.push(newData);
			}
		}	
			// recurrence items
		else
		{
			var duration:Number = item.dtEnd.time - item.dtStart.time;
			var recurDates:Array = iCalendarRecurrence.generateRecurrenceDates(item);
			
			// if the recurrence generates no events, delete the SchedulingData
			// from the dataProvider
			if (recurDates.length == 0)
			{
				var index:int = dataProvider.getItemIndex(item);
				if (index < 0)
					return;
				
				dataProvider.removeItemAt(index);
				return;
			}
			
			// recurDates times are scrubbed, must add them back in
			var startHours:Number = item.dtStart.hours;
			var startMinutes:Number = item.dtStart.minutes;
			
			// build the data items
			for each (var recurDate:Date in recurDates)
			{
				newData = new StoreData();
				newData.schedulingData = item;
				newData.dtStart = new Date(recurDate.fullYear, recurDate.month, 
					recurDate.date, startHours, startMinutes);
				newData.dtEnd = new Date(newData.dtStart.time + duration);
				
				// fix DST issue
				if (newData.schedulingData.allDay)
					newData.dtEnd = DateUtils.getClosestDay(newData.dtEnd);
				
				// if sorted, insert in the proper sorted index
				if (sort)
				{
					newIndex = returnSortedIndex(newData);
					store.splice(newIndex, 0, newData);
				}
				else
				{
					store.push(newData);
				}
			}
			
			// update the infiniteRecurrenceArray; only add infinite items and 
			// only add the last recurring date
			if (!item.until && !item.count && !item.rDate)
			{
				infiniteRecurrenceVector.push(new InfiniteRecurrenceData(item, 
					recurDates[recurDates.length - 1]));
			}
		}
	}
	
	//--------------------------------------------------------------------------
	//
	//  Overridden event handlers
	//
	//--------------------------------------------------------------------------
	
	/**
	 *  @private
	 */
	override protected function dataProvider_collectionChangeHandler(event:Event):void
	{
		if (swallowCollection)
			return;
		
		super.dataProvider_collectionChangeHandler(event);
		
		dataProviderDirty = true;
		invalidateProperties();		
	}
	
	//--------------------------------------------------------------------------
	//
	//  Event handlers
	//
	//--------------------------------------------------------------------------

	/**
	 *  @private
	 */
	protected function eventHandler(event:Event):void
	{
		switch (event.type)
		{
			
			case CollectionEvent.COLLECTION_CHANGE:
				schedulingDataProviderDirty = true;
				invalidateProperties();
				
				break;
			
			case IndexChangeEvent.CHANGE:
				if (selectedItem)
				{
					var newSelection:Vector.<SelectedCalendarData> = new Vector.<SelectedCalendarData>();
					newSelection.push(new SelectedCalendarData(selectedItem.dtStart, 
						selectedItem.dtEnd, selectedItem.schedulingData))
					_selectedData = newSelection;
				}
				else
				{
					_selectedData = null;
				}
				
				dispatchEvent(new CalendarEvent(CalendarEvent.SELECTED_DATA_CHANGE));
				
				break;
			
		}
		
	}
	
}
	
}