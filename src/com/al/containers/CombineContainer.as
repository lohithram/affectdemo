/**
 * Created by lram on 24/02/2015.
 */
package com.al.containers
{

	import com.al.combine.CombineDragManager;
	import com.al.combine.CombineFullScreenManager;
	import com.al.combine.CombineResizeManager;
	import com.al.combine.ICombine;
	import com.al.components.CombineDropPlaceHolder;
	import com.al.config.CombineConfig;
	import com.al.descriptors.ICombineDescriptor;
	import com.al.descriptors.TileContainerDescriptor;
	import com.al.factory.TileContainerFactory;
	import com.al.layouts.CombineLayout;
	import com.al.model.PropertyDependency;
	import com.al.pms.CombinePM;
	import com.al.tile.ICombineElement;
	import com.al.tile.ITileDropIndicator;
	import com.al.util.ContextUtil;
	import com.al.util.EventUtils;

	import flash.events.Event;

	import mx.core.ClassFactory;
	import mx.core.IVisualElementContainer;
    import mx.events.FlexEvent;
    import mx.styles.IStyleClient;

	import spark.components.Group;
	import spark.components.NavigatorContent;
	import spark.events.ElementExistenceEvent;

	/**
     *
     */
    public class CombineContainer extends NavigatorContent implements ICombine
    {
		public var pm: CombinePM;

        public var fullScreenGroup: Group;

		private var resizeManager: CombineResizeManager;
		
        public function CombineContainer() {

            //set layout
            layout = new CombineLayout();

            //set Drag manager
            new CombineDragManager().manageCombine(this);

            // set FullScreen manager
            new CombineFullScreenManager().manageCombine(this);

			//set Resize manager
			resizeManager = new CombineResizeManager();
			resizeManager.manageCombine(this);

			//set defaults
            setStyle("backgroundColor", 0xEFEFEF);

            _dropIndicatorFactory = new ClassFactory(CombineDropPlaceHolder);

			ContextUtil.buildContext(CombineConfig, this,
					[ new PropertyDependency("pm", CombinePM)]);

            addListeners();

        }
		
        //--------------------------------------------------------------------------
        //
        //  Properties
        //
        //--------------------------------------------------------------------------
		//---------------------------------------------------
		// animate
		//---------------------------------------------------
		var _animate: Boolean;
		public function get animate():Boolean {

			return _animate;
		}

		public function set animate(value:Boolean):void {

			_animate = value;
            if(layout as CombineLayout)
                (layout as CombineLayout).animate = value;
		}
		//---------------------------------------------------
		// resizeGroup
		//---------------------------------------------------
		private var _resizeGroup: Group;
		[SkinPart(required="true")]
		public function get resizeGroup(): Group{
			return _resizeGroup;
		}
		
		public function set resizeGroup(value: Group): void{
			_resizeGroup = value;	
		}

		//---------------------------------------------------
		// dropIndicatorFactory
		//---------------------------------------------------
        private var _dropIndicatorFactory: ClassFactory;
        public function get dropIndicatorFactory(): ClassFactory{
            return _dropIndicatorFactory;
        }

		//---------------------------------------------------
		// combineDescriptor
		//---------------------------------------------------
		private var descriptorChanged: Boolean;
        private var _descriptor: ICombineDescriptor;
        public function set combineDescriptor(value: ICombineDescriptor): void {

            if(_descriptor != value){

				_descriptor = value;
                descriptorChanged = true;
                invalidateProperties();
            }
        }
		
		public function get combineDescriptor(): ICombineDescriptor {
			return _descriptor;
		}
		
        //--------------------------------------------------------------------------
        //
        //  Public methods
        //
        //--------------------------------------------------------------------------
		public function calculateVisualIndices(): Array {
			
			var sortedElements: Array = [];
			if(numElements > 0 ){
			
				for(var i: int=0; i<numElements; ++i){
					
					var element: ICombineElement = getElementAt(i) as ICombineElement;
					if(element && !(element is ITileDropIndicator))
						sortedElements.push(element);
				}
				sortedElements.sort(visualHierarchyFunction);
				i = 0;
				for each(element in sortedElements)
					element.visualIndex = i++;
			}
				
			return sortedElements;
		}

        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------
		override protected function partAdded(partName:String, instance:Object):void {
			
			super.partAdded(partName, instance);
			if(instance == resizeGroup){
			
				var combineLayout: CombineLayout = new CombineLayout();
				combineLayout.GAP = 0;
				resizeGroup.layout = combineLayout;
			}
			else if(instance == contentGroup){
				
				contentGroup.addEventListener(ElementExistenceEvent.ELEMENT_ADD, onContentGroupElementAdd);
			}
		}
		
        override protected function commitProperties(): void {

            super.commitProperties();

            if(descriptorChanged){
                
				//removeAllElements();
				createElements();

                descriptorChanged = false;
                pm.setCombineDescriptor(combineDescriptor);
            }
			
			resizeManager.commitProperties();
        }

        //--------------------------------------------------------------------------
        //
        //  Event handlers
        //
        //--------------------------------------------------------------------------
		private function onContentGroupElementAdd(event: Event): void{
			
			invalidateProperties();
			invalidateDisplayList();
			//EventUtils.removeListener(event, onContentGroupElementAdd);
		}

        private function onComplete(event: Event): void{

            EventUtils.removeListener(event, onComplete);
            if(pm)
                pm.onCombineCreation(this);
        }

        private function onElementAdd(event: ElementExistenceEvent): void{

			var tile: TileContainer = event.element as TileContainer;
			if(tile) {

				if (pm)
					pm.addNewTile(tile);

				// QUICK AND DIRTY to sync model with view
				if (combineDescriptor.tileContainers.getItemIndex(tile.tileDescriptor) == -1)
					combineDescriptor.tileContainers.addItem(tile.tileDescriptor);
			}
        }

        private function onElementRemove(event: ElementExistenceEvent): void{

			var tile: TileContainer = event.element as TileContainer;
			if(tile) {

				if (pm)
					pm.tileRemoved(tile);

				// QUICK AND DIRTY to sync model with view
				if(combineDescriptor.tileContainers.getItemIndex(tile.tileDescriptor) != -1)
					combineDescriptor.tileContainers.removeItem(tile.tileDescriptor);
			}
        }
		
        //--------------------------------------------------------------------------
        //
        //  Private
        //
        //--------------------------------------------------------------------------

        private function addListeners(): void{

            addEventListener(FlexEvent.CREATION_COMPLETE, onComplete);
            addEventListener(ElementExistenceEvent.ELEMENT_ADD, onElementAdd);
            addEventListener(ElementExistenceEvent.ELEMENT_REMOVE, onElementRemove);
        }

        private function removeListeners(): void{

            removeEventListener(ElementExistenceEvent.ELEMENT_ADD, onElementAdd);
            removeEventListener(ElementExistenceEvent.ELEMENT_REMOVE, onElementRemove);
        }



        private function createElements(): void{

            for each( var descriptor: TileContainerDescriptor in _descriptor.tileContainers.source ){

                if(!descriptor) continue;
				var tileContainer: ICombineElement = TileContainerFactory.createTileContainer(descriptor);
                addElement(tileContainer);
            }
        }
		
		private function visualHierarchyFunction(elementA: ICombineElement, elementB: ICombineElement): int{
			
			if(elementA.reducedY > elementB.reducedY){
				return 1;
			}
			else if(elementA.reducedY < elementB.reducedY){
				return -1;
			}
			else{
				//both elements reducedY are equal
				if(elementA.reducedX > elementB.reducedX)
					return 1;
				else if(elementA.reducedX < elementB.reducedX)
					return -1;
				else
					return 0;
			}
		}
	}
}
