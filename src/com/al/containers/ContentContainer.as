package com.al.containers
{
	import com.al.collaboration.annotation.pms.ContentContainerPM;
	import com.al.collaboration.config.CollaborationConfig;
	import com.al.collaboration.view.CollaborationLayer;
	import com.al.enums.ContentType;
	import com.al.events.ContentUnitChangeEvent;
	import com.al.events.DependenciesSatisfiedEvent;
	import com.al.model.ConceptBoardTemplate;
	import com.al.model.PropertyDependency;
	import com.al.model.TileContainerModel;
	import com.al.model.TileContentDescriptor;
	import com.al.util.ContextUtil;
	import com.al.util.EventUtils;

	import flash.events.Event;
	import flash.events.MouseEvent;

	import mx.binding.utils.BindingUtils;
	import mx.core.ClassFactory;
	import mx.core.IVisualElement;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;

	import spark.components.Image;
	import spark.components.SkinnableContainer;

	public class ContentContainer extends SkinnableContainer
	{
		
		[SkinPart]
		public var collaborationLayer: CollaborationLayer;
		
		[Bindable]
		public var contentX: Number;

		[Bindable]
		public var contentY: Number;

		[Bindable]
		public var contentScaledWidth: Number;

		[Bindable]
		public var contentScaledHeight: Number;

        public var pm: ContentContainerPM;

		public function ContentContainer(): void {
			
			addEventListener(ResizeEvent.RESIZE, onResize);
            addEventListener(DependenciesSatisfiedEvent.DEPENDENCIES_SATISFIED, onDependencyInjected);
            ContextUtil.buildContext(CollaborationConfig, this,
                    [new PropertyDependency("pm", ContentContainerPM)]);
		}

		//--------------------------------------------------------------------------
		//
		//  Properties
		//
		//--------------------------------------------------------------------------
        //---------------------------------------------------
        // tileModel
        //---------------------------------------------------
        private var _tileModel: TileContainerModel;
        private var tileContainerModelChanged: Boolean;
        public function get tileModel(): TileContainerModel{
            return _tileModel;
        }

        public function set tileModel(value: TileContainerModel): void {

            if(value != _tileModel){

                _tileModel = value;
                tileContainerModelChanged = true;
                invalidateProperties();
                if(pm)
                    pm.tileContainerModel = tileModel;
            }
        }

		//---------------------------------------------------
		// contentDescriptor
		//---------------------------------------------------
		private var modelChanged: Boolean = false;
		private var _model: TileContentDescriptor;
		public function get contentDescriptor(): TileContentDescriptor{
			return _model;
		}
		
		public function set contentDescriptor(value: TileContentDescriptor): void {
			
			if(value != _model){

				_model = value;
				modelChanged = true;
				invalidateProperties();
			}
		}

        //---------------------------------------------------
        // zoomFactor
        //---------------------------------------------------
        // value of 1.0 mean content will be displayed at actual size
        private var _zoomFactor: Number = 1.0;

        public function get zoomFactor(): Number {
            return _zoomFactor;
        }

        public function set zoomFactor(value: Number): void {

            if(_zoomFactor == value) return;

            _zoomFactor = value;
            invalidateDisplayList();
        }
		
		//--------------------------------------------------------------------------
		//
		//  Overrides
		//
		//--------------------------------------------------------------------------
		override protected function partAdded(partName:String, instance:Object):void{
			
			super.partAdded(partName, instance);

			if(instance == collaborationLayer){

				collaborationLayer.addEventListener(MouseEvent.MOUSE_OUT, hideCursor);
				collaborationLayer.addEventListener(MouseEvent.MOUSE_OVER, showCursor);
			}
		}

		override protected function commitProperties():void{
			
			super.commitProperties();
			if( modelChanged ){
				
				removeAllElements();
				if(contentDescriptor.contentFactory){
					
					// Can have more than one content
					var tileContent: IVisualElement = createContentFactoryInstance(contentDescriptor.contentFactory);
					addElement(tileContent);

					if(tileContent is HTMLContainer){

						htmlContainer = tileContent as HTMLContainer;
						htmlContainer.contentDescriptor = contentDescriptor;
						htmlContainer.addEventListener(Event.COMPLETE, onHTMLLoadComplete);
						htmlContainer.addEventListener(ContentUnitChangeEvent.CHANGE, onContentUnitChange);
					}
                    else if(tileContent is HTML5Container){

                        (tileContent as HTML5Container).location = contentDescriptor.contentUrl;
                    }
					else if(tileContent is ConceptBoard){

						conceptBoard = tileContent as ConceptBoard;
						setUpConceptBoard();
					}
                    else if(tileContent is MultiPageContentContainer){

                        multiPageContainer = tileContent as MultiPageContentContainer;
                        setupMultipage();
                    }
                    else if(tileContent is ConceptBoard){

                        conceptBoard = tileContent as ConceptBoard;
                        setUpConceptBoard();
                    }
				}

				else if(contentDescriptor.contentType == ContentType.MULTI_PAGE){
					
					createAndAddMultiPageContentContainer();
				}
                else if(contentDescriptor.contentType == ContentType.NOTE){

                    createNote();
                }
				else if(contentDescriptor.contentType == ContentType.CONCEPT_BOARD){

					createConceptBoard();
				}
				else if(contentDescriptor.contentUrl){

                    createAndAddImage(contentDescriptor.contentUrl);
                }
				
				if(collaborationLayer)
					collaborationLayer.contentDescriptor = contentDescriptor;
				modelChanged = false;
			}

            if(tileContainerModelChanged){

                for(var i: int = 0; i<numElements; ++i){

                    var element: IContentContainer = getElementAt(i) as IContentContainer;
                    if(element)
                        element.tileContainerModel = tileModel;
					else if(element is ConceptBoard)
						(element as ConceptBoard).tileContainerModel = tileModel;
                }
                tileContainerModelChanged = false;
            }

			// Do not show collaboration layer if there is no content...
			/*if(collaborationLayer)
				collaborationLayer.visible =
						(contentDescriptor != null && contentDescriptor.contentType != ContentType.NO_CONTENT);*/
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			
			super.updateDisplayList(unscaledWidth, unscaledHeight);

            if(unscaledHeight <= 0 || unscaledWidth <= 0)
                return;

			// REVISIT: Can extract as interface. There is a pattern here !
			if(imageLoaded){

                if(isNaN(zoomFactor)){

                    // This will result in displaying the image at its actual size.
                    var contentScale:Number =
                            Math.min(unscaledWidth / imgCtrl.sourceWidth, unscaledHeight / imgCtrl.sourceHeight);
                    pm.tileContainerModel.zoomFactor = zoomFactor = contentScale;
                }

                imgCtrl.width = contentScaledWidth = (imgCtrl.sourceWidth) * zoomFactor;
                imgCtrl.height = contentScaledHeight = (imgCtrl.sourceHeight) * zoomFactor;

                imgCtrl.x = contentX = Math.max(0, (unscaledWidth - contentScaledWidth)/2);
				imgCtrl.y = contentY = Math.max(0, (unscaledHeight - contentScaledHeight)/2);
			}
			if(htmlContentLoaded){

				htmlContainer.width = unscaledWidth;
				htmlContainer.height = unscaledHeight;
			}
			if(multiPageLoaded){
				
				multiPageContainer.width = unscaledWidth;
				multiPageContainer.height = unscaledHeight;
			}
            if(conceptBoardLoaded){

                conceptBoard.width = unscaledWidth;
                conceptBoard.height = unscaledHeight;
            }

            if(noteReady)
            {
                note.width = unscaledWidth;
                note.height = unscaledHeight;
            }
			
			// In reality this should not happen, only being defensive for demo
			if(isNaN(contentX)) contentX = 0;
			if(isNaN(contentY)) contentY = 0;
			if(isNaN(contentScaledWidth)) contentScaledWidth = 100;
			if(isNaN(contentScaledHeight)) contentScaledHeight = 100;
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event Listeners
		//
		//--------------------------------------------------------------------------
		private function onDependencyInjected(event: Event): void {

            EventUtils.removeListener(event, onDependencyInjected);
            pm.tileContainerModel = tileModel;
        }

		protected function onResize(event: ResizeEvent): void{
			
			//trace("Resized content container - " + contentDescriptor ? contentDescriptor.id : null);
			invalidateDisplayList();
		}
		
		private var htmlContentLoaded: Boolean;
		protected var htmlContainer: HTMLContainer;
		protected function onHTMLLoadComplete(event: Event): void {
			
			htmlContentLoaded = true;
			// collaborationLayer.visible = tileModel.isCollaborationEnabled;
			// QUICK AND DIRTY
			// #HACK
			if(!htmlContainer.containsElement(collaborationLayer))
				htmlContainer.medGroup.addElement(collaborationLayer);

			BindingUtils.bindSetter(updateDimensionProperties2, htmlContainer, 'contentX');
			BindingUtils.bindSetter(updateDimensionProperties2, htmlContainer, 'contentY');
			BindingUtils.bindSetter(updateDimensionProperties2, htmlContainer, 'contentScaledWidth');
			BindingUtils.bindSetter(updateDimensionProperties2, htmlContainer, 'contentScaledHeight');
			updateDimensionProperties2(null)
			invalidateDisplayList();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Private methods
		//
		//--------------------------------------------------------------------------
		private function createContentFactoryInstance(contentFactory: ClassFactory): IVisualElement{
			
			var tileContent: IVisualElement = contentFactory.newInstance();
			tileContent.percentWidth = 100;
			tileContent.percentHeight = 100;
			
			return tileContent;
		}
		
		protected var imgCtrl: Image;
		protected var imageLoaded: Boolean;
		private function createAndAddImage(url: String): void{
			
			imgCtrl = new Image();
            imgCtrl.source = url;
			imgCtrl.addEventListener(FlexEvent.READY, onImageReady);

			addElement(imgCtrl);
		}
		
		protected var multiPageContainer: MultiPageContentContainer;
		private function createAndAddMultiPageContentContainer(): void{
			
			multiPageContainer = new MultiPageContentContainer();

			setupMultipage();
			addElement(multiPageContainer);
		}

        public function setupMultipage():void
        {
            multiPageContainer.contentDescriptor = contentDescriptor;
            multiPageContainer.addEventListener(FlexEvent.READY, onMultiPageContainerReady);
            multiPageContainer.addEventListener(ContentUnitChangeEvent.CHANGE, onContentUnitChange);
            BindingUtils.bindSetter(updateDimensionProperties, multiPageContainer, 'contentX');
            BindingUtils.bindSetter(updateDimensionProperties, multiPageContainer, 'contentY');
            BindingUtils.bindSetter(updateDimensionProperties, multiPageContainer, 'contentScaledWidth');
            BindingUtils.bindSetter(updateDimensionProperties, multiPageContainer, 'contentScaledHeight');
        }

		protected var conceptBoard: ConceptBoard
        protected var conceptBoardLoaded: Boolean;
		private function createConceptBoard(): void{

			conceptBoard = new ConceptBoard();
			conceptBoard.model = new ConceptBoardTemplate();
			setUpConceptBoard();
			addElement(conceptBoard);
		}

		private function setUpConceptBoard(): void{

			conceptBoard.tileContainerModel = tileModel;
			conceptBoard.addEventListener(ContentUnitChangeEvent.CHANGE, onContentUnitChange);
			BindingUtils.bindSetter(updateDimensionProperties1, conceptBoard, 'contentX');
			BindingUtils.bindSetter(updateDimensionProperties1, conceptBoard, 'contentY');
			BindingUtils.bindSetter(updateDimensionProperties1, conceptBoard, 'contentScaledWidth');
			BindingUtils.bindSetter(updateDimensionProperties1, conceptBoard, 'contentScaledHeight');
			conceptBoardLoaded = true;
		}

        private var note:NoteContainer;
        private var noteReady:Boolean;
        private function createNote():void
        {
            note = new NoteContainer();
            note.contentDescriptor = contentDescriptor;
            note.addEventListener(FlexEvent.CREATION_COMPLETE, note_creationCompleteHandler);
            addElement(note);
        }


		//--------------------------------------------------------------------------
		//
		//  Event handlers
		//
		//--------------------------------------------------------------------------
		/**
		 * @private
		 */
		private var cursorID: int;
		protected function showCursor(p_evt:MouseEvent):void
		{
			if (pm && pm.cursorClass) {

				cursorManager.removeAllCursors();
				cursorID =
						cursorManager.setCursor(pm.cursorClass, 1,
											    pm.cursorXOffset, pm.cursorYOffset);
			}
			else{

				hideCursor(p_evt);
			}
		}

		/**
		 * @private
		 */
		protected function hideCursor(p_evt:MouseEvent=null):void
		{
			if (cursorID !=- 1) {

				cursorManager.removeCursor(cursorID);
				cursorID = -1;
			}
		}

        private function note_creationCompleteHandler(event:FlexEvent):void
        {
            noteReady = true;
            invalidateDisplayList();
        }

        protected function onContentUnitChange(event: ContentUnitChangeEvent): void{

			if(htmlContainer)
				tileModel.isCollaborationEnabled = false;

            collaborationLayer.pm.setCurrentContentUnit( event.contentUnit );
        }

		protected var multiPageLoaded: Boolean;
		private function onMultiPageContainerReady(event: FlexEvent): void {
			
			multiPageLoaded = true;
			updateDimensionProperties();
		}
		
		private function onImageReady(event: FlexEvent): void {
			
			imageLoaded = true;
			invalidateDisplayList();
		}
		
		/**
		 * TODO: Have to build a much robust way of calculating the dimensions 
		 * @param value
		 * 
		 */
		private function updateDimensionProperties(value: Object=null): void {
			
			contentScaledWidth = multiPageContainer.contentScaledWidth;
			contentScaledHeight = multiPageContainer.contentScaledHeight;
			contentX = multiPageContainer.contentX;
			contentY = multiPageContainer.contentY;
		}

        /**
		 * TODO: Have to build a much robust way of calculating the dimensions
		 * @param value
		 *
		 */
		private function updateDimensionProperties1(value: Object=null): void {

			contentScaledWidth = conceptBoard.contentScaledWidth;
			contentScaledHeight = conceptBoard.contentScaledHeight;
			contentX = conceptBoard.contentX;
			contentY = conceptBoard.contentY;
		}
        /**
		 * TODO: Have to build a much robust way of calculating the dimensions
		 * @param value
		 *
		 */
		private function updateDimensionProperties2(value: Object=null): void {

			contentScaledWidth = htmlContainer.contentScaledWidth;
			contentScaledHeight = htmlContainer.contentScaledHeight;
			contentX = htmlContainer.contentX;
			contentY = htmlContainer.contentY;
		}
    }
}