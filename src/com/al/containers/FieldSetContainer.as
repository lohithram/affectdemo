/**
 * Created by lohithram on 20/05/15.
 */
package com.al.containers {

    import ardisia.components.fieldSet.FieldSet;

    import com.al.components.EditableLabel;

    public class FieldSetContainer extends FieldSet{

        [SkinPart]
        public var editableLabel: EditableLabel;
    }
}
