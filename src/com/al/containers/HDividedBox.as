/**
 * Created by lohithram on 17/07/15.
 */
package com.al.containers {

    import mx.containers.HDividedBox;
    import mx.containers.dividedBoxClasses.BoxDivider;
    import mx.core.mx_internal;
    import mx.managers.CursorManager;
    import mx.managers.CursorManagerPriority;

    use namespace mx_internal;

    public class HDividedBox extends mx.containers.HDividedBox {

        private var cursorID: int;

        override mx_internal function changeCursor(divider:BoxDivider):void {

            cursorID = cursorManager.currentCursorID;
            if (cursorID == CursorManager.NO_CURSOR)
            {
                var cursorClass:Class = getStyle("horizontalDividerCursor") as Class;
                var offsets: Array = getStyle("cursorOffsets") as Array;

                cursorID = cursorManager.setCursor(cursorClass,
                        CursorManagerPriority.HIGH, offsets[0], offsets[1]);
            }
        }

        /**
         *  @private
         */
        override mx_internal function restoreCursor():void
        {
            if (cursorID != CursorManager.NO_CURSOR)
            {
                cursorManager.removeCursor(cursorID);
                cursorID = CursorManager.NO_CURSOR;
            }
        }
    }
}
