/**
 * Created by lohithram on 15/05/15.
 */
package com.al.containers {

    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.media.StageWebView;

    import spark.components.Group;

    public class HTML5Container extends Group {

        private var _location:String;
        private var locationChanged:Boolean;

        public function get location():String {

            return _location;
        }

        public function set location(value:String):void {

            if (value != location) {

                _location = value;
                locationChanged = true;
                invalidateProperties();
            }
        }

        public var webView:StageWebView;

        override protected function commitProperties():void {

            super.commitProperties();
            if (locationChanged) {

                if (!webView) {
                    webView = new StageWebView(true);
                    webView.stage = this.stage;
                }
                locationChanged = false;

                webView.loadURL(location);
            }
        }

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {

            super.updateDisplayList(unscaledWidth, unscaledHeight);

            var stagePoint:Point = localToGlobal(new Point(0, 0));
            webView.viewPort = new Rectangle(stagePoint.x, stagePoint.y, unscaledWidth, unscaledHeight);
        }
    }
}
