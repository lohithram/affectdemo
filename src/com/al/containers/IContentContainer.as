/**
 * Created by lohithram on 22/05/15.
 */
package com.al.containers {

    import com.al.model.TileContainerModel;
    import com.al.model.TileContentDescriptor;

    /**
     * Containers expecting to receive the content descriptor and model should implement this interface
      */
    public interface IContentContainer {

        function get contentDescriptor(): TileContentDescriptor;
        function set contentDescriptor(value: TileContentDescriptor):void;

        function get tileContainerModel(): TileContainerModel;
        function set tileContainerModel(value: TileContainerModel):void;
    }
}
