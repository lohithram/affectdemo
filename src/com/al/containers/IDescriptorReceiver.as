/**
 * Created by lohithram on 22/05/15.
 */
package com.al.containers {

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.model.TileContainerModel;
    import com.al.model.TileContentDescriptor;

    /**
     * Containers expecting to receive the descriptor and model should implement this interface
      */
    public interface IDescriptorReceiver {

        function set contentDescriptor(value: TileContainerDescriptor):void;

    }
}
