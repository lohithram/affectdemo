/**
 * Created by lohithram on 22/05/15.
 */
package com.al.containers {

    import com.al.model.TileContainerModel;
    import com.al.model.TileContentDescriptor;
    import com.al.model.TileDataModel;

    /**
     * Containers expecting to receive the content descriptor and model should implement this interface
      */
    public interface ITileModelReceiver {

        function get tileDataModel(): TileDataModel;
        function set tileDataModel(value: TileDataModel):void;
    }
}
