/**
 * Created by lram on 09/02/2015.
 */
package com.al.containers
{

    import mx.core.UIComponent;

    import spark.components.Group;

    import spark.layouts.BasicLayout;

    /**
     * This class provides easy way to layout children at the top, bottom, left, right and center
     * Also handles drag and drop of 'draggable' children.
     */
    public class MainContainer extends Group
    {

        public var centreContainer: UIComponent;

        public var leftContainer: UIComponent;
        public var rightContainer: UIComponent;
        public var topContainer: UIComponent;
        public var bottomContainer: UIComponent;

        public function MainContainer() {
            layout = new BasicLayout();
        }

        /**
         * Order of children
         * centreContainer - added first
         * left, right and bottom - goes next
         * top  - sits over all of the above
         *
         * The above order matters specifically while resizing..
         * For example you want the topContainer which might contain header and navigation elements
         * always overlap the centre container.
         */
        override protected function createChildren(): void{

            if(centreContainer)
                addElement(centreContainer);
            if(leftContainer)
                addElement(leftContainer);
            if(rightContainer)
                addElement(rightContainer);
            if(bottomContainer)
                addElement(bottomContainer);
            if(topContainer)
                addElement(topContainer);

            layoutDisturbed = true;
        }

        private var layoutDisturbed: Boolean;

        /**
         * Set the constraints on each container appropriately depending on their position
         * The Basic Layout used by this container will ultimately size and position the chile elements.
         */
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			
			super.updateDisplayList(unscaledWidth, unscaledHeight);

            if(layoutDisturbed)
            {
                if (topContainer)
                {
                    clearBoundsConstraints(topContainer);
                    topContainer.top = 0;
                }
                if (bottomContainer)
                {
                    clearBoundsConstraints(bottomContainer);
                    bottomContainer.bottom = 0;
                }
                if (leftContainer)
                {
                    clearBoundsConstraints(leftContainer);
                    leftContainer.left = 0;
                    leftContainer.top = topContainer ? topContainer.height : 0;
                    leftContainer.bottom = bottomContainer ? bottomContainer.height : 0;
                }
                if (rightContainer)
                {
                    clearBoundsConstraints(rightContainer);
                    rightContainer.right = 0;
                    rightContainer.top = topContainer ? topContainer.height : 0;
                    rightContainer.bottom = bottomContainer ? bottomContainer.height : 0;
                }
                if (centreContainer)
                {
                    centreContainer.top = topContainer ? topContainer.height : 0;
                    centreContainer.left = leftContainer ? leftContainer.width : 0;
                    centreContainer.right = rightContainer ? rightContainer.width : 0;
                    centreContainer.bottom = bottomContainer ? bottomContainer.height : 0;
                }
                layoutDisturbed = false;
            }
        }

        private function clearBoundsConstraints(element: UIComponent): void{

            element.top = element.bottom = element.left = element.right = NaN;
        }

    }
}
