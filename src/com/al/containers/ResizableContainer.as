/**
 * Created by lohithram on 14/05/15.
 */
package com.al.containers {

    import ardisia.components.pane.events.PaneEvent;
    import ardisia.dataTypes.CompassQuadrant;
    import ardisia.managers.cursorManager.CursorManager;
    import ardisia.managers.cursorManager.CursorPriority;
    import ardisia.managers.cursorManager.DefaultCursors;

    import flash.display.DisplayObject;

    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import spark.components.Group;

    [Event(name="resizeFinished", type="flash.events.Event")]
    public class ResizableContainer extends Group {

        public var allowResizeDirections: Array;
        /**
         * List of components over which if mouse pointer is present, we wont try to resize the container
         */
        public var excludeComponents: Array;

        /**
         * 	The threshold within the outer boundaries of the transformRegionPart
         * 	that will trigger a pane resize, rather than a drag.
         *
         * 	@default 7
         */
        public var resizeThreshold:Number = 7;

        /**
         *  @private
         *
         * 	The resize region currently hovered.
         *
         *  <p>Possible values: north, northEast, east, southEast, south, southWest,
         *  west, northWest.</p>
         */
        protected var hoveredResizeLocation:String;

        /**
         *  @private
         *
         * 	The resize region currently being resized.
         *
         *  <p>Possible values: north, northEast, east, southEast, south, southWest,
         *  west, northWest.</p>
         */
        protected var resizingLocation:String;

        /**
         * 	@private
         *
         * 	Resize is in progress.
         */
        protected var resizeInProgress:Boolean;
        /**
         *  @private
         *
         * 	Initial pane rectangle when resizing.
         */
        protected var resizeRectangle:Rectangle;

        /**
         *  @private
         *
         * 	Mouse down point used for dragging.
         */
        protected var mouseDownPt:Point;

        override protected function childrenCreated():void {

            super.childrenCreated();
            addEventListener(MouseEvent.ROLL_OUT,
                    transformRegionPartHandler);
            addEventListener(MouseEvent.MOUSE_MOVE,
                    transformRegionPartHandler);
            addEventListener(MouseEvent.MOUSE_DOWN,
                    transformRegionPartHandler);

            if(!allowResizeDirections)
                allowResizeDirections = [CompassQuadrant.NORTH_EAST,
                  CompassQuadrant.SOUTH_WEST,
                  CompassQuadrant.SOUTH_EAST,
                  CompassQuadrant.NORTH_WEST,
                  CompassQuadrant.SOUTH,
                  CompassQuadrant.EAST,
                  CompassQuadrant.NORTH,
                  CompassQuadrant.WEST];
        }

        /**
         * Functionality from Ardisia Pane container
         * @param event
         */
        protected function transformRegionPartHandler(event:Event):void
        {
            var mouseEvt:MouseEvent = event as MouseEvent;

            switch (event.type)
            {

                case MouseEvent.ROLL_OUT:
                    hoveredResizeLocation = null;
                    CursorManager.removeGroup("paneResizeRollOverCursorGroup");

                    break;

                case MouseEvent.MOUSE_MOVE:
                    var deltaX:Number;
                    var deltaY:Number;
                    if (resizeInProgress)
                    {
                        // bound mouse event to the screen
                        var bounds:Rectangle = systemManager.getVisibleApplicationRect();

                        // bound the deltas
                        deltaX = mouseEvt.stageX - mouseDownPt.x;
                        deltaY = mouseEvt.stageY - mouseDownPt.y;

                        var upperLeftX:Number = resizeRectangle.x;
                        var upperLeftY:Number = resizeRectangle.y;
                        var lowerRightX:Number = resizeRectangle.x + resizeRectangle.width;
                        var lowerRightY:Number = resizeRectangle.y + resizeRectangle.height;

                        // min dimensions come from measure() calling skin
                        // dimensions... for some reason not working, call skin
                        // sized directly
                        // if min/max is explicitly set on the host component, use it
                        // otherwise use the skin's min/max
                        var minW:Number = minWidth > 0 ? minWidth : 100;
                        var minH:Number = minHeight > 0 ? minHeight : 100;
                        var maxW:Number = maxWidth > 0 ? maxWidth : 4000;
                        var maxH:Number = maxHeight > 0 ? maxHeight : 3000;
                        if (resizingLocation == CompassQuadrant.EAST ||
                                resizingLocation == CompassQuadrant.NORTH_EAST ||
                                resizingLocation == CompassQuadrant.SOUTH_EAST)
                        {
                            lowerRightX = lowerRightX + deltaX;
                            lowerRightX = Math.min(lowerRightX, bounds.width);
                            lowerRightX = Math.min(lowerRightX, upperLeftX + maxW);
                            lowerRightX = Math.max(lowerRightX, upperLeftX + minW);
                        }
                        if (resizingLocation == CompassQuadrant.WEST ||
                                resizingLocation == CompassQuadrant.NORTH_WEST ||
                                resizingLocation == CompassQuadrant.SOUTH_WEST)
                        {
                            upperLeftX = upperLeftX + deltaX;
                            upperLeftX = Math.max(upperLeftX, 0);
                            upperLeftX = Math.max(upperLeftX, lowerRightX - maxW);
                            upperLeftX = Math.min(upperLeftX, lowerRightX - minW);
                        }
                        if (resizingLocation == CompassQuadrant.NORTH ||
                                resizingLocation == CompassQuadrant.NORTH_WEST ||
                                resizingLocation == CompassQuadrant.NORTH_EAST)
                        {
                            upperLeftY = upperLeftY + deltaY;
                            upperLeftY = Math.max(upperLeftY, 0);
                            upperLeftY = Math.max(upperLeftY, lowerRightY - maxH);
                            upperLeftY = Math.min(upperLeftY, lowerRightY - minH);
                        }
                        if (resizingLocation == CompassQuadrant.SOUTH ||
                                resizingLocation == CompassQuadrant.SOUTH_EAST ||
                                resizingLocation == CompassQuadrant.SOUTH_WEST)
                        {
                            lowerRightY = lowerRightY + deltaY;
                            lowerRightY = Math.min(lowerRightY, bounds.height);
                            lowerRightY = Math.min(lowerRightY, upperLeftY + maxH);
                            lowerRightY = Math.max(lowerRightY, upperLeftY + minH);
                        }

                        move(upperLeftX, upperLeftY);

                        // must use explicit dimensions since parented by non-flex
                        // container
                        width = lowerRightX - upperLeftX;
                        height = lowerRightY - upperLeftY;

                        mouseEvt.updateAfterEvent();

                        return;
                    }

                    // abort if the mouse button is down
                    if (mouseEvt.buttonDown)
                        return;

                    if(mouseOverExcludedComponents(mouseEvt.stageX, mouseEvt.stageY))
                        return;

                    var location:String = getResizeMouseLocation();
                    if (location != hoveredResizeLocation)
                    {
                        hoveredResizeLocation = location;
                        switch (hoveredResizeLocation)
                        {

                            case CompassQuadrant.NORTH_EAST:
                            case CompassQuadrant.SOUTH_WEST:
                                CursorManager.setCursor(DefaultCursors.NE_SW_RESIZE,
                                        CursorPriority.LOW, "paneResizeRollOverCursorGroup", false, systemManager);

                                break;

                            case CompassQuadrant.NORTH_WEST:
                            case CompassQuadrant.SOUTH_EAST:
                                CursorManager.setCursor(DefaultCursors.NW_SE_RESIZE,
                                        CursorPriority.LOW, "paneResizeRollOverCursorGroup", false, systemManager);

                                break;

                            case CompassQuadrant.NORTH:
                            case CompassQuadrant.SOUTH:
                                CursorManager.setCursor(DefaultCursors.N_S_RESIZE,
                                        CursorPriority.LOW, "paneResizeRollOverCursorGroup", false, systemManager);

                                break;

                            case CompassQuadrant.WEST:
                            case CompassQuadrant.EAST:
                                CursorManager.setCursor(DefaultCursors.E_W_RESIZE,
                                        CursorPriority.LOW, "paneResizeRollOverCursorGroup", false, systemManager);

                                break;

                            default:
                                CursorManager.removeGroup("paneResizeRollOverCursorGroup");

                                break;
                        }
                    }

                    break;

                case MouseEvent.MOUSE_DOWN:

                    if(mouseOverExcludedComponents(mouseEvt.stageX, mouseEvt.stageY))
                        return;

                    location = getResizeMouseLocation();
                    if(!location)
                        return;

                    switch (location)
                    {

                        case CompassQuadrant.NORTH_EAST:
                        case CompassQuadrant.SOUTH_WEST:
                            CursorManager.setCursor(DefaultCursors.NE_SW_RESIZE,
                                    CursorPriority.HIGH, "paneResizingCursorGroup", false, systemManager);

                            break;

                        case CompassQuadrant.NORTH_WEST:
                        case CompassQuadrant.SOUTH_EAST:
                            CursorManager.setCursor(DefaultCursors.NW_SE_RESIZE,
                                    CursorPriority.HIGH, "paneResizingCursorGroup", false, systemManager);

                            break;

                        case CompassQuadrant.NORTH:
                        case CompassQuadrant.SOUTH:
                            CursorManager.setCursor(DefaultCursors.N_S_RESIZE,
                                    CursorPriority.HIGH, "paneResizingCursorGroup", false, systemManager);

                            break;

                        case CompassQuadrant.WEST:
                        case CompassQuadrant.EAST:
                            CursorManager.setCursor(DefaultCursors.E_W_RESIZE,
                                    CursorPriority.HIGH, "paneResizingCursorGroup", false, systemManager);

                            break;
                    }

                    mouseDownPt = new Point(mouseEvt.stageX, mouseEvt.stageY);
                    resizeRectangle = new Rectangle(x, y, width, height);

                    systemManager.addEventListener(MouseEvent.MOUSE_MOVE,
                            transformRegionPartHandler);
                    systemManager.addEventListener(MouseEvent.MOUSE_UP,
                            transformRegionPartHandler);

                    resizeInProgress = true;

                    resizingLocation = location;

                    break;

                case MouseEvent.MOUSE_UP:

                    if (resizeInProgress)
                    {
                        resizeInProgress = false;
                        resizingLocation = null;

                        systemManager.removeEventListener(MouseEvent.MOUSE_MOVE,
                                transformRegionPartHandler);
                        systemManager.removeEventListener(MouseEvent.MOUSE_UP,
                                transformRegionPartHandler);

                        CursorManager.removeGroup("paneResizingCursorGroup");

                        dispatchEvent(new Event("resizeFinished"));
                        transformRegionPartHandler(new MouseEvent(MouseEvent.MOUSE_MOVE));
                    }

                    break;

            }
        }

        protected function mouseOverExcludedComponents(stageX:Number, stageY:Number): Boolean{

            for each(var displayObject: DisplayObject in excludeComponents){

                if(displayObject && displayObject.hitTestPoint(stageX, stageY)) {
                    CursorManager.removeGroup("paneResizeRollOverCursorGroup");
                    return true;
                }
            }
            return false;
        }

        /**
         *  Get the string description of where the mouse cursor is in relation to
         *  the edges of the dragRegionPart and the "resizeThreshold".
         *
         *  @return CompassQuadrant string
         */
        protected function getResizeMouseLocation():String
        {
            var mX:Number = this.mouseX;
            var mY:Number = this.mouseY;
            var contextWidth:Number = this.width;
            var contextHeight:Number = this.height;

            // abort if not over the resizeRegionPart
            if (contextWidth < 1 || contextHeight < 1 || mX < 0 || mY < 0 ||
                    mX > contextWidth || mY >  contextHeight)
                return null;

            if (mY <= resizeThreshold)
            {
                if (mX > contextWidth - resizeThreshold * 2)
                    var contextCursor:String = CompassQuadrant.NORTH_EAST;
                else if (mX <= resizeThreshold * 2)
                    contextCursor = CompassQuadrant.NORTH_WEST;
                else
                    contextCursor = CompassQuadrant.NORTH;
            }
            else if (mY >= contextHeight - resizeThreshold)
            {
                if (mX > contextWidth - resizeThreshold * 2)
                    contextCursor = CompassQuadrant.SOUTH_EAST;
                else if (mX <= resizeThreshold * 2)
                    contextCursor = CompassQuadrant.SOUTH_WEST;
                else
                    contextCursor = CompassQuadrant.SOUTH;
            }
            else if (mX <= resizeThreshold)
            {
                if (mY > contextHeight - resizeThreshold * 2)
                    contextCursor = CompassQuadrant.SOUTH_WEST;
                else if (mY <= resizeThreshold * 2)
                    contextCursor = CompassQuadrant.NORTH_WEST;
                else
                    contextCursor = CompassQuadrant.WEST;
            }
            else if (mX >= contextWidth - resizeThreshold)
            {
                if (mY > contextHeight - resizeThreshold * 2)
                    contextCursor = CompassQuadrant.SOUTH_EAST;
                else if (mY <= resizeThreshold * 2)
                    contextCursor = CompassQuadrant.NORTH_EAST;
                else
                    contextCursor = CompassQuadrant.EAST;
            }
            else
            {
                contextCursor = null;
            }

            // Not supporting the below resize areas
            if(allowResizeDirections.indexOf(contextCursor) == -1)
                contextCursor = null;


            return contextCursor;
        }
    }
}
