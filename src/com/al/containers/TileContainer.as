/**
 * Created by lram on 24/02/2015.
 */
package com.al.containers
{

    import com.al.collaboration.components.MaskGroup;
    import com.al.config.TileContainerConfig;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.dragging.StartDraggingStrategy;
    import com.al.enums.DragSourceFormats;
    import com.al.enums.TileDataFormats;
    import com.al.enums.TileDropAction;
    import com.al.enums.ViewState;
    import com.al.events.DependenciesSatisfiedEvent;
    import com.al.events.TileDropEvent;
    import com.al.events.TileFocusedEvent;
    import com.al.events.TileFullScreenEvent;
    import com.al.events.TileRemoveEvent;
    import com.al.model.PropertyDependency;
    import com.al.model.TileContainerModel;
    import com.al.pms.TileContainerPM;
    import com.al.renderers.TileMenuItemRenderer;
    import com.al.tile.ICombineElement;
    import com.al.tile.ITileContainer;
    import com.al.util.ContextUtil;
    import com.al.util.EventUtils;

    import flash.events.Event;
    import flash.events.FullScreenEvent;
    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.geom.Point;
    import flash.utils.Timer;

    import mx.binding.utils.BindingUtils;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;
    import mx.events.DragEvent;
    import mx.events.FlexEvent;
    import mx.managers.DragManager;

    import spark.components.BorderContainer;
    import spark.components.DataGroup;
    import spark.components.Group;
    import spark.components.Label;
    import spark.components.supportClasses.Skin;
    import spark.events.ElementExistenceEvent;

    /**
	 *  Color of the border.
	 *  
	 *  @default Orange
	 *  
	 *  @langversion 3.0
	 *  @playerversion Flash 10
	 *  @playerversion AIR 1.5
	 *  @productversion Flex 4
	 */
	/*[Style(name="selectionColor", type="uint", format="Color", inherit="no")]*/
	
	[SkinState("normal")]
	[SkinState("mouseOver")]
	[SkinState("selected")]
	[SkinState("dragOver")]
	[SkinState("selectedAndOver")]

    public class TileContainer extends BorderContainer implements ICombineElement, ITileContainer
    {
        [SkinPart]
        public var titleLabel: Label;
        
		[SkinPart]
        public var infoLabel: Label;

        [SkinPart]
        public var toolsGroup: DataGroup;

        [SkinPart]
        public var headerGroup: Group;

        [SkinPart(required=true)]
        public var swapDropIndicator: IVisualElement;

        [SkinPart(required=true)]
        public var overlayDropIndicator: IVisualElement;

        [SkinPart(required=true)]
        public var dragArea: Group;

        [Bindable]
        public var pm: TileContainerPM;

        public var tileContainerModel: TileContainerModel;

		public var tileDescriptor: TileContainerDescriptor;

        private var skinState: String = "normal";

        protected var maskGroup: MaskGroup;

        public function TileContainer(descriptor: TileContainerDescriptor = null){

            tabEnabled = true;
            tabFocusEnabled = true;
			tileContainerModel = new TileContainerModel();

            tileDescriptor = descriptor;

            addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
            addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
            addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);

            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            addEventListener(TileFocusedEvent.TILE_FOCUSED, onTileFocused);
            addEventListener(ElementExistenceEvent.ELEMENT_ADD, onElementAdded);
            addEventListener(TileFullScreenEvent.FULL_SCREEN_EXITED, onFullScreenExit);
            addEventListener(DependenciesSatisfiedEvent.DEPENDENCIES_SATISFIED, onDependencyInjection);

            ContextUtil.buildContext(TileContainerConfig, this, [new PropertyDependency("pm", TileContainerPM)]);
        }

        //--------------------------------------------------------------------------
        //
        //  Properties
        //
        //--------------------------------------------------------------------------
        //---------------------------------------------------
        // visualIndex
        //---------------------------------------------------
        private var _visualIndex: int;
        public function get visualIndex(): int {
            return _visualIndex;
        }

        public function set visualIndex(value: int): void {
            _visualIndex = value;
            invalidateProperties();
        }

        //---------------------------------------------------
        // reducedX
        //---------------------------------------------------
        private var _reducedX: Number = 0;
        public function get reducedX(): Number {
            return _reducedX;
        }

        public function set reducedX(value: Number): void{

            _reducedX = value;
            _roundedX = Math.ceil(value);
            if(tileDescriptor)
                tileDescriptor.reducedX = reducedX;
            _roundedRight = Math.ceil(_reducedX+_reducedWidth);
        }

        //---------------------------------------------------
        // reducedY
        //---------------------------------------------------
        private var _reducedY: Number = 0;
        public function get reducedY(): Number{
            return _reducedY;
        }
        public function set reducedY(value: Number): void{

            _reducedY = value;
            _roundedY = Math.ceil(value);
            if(tileDescriptor)
                tileDescriptor.reducedY = reducedY;
            _roundedBottom = Math.ceil(_reducedY+_reducedHeight);
        }

        //---------------------------------------------------
        // reducedWidth
        //---------------------------------------------------
        private var _reducedWidth: Number;
        public function get reducedWidth(): Number{
            return _reducedWidth;
        }
        public function set reducedWidth(value: Number): void{

            _reducedWidth = value;
            if(tileDescriptor)
                tileDescriptor.reducedWidth = reducedWidth;
            _roundedRight = Math.ceil(_reducedX+_reducedWidth);
        }

        //---------------------------------------------------
        // reducedHeight
        //---------------------------------------------------
        private var _reducedHeight: Number;
        public function get reducedHeight(): Number{
            return _reducedHeight;
        }
        public function set reducedHeight(value: Number): void{

            _reducedHeight = value;
            if(tileDescriptor)
                tileDescriptor.reducedHeight = reducedHeight;
            _roundedBottom = Math.ceil(_reducedY+_reducedHeight);
        }

        //---------------------------------------------------
        // reducedX
        //---------------------------------------------------
        private var _selected: Boolean;
        private var selectionChanged: Boolean;
        public function set selected(value: Boolean): void{

            if(_selected != value){

                _selected = value;
                selectionChanged = true;
                invalidateProperties();
            }
        }

        public function get selected(): Boolean{
            return _selected;
        }

        //---------------------------------------------------
        // roundedX
        //---------------------------------------------------
        private var _roundedX: Number;
        public function get roundedX(): Number {
            return _roundedX;
        }

        //---------------------------------------------------
        // roundedY
        //---------------------------------------------------
        private var _roundedY: Number;
        public function get roundedY(): Number {
            return _roundedY;
        }

        //---------------------------------------------------
        // roundedWidth
        //---------------------------------------------------
        private var _roundedRight: Number;
        public function get roundedRight(): Number {
            return _roundedRight;
        }

        //---------------------------------------------------
        // roundedHeight
        //---------------------------------------------------
        private var _roundedBottom: Number;
        public function get roundedBottom(): Number {
            return _roundedBottom;
        }

        //---------------------------------------------------
        // includeInSizing
        //---------------------------------------------------
        public function get includeInSizing(): Boolean {
            return true;
        }

        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------
        private var timer: Timer;

        protected function onHeaderMouseOver(event: Event): void {

            if(pm.tileContainerModel.showHeaderAndFooter)
                return;

            if(!timer)
            {
                timer = new Timer(340, 1);
                timer.addEventListener(TimerEvent.TIMER, showHeader);
            }
            timer.start();
            dragArea.addEventListener(MouseEvent.MOUSE_OUT, onHeaderMouseOut);
        }

        protected function onHeaderMouseOut(event: Event): void {

            if(timer.running)
                timer.stop();
            hideHeader(null);
        }

        private function showHeader(event: TimerEvent=null): void{

            // show the header but its not interactive
            headerGroup.enabled = false;
            pm.tileContainerModel.showHeaderAndFooter = true;
            dragArea.addEventListener(MouseEvent.DOUBLE_CLICK, onHeaderDoubleClick);
        }

        private function onHeaderDoubleClick(event: MouseEvent): void{

            if(!headerGroup)
                return;

            if(!headerGroup.enabled)
            {
                // we are sticking to the showHeader state
                headerGroup.enabled = true;
                dragArea.removeEventListener(MouseEvent.MOUSE_OUT, onHeaderMouseOut);
            }else{
                hideHeader(null);
            }
        }

        private function hideHeader(event: TimerEvent=null): void{

            headerGroup.enabled = true;
            pm.tileContainerModel.showHeaderAndFooter = false;
            dragArea.removeEventListener(MouseEvent.DOUBLE_CLICK, onHeaderDoubleClick);
        }

        override protected function partAdded(partName: String, instance: Object): void{

            super.partAdded(partName, instance);

            if(instance == titleLabel){

				titleLabel.text = tileContainerModel.tileTitle;
            }
            else if(instance == overlayDropIndicator){

				overlayDropIndicator.visible = false;
                overlayDropIndicator.addEventListener(DragEvent.DRAG_ENTER, onOverlayDragEnter);
            }
            else if(instance == swapDropIndicator){

				swapDropIndicator.visible = false;
                swapDropIndicator.addEventListener(DragEvent.DRAG_ENTER, onSwapDragEnter);
            }
			else if(instance == dragArea){

                dragArea.doubleClickEnabled = true;
                dragArea.addEventListener(MouseEvent.MOUSE_OVER, onHeaderMouseOver);
                dragArea.addEventListener(MouseEvent.DOUBLE_CLICK, onHeaderDoubleClick);
				StartDraggingStrategy.dragWatch(dragArea, 2);
			}
            else if(instance == toolsGroup){

                toolsGroup.dataProvider = pm ? pm.menuItems : null;
                toolsGroup.addEventListener("tileMenuSelected", onMenuItemClick);
            }else if(instance == headerGroup){

                headerGroup.clipAndEnableScrolling = true;
            }
            else if(instance == contentGroup) {

                contentGroup.clipAndEnableScrolling = true;
                contentGroup.addEventListener(MouseEvent.MOUSE_OVER, onConentMouseOver);
                contentGroup.addEventListener(MouseEvent.MOUSE_OUT, onContentMouseOut);
                contentGroup.addEventListener(MouseEvent.CLICK, onContentClick);
            }

            //if(skin is Skin)
                //(skin as Skin).clipAndEnableScrolling = true;
        }

        override protected function commitProperties():void {

            if(titleLabel){
                //titleLabel.text = String(visualIndex);
            }
			if(infoLabel){
				var right: Number = reducedX+reducedWidth;
				var bottom: Number = reducedY+reducedHeight;
				infoLabel.text = "("+reducedX+","+reducedY+") - ("+right+","+bottom+")";
			}
			if(selectionChanged){
				
				skinState = selected ? ViewState.SELECTED : "";
				invalidateSkinState();
				selectionChanged = false;
			}
			super.commitProperties();
        }
		
		/**
		 * Enforces the children are of type ContentContainer only 
		 * @param element
		 * @return 
		 * 
		 */
		override public function addElement(element:IVisualElement):IVisualElement {
			
			if(!(element is ContentContainer))
				//throw new Error("Child element of TileContainer should be of type ContentContainer");
				trace("Child element of TileContainer should be of type ContentContainer");
			
			return super.addElement(element);
		}
		
		/**
		 * Enforces the children are of type ContentContainer only 
		 *  
		 * @param element
		 * @param index
		 * @return 
		 * 
		 */
		override public function addElementAt(element:IVisualElement, index:int):IVisualElement {
			
			//if(!(element is ContentContainer))
				//throw new Error("Child element of TileContainer should be of type ContentContainer");
			//trace("Child element of TileContainer should be of type ContentContainer");
			
			return super.addElementAt(element, index);
		}
		
		override protected function getCurrentSkinState():String
		{
			if(skinState)
				return skinState;
			else
				return super.getCurrentSkinState();
		}


        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {

            super.updateDisplayList(unscaledWidth, unscaledHeight);

        }

        //--------------------------------------------------------------------------
        //
        //  Privates
        //
        //--------------------------------------------------------------------------

        private function onFullScreenExit(event: Event): void{

            isFullScreen = false;
            pm.exitFullScreen();
        }

        private function onTileFocused(event: Event): void{

            pm.onTileFocused();
        }

        private function onDependencyInjection(event: Event): void {

            addListeners(pm);

            pm.setTileContainerModel(tileContainerModel);
            pm.tileDescriptor = tileDescriptor;
            pm.tileDataModel.setTileData(TileDataFormats.TILE_DESCRIPTOR, tileDescriptor);
        }

        private function onElementAdded(event: ElementExistenceEvent): void{

            var contentContainer: ContentContainer = event.element as ContentContainer;

            if(contentContainer)
                contentContainer.tileModel = tileContainerModel;
        }

        protected function onAddedToStage(event: Event): void{

            stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreenChange);
        }

        protected function onFullScreenChange(event: FullScreenEvent): void {

            if(event.fullScreen)
                pm.tileContainerModel.showHeaderAndFooterInFullScreen = false;
            pm.tileContainerModel.isFullScreenMode = event.fullScreen;
        }

        private function addListeners(pm: TileContainerPM): void {

            // pass on the tileContainerModel to the siblings
            if(pm){

                pm.addEventListener("removeTile", onRemoveTile);
                pm.addEventListener("toggleFullScreen", toggleFullScreen);

                BindingUtils.bindSetter(onShowHeaderAndFooter, pm, ["tileContainerModel", "showHeaderAndFooter"]);
                BindingUtils.bindSetter(updateMask, pm, ["drawingModel", "maskBlur"]);
                BindingUtils.bindSetter(updateMask, pm, ["drawingModel", "maskBlur"]);
                BindingUtils.bindSetter(updateMask, pm, ["drawingModel", "maskSize"]);
                BindingUtils.bindSetter(updateMask, pm, ["drawingModel", "roundMaskSelected"]);
                BindingUtils.bindSetter(updateTitle, tileContainerModel, "tileTitle");
            }
        }

        private function updateTitle(newValue: Object): void{

            if(titleLabel)
                titleLabel.text = pm.tileContainerModel.tileTitle;
        }

        private function onShowHeaderAndFooter(value: Object): void {

            if(headerGroup){

                headerGroup.visible = pm.tileContainerModel.showHeaderAndFooter;
                headerGroup.includeInLayout = pm.tileContainerModel.showHeaderAndFooter;
            }
        }

        private function onConentMouseOver(event: MouseEvent): void{

            if(pm.drawingModel.maskToolSelected && numElements == 2){

                setupMask();
                positionMask(event.stageX, event.stageY);
                contentGroup.addEventListener(MouseEvent.MOUSE_MOVE, onContentMouseMove);
            }
        }


        /**
         * All the below logic should be in PM
         * @param event
         */
        private function onContentClick(event: MouseEvent): void{

            var applyChange: Boolean ;
            if (pm.drawingModel.zoomInSelected) {

                pm.tileContainerModel.zoomFactor += 0.1;
                applyChange = true;
            }
            else if (pm.drawingModel.zoomOutSelected) {

                pm.tileContainerModel.zoomFactor -= 0.1;
                applyChange = true;
            }
            else if (pm.drawingModel.exactFitSelected) {

                // value of NaN will result in determining the zoom factor required
                // to fit the content to the size of the window..
                pm.tileContainerModel.zoomFactor = NaN;
                applyChange = true;
            }

            if(applyChange) {

                for (var i:int = 0; i < numElements; ++i) {

                    var contentContainer:ContentContainer = getElementAt(i) as ContentContainer;
                    if (contentContainer)
                        contentContainer.zoomFactor = pm.tileContainerModel.zoomFactor;
                }
            }
        }

        private function onRemoveTile(event: Event): void{

            dispatchEvent(new TileRemoveEvent(this));
        }

        private var isFullScreen: Boolean;
        private function toggleFullScreen(event: Event): void{

            isFullScreen = !isFullScreen;
            dispatchEvent(new
                    TileFullScreenEvent(
                    this, isFullScreen ? TileFullScreenEvent.FULL_SCREEN : TileFullScreenEvent.EXIT_FULL_SCREEN, true));
        }

        protected function onMenuItemClick(event: Event): void{

            event.stopImmediatePropagation();
            var menuItem: TileMenuItemRenderer = event.target as TileMenuItemRenderer;
            if(menuItem && menuItem.data){

                pm.handleMenuSelection(menuItem.data.iconCode);
            }
        }
		/**
		 * 
		 * @param event
		 * 
		 */
        private function onMouseOver(event: MouseEvent): void{
			
			skinState = selected ? ViewState.SELECTED_AND_OVER: ViewState.MOUSE_OVER;
			invalidateSkinState();
        }
		
        private function onMouseOut(event: MouseEvent): void{

			skinState = selected ? ViewState.SELECTED: ViewState.NORMAL;
			invalidateSkinState();
		}

        private function onContentMouseMove(event: MouseEvent): void{

            var localPoint: Point = contentGroup.globalToLocal(new Point(event.stageX, event.stageY));

            ////trace("localX: "+ localPoint.x);
            ////trace("localY: "+ localPoint.y);

            if(localPoint.x <= 0 || localPoint.y<=0 || localPoint.x >= contentGroup.width || localPoint.y >= contentGroup.height) {

                removeMask();
            }
            else{
                positionMask(event.stageX, event.stageY);
            }
        }

        private function onContentMouseOut(event: MouseEvent): void{

            var localPoint: Point = contentGroup.globalToLocal(new Point(event.stageX, event.stageY));

            if(localPoint.x <= 0 || localPoint.y<=0 || localPoint.x >= contentGroup.width || localPoint.y >= contentGroup.height) {

                removeMask();
            }
        }

        private function removeMask(): void{

            if(!maskGroup)
                return;

            if (contains(maskGroup)) {
                removeElement(maskGroup);
                var topContent: ContentContainer = (getElementAt(numElements-1) as ContentContainer);
                topContent.mask = null;
                topContent.cacheAsBitmap = false;
                addElementAt(topContent, 0);
            }
            contentGroup.removeEventListener(MouseEvent.MOUSE_MOVE, onContentMouseMove);
        }

        private function setupMask(): void{

            if(!maskGroup) {

                maskGroup = new MaskGroup();
                maskGroup.cacheAsBitmap = true;
            }

            if(!contains(maskGroup)) {

                var bottomContent:ContentContainer = (getElementAt(0) as ContentContainer);
                bottomContent.cacheAsBitmap = true;

                addElement(bottomContent);
                addElement(maskGroup);
                bottomContent.mask = maskGroup;
                maskGroup.roundedMask = pm.drawingModel.roundMaskSelected;
            }
        }

        private function positionMask(stageX: Number, stageY: Number): void{

            var localPoint: Point = contentGroup.globalToLocal(new Point(stageX, stageY));

            // other drynamic variables
            maskGroup.blurSize = 3 * pm.drawingModel.maskBlur;
            maskGroup.height = 40 * pm.drawingModel.maskSize;
            maskGroup.width = 40 * pm.drawingModel.maskSize;

            maskGroup.x = localPoint.x - maskGroup.width/2;
            maskGroup.y = localPoint.y - maskGroup.height/2;
        }

        private function updateMask(value: Object): void{

            if(!pm.drawingModel.maskToolSelected){

                removeMask();
            }
            // update only if we are currently using mask
            else if(maskGroup && contains(maskGroup)){

                positionMask(stage.mouseX, stage.mouseY);
                maskGroup.roundedMask = pm.drawingModel.roundMaskSelected;
            }
        }
		
		/**
		 * 
		 * @param event
		 * 
		 */
        private function onDragEnter(event: DragEvent): void{

            var draggedTileCanBeOverlayed: Boolean;

            if(event.dragInitiator == this)
                return;
            else if(event.dragInitiator is TileContainer){

                // TODO: Use drag source to received the tileContainerModel
                draggedTileCanBeOverlayed = (event.dragInitiator as  TileContainer).tileContainerModel.overlaySupported;
            }

            if(event.dragSource.hasFormat(DragSourceFormats.TILE))
            {
				swapDropIndicator.visible = true;
				overlayDropIndicator.visible = tileContainerModel.overlaySupported && draggedTileCanBeOverlayed;

				skinState = ViewState.DRAG_OVER;
				invalidateSkinState();
                addEventListener(DragEvent.DRAG_EXIT, onDragExit);
            }
        }

        private function onOverlayDragEnter(event: DragEvent): void {

            DragManager.acceptDragDrop(overlayDropIndicator as IUIComponent);
            DragManager.showFeedback(DragManager.COPY);

            overlayDropIndicator.addEventListener(DragEvent.DRAG_DROP, onOverlayDragDrop);
        }

        private function onSwapDragEnter(event: DragEvent): void {

            DragManager.acceptDragDrop(swapDropIndicator as IUIComponent);

            if(!event.dragSource.hasFormat(DragSourceFormats.ASSET_CELL))
            {
                DragManager.showFeedback(DragManager.MOVE);
            }

            swapDropIndicator.addEventListener(DragEvent.DRAG_DROP, onSwapDragDrop);
        }

        private function onDragExit(event: DragEvent): void {

            if(event.dragInitiator == this)
                return;

            cleanUpAfterDrag();
            EventUtils.removeListener(event, onDragExit);
        }

        private function onSwapDragDrop(event: DragEvent): void {

            cleanUpAfterDrag();
            var draggedTile: ICombineElement = event.dragInitiator as ICombineElement;
            dispatchEvent(new TileDropEvent(draggedTile, this, TileDropAction.SWAP_ACTION));
        }

        private function onOverlayDragDrop(event: DragEvent): void {

            cleanUpAfterDrag();
            var draggedTile: ICombineElement = event.dragInitiator as ICombineElement;
            dispatchEvent(new TileDropEvent(draggedTile, this, TileDropAction.OVERLAY_ACTION));
        }

        private function cleanUpAfterDrag(): void {

            swapDropIndicator.visible = false;
            overlayDropIndicator.visible = false;
            swapDropIndicator.removeEventListener(DragEvent.DRAG_DROP, onSwapDragDrop);
            overlayDropIndicator.removeEventListener(DragEvent.DRAG_DROP, onOverlayDragDrop);
			//setStyle("borderColor",ColorUtils.Asbestos);
			setStyle("borderAlpha", 0.5);
        }

    }
}
