package com.al.controllers
{
	import com.adobe.rtc.events.CollectionNodeEvent;
	import com.adobe.rtc.events.SessionEvent;
	import com.adobe.rtc.events.SharedPropertyEvent;
	import com.adobe.rtc.messaging.MessageItem;
	import com.adobe.rtc.session.IConnectSession;
	import com.adobe.rtc.sharedModel.CollectionNode;
	import com.adobe.rtc.sharedModel.SharedProperty;
	import com.al.collaboration.model.SharedNode;
	import com.al.model.CollaborationConfigurationModel;
	
	import mx.binding.utils.BindingUtils;
	import mx.utils.UIDUtil;

	/**
	 *  Subscribes to configuration node and Collaboration shared node.
	 * @author lram
	 * 
	 */
	public class RoomController
	{
		
		public static const COLLABORATION_CONFIG: String = "CollaborationConfig";
		
		[Inject]
		public var connectSession: IConnectSession;
		
		[Inject(id="configNode")]
		public var configNode: CollectionNode;
		
		[Inject]
		public var configModel: CollaborationConfigurationModel;
		
		private var collaborationNode: SharedNode;
		
		[Init]
		public function init(): void{
			
			BindingUtils.bindSetter(onConfigUpdate, configModel, "commentsCount");
			BindingUtils.bindSetter(onConfigUpdate, configModel, "annotationsCount");
			if(connectSession.isSynchronized){
				
				subscribe();
			}
			else{
				connectSession.addEventListener(SessionEvent.SYNCHRONIZATION_CHANGE,onSessionSynchronization);
			}
		}
		
		//--------------------------------------------------------------------------
		//
		//  Private Methods
		//
		//--------------------------------------------------------------------------

		protected function subscribe(): void{

			configNode.addEventListener(CollectionNodeEvent.SYNCHRONIZATION_CHANGE, onSynchronizationChange);
			configNode.addEventListener(CollectionNodeEvent.ITEM_RECEIVE, onItemReceive);
			configNode.addEventListener(CollectionNodeEvent.ITEM_RETRACT, onItemRetract);

			configNode.subscribe();
			subscrbeToCollaborationsConfig();
			
		}
		
		protected function subscrbeToCollaborationsConfig(): void{
			
			//trace("Subscribing to CollaborationConfig");
			collaborationNode = new SharedNode(COLLABORATION_CONFIG, configNode, connectSession);
			collaborationNode.addEventListener(SharedPropertyEvent.CHANGE, onSharedNodeChange);
			collaborationNode.subscribe();
		}
		
		protected function onConfigUpdate(config: Object): void {
			
			if(!configNode.isSynchronized) return;
			
			if(!configModel.id){
				// Very first time we are publishing the config object
				configNode.publishItem(
					new MessageItem( collaborationNode.nodeName, configModel.getObject(), UIDUtil.createUID() ) );
			}
			else{
				configNode.publishItem(
					new MessageItem( collaborationNode.nodeName, configModel.getObject(), configModel.id), true );
			}
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event handlers
		//
		//--------------------------------------------------------------------------

		protected function onSharedNodeChange(event:SharedPropertyEvent): void{
			
			//trace("SharedNodeChange. Publisher id - "+event.publisherID);
		}
		
		protected function onItemReceive(event:CollectionNodeEvent):void {
			
			configModel.update(event.item);
		}
		
		protected function onItemRetract(event:CollectionNodeEvent):void {
			
			//trace("Not expecting for configuration to be deleted");
			throw new Error("Who deleted the collaboration config..");
		}
		
		protected function onSynchronizationChange(event:CollectionNodeEvent):void
		{
			if(event.target is SharedProperty)
				trace("Synchronization change -> " + (event.target as SharedProperty).nodeName);
			else if(event.target is CollectionNode)
				trace("Synchronization change -> " + (event.target as CollectionNode).sharedID);
		}
		
		private function onSessionSynchronization(ev: SessionEvent): void{
			
			if(connectSession.isSynchronized){
				
				subscribe();
				connectSession.removeEventListener(SessionEvent.SYNCHRONIZATION_CHANGE,onSessionSynchronization);
			}
		}
	}
}