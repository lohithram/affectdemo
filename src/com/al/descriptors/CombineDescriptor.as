/**
 * Created by lram on 27/02/2015.
 */
package com.al.descriptors
{

    import flash.events.Event;
    import flash.events.EventDispatcher;

    import mx.collections.ArrayList;
    import mx.events.CollectionEvent;
import mx.events.PropertyChangeEvent;

public class CombineDescriptor extends EventDispatcher implements ICombineDescriptor, ITabDescriptor
    {

        public function CombineDescriptor(index: int, title: String="", canClose: Boolean = true) {

            this.index = index;
            this.title = title;
            this.canClose = canClose;
            tileContainers = new ArrayList();
        }

        private var _index: int;
        public function get index():int {
            return _index;
        }

        public function set index(value:int):void {
            _index = value;
        }

        private var _title: String;
        [Bindable("titleChange")]
        [Bindable("tileContainersChange")]
        public function get title():String {

            return _title ? _title : determineCombineTitle();
        }

        public function set title(value:String):void {

            if(_title != value){

                _title = value;
                dispatchEvent(new Event("titleChange"));
            }
        }

        private var _canClose: Boolean;
        public function get canClose():Boolean {
            return _canClose;
        }

        public function set canClose(value:Boolean):void {
            _canClose = value;
        }

        [ArrayElementType("com.al.descriptors.TileContainerDescriptor")]
        private var _tileContainers: ArrayList;
        public function get tileContainers():ArrayList {
            return _tileContainers;
        }

        public function set tileContainers(value:ArrayList):void {

            if(_tileContainers != value){

                if(_tileContainers)
                    _tileContainers.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionChange);

                _tileContainers = value;

                if(_tileContainers)
                    _tileContainers.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCollectionChange);

                dispatchEvent(new Event("tileContainersChange"));
            }
        }

		private var _combineClass: Class;
		public function get combineClass(): Class {
			return _combineClass;
		}
		
		public function set combineClass(value: Class): void {
			_combineClass = value;
		}


        private function onCollectionChange(event: CollectionEvent): void{

            if(!_title){

                dispatchEvent(new Event("tileContainersChange"));
                // HACK: Dispatching change event updates the title of the combine tab!
                dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, title, _title, title));
            }
        }
        /**
         * If title not set explicitly, construct one
         * @return
         */
        private function determineCombineTitle(): String {

            var derivedTitle: String;
            if(tileContainers && tileContainers.length > 0){

                derivedTitle = (tileContainers.getItemAt(0) as TileContainerDescriptor).title;

                if(tileContainers.length > 1){

                    derivedTitle += " and " + String(tileContainers.length-1) + " more";
                }
            }

            return derivedTitle;
        }

    }
}
