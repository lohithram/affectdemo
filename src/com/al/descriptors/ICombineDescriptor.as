/**
 * Created by lram on 03/03/2015.
 */
package com.al.descriptors
{

import flash.events.IEventDispatcher;

import mx.collections.ArrayList;

    public interface ICombineDescriptor extends IEventDispatcher
    {
        function get index(): int;
        function set index(value: int): void;

        function get title(): String;
        function set title(value: String): void;

        function get canClose(): Boolean;
        function set canClose(value: Boolean): void;
		
		function get combineClass(): Class;
		function set combineClass(value: Class): void;
		

        function get tileContainers(): ArrayList;
        function set tileContainers(value: ArrayList): void;
    }
}
