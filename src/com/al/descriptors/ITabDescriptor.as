/**
 * Created by lram on 03/03/2015.
 */
package com.al.descriptors
{

    public interface ITabDescriptor
    {
        function get index(): int;

        function get title(): String;

        function get canClose(): Boolean;

    }
}
