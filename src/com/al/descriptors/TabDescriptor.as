/**
 * Created by lram on 27/02/2015.
 */
package com.al.descriptors
{

    import spark.supportClasses.INavigator;

    public class TabDescriptor
    {
        [Bindable]
        public var index: int;
        [Bindable]
        public var title: String;
        [Bindable]
        public var canClose: Boolean;

        public function TabDescriptor(title: String, index: int, canClose: Boolean=false) {

            this.index = index;
            this.title = title;
            this.canClose = canClose;
        }
    }
}
