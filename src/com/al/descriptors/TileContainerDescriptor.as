/**
 * Created by lram on 03/03/2015.
 */
package com.al.descriptors
{

	import com.al.enums.TileType;
	import com.al.model.IContentUnit;
    import com.al.model.Project;
    import com.al.model.TileContentDescriptor;
    import com.al.utility.panels.common.ITileContainerDescriptorProvider;

    import mx.collections.ArrayCollection;

    import mx.utils.UIDUtil;

	//TODO: Create interface
	/**
	 * Represents Tile container persistent state
	 */
	public class TileContainerDescriptor implements IContentUnit, ITileContainerDescriptorProvider
    {
        public var reducedX: Number;
        public var reducedY: Number;

        public var reducedWidth: Number;
        public var reducedHeight: Number;

		public var model: Object;

		public var tileType: String;

		public var title: String;

        public function get contentId():String
        {
            return currentContent ? currentContent.id : null;
        }


		// TODO: Find the right place and structure for project property
        public var project: Project;


		public var currentContent: TileContentDescriptor;

		// property to represent multiple content inside the tile
		// For example: when content is overlayed..
		[ArrayElementType("com.al.model.TileContentDescriptor")]
		public var contents: Array;

        [Transient]
        public var backgroundColor: uint;
		
		[Transient]
		//TODO: remove this property
		public var contentUrl: String;

		public function TileContainerDescriptor(type: String = TileType.CONTENT, title: String=""){

			tileType = type;
			this.title = title;
			contents = new Array();
		}

		private var _id: String;
		public function get id():String {

			if(!_id)
				_id = UIDUtil.createUID();
			return _id;
		}

		public function clone(): TileContainerDescriptor{
			
			var desc: TileContainerDescriptor = new TileContainerDescriptor();
			desc.title = title;
            desc.project = project;
			desc.reducedX = reducedX;
			desc.reducedY = reducedY;
			desc.tileType = tileType;
			desc.reducedWidth = reducedWidth;
			desc.reducedHeight = reducedHeight;
			desc.backgroundColor = backgroundColor;

			desc.contents = contents ? contents.slice() : null;
			desc.currentContent = currentContent ? currentContent.clone() : null;

            desc.model = model;
			
			return desc;
		}

        public function getTileContainerDescriptor():TileContainerDescriptor
        {
            return this;
        }

    }
}
