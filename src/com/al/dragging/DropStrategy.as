/**
 * Created by abhisekpaul on 02/06/15.
 */
package com.al.dragging
{

    import flash.desktop.Clipboard;

    import mx.core.DragSource;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;

    import flash.utils.Dictionary;

    public class DropStrategy
    {
        private static var dictionary: Dictionary = new Dictionary(true);
        public static function dropWatch(component: IVisualElement,formats:Array, allowNative:Boolean = false): void{

            dictionary[component] = new Watch(component, formats, allowNative);

        }

        public static function unWatch(component: IUIComponent): void{

            if(dictionary[component]) {
                (dictionary[component] as Watch).unwatch();
                delete dictionary[component];
            }
        }

        public static function canAccept(dragSource:DragSource, formats: Array):Boolean
        {
            for each(var format:String in formats)
            {
                if(dragSource.hasFormat(format))
                    return true;
            }
            return false;
        }

        public static function canAcceptClipboard(clipboard: Clipboard, formats: Array):Boolean
        {
            for each(var format:String in formats)
            {
                if(clipboard.hasFormat(format))
                    return true;
            }
            return false;
        }
    }

}

import com.al.events.DroppingEvent;
import com.al.events.DroppingEvent;

import flash.display.DisplayObject;

import flash.display.DisplayObjectContainer;

import flash.events.Event;

import flash.events.NativeDragEvent;

import mx.core.DragSource;
import mx.core.IUIComponent;
import mx.core.IVisualElement;
import mx.events.DragEvent;
import mx.managers.DragManager;

class Watch{

    private var watchedComponent: IVisualElement;
    private var formats: Array;
    private var allowNative:Boolean;

    public function Watch(component: IVisualElement, formats: Array, iallowNative:Boolean = false) {

        this.formats = formats;
        watchedComponent = component;
        watchedComponent.addEventListener(DragEvent.DRAG_ENTER, onDragEnter);

        allowNative = iallowNative;
        if(allowNative)
        {
            watchedComponent.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onNativeDragEnter);
        }
    }

    //--------------------------------------------------------------------------
    //
    //  Public methods
    //
    //--------------------------------------------------------------------------
    public function unwatch(): void {

        watchedComponent.removeEventListener(DragEvent.DRAG_ENTER, onDragEnter);

        if(allowNative)
        {
            watchedComponent.removeEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onNativeDragEnter);
        }

        watchedComponent = null;
    }
    //--------------------------------------------------------------------------
    //
    //  Privates
    //
    //--------------------------------------------------------------------------

    private function isAcceptable(dragSource:Object):Boolean
    {
        for each(var format:String in formats)
        {
            if(dragSource.hasFormat(format))
                return true;
        }

        return false;
    }

    private function onDragEnter(event: DragEvent): void
    {
        if(isAcceptable(event.dragSource) && event.dragInitiator && !DisplayObjectContainer(watchedComponent).contains(DisplayObject(event.dragInitiator)))
        {
            DragManager.acceptDragDrop(event.currentTarget as IUIComponent);

            watchedComponent.addEventListener(DragEvent.DRAG_OVER, onDragOver);
            watchedComponent.addEventListener(DragEvent.DRAG_DROP, onDragDrop);
            watchedComponent.addEventListener(DragEvent.DRAG_EXIT, onDragExit);
        }
    }

    private function onNativeDragEnter(event: NativeDragEvent): void
    {
        if(isAcceptable(event.clipboard))
        {
            DragManager.acceptDragDrop(event.currentTarget as IUIComponent);

            watchedComponent.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onNativeDragDrop);
            watchedComponent.addEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onNativeDragExit);
        }
    }

    private function onDragDrop(event: Event): void
    {
        watchedComponent.dispatchEvent(new DroppingEvent(event));

        onDragExit(event);

    }

    private function onDragOver(event: DragEvent): void
    {
        watchedComponent.dispatchEvent(new DroppingEvent(event,DroppingEvent.DRAG_OVER));
    }

    private function onNativeDragDrop(event: Event): void
    {
        watchedComponent.dispatchEvent(new DroppingEvent(event));

        onNativeDragExit(event);

    }

    private function onDragExit(event: Event): void
    {
        watchedComponent.removeEventListener(DragEvent.DRAG_OVER, onDragOver);
        watchedComponent.removeEventListener(DragEvent.DRAG_DROP, onDragDrop);
        watchedComponent.removeEventListener(DragEvent.DRAG_EXIT, onDragExit);
    }

    private function onNativeDragExit(event: Event): void
    {
        watchedComponent.removeEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onDragDrop);
        watchedComponent.removeEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onDragExit);
    }
}
