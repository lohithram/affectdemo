package com.al.dragging
{
	import com.al.events.StartDraggingEvent;
	
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	
	import mx.core.IUIComponent;
	import mx.core.IVisualElement;

	public class StartDraggingStrategy
	{
		
		private static const DRAG_THRESHOLD: Number = 4;
		
		private static var dictionary: Dictionary = new Dictionary(true);
		
		public static function dragWatch(component: IVisualElement, threshold: Number = DRAG_THRESHOLD): void{
			
			dictionary[component] = new Watch(component, threshold);
			
		}
		
		public static function unWatch(component: IUIComponent): void{
			
			if(dictionary[component]) {
				(dictionary[component] as Watch).unwatch();
				delete dictionary[component];
			}
		}
	}
}


	import ardisia.managers.cursorManager.CursorManager;
	
	import com.al.events.StartDraggingEvent;
	
	import flash.events.MouseEvent;
	
	import mx.core.IUIComponent;
	import mx.core.IVisualElement;

	class Watch{
	
		private var watchedComponent: IVisualElement;
		private var threshold: Number;
		
		public function Watch(component: IVisualElement, threshold: Number) {
			
			this.threshold = threshold;
			watchedComponent = component;
			watchedComponent.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);	
		}
		
		//--------------------------------------------------------------------------
		//
		//  Public methods
		//
		//--------------------------------------------------------------------------
		public function unwatch(): void {
			
			watchedComponent.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			watchedComponent = null;
		}
		//--------------------------------------------------------------------------
		//
		//  Privates
		//
		//--------------------------------------------------------------------------

		private var startX: Number = 0;
		private var startY: Number = 0;
		
		private function onMouseDown(event: MouseEvent): void{
			
			startX = event.stageX;
			startY = event.stageY;
			event.currentTarget.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			event.currentTarget.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function onMouseUp(event: MouseEvent): void{
			
			CursorManager.removeAllCursors();
			event.currentTarget.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			event.currentTarget.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function onMouseMove(event: MouseEvent): void{
			
			var deltaX: Number = event.stageX-startX;
			var deltaY: Number = event.stageY-startY;
			
			if(Math.abs(deltaX) >= threshold || Math.abs(deltaY) >= threshold ){
				
				event.currentTarget.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				event.currentTarget.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				
				watchedComponent.dispatchEvent(new StartDraggingEvent(event, deltaX, deltaY, true));
			}
			
		}		
	}
