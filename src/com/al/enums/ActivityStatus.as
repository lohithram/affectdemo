/**
 * Created by lram on 18/02/2015.
 */
package com.al.enums
{

    import mx.collections.ArrayList;

    public class ActivityStatus
    {
        public static const NEW: String="NEW";
        public static const PENDING: String="PENDING";
        public static const REJECTED: String="REJECTED";
        public static const APPROVED: String="APPROVED";
        public static const PROGRESS: String="PROGRESS";

        public static const ALL_STATUSES: ArrayList = new ArrayList([
                NEW, PENDING, REJECTED, APPROVED, PROGRESS
        ]);
    }
}
