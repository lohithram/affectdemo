/**
 * Created by abhisekpaul on 15/06/15.
 */
package com.al.enums
{
    public class CombineType
    {
        public static const PROJECT: String = "Project";
        public static const DASHBOARD: String = "Dashboard";
    }
}
