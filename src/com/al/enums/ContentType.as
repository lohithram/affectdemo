package com.al.enums
{
	public class ContentType {
		
		public static const CONCEPT_BOARD: String = "conceptBoard";
		public static const MULTI_PAGE: String = "multiPage";
		public static const AUDIO: String = "";
		public static const VIDEO: String = "";

		// Represents BROCHURE, PHOTO, HTML PAGE...
		// any spatial single document
		//
		public static const SINGLE_DOCUMENT: String = "singleDoc";
        public static const NOTE: String = "note";

		public static const NO_CONTENT: String = "noContent";

	}
}