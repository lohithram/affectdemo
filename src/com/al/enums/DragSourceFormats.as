/**
 * Created by lram on 03/03/2015.
 */
package com.al.enums
{

    public class DragSourceFormats
    {
        public static const TILE: String = "Tile";
        public static const COMBINE: String = "Combine";
        public static const MULTI_TILE: String = "MultiTile";
        public static const UTILITY_BAR: String = "UtilityBar";
        public static const TILE_DESCRIPTOR: String = "TileDescriptor";
        public static const ASSET_CELL_ITEM: String = "AssetCellItem";
        public static const ASSET_CELL: String = "AssetCell";
        public static const USER_CELL: String = "UserCell";
        public static const TASK_CELL: String = "TaskCell";
        public static const COMMENT_CELL: String = "CommentCell";

        public static const ITMES_BY_INDEX: String = "itemsByIndex";
    }
}
