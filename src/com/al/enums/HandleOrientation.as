package com.al.enums
{
	public class HandleOrientation
	{
		//   o
		//   o o
		//   o
		public static const RIGHT_SPLIT: String = "right";
		
		//    o
		//  o o
		//    o
		public static const LEFT_SPLIT: String = "left";
		
		//   o o o
		//     o
		public static const BOTTOM_SPLIT: String = "bottom";
		
		//    o 
		//  o o o
		public static const TOP_SPLIT: String = "top";
		
		public static const VERTICAL_SPLIT: String = "vertical";
		
		public static const HORIZONTAL_SPLIT: String = "horizontal";

		public static const FOUR_WAY_SPLIT: String = "fourWay";
	}
}