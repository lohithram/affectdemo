/**
 * Created by lohithram on 01/05/15.
 */
package com.al.enums {

    public class ShapeProperties {

        public static const PRIMARY_COLOR: String = "primaryColor";
        public static const LINE_COLOR: String = "lineColor";
        public static const ALPHA: String = "alpha";
        public static const LINE_THICKNESS: String = "lineThickness";
    }
}
