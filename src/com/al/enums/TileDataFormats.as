/**
 * Created by lohithram on 28/05/15.
 */
package com.al.enums {

    public class TileDataFormats {

        public static const ANNOTATIONS: String = "annotations";
        public static const TILE_DESCRIPTOR: String = "tileDescriptor";
        public static const MODEL: String = "model";
        //public static const BRIEF: String = "brief";
        //public static const BRIEF: String = "brief";
    }
}
