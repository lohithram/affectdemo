/**
 * Created by lram on 18/02/2015.
 */
package com.al.enums
{

    public class TileDropAction
    {

        public static const SWAP_ACTION: String = "swap";
        public static const OVERLAY_ACTION: String = "overlay";
        public static const TOP_DROP_ACTION: String = "top";
        public static const LEFT_DROP_ACTION: String = "left";
        public static const RIGHT_DROP_ACTION: String = "right";
        public static const BOTTOM_DROP_ACTION: String = "bottom";    }
}
