/**
 * Created by lram on 18/02/2015.
 */
package com.al.enums
{

    public class TileSide
    {
        public static const TOP_SIDE: String = "top";
        public static const LEFT_SIDE: String = "left";
        public static const RIGHT_SIDE: String = "right";
        public static const BOTTOM_SIDE: String = "bottom";
		
		public static const TOP_LEFT: String = "topLeft";
		public static const TOP_RIGHT: String = "topRight";
		public static const BOTTOM_LEFT: String = "bottomLeft";
		public static const BOTTOM_RIGHT: String = "bottomRight";
    }
}
