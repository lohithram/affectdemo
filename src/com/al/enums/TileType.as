package com.al.enums
{
	public class TileType {
		
		public static const BRIEF: String = "briefTile";
		public static const CONTENT: String = "contentTile";
		public static const REPORT: String = "reportTile";
		public static const ACTIVITIES: String = "activitiesTile";
		public static const PROJECT_ASSETS: String = "projectAssetsTile";
		public static const PROJECT_DETAILS: String = "projectDetailsTile";
		public static const CONCEPT_BOARD: String = "conceptBoardTile";
		public static const REVIEW: String = "reviewTile";

        public static const BRIEF_LABEL: String = "Briefs";
        public static const REVIEW_LABEL: String = "Review";
        public static const CONTENT_LABEL: String = "Assets";
        public static const CONCEPT_BOARD_LABEL: String = "Concept Board";

        public static const HTML: String = "HTML";
        public static const MULTI_PAGE:String = "multiPage";
        public static const WB:String = "whiteboard";
        public static const TEAM:String = "teamTile";


    }
}