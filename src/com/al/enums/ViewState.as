package com.al.enums
{
	public class ViewState
	{
		public static const UP: String = "up";
		public static const NORMAL: String = "normal";
		public static const DISABLED: String = "disabled";
		public static const OVER: String = "over";
		public static const SELECTED: String = "selected";
		public static const SELECTED_AND_OVER: String = "selectedAndOver";
		public static const DRAG_OVER: String = "dragOver";
		public static const MOUSE_OVER: String = "mouseOver";
	}
}