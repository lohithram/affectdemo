/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import com.al.tile.ICombineElement;
    import com.al.tile.ITileDropIndicator;

    import flash.events.Event;

    public class CombineDropEvent extends Event
    {

        public static const DROP: String = "combineDrop";

        public function CombineDropEvent(draggedCombine: *, dropAction: String) {

            super(DROP);
            _dropAction = dropAction;
            _draggedCombine = draggedCombine;
        }

        private var _dropAction: String;
        public function get dropAction(): String {
            return _dropAction;
        }

        private var _draggedCombine: *;
        public function get draggedCombine(): * {
            return _draggedCombine;
        }

        override public function clone(): Event {

            return new CombineDropEvent(draggedCombine, dropAction);
        }
    }
}
