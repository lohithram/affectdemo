package com.al.events
{
	import flash.events.Event;
	
	public class CombinePopupEvent extends Event
	{
		public static const POPUP_ADD: String = "popupAdd";
		public static const POPUP_REMOVE: String = "popupRemove";
		
		public function CombinePopupEvent(type:String, bubbles:Boolean=true)
		{
			super(type, bubbles);
		}
	}
}