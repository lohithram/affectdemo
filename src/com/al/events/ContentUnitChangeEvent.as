/**
 * Created by synesis on 20/04/2015.
 */
package com.al.events
{

    import com.al.model.IContentUnit;

    import flash.events.Event;

    public class ContentUnitChangeEvent extends Event
    {
        public static const CHANGE: String = "contentUnitChange";

        public function ContentUnitChangeEvent(contentUnit: IContentUnit, bubbles:Boolean = false) {

            super(CHANGE, bubbles, cancelable);
            _contentUnit = contentUnit;
        }


        private var _contentUnit: IContentUnit;
        public function get contentUnit(): IContentUnit {

            return _contentUnit;
        }

        override public function clone(): Event {

            return new ContentUnitChangeEvent(contentUnit, bubbles);
        }
    }
}
