/**
 * Created by lohithram on 06/05/15.
 */
package com.al.events {

    import flash.events.Event;

    public class DependenciesSatisfiedEvent extends Event {

        public static const DEPENDENCIES_SATISFIED: String = "dependenciesSatisfied";

        public function DependenciesSatisfiedEvent(bubbles:Boolean = false, cancelable:Boolean = false) {

            super(DEPENDENCIES_SATISFIED, bubbles, cancelable);
        }
    }
}
