/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import flash.events.Event;

    import mx.events.DragEvent;

    public class DroppingEvent extends Event
    {

        public static const DROPPING_EVENT: String = "droppingEvent";
        public static const DRAG_OVER: String = "dragOverEvent";

        public function DroppingEvent(event: Event = null, type:String = DROPPING_EVENT, bubbles: Boolean = false) {

            super(type, bubbles, true);
            this.sourceEvent = event;
        }

        public var sourceEvent: Event;

        override public function clone(): Event {

            return new DroppingEvent(sourceEvent, type, bubbles);
        }
    }
}
