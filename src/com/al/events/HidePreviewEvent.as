/**
 * Created by synesis on 20/04/2015.
 */
package com.al.events
{

    import flash.events.Event;

    public class HidePreviewEvent extends Event
    {
        public static const HIDE_PREVIEW: String = "hidePreview";

        public function HidePreviewEvent(bubbles:Boolean = false) {

            super(HIDE_PREVIEW, bubbles, cancelable);
        }



        override public function clone(): Event {

            return new HidePreviewEvent(bubbles);
        }
    }
}
