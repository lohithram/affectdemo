/**
 * Created by abhisekpaul on 01/06/15.
 */
package com.al.events
{
    import flash.events.Event;

    public class ItemEvent extends Event
    {
        public static const ZOOM_FACTOR_CHANGE:String = "zoomFactorChange";

        public static const HIGHLIGHT_COMMENT:String = "highlighComment";

        public static const ITEM_CLICK:String = "itemClick";

        public static const ITEM_MOVE:String = "moveItemComplete";

        public static const DOWNLOAD:String = "download";

        public static const CLIPBOARD:String = "clipboard";

        public static const REMOVE:String = "remove";

        public function ItemEvent(type:String,iitem:*,bubbles:Boolean = false)
        {
            super(type,bubbles);
            item = iitem;
        }

        public var item:*;

        override public function clone(): Event
        {

            return new ItemEvent(type, item, bubbles);
        }
    }
}
