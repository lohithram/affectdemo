/**
 * Created by lohithram on 15/05/15.
 */
package com.al.events {

    import flash.events.Event;
    import flash.net.URLRequest;

    public class OpenURLEvent extends Event {

        public static var OPEN_URL: String = "openURL";

        public function OpenURLEvent(urlRequest: URLRequest, bubbles:Boolean = true) {

            super(OPEN_URL, bubbles);
            _url = urlRequest;
        }

        private var _url: URLRequest;

        public function get urlRequest(): URLRequest{
            return _url;
        }

        override public function clone(): Event {

            return new OpenURLEvent(urlRequest, bubbles);
        }
    }
}
