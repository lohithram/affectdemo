/**
 * Created by synesis on 20/04/2015.
 */
package com.al.events
{

    import flash.events.Event;

    public class PageChangeEvent extends Event
    {
        public static const PAGE_CHANGE: String = "pageChange";

        public function PageChangeEvent(index: Number, bubbles:Boolean = false) {

            super(PAGE_CHANGE, bubbles, cancelable);
            _index = index;
        }


        private var _index: Number;
        public function get index(): Number {

            return _index;
        }

        override public function clone(): Event {

            return new PageChangeEvent(index, bubbles);
        }
    }
}
