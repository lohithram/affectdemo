/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import flash.events.Event;

    import mx.core.DragSource;

    /**
     * Generic event dispatched when items are dropped on place holder
     */
    public class PlaceHolderDropEvent extends Event
    {

        public static const DROP: String = "placeHolderDrop";

        public function PlaceHolderDropEvent(dragSource: DragSource, dropAction: String) {

            super(DROP);
            _dropAction = dropAction;
            _dragSource = dragSource;
        }

        private var _dropAction: String;
        public function get dropAction(): String {
            return _dropAction;
        }

        private var _dragSource: DragSource;
        public function get dragSource(): DragSource {
            return _dragSource;
        }

        override public function clone(): Event {

            return new PlaceHolderDropEvent(dragSource, dropAction);
        }
    }
}
