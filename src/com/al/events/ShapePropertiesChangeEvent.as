/**
 * Created by lohithram on 01/05/15.
 */
package com.al.events {

    import flash.events.Event;

    public class ShapePropertiesChangeEvent extends Event {

        public static const PROPERTY_CHANGE: String = "shapePropertyChange";

        public function ShapePropertiesChangeEvent(propertySet: Object, bubbles:Boolean = true) {

            super(PROPERTY_CHANGE, bubbles);
            _propertySet = propertySet;
        }

        private var _propertySet: Object;
        public function get propertySet(): Object{

            return _propertySet;
        }


        override public function clone():Event {

            return new ShapePropertiesChangeEvent(propertySet);
        }
    }
}
