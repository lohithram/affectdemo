/**
 * Created by synesis on 20/04/2015.
 */
package com.al.events
{

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class ShowPreviewEvent extends Event
    {
        public static const SHOW_PREVIEW: String = "showPreview";

        public function ShowPreviewEvent(index: Number, mouseEvent: MouseEvent, bubbles:Boolean = false) {

            super(SHOW_PREVIEW, bubbles, cancelable);
            _index = index;
            _mouseEvent = mouseEvent;
        }


        private var _index: Number;
        public function get index(): Number {

            return _index;
        }

        private var _mouseEvent: MouseEvent;
        public function get mouseEvent(): MouseEvent {
            return _mouseEvent;
        }

        override public function clone(): Event {

            return new ShowPreviewEvent(index, mouseEvent, bubbles);
        }
    }
}
