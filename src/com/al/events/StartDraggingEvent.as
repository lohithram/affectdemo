/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class StartDraggingEvent extends Event
    {

        public static const START_DRAGGING: String = "startDragging";

        public function StartDraggingEvent(mouseEvent: MouseEvent, deltaX: Number, deltaY: Number, bubbles: Boolean = false) {

            super(START_DRAGGING, bubbles, true);
            _mouseEvent = mouseEvent;
			_deltaX = deltaX;
			_deltaY = deltaY;
        }

        private var _mouseEvent: MouseEvent;
        public function get mouseEvent(): MouseEvent {
            return _mouseEvent;
        }
		
		private var _deltaX: Number;
		public function get deltaX(): Number{
			return _deltaX;
		}

		private var _deltaY: Number;
		public function get deltaY(): Number{
			return _deltaY;
		}

        override public function clone(): Event {

            return new StartDraggingEvent(mouseEvent, deltaX, deltaY);
        }
    }
}
