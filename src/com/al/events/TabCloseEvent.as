/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import flash.events.Event;

    public class TabCloseEvent extends Event
    {

        public static const CLOSE: String = "tabClose";

        public function TabCloseEvent(data: *, bubbles: Boolean) {

            super(CLOSE, bubbles);
            _data = data;
        }

        private var _data: *;
        public function get data(): * {
            return _data;
        }

        override public function clone(): Event {

            return new TabCloseEvent(data, bubbles);
        }
    }
}
