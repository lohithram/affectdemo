/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import com.al.tile.ICombineElement;

    import flash.events.Event;

    public class TileDropEvent extends Event
    {

        public static const DROP: String = "tileDrop";

        public function TileDropEvent(draggedTile: ICombineElement, droppedTile: ICombineElement, dropAction: String) {

            super(DROP);
            _dropAction = dropAction;
            _draggedTile = draggedTile;
            _droppedTile = droppedTile;
        }

        private var _dropAction: String;
        public function get dropAction(): String {
            return _dropAction;
        }

        private var _draggedTile: ICombineElement;
        public function get draggedTile(): ICombineElement {
            return _draggedTile;
        }

        private var _droppedTile: ICombineElement;
        public function get droppedTile(): ICombineElement {
            return _droppedTile;
        }


        override public function clone(): Event {

            return new TileDropEvent(draggedTile, droppedTile, dropAction);
        }
    }
}
