/**
 * Created by lohithram on 06/05/15.
 */
package com.al.events {

    import flash.events.Event;

    public class TileFocusedEvent extends Event {

        public static const TILE_FOCUSED: String = "tileFocused";

        public function TileFocusedEvent(bubbles: Boolean = true) {

            super(TILE_FOCUSED, bubbles);
        }
    }
}
