/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import com.al.containers.TileContainer;

    import flash.events.Event;

    public class TileFullScreenEvent extends Event
    {

        public static const FULL_SCREEN: String = "FULL_SCREEN";
        public static const EXIT_FULL_SCREEN: String = "EXIT_FULL_SCREEN";
        public static const FULL_SCREEN_EXITED: String = "FULL_SCREEN_EXITED";

        public function TileFullScreenEvent(tile: TileContainer, type: String, bubbles: Boolean=false) {

            super(type, bubbles);
            _tile = tile;
        }


        private var _tile: TileContainer;
        public function get tile(): TileContainer {
            return _tile;
        }

        override public function clone(): Event {

            return new TileFullScreenEvent(tile, type, bubbles);
        }
    }
}
