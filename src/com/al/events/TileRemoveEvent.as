/**
 * Created by lram on 26/02/2015.
 */
package com.al.events
{

    import com.al.tile.ICombineElement;

    import flash.events.Event;

    public class TileRemoveEvent extends Event
    {

        public static const REMOVE: String = "tileRemove";

        public function TileRemoveEvent(tile: ICombineElement, bubbles: Boolean = true) {

            super(REMOVE, bubbles);
            _tile = tile;
        }

        private var _tile: ICombineElement;
        public function get tile(): ICombineElement {
            return _tile;
        }


        override public function clone(): Event {

            return new TileRemoveEvent(tile);
        }
    }
}
