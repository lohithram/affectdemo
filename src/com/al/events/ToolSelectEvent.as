/**
 * Created by lohithram on 01/05/15.
 */
package com.al.events {

    import flash.events.Event;

    public class ToolSelectEvent extends Event {

        public static const SELECT: String = "drawingToolSelect";
        public function ToolSelectEvent(data: *, bubbles: Boolean=true) {

            super(SELECT, bubbles);
            _data = data;
        }

        private var _data: *;
        public function get data(): * {
            return _data
        }
    }
}
