/**
 * Created by lohithram on 01/07/15.
 */
package com.al.factory {

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.TileType;
    import com.al.model.Cell;
    import com.al.model.CellType;

    public class CellFactory {

        public static function getCellFor(tileDescriptor: TileContainerDescriptor): Cell {

            var cell: Cell = new Cell();
            cell.name = tileDescriptor.title;
            cell.filePath = tileDescriptor.currentContent.contentUrl ? tileDescriptor.currentContent.contentUrl : tileDescriptor.contentUrl;
            cell.project = tileDescriptor.project;

            switch (tileDescriptor.tileType) {

                case TileType.TEAM:
                    cell.type = CellType.TEAM;
                    break;
                case TileType.BRIEF:
                    cell.type = CellType.BRIEF;
                    break;
                case TileType.REVIEW:
                    cell.type = CellType.REVIEW;
                    break;
                case TileType.REPORT:
                    cell.type = CellType.REPORT;
                    break;
                case TileType.ACTIVITIES:
                    cell.type = CellType.ACTIVITIES;
                    break;
                case TileType.CONCEPT_BOARD:
                    cell.type = CellType.CONCEPT;
                    break;

                case TileType.HTML:
                    cell.type = CellType.HTML;
                    break;

                case TileType.CONTENT:
                        default:
                    cell.type = CellType.ASSET;
                    break;
            }

            return cell;
        }
    }
}
