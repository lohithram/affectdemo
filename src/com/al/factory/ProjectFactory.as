/**
 * Created by lohithram on 15/05/15.
 */
package com.al.factory {

    import com.al.descriptors.CombineDescriptor;
    import com.al.descriptors.ICombineDescriptor;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.ActivityStatus;
    import com.al.enums.TileType;
    import com.al.model.Project;

    import mx.collections.ArrayList;

    import mx.utils.UIDUtil;

    /**
     * This class is used as factory for creating descriptors for various types of Tiles
     */
    public class ProjectFactory {

        public static function createNewProject(): Project{

            var project:Project = new Project(UIDUtil.createUID().substr(0, 12));
            project.status = ActivityStatus.NEW;
            project.activities = new ArrayList();
            project.title = "New Project";
            return project;
        }

        public static function createProjectCombine(project: Project, index: int): ICombineDescriptor {

            var combine: ICombineDescriptor;

            combine = new CombineDescriptor(index, project.title);
            createProjectTemplate1(combine);

            for each(var desc: TileContainerDescriptor in combine.tileContainers.source){

                desc.project = project;
            }
            return combine;
        }

        public static function createCombineForTileDesc(tileDesc: TileContainerDescriptor, index: int): ICombineDescriptor {

            var combine: ICombineDescriptor = new CombineDescriptor(index, tileDesc.title);
            combine.tileContainers.addItem(tileDesc);
            tileDesc.reducedX = tileDesc.reducedY = 0;
            tileDesc.reducedWidth = tileDesc.reducedHeight = 100;

            return combine;
        }


        private static function createProjectTemplate1(combine: ICombineDescriptor): void{

            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.PROJECT_DETAILS, 0, 0, 60, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.PROJECT_ASSETS, 0, 50, 60, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.ACTIVITIES, 60, 0, 40, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.REPORT, 60, 50, 40, 50));
        }

        private static function createProjectTemplate2(combine: ICombineDescriptor): void{

            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.PROJECT_ASSETS, 0, 0, 100, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.ACTIVITIES, 0, 50, 50, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.REPORT, 50, 50, 50, 50));
        }

        private static function createProjectTemplate3(combine: ICombineDescriptor): void{

            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.PROJECT_ASSETS, 0, 0, 40, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.ACTIVITIES, 0, 50, 40, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.BRIEF, 40, 0, 60, 50));
            combine.tileContainers.addItem(TileContainerFactory.createTileDescriptor(TileType.REPORT, 40, 50, 60, 50));
        }
    }

}
