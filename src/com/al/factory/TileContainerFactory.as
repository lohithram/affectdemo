/**
 * Created by lohithram on 15/05/15.
 */
package com.al.factory
{

    import com.al.containers.ActivitiesTile;
    import com.al.containers.BriefTile;
    import com.al.containers.CollectionsTile;
    import com.al.containers.CollectionsTile;
    import com.al.containers.ConceptBoard;
    import com.al.containers.ConceptBoard;
    import com.al.containers.ContentContainer;
    import com.al.containers.HTMLContainer;
    import com.al.containers.IDescriptorReceiver;
    import com.al.containers.MultiPageContentContainer;
    import com.al.containers.ProjectDetailsTile;
    import com.al.containers.ReportTile;
    import com.al.containers.ReviewTile;
    import com.al.containers.TileContainer;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.ContentType;
    import com.al.enums.TileDataFormats;
    import com.al.enums.TileType;
    import com.al.model.Cell;
    import com.al.model.CellType;
    import com.al.model.ConceptBoardTemplate;
    import com.al.model.FormTemplate;
    import com.al.model.ImageInfo;
    import com.al.model.PageContent;
    import com.al.model.Project;
    import com.al.model.TileContentDescriptor;
    import com.al.model.TileDataModel;
    import com.al.tiles.team.TeamTile;
    import com.al.util.ContentUtil;
    import com.al.util.FileUtils;

    import mx.core.ClassFactory;

    import mx.core.IVisualElement;
    import mx.core.IVisualElementContainer;
    import mx.styles.IStyleClient;
    import mx.utils.UIDUtil;
    import mx.utils.URLUtil;

    /**
     * This class is used as factory for creating descriptors for various types of Tiles
     */
    public class TileContainerFactory
    {

        public static function createTileDescriptor(tileType:String, reducedX:Number, reducedY:Number, reducedWidth:Number, reducedHeight:Number, project:Project = null):TileContainerDescriptor
        {

            var desc:TileContainerDescriptor = createTileDescriptorForType(tileType, project);
            desc.reducedHeight = reducedHeight;
            desc.reducedWidth = reducedWidth;
            desc.reducedY = reducedY;
            desc.reducedX = reducedX;

            return desc;
        }

        public static function createTileContainerForCell(cell:Cell):TileContainer
        {

            var tileDescriptor:TileContainerDescriptor = cell.getTileContainerDescriptor();
            var tileContainer:TileContainer = createTileContainer(tileDescriptor);
            var tileContent:IVisualElement = tileContainer.getElementAt(0);
            if (tileContent is BriefTile)
            {
                (tileContent as BriefTile).form = cell.model;
            }
            else if (tileContent is ReviewTile)
            {

            }
            else if (tileContent is TeamTile)
            {

            }
            else if (tileContent is ActivitiesTile)
            {

            }
            else if (tileContent is ReportTile)
            {

            }

            return tileContainer;

        }

        public static function createTileContainer(descriptor:TileContainerDescriptor):TileContainer
        {

            var tileContainer:TileContainer = new TileContainer(descriptor);
            tileContainer.tileContainerModel.tileTitle = descriptor.title;

            switch (descriptor.tileType)
            {

                case TileType.CONTENT:
                case TileType.HTML:
                    tileContainer.tileContainerModel.canCollaborate = true;
                    tileContainer.tileContainerModel.overlaySupported = true;
                    break;

                case TileType.MULTI_PAGE:
                case TileType.CONCEPT_BOARD:
                    tileContainer.tileContainerModel.canCollaborate = true;
                    tileContainer.tileContainerModel.overlaySupported = false;
                    break;
                case TileType.TEAM:
                case TileType.BRIEF:
                case TileType.REVIEW:
                case TileType.REPORT:
                case TileType.ACTIVITIES:
                case TileType.PROJECT_ASSETS:
                    tileContainer.tileContainerModel.canCollaborate = false;
                    tileContainer.tileContainerModel.overlaySupported = false;
                    break;
            }

            (tileContainer as IVisualElementContainer).addElement(getChildContent(descriptor));
            tileContainer.reducedX = descriptor.reducedX;
            tileContainer.reducedY = descriptor.reducedY;
            tileContainer.reducedWidth = descriptor.reducedWidth;
            tileContainer.reducedHeight = descriptor.reducedHeight;
            (tileContainer as IStyleClient).setStyle("backgroundColor", descriptor.backgroundColor);

            return tileContainer;
        }

        public static function createTileDescriptorForAssetURL(contentURL:String):TileContainerDescriptor
        {

            var contentDescriptor:TileContentDescriptor = ContentUtil.createContentDescriptorForAssetURL(contentURL);

            var tileType:String = TileType.CONTENT;
            if(URLUtil.isHttpURL(contentURL) && !FileUtils.isImageUrl(contentURL))
            {
                tileType = TileType.HTML;
            }
            var tileDesc:TileContainerDescriptor =
                    TileContainerFactory.createTileDescriptorForType(tileType);


            tileDesc.title = ContentUtil.determineContentTitle(contentURL);
            tileDesc.currentContent = contentDescriptor;

            return tileDesc;
        }

        public static function createTileDescriptorForPreview(preview:Array):TileContainerDescriptor
        {

            var tileDesc:TileContainerDescriptor =
                    TileContainerFactory.createTileDescriptorForType(TileType.CONTENT);
            var contentDescriptor:TileContentDescriptor =
                    ContentUtil.createContentDescriptorForAssetURL((preview[0] as ImageInfo).url);

            contentDescriptor.contentType =
                            preview.length > 1 ? ContentType.MULTI_PAGE : ContentType.SINGLE_DOCUMENT;
            for (var i:int = 0; i < preview.length; ++i)
            {

                var page:PageContent = new PageContent(String(i + 1), i + 1, (preview[i] as ImageInfo).url);
                contentDescriptor.contentUnits.addItem(page);
            }

            tileDesc.currentContent = contentDescriptor;
            tileDesc.title = ContentUtil.determineContentTitle(contentDescriptor.contentUrl);

            return tileDesc;
        }


        public static function createTileDescriptorForType(tileType:String, project:Project = null):TileContainerDescriptor
        {

            var desc:TileContainerDescriptor = new TileContainerDescriptor(tileType);
            desc.project = project;

            switch (tileType)
            {

                case TileType.CONTENT:
                case CellType.ASSET:
                    desc.title = "Content";
                    break;

                case TileType.HTML:
                case CellType.HTML:
                    desc.title = "Web";
                    break;

                case TileType.BRIEF:
                case CellType.BRIEF:
                    desc.title = "Brief Form";
                    break;

                case TileType.REVIEW:
                case CellType.REVIEW:
                    desc.title = "Review [ID " + project.code + "]";
                    break;

                case TileType.ACTIVITIES:
                    desc.title = "Activities and Estimates";
                    break;

                case TileType.REPORT:
                    desc.title = "Actual + Budget Activities";
                    break;

                case TileType.PROJECT_ASSETS:
                    desc.title = "Project Assets";
                    break;

                case TileType.PROJECT_DETAILS:
                    desc.title = "Project Details";
                    break;

                case TileType.CONCEPT_BOARD:
                case CellType.CONCEPT:
                    desc.title = "Concept Board";
                    desc.currentContent = new TileContentDescriptor(UIDUtil.createUID());
                    desc.currentContent.contentType = ContentType.CONCEPT_BOARD;
                    break;

                case TileType.TEAM:
                    desc.title = "Your Team [ID " + project.code + "]";
                    break;
            }
            return desc;
        }

        public static function getTileType(cellType:String):String
        {

            switch (cellType)
            {
                case CellType.MULTI_PAGE:
                    return TileType.MULTI_PAGE;

                case CellType.ASSET:
                    return TileType.CONTENT;

                case CellType.BRIEF:
                    return TileType.BRIEF;

                case CellType.REVIEW:
                    return TileType.REVIEW;

                case CellType.CONCEPT:
                    return TileType.CONCEPT_BOARD;

                case CellType.TEAM:
                    return TileType.TEAM;

                case CellType.ACTIVITIES:
                    return TileType.ACTIVITIES;

                case CellType.REPORT:
                    return TileType.REPORT;

                case CellType.PROJECT_ASSETS:
                    return TileType.PROJECT_ASSETS;

                case CellType.PROJECT_DETAILS:
                    return TileType.PROJECT_DETAILS;

                case CellType.HTML:
                    return TileType.HTML;

                default:
                    return TileType.CONTENT;
            }

        }

        public static function createTileDescriptorForCell(cell:Cell):TileContainerDescriptor
        {

            var desc:TileContainerDescriptor = new TileContainerDescriptor(getTileType(cell.type));
            desc.project = cell.project;

            desc.currentContent = new TileContentDescriptor(cell.id ? cell.id : UIDUtil.createUID());
            desc.currentContent.pendingAnnotations = cell.pendingAnnotations;
            //td.currentContent.collaborationSession = collaborationSession;

            desc.currentContent.contentType = ContentType.SINGLE_DOCUMENT;

            desc.currentContent.contentUnits = cell.pages;

            if(cell.type == CellType.CONCEPT)
            {
                desc.currentContent.contentType = ContentType.CONCEPT_BOARD;
            }
            else if(cell.type == CellType.MULTI_PAGE)
            {
                desc.currentContent.contentType = ContentType.MULTI_PAGE;
                desc.contents = new Array(desc.currentContent);
            }


            desc.currentContent.contentUrl = cell.filePath;
            desc.title = cell.name;

            desc.model = cell.model;

            switch (cell.type)
            {

                case CellType.NOTE:
                    desc.currentContent.contentType = ContentType.NOTE;
                    break;

                case CellType.REVIEW:
                    desc.title = cell.name ? cell.name + " [ID " + cell.project.code + "]":"Review [ID " + cell.project.code + "]";
                    break;

                case CellType.CONCEPT:
                    desc.currentContent.contentType = ContentType.CONCEPT_BOARD;
                    break;

                case CellType.TEAM:
                    desc.title = cell.name ? cell.name + " [ID " + cell.project.code + "]" : "Your Team [ID " + cell.project.code + "]";
                    break;

                case CellType.ASSET:
                    desc.title = cell.name ? cell.name : "Content";
                    break;

                case CellType.BRIEF:
                    desc.title = cell.name ? cell.name : "Brief";
                    break;

                case CellType.ACTIVITIES:
                    desc.title = cell.name ? cell.name : "Activities";
                    break;

                case CellType.REPORT:
                    desc.title = cell.name ? cell.name : "Report";
                    break;

                case CellType.PROJECT_ASSETS:
                    desc.title = cell.name ? cell.name : "Project Assets";
                    break;

                case CellType.PROJECT_DETAILS:
                    desc.title = cell.name ? cell.name : "Project Details";
                    break;

                case CellType.HTML:
                    desc.title = cell.name ? cell.name : "Web";
                    break;
            }

            return desc;
        }

        private static function getChildContent(descriptor:TileContainerDescriptor):IVisualElement
        {

            var contentContainer:IVisualElement;

            switch (descriptor.tileType)
            {

                case TileType.CONTENT:
                    contentContainer = new ContentContainer();
                    if (descriptor.currentContent)
                        (contentContainer as ContentContainer).contentDescriptor = descriptor.currentContent;
                    break;

                case TileType.MULTI_PAGE:

                    contentContainer = new ContentContainer();
                    if (descriptor.currentContent)
                    {
                        (contentContainer as ContentContainer).contentDescriptor = descriptor.currentContent;
                        (contentContainer as ContentContainer).contentDescriptor.contentFactory = new ClassFactory(MultiPageContentContainer);
                    }
                    break;

                case TileType.CONCEPT_BOARD:

                    contentContainer = new ContentContainer();
                    if (descriptor.currentContent)
                    {
                        var factory: ClassFactory = new ClassFactory(ConceptBoard);
                        factory.properties = {model: descriptor.model};
                        (contentContainer as ContentContainer).contentDescriptor = descriptor.currentContent;
                        (contentContainer as ContentContainer).contentDescriptor.contentFactory = factory;
                    }
                    break;

                case TileType.HTML:
                    contentContainer = new ContentContainer();
                    if (descriptor.currentContent)
                    {
                        (contentContainer as ContentContainer).contentDescriptor = descriptor.currentContent;
                        (contentContainer as ContentContainer).contentDescriptor.contentFactory = new ClassFactory(HTMLContainer);
                    }
                    break;

                case TileType.BRIEF:
                    contentContainer = new BriefTile();
                    (contentContainer as BriefTile).project = descriptor.project;
                    (contentContainer as BriefTile).form = descriptor.model as FormTemplate;
                    break;

                case TileType.REVIEW:
                    contentContainer = new ReviewTile();
                    (contentContainer as ReviewTile).project = descriptor.project;
                    break
                case TileType.ACTIVITIES:
                    contentContainer = new ActivitiesTile();
                    (contentContainer as ActivitiesTile).project = descriptor.project;
                    break;
                case TileType.REPORT:
                    contentContainer = new ReportTile();
                    (contentContainer as ReportTile).project = descriptor.project;
                    break;
                case TileType.PROJECT_ASSETS:
                    contentContainer = new CollectionsTile();
                    (contentContainer as CollectionsTile).project = descriptor.project;
                    break;

                case TileType.PROJECT_DETAILS:
                    contentContainer = new ProjectDetailsTile();
                    (contentContainer as ProjectDetailsTile).project = descriptor.project;
                    break;

                case TileType.TEAM:
                    contentContainer = new TeamTile();
                    (contentContainer as TeamTile).project = descriptor.project;
                    break;

                default:
                    contentContainer = new CollectionsTile();
                    (contentContainer as CollectionsTile).project = descriptor.project;
                    break;
            }

            contentContainer.top = 0;
            contentContainer.left = 0;
            contentContainer.right = 0;
            contentContainer.bottom = 0;

            if (contentContainer is IDescriptorReceiver)
            {

                (contentContainer as IDescriptorReceiver).contentDescriptor = descriptor;
            }

            return contentContainer;
        }
    }

}

import mx.core.ClassFactory;

class ConceptBoardFactory extends ClassFactory{


}
