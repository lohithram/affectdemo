/**
 * Created by abhisekpaul on 08/06/15.
 */
package com.al.interfaces
{
    import mx.collections.ArrayCollection;

    public interface IUtilityGroup
    {
        function get contents():ArrayCollection
        function get title():String;
    }
}
