/**
 * Created by lram on 24/02/2015.
 */
package com.al.layouts
{

    import com.al.tile.ICombineElement;
    import com.al.tile.ITileContainer;
    import com.al.tile.ITileDropIndicator;
    import com.al.util.EventUtils;
    import com.al.util.TransitionUtil;

    import flash.display.InteractiveObject;

    import flash.geom.Matrix;
    import flash.geom.Point;

    import mx.core.IUIComponent;

    import mx.core.IVisualElement;
    import mx.effects.Effect;

    import mx.effects.Parallel;
    import mx.events.EffectEvent;

    import spark.effects.Animate;

    import spark.effects.Fade;

    import spark.effects.Resize;
    import spark.effects.animation.MotionPath;
    import spark.effects.animation.SimpleMotionPath;

    import spark.layouts.supportClasses.LayoutBase;

    public class CombineLayout extends LayoutBase
    {

        protected static const DEFAULT_WIDTH: Number = 800;
        protected static const DEFAULT_HEIGHT: Number = 600;
        protected static const DEFAULT_MIN_WIDTH: Number = 400;
        protected static const DEFAULT_MIN_HEIGHT: Number = 300;
        
		public var GAP: Number = 4;
		public var BORDER_GAP: Number = 8;

        public var animate: Boolean;

        //--------------------------------------------------------------------------
        //
        //  Constructor
        //
        //--------------------------------------------------------------------------

        public function CombineLayout() {
        }


        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------
        override public function measure(): void {

            // The container using this layout will be measured by its parent. Any way
            // we set default/desired dimension here

            target.measuredWidth = DEFAULT_WIDTH;
            target.measuredHeight = DEFAULT_HEIGHT;
            target.measuredMinWidth = DEFAULT_MIN_WIDTH;
            target.measuredMinHeight = DEFAULT_MIN_HEIGHT;
        }

        private var animating: Boolean;
        private var effects: Array;

        override public function updateDisplayList(width: Number, height: Number): void {

            if(animating) {

                ////trace("INVALIDATE Requested");
                target.invalidateDisplayList();
                return;
            }

            var scaleX: Number = width / 100;
            var scaleY: Number = height / 100;

            var scaleMatrix: Matrix = new Matrix();
            scaleMatrix.scale(scaleX,scaleY);

			if(animate) {

                effects = new Array();
                ////trace("Start ANIMATION");
            }

            for( var i: int = 0; i < target.numElements; ++i ) {

                var element: ICombineElement = target.getElementAt(i) as ICombineElement;
                
				if(element && element.includeInLayout){

	                var locationPoint: Point = new Point(element.reducedX, element.reducedY);
	                var sizePoint: Point = new Point(element.reducedWidth, element.reducedHeight);
	                locationPoint = scaleMatrix.transformPoint(locationPoint);
	                sizePoint = scaleMatrix.transformPoint(sizePoint);
                    var oldX: Number = element.x;
                    var oldY: Number = element.y;
                    var oldWidth: Number = element.width;
                    var oldHeight: Number = element.height;

	
					// sitting on the border
					if(element.reducedX == 0){
						locationPoint.x += BORDER_GAP;
						sizePoint.x -= BORDER_GAP;
					}else{
						locationPoint.x += GAP;	
						sizePoint.x -= GAP;
					}
					
					if(element.reducedY == 0){
						locationPoint.y += BORDER_GAP;
						sizePoint.y -= BORDER_GAP;
					}else{
						locationPoint.y += GAP;
						sizePoint.y -= GAP;			
					}
					
					element.x = locationPoint.x;
					element.y = locationPoint.y;
					
					if(element.includeInSizing){
						
						if(element.reducedX + element.reducedWidth == 100 ){
							sizePoint.x -= BORDER_GAP;	
						}else{
							sizePoint.x -= GAP;		
						}
						if(element.reducedY + element.reducedHeight == 100 ){
							sizePoint.y -= BORDER_GAP;	
						}else{
							sizePoint.y -= GAP;		
						}
					
						element.width = sizePoint.x;
						element.height = sizePoint.y;
					}

                    if(animate){

                        animating = true;
                        if(element is ITileContainer)
                            getResizeEffect(element, oldX, oldY, oldWidth, oldHeight ).play();
                        else if(element is ITileDropIndicator)
                            getRevealEffect(element).play();
                    }
				}
            }

            //consume once..
            animate = false;
            reCalculateVisualIndices();
        }

        private function checkForAnimationCompletion(): void{

            if(effects.length == 0) {

                animating = false;
                ////trace("ANIMATION Complete");
            }
        }

        //--------------------------------------------------------------------------
        //
        //  Private
        //
        //--------------------------------------------------------------------------
        private function reCalculateVisualIndices(): void {

            if(target.numElements == 0 )
                return;

            var sortedElements: Array = [];

            for(var i: int=0; i<target.numElements; ++i){

                var element: ICombineElement = target.getElementAt(i) as ICombineElement;
                if(element && !(element is ITileDropIndicator))
					sortedElements.push(element);
            }
            sortedElements.sort(visualHierarchyFunction);
            i = 0;
            for each(element in sortedElements)
                element.visualIndex = i++;
        }

		/**
		 * 
		 * @param elementA
		 * @param elementB
		 * @return 
		 * 
		 */
        private function visualHierarchyFunction(elementA: ICombineElement, elementB: ICombineElement): int{

            if(elementA.reducedY > elementB.reducedY){
                return 1;
            }
            else if(elementA.reducedY < elementB.reducedY){
                return -1;
            }
            else{
                //both elements reducedY are equal
                if(elementA.reducedX > elementB.reducedX)
                    return 1;
                else if(elementA.reducedX < elementB.reducedX)
                    return -1;
                else
                    return 0;
            }
        }


        var effectDuration: int = TransitionUtil.SHRINK_TIME;

        private function getResizeEffect(element: IVisualElement, oldX: Number, oldY: Number, oldWidth: Number, oldHeight: Number): Animate{

            var effect: Animate = new Animate(element);
            effect.duration = effectDuration;
            effect.motionPaths = new Vector.<MotionPath>();
            effect.motionPaths.push( new SimpleMotionPath("x", oldX, element.x) );
            effect.motionPaths.push( new SimpleMotionPath("y", oldY, element.y) );
            effect.motionPaths.push( new SimpleMotionPath("width", oldWidth, element.width) );
            effect.motionPaths.push( new SimpleMotionPath("height", oldHeight, element.height) );
            effect.addEventListener(EffectEvent.EFFECT_END, onResizeEffectEnd);

            effects.push(effect);
            return effect;
        }


        private function getRevealEffect(element: IVisualElement): Effect{

            var effect: Parallel = new Parallel(element);
            effect.startDelay = effectDuration * 0.2;
            var resizeEffect: Resize = new Resize();
            resizeEffect.duration = effectDuration;
            resizeEffect.heightFrom = 0;
            resizeEffect.widthFrom = 0;
            resizeEffect.heightTo = element.height;
            resizeEffect.widthTo = element.width;

            var fade: Fade = new Fade();
            fade.duration = effectDuration;
            fade.alphaFrom = 0.1;
            fade.alphaTo = 1.0;
            effect.addChild(fade);

            element.visible = false;
            (element as IUIComponent).enabled = false;
            effect.addEventListener(EffectEvent.EFFECT_START, onRevealEffectStart);
            effect.addEventListener(EffectEvent.EFFECT_END, onRevealEffectEnd);

            effects.push(effect);
            return effect;
        }

        private function onRevealEffectStart(event: EffectEvent){

            (event.effectInstance.target as InteractiveObject).visible = true;
        }

        private function onRevealEffectEnd(event: EffectEvent){

            EventUtils.removeListener(event, onRevealEffectEnd);
            (event.effectInstance.target as IUIComponent).enabled = true;
            effects.pop();
            checkForAnimationCompletion();
        }

        private function onResizeEffectEnd(event: EffectEvent){

            EventUtils.removeListener(event, onResizeEffectEnd);
            effects.pop();
            checkForAnimationCompletion();
        }
    }
}

