/**
 * Created by lram on 04/02/2015.
 */
package com.al.layouts
{

    import flash.events.Event;
    import flash.events.IEventDispatcher;
    import flash.events.MouseEvent;

    import mx.core.FlexGlobals;

    import mx.events.FlexEvent;
    import mx.events.ResizeEvent;

    import spark.components.supportClasses.GroupBase;
    import spark.layouts.TileLayout;

    [Event("change", type="flash.events.Event")]
    public class DashboardLayout extends TileLayout
    {

        private static const MIN_COL_WIDTH: Number = 250;
        private static const MAX_COL_WIDTH: Number = 300;
        private static const MIN_ROW_HEIGHT: Number = 220;
        private static const MAX_ROW_HEIGHT: Number = 260;


        public var rows: Number;

        public var columns: Number;

        override public function set target(value:GroupBase):void{

            if(target){
                target.removeEventListener(ResizeEvent.RESIZE, onResize);
            }

            super.target = value;

            if(value){
                value.addEventListener(FlexEvent.CREATION_COMPLETE, onInit);
                FlexGlobals.topLevelApplication.addEventListener(FlexEvent.CREATION_COMPLETE, onInit);
                //value.addEventListener(ResizeEvent.RESIZE, onResize);
                FlexGlobals.topLevelApplication.addEventListener(ResizeEvent.RESIZE, onResize);
            }
        }

        private function onInit(event: FlexEvent): void {

            (event.target as IEventDispatcher).removeEventListener(event.type, onInit);

            calculateRowAndColumns();
        }

        private function onResize(event: ResizeEvent): void{

            var currentWidth: Number = FlexGlobals.topLevelApplication.width;
            var currentHeight: Number = FlexGlobals.topLevelApplication.height;


            if(currentWidth > event.oldWidth)
                onGoingWider();
            else if( currentWidth < event.oldWidth)
                onGoingNarrower();

            if(currentHeight > event.oldHeight)
                onGoingTaller();
            else if (currentHeight < event.oldHeight)
            {
                onGoingShorter();
            }
        }

        private function calculateRowAndColumns(): void{

            var currentWidth: Number =
                    FlexGlobals.topLevelApplication.width - ((columns-1) * horizontalGap) - paddingLeft - paddingRight;
            var currentHeight: Number =
                    FlexGlobals.topLevelApplication.height - ((rows-1) * verticalGap) - paddingBottom - paddingTop;

            var calculatedColumns: int = Math.floor((currentWidth-horizontalGap)/MIN_COL_WIDTH);
            var calculatedRows: int = Math.floor((currentHeight-verticalGap)/MIN_ROW_HEIGHT);

            //trace("Calculated rows vs current rows: " + calculatedRows+'-'+rows);
            //trace("Calculated cols vs current cols: " + calculatedColumns+'-'+columns);

            if(calculatedRows != rows || calculatedColumns != columns)
                var notify: Boolean = true;

            requestedColumnCount = columns = calculatedColumns;
            requestedRowCount = rows = calculatedRows;

            rowHeight = Math.min(MAX_ROW_HEIGHT, Math.floor(currentHeight/calculatedRows));
            columnWidth = Math.min(MAX_COL_WIDTH, Math.floor(currentWidth/calculatedColumns));

            if(notify)
                dispatchEvent(new Event(Event.CHANGE));
        }

        private function calculateRowAndColumns2(): void{

            var currentWidth: Number =
                    FlexGlobals.topLevelApplication.width - ((columns-1) * horizontalGap) - paddingLeft - paddingRight;
            var currentHeight: Number =
                    FlexGlobals.topLevelApplication.height - ((rows-1) * verticalGap) - paddingBottom - paddingTop;

            var availableWidth: Number =
                    FlexGlobals.topLevelApplication.width -
                        (columns * columnWidth) - ((columns-1) * horizontalGap) - paddingLeft - paddingRight;
            var availableHeight: Number =
                    FlexGlobals.topLevelApplication.height -
                    (rows * rowHeight) - ((rows-1) * verticalGap) - paddingBottom - paddingTop;


            var calculatedRows: int = rows;
            var calculatedColumns: int = columns;

            if(availableWidth >= MIN_COL_WIDTH){
                ++calculatedColumns;
            }

            if(availableHeight > MIN_ROW_HEIGHT){
                ++calculatedRows;
            }

            //trace("Calculated rows vs current rows: " + calculatedRows+'-'+rows);
            //trace("Calculated cols vs current cols: " + calculatedColumns+'-'+columns);

            if(calculatedRows != rows || calculatedColumns != columns)
                var notify: Boolean = true;

            requestedColumnCount = columns = calculatedColumns;
            requestedRowCount = rows = calculatedRows;

            columnWidth = Math.min(MAX_COL_WIDTH, Math.floor(currentWidth/calculatedColumns));
            rowHeight = Math.min(MAX_ROW_HEIGHT, Math.floor(currentHeight/calculatedRows));

            if(notify)
                dispatchEvent(new Event(Event.CHANGE));
        }

        private function onGoingWider(): void {

            calculateRowAndColumns();
        }

        private function onGoingNarrower(): void {
            calculateRowAndColumns();

        }

        private function onGoingTaller(): void {
            calculateRowAndColumns();
        }

        private function onGoingShorter(): void {
            calculateRowAndColumns();

        }

        override public function measure(): void{

            super.measure();
        }

        override public function updateDisplayList(width: Number, height: Number): void{

            super.updateDisplayList(width, height);
        }


    }
}
