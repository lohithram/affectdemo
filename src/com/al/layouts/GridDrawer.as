/**
 * Created by lram on 09/02/2015.
 */
package com.al.layouts
{

    import flash.display.Graphics;

    import mx.core.UIComponent;

    public class GridDrawer
    {

        public var rows: Number;

        public var columns: Number;

        public var columnWidth: Number;

        public var rowHeight: Number;

        public var paddingLeft: Number;
        public var paddingRight: Number;
        public var paddingTop: Number;
        public var paddingBottom: Number;

        public function draw(target: UIComponent) {

            var graphics: Graphics = target.graphics;
            graphics.clear();

            var width: Number = (columns*columnWidth);
            var height: Number = (rows*rowHeight);

            var x: int=paddingLeft;
            var y: int=paddingTop;

            graphics.lineStyle(2, 0x000000, 0.4);
            for(var row: int = 0; row<rows; ++row){

                graphics.moveTo(x, y+(rowHeight*row));
                graphics.lineTo(x+width, y+(rowHeight*row));
            }

            for(var col: int = 0; col<columns; ++col){

                graphics.moveTo(x+(columnWidth*col), y);
                graphics.lineTo(x+(columnWidth*col), y+height);
            }

            if(rows > 0 && columns > 0){
                graphics.lineStyle(4, 0x000000, 0.6);
                graphics.drawRect(x,y,width,height);
            }
        }
    }
}
