/**
 * Created by lram on 04/02/2015.
 */
package com.al.layouts
{

    import com.al.util.EventUtils;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;

    import mx.core.FlexGlobals;

    import mx.events.FlexEvent;
    import mx.events.ResizeEvent;

    import spark.components.supportClasses.GroupBase;
    import spark.layouts.TileLayout;

    [Event("change", type="flash.events.Event")]
    public class GridMakerOld extends EventDispatcher //extends TileLayout
    {

        private static const MIN_COL_WIDTH: Number = 250;
        private static const MAX_COL_WIDTH: Number = 300;
        private static const MIN_ROW_HEIGHT: Number = 220;
        private static const MAX_ROW_HEIGHT: Number = 260;

        [Bindable]
        public var rows: Number;

        [Bindable]
        public var columns: Number;

        [Bindable]
        public var columnWidth: Number;

        [Bindable]
        public var rowHeight: Number;

        public var paddingLeft: Number;
        public var paddingRight: Number;
        public var paddingTop: Number;
        public var paddingBottom: Number;

        public function GridMaker(): void{

            rows = columns = 0;
            rowHeight = MIN_ROW_HEIGHT;
            columnWidth = MIN_COL_WIDTH;
            paddingLeft = paddingRight = paddingBottom = paddingTop = 8;
        }

        private var _target: GroupBase;

        public function set target(value:GroupBase): void{
            if(_target){
                _target.removeEventListener(ResizeEvent.RESIZE, onResize);
            }
            _target = value;
            if(value){
                value.addEventListener(FlexEvent.CREATION_COMPLETE, onInit);
                FlexGlobals.topLevelApplication.addEventListener(FlexEvent.CREATION_COMPLETE, onInit);
                FlexGlobals.topLevelApplication.addEventListener(ResizeEvent.RESIZE, onResize);
            }
        }

        public function get target(): GroupBase{
            return _target;
        }


        private function onInit(event: FlexEvent): void {

            EventUtils.removeListener(event, onInit);

            calculateRowAndColumns();
        }

        private function onResize(event: ResizeEvent): void{

            var currentWidth: Number = FlexGlobals.topLevelApplication.width;
            var currentHeight: Number = FlexGlobals.topLevelApplication.height;

            var notify: Boolean;

            if(currentWidth > event.oldWidth)
                notify = onGoingWider();
            else if( currentWidth < event.oldWidth)
                notify = onGoingNarrower();

            if(currentHeight > event.oldHeight)
                notify ||= onGoingTaller();
            else if (currentHeight < event.oldHeight)
                notify ||= onGoingShorter();

            if(notify)
                dispatchEvent(new Event(Event.CHANGE));
        }

        private function calculateRowAndColumns(): void{

            var currentWidth: Number =
                    FlexGlobals.topLevelApplication.width - paddingLeft - paddingRight;
            var currentHeight: Number =
                    FlexGlobals.topLevelApplication.height - paddingBottom - paddingTop;

            var calculatedColumns: int = Math.floor(currentWidth/MIN_COL_WIDTH);
            var calculatedRows: int = Math.floor(currentHeight/MIN_ROW_HEIGHT);

            //trace("Calculated rows vs current rows: " + calculatedRows+'-'+rows);
            //trace("Calculated cols vs current cols: " + calculatedColumns+'-'+columns);

            if(calculatedRows != rows || calculatedColumns != columns)
                var notify: Boolean = true;

            columns = calculatedColumns;
            rows = calculatedRows;

            rowHeight = Math.min(MAX_ROW_HEIGHT, Math.floor(currentHeight/calculatedRows));
            columnWidth = Math.min(MAX_COL_WIDTH, Math.floor(currentWidth/calculatedColumns));

            if(notify)
                dispatchEvent(new Event(Event.CHANGE));
        }

        private function onGoingWider(): Boolean {

            var currentWidth: Number =
                    FlexGlobals.topLevelApplication.width - paddingLeft - paddingRight;

            var calculatedColumns: int = Math.floor(currentWidth/MIN_COL_WIDTH);

            //trace("Calculated cols vs current cols: " + calculatedColumns+'-'+columns);

            if(calculatedColumns != columns)
                var notify: Boolean = true;

            columns = calculatedColumns;

            columnWidth = Math.min(MAX_COL_WIDTH, Math.floor(currentWidth/calculatedColumns));

            return notify;

        }

        private function onGoingNarrower(): Boolean {
            var currentWidth: Number =
                    FlexGlobals.topLevelApplication.width - paddingLeft - paddingRight;

            var calculatedColumns: int = columns;

            if(currentWidth < (columns*MIN_COL_WIDTH))
            {
                --calculatedColumns;
            }


            //trace("Calculated cols vs current cols: " + calculatedColumns + '-' + columns);

            var notify:Boolean = calculatedColumns != columns;

            columns = calculatedColumns;

            columnWidth = Math.min(MAX_COL_WIDTH, Math.floor(currentWidth / calculatedColumns));


            return notify;

        }

        private function onGoingTaller(): Boolean {

            var currentHeight: Number =
                    FlexGlobals.topLevelApplication.height - paddingBottom - paddingTop;
            var availableHeight: Number =
                    FlexGlobals.topLevelApplication.height -
                    (rows * rowHeight) - paddingBottom - paddingTop;


            var calculatedRows: int = rows;


            if(availableHeight > MIN_ROW_HEIGHT){
                ++calculatedRows;
            }

            //trace("Calculated rows vs current rows: " + calculatedRows+'-'+rows);

            if(calculatedRows != rows )
                var notify: Boolean = true;

            rows = calculatedRows;
            rowHeight = Math.min(MAX_ROW_HEIGHT, Math.floor(currentHeight/calculatedRows));

            return notify;
        }

        private function onGoingShorter(): Boolean {

            var currentHeight: Number =
                    FlexGlobals.topLevelApplication.height - paddingBottom - paddingTop;

            var calculatedRows: int = rows;


            if(currentHeight < (rows * MIN_ROW_HEIGHT) ){
                --calculatedRows;
            }

            //trace("Calculated rows vs current rows: " + calculatedRows+'-'+rows);

            var notify: Boolean = (calculatedRows != rows);

            rows = calculatedRows;
            rowHeight = Math.min(MAX_ROW_HEIGHT, Math.floor(currentHeight/calculatedRows));

            return notify;

        }

    }
}
