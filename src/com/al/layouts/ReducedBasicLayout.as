package com.al.layouts
{
	import com.al.visualElement.IReducedVisualElement;
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import mx.core.IVisualElement;
	
	import spark.layouts.BasicLayout;
	import spark.layouts.supportClasses.LayoutBase;
	
	public class ReducedBasicLayout extends BasicLayout
	{
		
		//--------------------------------------------------------------------------
		//
		//  Overrides
		//
		//--------------------------------------------------------------------------
		override public function updateDisplayList(width: Number, height: Number): void {
		
			var scaleX: Number = width / 100;
			var scaleY: Number = height / 100;
			
			var scaleMatrix: Matrix = new Matrix();
			scaleMatrix.scale(scaleX,scaleY);
			
			var element: IReducedVisualElement;

            var locationPoint: Point;
            var sizePoint: Point;
            for( var i: int = 0; i < target.numElements; ++i ) {

                if(!target.getElementAt(i)) continue;

                sizePoint = null;
                locationPoint = null;
				element = target.getElementAt(i) as IReducedVisualElement;

				if(element && element.includeInLayout){
					
					locationPoint = new Point(element.reducedX, element.reducedY);
				}
				else{

					var visualElement: IVisualElement = target.getElementAt(i);
					if(visualElement.includeInLayout &&
						(visualElement as Object).hasOwnProperty("reducedX") && 
						(visualElement as Object).hasOwnProperty("reducedY")){
						
						locationPoint = new Point(visualElement["reducedX"], visualElement["reducedY"]);
					}
				}
				if(locationPoint && locationPoint.x <= 100 && locationPoint.y <= 100){

					locationPoint = scaleMatrix.transformPoint(locationPoint);
					element.x = locationPoint.x;
					element.y = locationPoint.y;
				}
			}
			
			// sets the width and height of the elements
			super.updateDisplayList(width, height);
		}
	}
}