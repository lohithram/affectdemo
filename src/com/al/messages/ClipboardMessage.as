package com.al.messages {

    import com.al.descriptors.TileContainerDescriptor;

    public class ClipboardMessage {

        public function ClipboardMessage(tileDescriptor: TileContainerDescriptor) {

            this.tileDescriptor = tileDescriptor;
        }

        public var tileDescriptor: TileContainerDescriptor;
    }
}
