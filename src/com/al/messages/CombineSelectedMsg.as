/**
 * Created by lohithram on 06/05/15.
 */
package com.al.messages {

    import com.al.descriptors.ICombineDescriptor;

    public class CombineSelectedMsg {

        public function CombineSelectedMsg(combineDesc: ICombineDescriptor) {

            _desc = combineDesc;
        }

        private var _desc: ICombineDescriptor;
        public function get combineDescriptor(): ICombineDescriptor{
            return _desc
        }
    }
}
