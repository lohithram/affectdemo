/**
 * Created by lram on 04/02/2015.
 */
package com.al.messages
{

    public class ItemMsg
    {
        public static const PROJECT_TITLE:String = "projectTitle";
        public var item: Object;

        [Selector]
        public var type:String;

        public function ItemMsg(type:String, item: Object){

           this.type = type;
            this.item = item;
        }
    }
}
