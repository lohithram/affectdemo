/**
 * Created by lohithram on 06/05/15.
 */
package com.al.messages {

    import com.al.model.Activity;

    public class OpenApprovalMsg {

        public function OpenApprovalMsg(activity: Activity) {

            _activity = activity;
        }

        private var _activity: Activity;
        public function get activity(): Activity{

            return _activity;
        }
    }
}
