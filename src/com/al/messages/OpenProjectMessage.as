/**
 * Created by lohithram on 06/05/15.
 */
package com.al.messages {

    import com.al.model.Project;

    public class OpenProjectMessage {

        public function OpenProjectMessage(project: Project) {

            _project = project;
        }

        private var _project: Project;
        public function get project(): Project{

            return _project;
        }
    }
}
