/**
 * Created by lram on 04/02/2015.
 */
package com.al.messages
{
    public class PublishActivitiesMsg
    {
        public function PublishActivitiesMsg(activities: Array, projectId: String){

            _activities = activities;
            _projectId = projectId;
        }


        private var _activities: Array;
        public function get activities(): Array{

            return _activities;
        }

        private var _projectId: String;
        public function get projectId(): String{

            return _projectId;
        }
    }
}
