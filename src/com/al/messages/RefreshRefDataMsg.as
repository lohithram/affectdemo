/**
 * Created by lram on 04/02/2015.
 */
package com.al.messages
{

    public class RefreshRefDataMsg
    {
        private var _category: String;

        [Scope]
        public function get category(): String{
            return _category;
        }

        public function RefreshRefDataMsg(category: String=null){

            _category = category;
        }
    }
}
