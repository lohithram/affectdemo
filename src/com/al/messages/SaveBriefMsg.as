/**
 * Created by lohithram on 06/05/15.
 */
package com.al.messages {

    import com.al.model.FormTemplate;

    public class SaveBriefMsg {

        public function SaveBriefMsg(projectId: String, brief: FormTemplate) {

            _projectId = projectId;
            _brief = brief;
        }

        private var _projectId: String;
        public function get projectId(): String{

            return _projectId;
        }

        private var _brief: FormTemplate;
        public function get brief(): FormTemplate{

            return _brief;
        }
    }
}
