package com.al.messages {

    import mx.collections.ArrayCollection;

    public class TemplateMessage {
        public static const FORM_TEMPLATE:String = "FORM_TEMPLATE";

        public static const CONCEPT_TEMPLATE:String = "CONCEPT_TEMPLATE";

        public function TemplateMessage(templates:ArrayCollection, type:String = "FORM_TEMPLATE") {

            this.templates = templates;
            this.type = type;
        }

        [Selector]
        public  var type:String;

        public var templates: ArrayCollection;
    }
}
