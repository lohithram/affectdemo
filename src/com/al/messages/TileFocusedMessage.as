/**
 * Created by lohithram on 06/05/15.
 */
package com.al.messages {

    import com.al.model.TileContainerModel;

    public class TileFocusedMessage {

        public function TileFocusedMessage(tileDescriptor: TileContainerModel) {

            _desc = tileDescriptor;
        }

        private var _desc: TileContainerModel;
        public function get tileDescriptor(): TileContainerModel{

            return _desc;
        }
    }
}
