/**
 * Created by lohithram on 06/05/15.
 */
package com.al.messages {

    public class ToolSelectMessage {


        public function ToolSelectMessage(toolDescriptor: Object) {

            _desc = toolDescriptor;
        }

        private var _desc: Object;
        public function get toolDescriptor(): Object{
            return _desc
        }
    }
}
