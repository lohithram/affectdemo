/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import com.al.enums.ActivityStatus;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.utils.UIDUtil;

    public class Activity {

        [Bindable]
        [ArrayElementType("com.al.model.Task")]
        public var tasks: ArrayCollection;

        public var order: Number;

        public function Activity(projectId: String){

            _projectId = projectId;
            _id = UIDUtil.createUID();
            status = ActivityStatus.NEW;
            tasks = new ArrayCollection();
        }

        //--------------------------------------------------
        //  projectId
        //--------------------------------------------------
        private var _projectId: String;

        public function get projectId(): String{

            return _projectId;
        }

        //--------------------------------------------------
        //  id
        //--------------------------------------------------
        private var _id: String;

        public function get id(): String{
            return _id;
        }

        [Bindable]
        public var title: String;

        [Bindable]
        public var unit: String;

        [Bindable]
        public var totalPlanned: String;

        [Bindable]
        public var totalActual: String;

        [Bindable]
        public var startDate: Date;

        [Bindable]
        public var completionDate: Date;

        [Bindable]
        public var status: String;

        [Bindable]
        public var deliverable: String;

        [Bindable]
        public var campaign: String;

        [Bindable]
        public var brand: String;

        [Bindable]
        public var product: String;

        [Bindable]
        public var logs: ArrayList;

        [Bindable]
        public var members: ArrayList;

        [Bindable]
        [ArrayElementType("com.al.model.ImageInfo")]
        public var preview: Array;

        public function clone(): Activity{

            var activity: Activity = new Activity(projectId);

            activity.title = title;
            activity.unit = unit;
            activity.status = status;
            activity.startDate = startDate;
            activity.totalActual = totalActual;
            activity.totalPlanned = totalPlanned;
            activity.completionDate = completionDate;
            activity.brand = brand;
            activity.product = product;
            activity.campaign = campaign;

            return activity;
        }

    }
}
