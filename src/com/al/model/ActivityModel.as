/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import mx.collections.ArrayList;

    public class ActivityModel extends ProjectRelatedDataModel{

        [Bindable]
        [ArrayElementType("com.al.model.Activity")]
        public var activities: ArrayList = new ArrayList();

        override protected function hookOntoTheProject(){

            //project.setProjectData(ProjectDataFormats.ACTIVITIES, this);
            if(project && project.activities)
                activities = project.activities;
        }
    }
}
