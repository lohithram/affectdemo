/**
 * Created by lram on 18/02/2015.
 */
package com.al.model
{

    [Bindable]
    public class Announcement
    {
        public var member: MemberInfo;

        public var content: String;

        public var timestamp: Date;
    }
}
