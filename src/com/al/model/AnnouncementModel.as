/**
 * Created by lram on 17/02/2015.
 */
package com.al.model
{

    import mx.collections.ArrayList;

    public class AnnouncementModel
    {
        [Bindable]
        [ArrayElementType("com.al.model.Announcement")]
        public var announcements: ArrayList;

    }
}
