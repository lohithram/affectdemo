/**
 * Created by lram on 09/02/2015.
 */
package com.al.model
{
    import com.al.util.DataUtils;
    import com.al.util.FileUtils;

    import mx.collections.ArrayCollection;

    public class ApplicationModel
    {
        [Bindable]
        public var tasks:ArrayCollection = new ArrayCollection();

        [Init]
        public function init()
        {
            loadTasks();
        }

        private function populateTaskInformation(task:Task,budgeted:String,actual:String,activity:String, projectId:String):void
        {
            task.budgeted = budgeted;
            task.activity = new Activity(projectId);
            task.actual = actual;

        }

        private function loadTasks():void
        {
            var xml:XML = FileUtils.loadRefDataXML("Tasks.xml");

            if (!xml) return;

            for each(var taskXML:XML in xml.item)
            {
                var task:Task = new Task();

                DataUtils.populateCellItemInstance(task,taskXML.id, taskXML.type, taskXML.name, taskXML.file,
                        taskXML.content, taskXML.startDate, taskXML.modifiedDate, taskXML.modifiedBy,
                        taskXML.projectId,null,null,taskXML.endDate);

                populateTaskInformation(task,taskXML.budgeted, taskXML.actual, taskXML.activity, taskXML.projectId) as Task;

                tasks.addItem(task);

            }
        }
    }
}
