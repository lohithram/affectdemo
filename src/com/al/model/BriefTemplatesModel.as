/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import com.al.messages.TemplateMessage;
    import com.al.util.FileUtils;

    import mx.collections.ArrayCollection;

    import mx.collections.ArrayList;

    public class BriefTemplatesModel{

        public static const CREATE_NEW: String = "Create New Brief...";

        [Bindable]
        [ArrayElementType("com.al.model.FormTemplate")]
        public var templates: ArrayCollection;

        [MessageDispatcher]
        public var dispatcher: Function;


        [Init]
        public function onInit(): void{

            templates = new ArrayCollection();
            templates.addItem(new FormTemplate(CREATE_NEW));

            readTemplatesFromXML();
        }

        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //
        //  Private Methods
        //
        //--------------------------------------------------------------------------
        private function readTemplatesFromXML(): void{

            var links: Array = [];
            var xml: XML = FileUtils.loadRefDataXML("BriefTemplates.xml");

            if(!xml)
                return;

            for each( var templateXML: XML in xml.template ){

                var template: FormTemplate = parseFormTemplate(templateXML);
                templates.addItem(template);
            }

            dispatcher(new TemplateMessage(templates));
        }

        private function parseFormTemplate(templateXML: XML): FormTemplate{

            var formTemplate: FormTemplate = new FormTemplate(templateXML.@name);
            var templateHidden:String = templateXML.@hidden;
            formTemplate.hidden = templateHidden && templateHidden != "" ? true:false;

            for each( var sectionXML: XML in templateXML.section ){

                var section: FormSection = new FormSection(sectionXML.@name);
                section.isOpen = sectionXML.@opened == "true" ? true : false;
                for each( var fieldXML: XML in sectionXML.field ){

                    var field: FormField = new FormField(section.id);
                    field.name = fieldXML.@name;
                    field.value = fieldXML.@value;
                    field.height = Number(fieldXML.@height);
                    if(field.height < FormField.FIELD_MIN_HEIGHT)
                        field.height = FormField.FIELD_MIN_HEIGHT;
                    section.fields.addItem(field);
                }
                formTemplate.sections.addItem(section);
            }

            return formTemplate;

        }
    }
}
