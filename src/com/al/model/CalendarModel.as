/**
 * Created by lram on 16/02/2015.
 */
package com.al.model
{

    import mx.collections.ArrayCollection;

    public class CalendarModel
    {
        [Bindable]
        public var calendarEvents: ArrayCollection;

    }
}
