/**
 * Created by abhisekpaul on 08/06/15.
 */
package com.al.model
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.ContentType;
    import com.al.enums.TileType;
    import com.al.factory.TileContainerFactory;
    import com.al.util.DateUtils;
    import com.al.utility.panels.common.ITileContainerDescriptorProvider;

    import flash.display.BitmapData;

    import mx.collections.ArrayCollection;
    import mx.utils.UIDUtil;

    [Bindable]
    public class Cell implements ITileContainerDescriptorProvider
    {
        public var id:String;
        public var type:String = CellType.ASSET;
        public var name:String="";
        public var filePath:String;
        public var startDate:String;
        public var modifiedDate:String;
        public var endDate:String;
        public var modifiedBy:String;
        public var content:String;
        public var tags:ArrayCollection;
        public var project:Project;
        public var projectId:String;
        public var collections: ArrayCollection;
        public var pendingAnnotations: Array = [];
        private var _startDateRaw:Date;
        public var model:*;

        public var assetType:String;
        public var pages:ArrayCollection;
        public var templateName:String;

        public var bitmapData:BitmapData;

        [Bindable]
        public function get startDateRaw():Date
        {
            return _startDateRaw;
        }

        public function set startDateRaw(value:Date):void
        {
            if(_startDateRaw != value)
            {
                _startDateRaw = value;
                startDate = DateUtils.getShortDateFormat(_startDateRaw);
            }
        }

        private var _endDateRaw:Date;

        [Bindable]
        public function get endDateRaw():Date
        {
            return _endDateRaw;
        }

        public function set endDateRaw(value:Date):void
        {
            if(_endDateRaw != value)
            {
                _endDateRaw = value;
                endDate = DateUtils.getShortDateFormat(_endDateRaw);
            }
        }


        public function getTileContainerDescriptor():TileContainerDescriptor
        {
            var td:TileContainerDescriptor = TileContainerFactory.createTileDescriptorForCell(this);

            return td;
        }
    }
}
