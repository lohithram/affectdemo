/**
 * Created by abhisekpaul on 11/06/15.
 */
package com.al.model
{
    public class CellType
    {
        public static const ASSET:String="ASSET";

        public static const PERSON:String="PERSON";

        public static const BRIEF:String="BRIEF";

        public static const CONCEPT:String="CONCEPT";

        public static const ACTIVITIES :String="ACTIVITIES";

        public static const TEAM:String="TEAM";

        public static const REPORT:String="REPORT";

        public static const MULTI_PAGE:String = "MULTI_PAGE";

        public static const NOTE:String="NOTE";

        public static const REVIEW:String="REVIEW";

        public static const PROJECT_ASSETS:String = "PROJECT_ASSETS";

        public static const PROJECT_DETAILS:String = "PROJECT_DETAILS";

        public static const HTML: String = "HTML";
    }
}
