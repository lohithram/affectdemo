/**
 * Created by lram on 09/03/2015.
 */
package com.al.model
{
	import com.adobe.rtc.messaging.MessageItem;
	import com.al.collaboration.model.ICollaborationItem;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;

    public class CollaborationConfigurationModel extends EventDispatcher implements ICollaborationItem
    {
		public function CollaborationConfigurationModel(id: String = ""){
			_id = id;
		}
		
		//--------------------------------------------------
		// universal unique identifier
		//--------------------------------------------------
		private var _id: String;
		public function get id(): String{
			return _id;
		}
		
		//--------------------------------------------------
		// comments counter
		//--------------------------------------------------
		private var _commentsCount: uint = 1;
		[Bindable("commentsCountChange")]
		public function get commentsCount(): uint{
			return _commentsCount;
		}
		
		public function set commentsCount(value: uint): void{
			
			if(_commentsCount == value) return;
			_commentsCount = value;
			dispatchEvent(new Event("commentsCountChange"));
		}

		//--------------------------------------------------
		// annotations counter
		//--------------------------------------------------
		private var _annotationsCount: uint = 1;
		[Bindable("annotationsCountChange")]
		public function get annotationsCount(): uint{
			return _annotationsCount;
		}
		
		public function set annotationsCount(value: uint): void{
			
			if(_annotationsCount == value)return;
			_annotationsCount = value;
			dispatchEvent(new Event("annotationsCountChange"));
		}
		
		//--------------------------------------------------------------------------
		//
		//  Methods
		//
		//--------------------------------------------------------------------------
		public function getCommentsCount(): uint{
			
			var count: uint = commentsCount++;
			return count;
		}
		
		public function getAnnotationsCount(): uint{
			
			var count: uint = annotationsCount++;
			return count;
		}
		
		public function getObject():Object
		{
			return {commentsCount: commentsCount, annotationsCount: annotationsCount};
		}
		
		public function update(withObject: Object):void
		{
			_id = (withObject as MessageItem).itemID;
			if(withObject.body){
				
				if(withObject.body.hasOwnProperty("commentsCount") && !isNaN(withObject.body.commentsCount))
					commentsCount = withObject.body.commentsCount;
				if(withObject.body.hasOwnProperty("annotationsCount") && !isNaN(withObject.body.annotationsCount))
					annotationsCount = withObject.body.annotationsCount;
			}
		}
    }
}
