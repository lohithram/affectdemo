/**
 * Created by lram on 09/02/2015.
 */
package com.al.model
{

    import com.al.descriptors.ICombineDescriptor;

    import mx.collections.ArrayList;

    public class CombineModel
    {
        public var focusedTile: *;

        public var combineDescriptor: ICombineDescriptor;

        public var selected: Boolean;

        [ArrayElementType("com.al.model.TileDataModel")]
        // List of all the tiles in this combine
        public var tiles: ArrayList = new ArrayList();

    }
}
