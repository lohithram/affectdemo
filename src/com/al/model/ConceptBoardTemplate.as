/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import mx.collections.ArrayCollection;

    [Bindable]
    public class ConceptBoardTemplate{

        public var name: String;

        public var hidden:Boolean;

        [ArrayElementType("com.al.model.PageContent")]
        public var pages: ArrayCollection;

        public function ConceptBoardTemplate(name: String = ""){

            this.name = name;
            pages = new ArrayCollection();
        }

        public function clone(): ConceptBoardTemplate{

            var template: ConceptBoardTemplate = new ConceptBoardTemplate(name);
            template.pages = new ArrayCollection();
            for each(var page: PageContent in pages.source)
                template.pages.addItem(page.clone());

            return template;
        }
    }
}
