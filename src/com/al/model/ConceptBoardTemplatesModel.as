package com.al.model {

    import com.al.messages.TemplateMessage;
    import com.al.util.FileUtils;

    import mx.collections.ArrayCollection;

    import mx.collections.ArrayList;

    public class ConceptBoardTemplatesModel{

        [Bindable]
        [ArrayElementType("com.al.model.ConceptBoardTemplate")]
        public var templates: ArrayCollection;

        [MessageDispatcher]
        public var dispatcher: Function;


        [Init]
        public function onInit(): void{

            templates = new ArrayCollection();

            readTemplatesFromXML();
        }

        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //
        //  Private Methods
        //
        //--------------------------------------------------------------------------
        private function readTemplatesFromXML(): void{

            var xml: XML = FileUtils.loadRefDataXML("conceptTemplates.xml");

            if(!xml)
                return;

            for each( var templateXML: XML in xml.template ){

                var template: ConceptBoardTemplate = parseFormTemplate(templateXML);
                templates.addItem(template);
            }

            dispatcher(new TemplateMessage(templates, TemplateMessage.CONCEPT_TEMPLATE));
        }

        private function parseFormTemplate(templateXML: XML): ConceptBoardTemplate{

            var template: ConceptBoardTemplate = new ConceptBoardTemplate(templateXML.@name);
            var templateHidden:String = templateXML.@hidden;
            template.hidden = templateHidden && templateHidden != "" ? true:false;
            template.pages = new ArrayCollection();

            var pageCount: int = 0;
            for each(var pageXML:XML in templateXML.pages.page)
            {
                var pageUrl: String = FileUtils.resolveDemoResource(pageXML.url);
                template.pages.addItem(new PageContent(String(++pageCount), pageCount, pageUrl ));
            }
            return template;
        }
    }
}
