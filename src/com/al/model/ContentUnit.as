package com.al.model
{
	public class ContentUnit implements IContentUnit {

		public function ContentUnit(id: String =null) {
			
			_id = id;
		}
		
		private var _id: String;
		public function get id():String {
			return _id;
		}
	}
}