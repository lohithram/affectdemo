/**
 * Created by lohithram on 06/05/15.
 */
package com.al.model {

    import mx.collections.ArrayList;

    public class DisplayAreaModel {

        [Bindable]
        public var mainViewIndex: int = 0;

        [Bindable]
        [ArrayElementType("com.al.descriptors.ICombineDescriptor")]
        public var combines: ArrayList;

        [Bindable]
        public var currentCombine: CombineModel;
    }
}
