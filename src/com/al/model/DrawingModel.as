/**
 * Created by lohithram on 30/04/15.
 */
package com.al.model {

    import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;


    [Bindable]
    /**
     * Contains all the state of the drawing tool bar in the utility panel
     */
    public class DrawingModel {

        public var maxMaskSize: Number = 10;
        public var maskSize: Number = 4;

        public var maskBlur: Number = 2;
        public var maxMaskBlur: Number = 10;

        public var squareMaskSelected: Boolean;

        public var roundMaskSelected: Boolean = true;

        public var maskToolSelected: Boolean;

        public var zoomInSelected: Boolean;

        public var zoomOutSelected: Boolean;

        public var exactFitSelected: Boolean;

        public var selectToolSelected: Boolean;

        public var annotationToolSelected: Boolean;

        public var shapeToolsSelected: Boolean;

        public var showTextFormatTools: Boolean;

        public var selectedShape: WBShapeDescriptor;

        public var shapePropertyData: Object = {alpha: 0.8, lineThickness: 5,
                                                lineColor: 0x333333, primaryColor: 0xffcc00};
    }
}
