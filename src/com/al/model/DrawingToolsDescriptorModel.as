/**
 * Created by lohithram on 30/04/15.
 */
package com.al.model {

    import com.adobe.coreUI.controls.whiteboardClasses.IWBShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBShapeToolBarDescriptor;
    import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBToolBarDescriptor;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBArrowShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBHighlightAreaShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBMarkerShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBSimpleShape;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBSimpleShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBTextShapeFactory;

    import mx.collections.ArrayList;

    import themes.affect.utils.IconUtils;

    public class DrawingToolsDescriptorModel {

        public static const ZOOM_IN: String = "zoomIn";
        public static const ZOOM_OUT: String = "zoomOut";
        public static const EXACT_FIT: String = "exactFit";
        public static const MASK: String = "mask";
        public static const ANNOTATE: String = "annotate";

        //--------------------------------------------------------------------------
        //
        //  Cursors
        //
        //--------------------------------------------------------------------------
        [Embed("/images/cursors2/cursor-zoomin.svg")]
        private var zoomInCursor: Class;

        [Embed("/images/cursors2/cursor-zoomout.svg")]
        private var zoomOutCursor: Class;

        [Embed("/images/cursors2/cursor-exactfit.svg")]
        private var exactFitCursor: Class;

        [Embed("/images/cursors2/cursor-annotate.svg")]
        private var annotationCursor: Class;

        [Bindable]
        public var selectionSet: ArrayList = new ArrayList();

        [Bindable]
        public var viewSet: ArrayList = new ArrayList();

        [Bindable]
        public var drawingSet: ArrayList = new ArrayList();

        [Bindable]
        public var textSet: ArrayList = new ArrayList();

        [Bindable]
        public var maskTool: Object;

        [Init]
        public function init(): void{

            createDrawingDescriptors();
            createViewDescriptors();
        }


        private function createViewDescriptors(): void {

            var zoomIn: Object = new Object();
            zoomIn.type = WBShapeToolBarDescriptor.TOOL;
            zoomIn.styleName = "iconToggleButton iconToggleButtonBlue";
            zoomIn.cursorClass = zoomInCursor;
            zoomIn.label = IconUtils.ToolZoomIn;
            zoomIn.toolTip = "Zoom in";
            zoomIn.toolName = ZOOM_IN;
            //zoomIn.styleName = "";

            var zoomOut: Object = new Object();
            zoomOut.type = WBShapeToolBarDescriptor.TOOL;
            zoomOut.styleName = "iconToggleButton iconToggleButtonBlue";
            zoomOut.cursorClass = zoomOutCursor;
            zoomOut.label = IconUtils.ToolZoomOut;
            zoomOut.toolTip = "Zoom out";
            zoomOut.toolName = ZOOM_OUT;

            var exactFit: Object = new Object();
            exactFit.type = WBShapeToolBarDescriptor.TOOL;
            exactFit.styleName = "iconToggleButton iconToggleButtonBlue";
            exactFit.cursorClass = exactFitCursor;
            exactFit.label = IconUtils.ToolExactFit;
            exactFit.toolTip = "Exact fit";
            exactFit.toolName = EXACT_FIT;

            viewSet.addItem(zoomIn);
            viewSet.addItem(zoomOut);
            viewSet.addItem(exactFit);

            maskTool = new Object();
            maskTool.type = WBShapeToolBarDescriptor.TOOL;
            maskTool.styleName = "iconToggleButton iconToggleButtonBlue";
            maskTool.label = IconUtils.ToolMask;
            maskTool.toolTip = "Mask";
            maskTool.toolName = MASK;
        }

        private function createDrawingDescriptors(): void{

            var toolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.LABEL);
            toolShape.label = "Tools";

            var selectionToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            selectionToolShape.toolTip = "Selection Tool";
            selectionToolShape.styleName = "iconToggleButton iconToggleButtonBlue";
            selectionToolShape.label = IconUtils.ToolSelect;

            var annotation: Object = new Object();
            annotation.styleName = "iconToggleButton iconToggleButtonBlue";
            annotation.type = WBShapeToolBarDescriptor.TOOL;
            annotation.cursorClass = annotationCursor;
            annotation.cursorXOffset = -7.5;
            annotation.cursorYOffset = -15;
            annotation.toolTip = "Annotation Tool";
            annotation.toolName = ANNOTATE;
            annotation.label = IconUtils.ToolAnnotate;

            var arrowToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            arrowToolShape.toolTip = "Arrow Tool";
            arrowToolShape.shapeFactory = arrowFactory;
            arrowToolShape.shapeData = WBArrowShapeFactory.ARROW_HEAD;
            arrowToolShape.label = IconUtils.ToolArrow;
            //arrowToolShape.icon = WBToolBarDescriptor.ICON_ARROW;

            var highLightPenToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            highLightPenToolShape.toolTip ="Highlighter Pen Tool";
            highLightPenToolShape.shapeFactory = markerFactory;
            highLightPenToolShape.shapeData = WBMarkerShapeFactory.HIGHLIGHTER;
            highLightPenToolShape.label = IconUtils.ToolHighlight;
            //highLightPenToolShape.icon = WBToolBarDescriptor.ICON_HIGHLIGHTER_PEN;

            var highlightRectangleToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            highlightRectangleToolShape.toolTip = "Highlight Rectangle Tool";
            highlightRectangleToolShape.shapeFactory = highlightAreaFactory;
            highlightRectangleToolShape.shapeData = WBSimpleShapeFactory.HIGHLIGHT_AREA;
            highlightRectangleToolShape.label = IconUtils.ToolHighlight;
            //highlightRectangleToolShape.icon = WBToolBarDescriptor.ICON_HIGHLIGHT_RECTANGLE;

            var textToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            textToolShape.toolTip = "Text Tool";
            textToolShape.shapeFactory = textFactory;
            textToolShape.label = IconUtils.ToolText;
            //textToolShape.icon = WBToolBarDescriptor.ICON_TEXT;

            var shapeSubToolBar:WBToolBarDescriptor = new WBToolBarDescriptor(false);
            var shapesLabelShape:WBShapeToolBarDescriptor = new WBShapeToolBarDescriptor(WBShapeToolBarDescriptor.LABEL);
            shapesLabelShape.label = "Shapes";

            var lineToolShape:ToolDescriptor  =  new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            lineToolShape.toolTip = "Line Tool";
            lineToolShape.shapeFactory = arrowFactory;
            lineToolShape.shapeData = WBArrowShapeFactory.NO_ARROW_HEAD;
            lineToolShape.label = IconUtils.ToolLine;
            //lineToolShape.icon = WBToolBarDescriptor.ICON_LINE;

            var ellipseToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            ellipseToolShape.toolTip = "Ellipse Tool";
            ellipseToolShape.shapeFactory = simpleShapeFactory;
            ellipseToolShape.shapeData = WBSimpleShape.ELLIPSE;
            ellipseToolShape.label = IconUtils.ToolEllipse;
            //ellipseToolShape.icon = WBToolBarDescriptor.ICON_ELLIPSE;

            var rectangleToolShape:ToolDescriptor = new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            rectangleToolShape.toolTip = "Rectangle Tool";
            rectangleToolShape.shapeFactory = simpleShapeFactory;
            rectangleToolShape.shapeData = WBSimpleShape.RECTANGLE;
            rectangleToolShape.label = IconUtils.ToolRectangle;
            //rectangleToolShape.icon = WBToolBarDescriptor.ICON_RECTANLGE;

            var roundedRectangleToolShape:ToolDescriptor  =  new ToolDescriptor(WBShapeToolBarDescriptor.TOOL);
            roundedRectangleToolShape.toolTip = "Rounded Rectangle Tool";
            roundedRectangleToolShape.shapeFactory = simpleShapeFactory;
            roundedRectangleToolShape.shapeData = WBSimpleShape.ROUNDED_RECTANGLE;
            roundedRectangleToolShape.label = IconUtils.ToolRounded;
            //roundedRectangleToolShape.icon = WBToolBarDescriptor.ICON_ROUNDED_RECTANGLE;

            var shapesToolShape:WBShapeToolBarDescriptor  =  new WBShapeToolBarDescriptor(WBShapeToolBarDescriptor.TOOL);
            shapesToolShape.toolTip = "Shapes";
            //shapesToolShape.icon = WBToolBarDescriptor.ICON_SHAPES;
            shapesToolShape.children = shapeSubToolBar.children;

            selectionSet.addItem(selectionToolShape);
            selectionSet.addItem(annotation);


            drawingSet.addItem(highLightPenToolShape);
//            drawingSet.addItem(highlightRectangleToolShape);
            drawingSet.addItem(arrowToolShape);
            drawingSet.addItem(lineToolShape);
            drawingSet.addItem(ellipseToolShape);
            drawingSet.addItem(rectangleToolShape);
            drawingSet.addItem(roundedRectangleToolShape);

            textSet.addItem(textToolShape);
        }

        protected var markerFactory:IWBShapeFactory = new WBMarkerShapeFactory();
        protected var textFactory:IWBShapeFactory = new WBTextShapeFactory();
        protected var simpleShapeFactory:WBSimpleShapeFactory = new WBSimpleShapeFactory();
        protected var arrowFactory:WBArrowShapeFactory = new WBArrowShapeFactory();
        protected var highlightAreaFactory:WBSimpleShapeFactory = new WBHighlightAreaShapeFactory();

    }
}
