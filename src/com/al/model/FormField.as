/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import com.al.collaboration.model.ICollaborationItem;

    import mx.utils.UIDUtil;

    public class FormField implements ICollaborationItem{

        public static const FIELD_MIN_HEIGHT: Number = 30;

        public function FormField(sectionId: String){

            _id = UIDUtil.createUID();
            name = "Field name";
            this.sectionId = sectionId;
        }

        private var _id: String;

        public function get id(): String{
            return _id;
        }

        [Bindable]
        public var name: String;

        [Bindable]
        public var value: String;

        [Bindable]
        public var height: Number = FIELD_MIN_HEIGHT;

        public var sectionId: String;

        public var order: Number;

        public function getObject():Object
        {
            return {name: name,
                value: value,
                order: order,
                height: height,
                sectionId: sectionId
            };
        }

        public function update(withObject: Object):void
        {
            if(withObject.body){

                if(withObject.body.hasOwnProperty("name"))
                    name = withObject.body.name;
                if(withObject.body.hasOwnProperty("value"))
                    value = withObject.body.value;
                if(withObject.body.hasOwnProperty("sectionId"))
                    sectionId = withObject.body.sectionId;
                if(withObject.body.hasOwnProperty("order") && !isNaN(withObject.body.order))
                    order = withObject.body.order;
                if(withObject.body.hasOwnProperty("height") && !isNaN(withObject.body.height))
                    height = withObject.body.height;
            }
        }

        public function clone(): FormField{

            var field: FormField = new FormField(sectionId);
            field.name = name;
            field.order = order;
            field.value = value;
            field.height = height;
            field.sectionId = sectionId;

            return field;
        }

    }
}
