/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.collections.Sort;
    import mx.collections.SortField;
    import mx.utils.UIDUtil;

    public class FormSection {

        [Bindable]
        public var name: String;

        [Bindable]
        [ArrayElementType("com.al.model.FormField")]
        public var fields: ArrayCollection = new ArrayCollection();

        [Bindable]
        [ArrayElementType("com.al.model.Cell")]
        public var people: ArrayCollection = new ArrayCollection();

        [Bindable]
        [ArrayElementType("com.al.model.Cell")]
        public var relatedAssets: ArrayCollection = new ArrayCollection();

        [Bindable]
        [ArrayElementType("com.al.collaboration.annotation.model.Comment")]
        public var instructions: ArrayCollection = new ArrayCollection();

        [Bindable]
        [ArrayElementType("com.al.model.Activity")]
        public var activities: ArrayList = new ArrayList();

        public var order: Number;

        [Bindable]
        [Transient]
        public var isOpen: Boolean = false;

        public function FormSection(name: String = "Form Section"){

            this.name = name;
            _id = UIDUtil.createUID();
            fields = new ArrayCollection();
        }

        private var _id: String;

        public function get id(): String{
            return _id;
        }

        public function updateOrder(){

            fields.source.sortOn("order", Array.NUMERIC);
            fields.refresh();
        }

        public function getObject():Object
        {
            return {id: id,
                    name: name,
                    order: order};
        }

        public function update(withObject: Object):void
        {
            if(withObject.body){

                if(withObject.body.hasOwnProperty("name"))
                    name = withObject.body.name;
                if(withObject.body.hasOwnProperty("order") && !isNaN(withObject.body.order))
                    order = withObject.body.order;
            }
        }

        public function clone(): FormSection{

            var section: FormSection = new FormSection(name);
            section.order = order;
            section.isOpen = isOpen;
            section.fields = new ArrayCollection();
            for each(var field: FormField in fields.source)
                section.fields.addItem(field.clone());

            return section;
        }

    }
}
