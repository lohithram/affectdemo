/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import mx.collections.ArrayList;
    import mx.utils.UIDUtil;


    [Bindable]
    public class FormTemplate{

        public var name: String;

        public var hidden:Boolean;

        [ArrayElementType("com.al.model.FormSection")]
        public var sections: ArrayList;

        public function FormTemplate(name: String = ""){

            this.name = name;
            sections = new ArrayList();
        }

        public function clone(): FormTemplate{

            var form: FormTemplate = new FormTemplate(name);
            form.sections = new ArrayList();
            for each(var section: FormSection in sections.source)
                form.sections.addItem(section.clone());

            return form;
        }
    }
}
