/**
 * Created by lram on 12/02/2015.
 */
package com.al.model
{

    public class ImageInfo
    {
        //TODO: Make the properties immutable
        public function ImageInfo(url: String, imageData: * = null) {

            this.url = url;
            this.imageData = imageData;
        }

        public var url: String;

        public var imageData: *;
    }
}
