/**
 * Created by lram on 10/02/2015.
 */
package com.al.model
{

    public class Link
    {
        public function Link(name: String, url: String) {

            this.name = name;
            this.url = url;
        }

        protected var id: String;

        [Bindable]
        public var name: String;

        [Bindable]
        public var url: String;

    }
}
