/**
 * Created by lram on 10/02/2015.
 */
package com.al.model
{

    import mx.collections.ArrayList;


    public class LinkGroup
    {

        public function LinkGroup(name: String) {

            this.name = name;
        }

        protected var id: String;

        [Bindable]
        public var name: String;

        [Bindable]
        [ArrayElementType("com.al.model.Link")]
        public var links: ArrayList;

        [Bindable]
        [ArrayElementType("com.al.model.LinkGroup")]
        public var childrenGroups: ArrayList;

    }
}
