/**
 * Created by lram on 12/02/2015.
 */
package com.al.model
{

    public class MemberInfo
    {
        //TODO: Make the properties immutable
        public function MemberInfo(name: String, role: String, avatar: String) {

            this.name = name;
            this.role = role;
            this.avatar = avatar as String;
        }

        [Bindable]
        public var name: String;

        [Bindable]
        public var role: String;

        [Bindable]
        public var avatar: *;
    }
}
