package com.al.model
{
	/**
	 *  Describes the attributes of a page in a multi page document
	 * @author lram
	 * 
	 */
    [Bindable]
	public class PageContent extends ContentUnit
	{
		public function PageContent(id:String, pageNumber: int = -1, pageUrl: String = null) {
			
			super(id);
			
			this.pageUrl = pageUrl;
			this.pageNumber = pageNumber;
		}
		
		public var pageNumber: int;
		
		public var pageUrl: String;

        public function clone(): PageContent
        {
            return new PageContent(id,pageNumber,pageUrl);

        }
	}
}