/**
 * Created by lohithram on 28/05/15.
 */
package com.al.model {

    import flash.utils.Dictionary;

    import mx.collections.ArrayCollection;

    import mx.collections.ArrayList;

    [Bindable]
    public class Project {

        private var _id: String;
        public function get id(): String{
            return _id;
        }

        //------------------------------------
        // code
        //------------------------------------
        private var _code: String;
        public function get code(): String{
            return _code ? _code : id;
        }
        public function set code(value: String): void{
            _code = value;
        }

        public var title: String;

        public var description: String;

        public var client: String;

        public var status: String;

        public var deliverable: String;

        public var campaign: String;

        public var brand: String;

        public var product: String;

        public var projectType: String;

        public var mediaType: String;

        public var region: String;

        public var publicationContact: String;

        public var dueDate: Date;

        public var publicationDate: Date;

        public var completionDate: Date;

        [ArrayElementType("com.al.model.ImageInfo")]
        public var preview: Array;

        [ArrayElementType("com.al.model.Activity")]
        public var activities: ArrayList;

        public var administrators: ArrayList;

        [ArrayElementType("com.al.model.FormTemplate")]
        public var briefs: ArrayList;

        [Bindable]
        public var assetsList: ArrayCollection;

        //public var firstActivity: Activity;

        private var projectData: Dictionary;


        public function Project(id: String){

            _id = id;
            projectData = new Dictionary();
            briefs = new ArrayList();
            administrators = new ArrayList();
            assetsList = new ArrayCollection();
        }

        public function getProjectData(dataFormat: String): Array{

            return projectData[dataFormat] as Array;
        }

        public function setProjectData(dataFormat: String, data: Object): void{

            projectData[dataFormat] = [data];
        }

        public function addProjectData(dataFormat: String, data: Object): void{

            if(!projectData[dataFormat])
                projectData[dataFormat] = [];

            projectData[dataFormat].push(data);
        }
    }
}
