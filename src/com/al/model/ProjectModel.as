/**
 * Created by lram on 10/02/2015.
 */
package com.al.model
{

    import com.al.messages.RefreshRefDataMsg;
    import com.al.refData.TimeLineRefData;

    import mx.collections.ArrayCollection;

    //TODO: Rename to ProjectModel
    public class ProjectModel
    {

        private static const MONTHS_AHEAD: int = 4;
        private static const MONTHS_BEHIND: int = 2;

        private static const INITIAL_WEEKS: int = 1;

        private var refData: TimeLineRefData = new TimeLineRefData();

        [Bindable]
        public var projectList: ArrayCollection = refData.projects;

        [Bindable]
        public var allActivities: ArrayCollection = refData.allActivities;

        [Bindable]
        public var beginDate: Date;

        [Bindable]
        public var endDate: Date;

        // The start of the range the user can
        // see on the timeline
        [Bindable]
        public var rangeBeginDate: Date;

        // The end of the range the user can
        // see on the timeline
        [Bindable]
        public var rangeEndDate: Date;

        public function ProjectModel(){

            // Entire range will be MONTHS_BEHIND months back from today
            // and MONTHS_AHEAD years front from now.
            rangeBeginDate = new Date();
            rangeBeginDate.month -= MONTHS_BEHIND;

            rangeEndDate = new Date();
            rangeEndDate.month += MONTHS_AHEAD;

            //TODAY - 2
            beginDate = new Date();
            beginDate.date -= 2;

            //can see ahead two weeks
            endDate = new Date();
            endDate.date += INITIAL_WEEKS * 7;
        }

        /**
         * Will be lost after the demo application is built
         * @param msg
         */
        [MessageHandler]
        public function refreshRefData(msg: RefreshRefDataMsg): void{

            refData.readFromXML();
        }

    }
}

