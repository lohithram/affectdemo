/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    public class ProjectRelatedDataModel {

        protected var project: Project;

        public function ProjectRelatedDataModel(){

        }

        public function setProject(project: Project): void{

            this.project = project;
            hookOntoTheProject();
        }

        public function get projectId(): String{

            return project ? project.id : "";
        }

        protected function hookOntoTheProject(){

            throw new Error("hookOntoTheProject should be implemented by sub-class");
        }
    }
}
