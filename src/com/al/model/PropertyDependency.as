/**
 * Created by lohithram on 06/05/15.
 */
package com.al.model {

    public class PropertyDependency {

        public function PropertyDependency(property: String, classType: Class) {

            _property = property;
            _classType = classType;
        }

        private var _property: String;

        private var _classType: Class;

        public function get propertyName(): String {
            return _property;
        }

        public function get type(): Class{
            return _classType;
        }
    }
}
