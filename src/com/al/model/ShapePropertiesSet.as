/**
 * Created by lohithram on 01/05/15.
 */
package com.al.model {

    [Bindable]
    public class ShapePropertiesSet {

        public var primaryColor: uint;
        public var lineColor: uint;

        public var lineThickness: Number;

        public var alpha: Number;

    }
}
