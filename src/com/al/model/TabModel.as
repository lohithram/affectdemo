/**
 * Created by lram on 27/02/2015.
 */
package com.al.model
{

    import com.al.descriptors.TabDescriptor;

    import mx.collections.ArrayList;

    public class TabModel
    {
        [Bindable]
        public var tabs: ArrayList = new ArrayList();

        [Init]
        public function init(): void{
            tabs.source = [ new TabDescriptor('Combine 1',0, false),
                        new TabDescriptor('Combine 2',1, false),
                        new TabDescriptor('Combine 3',2, false),
                        new TabDescriptor('Combine 4',3, false),
                        new TabDescriptor('Combine 5',4, false),
                        new TabDescriptor('Dashboard',5, false)];

        }
    }
}
