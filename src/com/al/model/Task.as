/**
 * Created by lohithram on 18/05/15.
 */
package com.al.model {

    import com.al.enums.TaskStatus;


    [Bindable]
    public class Task extends Cell
    {
        public function Task(id: String="")
        {
            this.id = id;
        }

        [Bindable]
        public var activity: Activity;

        [Bindable]
        public var budgeted: String;

        [Bindable]
        public var actual: String;

        [Bindable]
        public var status: String = TaskStatus.NEW;

    }
}
