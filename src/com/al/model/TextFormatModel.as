/**
 * Created by lohithram on 30/04/15.
 */
package com.al.model {

    import flash.events.EventDispatcher;

    import spark.components.RichEditableText;
    import spark.core.IEditableText;


    [Bindable]
    /**
     * Contains all the formatting info for the selected Text
     */
    public class TextFormatModel extends EventDispatcher{

        public var textColor: uint;

        public var textAlignment: String;

        public var isBullet: Boolean = true;

        public var fontFamily: * = "Arial";

        public var fontSize: *;

        public var fontWeight: *;

        public var fontStyle: *;

        public var textDecoration: *;

        public var currentRichTextControl: IEditableText;

    }
}
