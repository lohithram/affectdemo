/**
 * Created by synesis on 23/04/2015.
 */
package com.al.model
{

    import com.al.model.TileDataModel;

    import flash.events.Event;
    import flash.events.EventDispatcher;

    /**
     *  Represents transient visual state of the tile container
     *
     */
    public class TileContainerModel extends EventDispatcher
    {
        public var tileDataModel: TileDataModel;

        // This is set upfront when tile is created
        public var canCollaborate: Boolean;

        public var overlaySupported: Boolean;

        [Bindable]
        public var tileTitle: String;

        //-------------------------------------------------
        //  isCollaborationEnabled
        //-------------------------------------------------
        public var _isCollaborationEnabled: Boolean = false;
        [Bindable("collaborationEnabledChange")]
        public function get isCollaborationEnabled(): Boolean{

            return canCollaborate && _isCollaborationEnabled;
        }

        public function set isCollaborationEnabled(value: Boolean): void{

            if(_isCollaborationEnabled == value) return;

            _isCollaborationEnabled = value;
            dispatchEvent(new Event("collaborationEnabledChange"))
        }

        [Bindable]
        public var showHeaderAndFooterInWindowMode: Boolean = true;

        [Bindable]
        public var showHeaderAndFooterInFullScreen: Boolean = false;

        [Bindable]
        // zoomFactor of 1.0 results in content getting displayed at actual size.
        // value of NaN will result in determining the zoom factor required
        // to fit the content to the size of the window..
        public var zoomFactor: Number = 1.0;

        private var _isFullScreenMode: Boolean;
        public function get isFullScreenMode(): Boolean {

            return _isFullScreenMode;
        }

        public function set isFullScreenMode(value: Boolean): void {

            _isFullScreenMode = value;
            dispatchEvent(new Event("showHeaderFooterChange"));
        }

       [Bindable("showHeaderFooterChange")]
       public function get showHeaderAndFooter(): Boolean {

           return _isFullScreenMode ? showHeaderAndFooterInFullScreen : showHeaderAndFooterInWindowMode;
       }

       public function set showHeaderAndFooter(value: Boolean): void {

           isFullScreenMode ? showHeaderAndFooterInFullScreen = value :
                   showHeaderAndFooterInWindowMode = value;
           dispatchEvent(new Event("showHeaderFooterChange"));
       }
    }
}
