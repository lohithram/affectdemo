package com.al.model
{
    import com.al.enums.ContentType;

    import flash.display.BitmapData;

    import mx.collections.ArrayCollection;

    import mx.collections.ArrayList;
	import mx.core.ClassFactory;

	/**
	 * Represents all the attributes of a content inside a Tile Container 
	 * @author lram
	 * 
	 */
	public class TileContentDescriptor
	{
		// TODO: Is it a good idea to have content types ?
		// Need factory method or class to generate content Info object

		
		// Whose responsibility is to consume and create instance of this content ?
		
		public function TileContentDescriptor(id: String)
		{
			_id = id;
			contentType = ContentType.NO_CONTENT;
		}
		
		private var _id: String;
		public function get id(): String {
			return _id;
		}
		
		public var contentType: String;
		
		public var contentUrl: String;

		//public var collaborationSession: String;

        [Bindable]
        public var text:String;
		
		public var bitMapInfo: BitmapData;
		
		// Depending on the type of content, visual elements are created dynamically
		public var contentFactory: ClassFactory;
		
		[Bindable]
		[ArrayElementType("com.al.model.ContentUnit")]
		public var contentUnits: ArrayCollection;


		[ArrayElementType("com.al.collaboration.annotation.model.Annotation")]
		public var pendingAnnotations: Array = [];

		public function clone(): TileContentDescriptor{
			
			var model: TileContentDescriptor = new TileContentDescriptor(id);
			model.contentUrl = contentUrl;
			model.contentType = contentType;
			model.contentFactory = contentFactory;
			model.pendingAnnotations = pendingAnnotations;
			//model.collaborationSession = model.collaborationSession;
			model.bitMapInfo = bitMapInfo ? bitMapInfo.clone() : null;
			model.contentUnits = contentUnits ? new ArrayCollection(contentUnits.source) : new ArrayCollection();
			return model;
		}
	}
}