/**
 * Created by synesis on 23/04/2015.
 */
package com.al.model
{

    import flash.events.EventDispatcher;
    import flash.utils.Dictionary;

    /**
     *  Represents data model of a given Tile
     *  This class acts as a data point of contact for the combine(or any interested parties)
     *  to request various formats of data related to this tile.
     */

    public class TileDataModel extends EventDispatcher {

        private var tileData: Dictionary = new Dictionary();

        public function getTileData(dataFormat: String): *{

            return tileData[dataFormat];
        }

        public function setTileData(dataFormat: String, data: *): void{

            tileData[dataFormat] = data;
        }
    }
}
