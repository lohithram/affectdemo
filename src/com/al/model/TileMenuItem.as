/**
 * Created by synesis on 22/04/2015.
 */
package com.al.model
{

    [Bindable]
    public class TileMenuItem
    {
        public var displayLabel: String;

        public var iconCode: String;

        public var canToggle: Boolean;

        public var isToggled: Boolean;
    }
}
