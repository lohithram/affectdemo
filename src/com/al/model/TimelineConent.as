package com.al.model
{
	/**
	 *  Describes the attributes of a timeline in a time based media like audio, video
	 * @author lram
	 * 
	 */
	public class TimelineConent extends ContentUnit
	{
		public function TimelineConent(id:String)
		{
			super(id);
		}
		
		public var startTime: uint;
		
		public var endTime: uint;
	}
}