/**
 * Created by lohithram on 10/07/15.
 */
package com.al.model {

    import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBShapeToolBarDescriptor;

    public class ToolDescriptor extends WBShapeToolBarDescriptor {

        [Bindable]
        public var styleName: String = "iconToggleButton iconToggleButtonBlue";

        public function ToolDescriptor(p_type:String) {
            super(p_type);
        }

    }
}
