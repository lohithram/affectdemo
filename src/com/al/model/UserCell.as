/**
 * Created by abhisekpaul on 09/06/15.
 */
package com.al.model
{
    import com.al.enums.ContentType;
    import com.al.enums.TileType;
    import com.al.factory.TileContainerFactory;
    import com.al.utility.panels.common.ITileContainerDescriptorProvider;
    import com.al.descriptors.TileContainerDescriptor;

    import mx.collections.ArrayCollection;

    import mx.utils.UIDUtil;

    [Bindable]
    public class UserCell extends Cell
    {
        public var firstName:String;
        public var lastName:String;
        public var username:String;
        public var position:String;
        public var location:String;
        public var tasks:ArrayCollection;
        public var contact:String;
        public var email:String;

        override public function getTileContainerDescriptor():TileContainerDescriptor
        {
            var td:TileContainerDescriptor = TileContainerFactory.createTileDescriptorForType(TileType.CONTENT);
            td.currentContent = new TileContentDescriptor(UIDUtil.createUID());
            td.currentContent.contentType = ContentType.SINGLE_DOCUMENT;
            td.currentContent.contentUrl = filePath;
            td.title = name;
            return td;
        }
    }
}
