/**
 * Created by lram on 09/02/2015.
 */
package com.al.model
{

    import flash.net.SharedObject;

    /**
     * The persisting logic should be in a PM or controller
     */
    public class UserModel
    {
        public function UserModel() {
        }
		
		[Bindable]
		public var displayName: String;

        public var influxisLoginId: String;


        [Init]
        public function onInit(): void{

            sharedObject = SharedObject.getLocal("login_data");
            if(sharedObject.data.userName)
                displayName = sharedObject.data.username;
        }

        private var sharedObject: SharedObject;
        public function onLogin(): void{

            sharedObject = SharedObject.getLocal("login_data");
            sharedObject.data.username = displayName;
            sharedObject.flush();
        }
    }
}
