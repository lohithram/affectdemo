/**
 * Created by abhisekpaul on 01/06/15.
 */
package com.al.model
{
    import com.al.interfaces.IUtilityGroup;

    import mx.collections.ArrayCollection;

    [Bindable]
    public class UtilityGroup implements IUtilityGroup
    {
        public var isDefault:Boolean;

        private var _title:String;

        public function get title():String
        {
            return _title;
        }

        public function set title(value:String):void
        {
            if(value != _title)
            {
                _title = value;
            }
        }

        public var items:ArrayCollection;

        public function get contents():ArrayCollection
        {
            return items;
        }

    }
}
