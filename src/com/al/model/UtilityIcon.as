/**
 * Created by abhisekpaul on 09/07/15.
 */
package com.al.model
{
    [Bindable]
    public class UtilityIcon
    {
        public function UtilityIcon(toolTip:String,label:String)
        {
            this.label = label;
            this.toolTip = toolTip;
        }
        public var label:String;
        public var toolTip:String;
    }
}
