/**
 * Created by abhisekpaul on 08/06/15.
 */
package com.al.model
{
    import com.al.interfaces.IUtilityGroup;

    import mx.collections.ArrayCollection;

    [Bindable]
    public class UtilityProject implements IUtilityGroup
    {
        private var _title:String;

        public function get title():String
        {
            return _title;
        }

        public function set title(value:String):void
        {
            if(value != _title)
            {
                _title = value;
            }
        }

        public var items:ArrayCollection;

        public function get contents():ArrayCollection
        {
            return items;
        }
    }
}
