/**
 * Created by lram on 27/02/2015.
 */
package com.al.model
{

    public class VersionModel
    {
        [Bindable]
        public var versionNumber: String = "0.1.3";
    }
}
