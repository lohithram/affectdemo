package com.al.model
{

	import flash.display.BitmapData;

	/**
	 *  Describes the attributes of a page in a multi page document
	 * @author lram
	 * 
	 */
    [Bindable]
	public class WebPage extends ContentUnit
	{
		public function WebPage(id:String, pageUrl: String, snapShot: BitmapData) {
			
			super(id);
			
			this.url = pageUrl;
			this.snapShot = snapShot;
		}
		
		public var url: String;

		public var snapShot: BitmapData;
	}
}