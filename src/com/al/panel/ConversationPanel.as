package com.al.panel
{
	
	import com.al.collaboration.annotation.model.Annotation;
	import com.al.collaboration.events.PostCommentEvent;
    import com.al.events.ItemEvent;

    import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayList;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;

	import org.spicefactory.lib.reflect.metadata.Required;
	
	import spark.components.Button;
	import spark.components.Group;
	import spark.components.Label;
	import spark.components.List;
	import spark.components.SkinnableContainer;
	import spark.components.TextInput;
	import spark.components.supportClasses.SkinnableTextBase;
	import spark.core.NavigationUnit;
	
	[Event(name="close", type="flash.events.Event")]
	[Event(name="postComment", type="com.al.collaboration.events.PostCommentEvent")]
	
	/**
	 * 
	 * @author lram
	 * 
	 */
	public class ConversationPanel extends SkinnableContainer
	{
		[SkinPart(required="true")]
		public var conversationList: List;
		
		[SkinPart(required="true")]
		public var commentTI: SkinnableTextBase;
		
		[SkinPart(required="true")]
		public var dragArea: Group;
		
		[SkinPart(required="true")]
		public var postButton: Button;
		
		[SkinPart]
		public var closeButton: Button;
		
		[SkinPart]
		public var label: Label;
		
		public function ConversationPanel()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onCreationComplete);
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		//--------------------------------------------------------------------------
		//
		//  Properties
		//
		//--------------------------------------------------------------------------
		//--------------------------------------------------
		// annotation
		//--------------------------------------------------
		private var _annotation: Annotation;
		private var annotationChange: Boolean;
		public function get annotation(): Annotation {
			
			return _annotation;
		}
		
		public function set annotation(value: Annotation): void {
			
			if(value == _annotation)
				return;
				
			if(_annotation)
            {
                _annotation.comments.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onCommentsChange);
                _annotation.removeEventListener(ItemEvent.HIGHLIGHT_COMMENT, onCommentHighlight);
            }
			_annotation = value;
			_annotation.comments.addEventListener(CollectionEvent.COLLECTION_CHANGE, onCommentsChange);
            _annotation.addEventListener(ItemEvent.HIGHLIGHT_COMMENT, onCommentHighlight);
			
			annotationChange = true;
			invalidateProperties();
		}

        private function onCommentHighlight(event:ItemEvent):void
        {
            var index:int = conversationList.dataProvider.getItemIndex(event.item);
            if(index>=0)
            {
                conversationList.selectedIndex = index;
                conversationList.ensureIndexIsVisible(index);
            }

        }

        public function onConversationPanelShown():void
        {
            scrollToBottom();
        }
		//--------------------------------------------------
		// conversationId
		//--------------------------------------------------
		private var _conversationId: String;
		public function get conversationId(): String {
			
			return _conversationId;
		}
		
		public function set conversationId(value: String): void {
			
			if(value != _conversationId){
				_conversationId = value;
				invalidateProperties();
			}
		}
		
		//--------------------------------------------------
		// comments
		//--------------------------------------------------
		private var _comments: ArrayList;
		public function get comments(): ArrayList {
			
			return _comments;
		}
		
		public function set comments(value: ArrayList): void {
			
			if(value != _comments){
				_comments = value;
				invalidateProperties();
			}
		}
		
		//--------------------------------------------------------------------------
		//
		//  Override methods
		//
		//--------------------------------------------------------------------------	
		override protected function getCurrentSkinState():String
		{
			return super.getCurrentSkinState();
		} 
		
		override protected function partAdded(partName:String, instance:Object) : void
		{
			super.partAdded(partName, instance);
			if(instance == postButton){
				postButton.addEventListener(MouseEvent.CLICK, onPostClick);
			}
			else if(instance == closeButton){
				
				closeButton.addEventListener(MouseEvent.CLICK, onCloseClick);
			}
		}
		
		override protected function partRemoved(partName:String, instance:Object) : void
		{
			super.partRemoved(partName, instance);
		}

        //this is to make sure, it only goes to bottom when loading or something is posted. Not when utility panel collaboration is driving it.
        private var forceScrollToBottom:Boolean;

		override protected function commitProperties():void {
			
			super.commitProperties();
			
			if(annotationChange){
				
				if(label)
					label.text = "Conversation " + 
						(annotation && annotation.conversationId ? annotation.conversationId : "--");
				conversationList.dataProvider = annotation ? annotation.comments : null;
				scrollToBottom();
				annotationChange = false;
			}
		}
		
		//--------------------------------------------------------------------------
		//
		//  Event handlers
		//
		//--------------------------------------------------------------------------
		protected function onCreationComplete(event: Event): void{

			if(commentTI)
				commentTI.setFocus();
		}

		protected function onCommentsChange(event: CollectionEvent): void {
		
			// TODO: This has to go into the list control.
			// TODO: Create a custom list control
            if((conversationList && conversationList.selectedIndex<0) || forceScrollToBottom)
            {
                forceScrollToBottom = false;
                callLater(scrollToBottom);
            }
		}
		
		protected function onPostClick(ev: Event): void {
			
			if(commentTI.text){

				forceScrollToBottom = true;
				dispatchEvent(new PostCommentEvent(commentTI.text, annotation) );

				commentTI.text = "";
				commentTI.setFocus();
			}
		}
		
		protected function onCloseClick(ev: Event): void {
			
			dispatchEvent(new Event(Event.CLOSE));
		}
		
		private function scrollToBottom():void {

			if(conversationList && conversationList.dataProvider)
            {
                conversationList.ensureIndexIsVisible(conversationList.dataProvider.length - 1);
            }
		}
	}
}