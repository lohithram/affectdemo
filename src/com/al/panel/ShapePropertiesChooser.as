/**
 * Created by lohithram on 01/05/15.
 */
package com.al.panel {

    import ardisia.components.colorPicker.ColorPicker;

    import com.al.enums.ShapeProperties;

    import flash.events.Event;
    import flash.events.IEventDispatcher;
    import flash.utils.Dictionary;

    import mx.core.IVisualElement;
    import mx.core.UIComponent;

    import spark.components.ColorPicker;
    import spark.components.NumericStepper;
    import spark.components.supportClasses.ListBase;
    import spark.components.supportClasses.SkinnableComponent;
    import spark.components.supportClasses.SliderBase;

    [Event(name='shapePropertyChange',type='flash.events.Event')]

    public class ShapePropertiesChooser extends SkinnableComponent {


        public static const MIN_LINE_THICKNESS: Number = 1;
        public static const MAX_LINE_THICKNESS: Number = 20;

        [SkinPart]
        public var fillColorPicker: IVisualElement;

        [SkinPart]
        public var lineColorPicker: IVisualElement;

        [SkinPart]
        public var alphaControl: SliderBase;

        [SkinPart]
        public var lineThickness: NumericStepper;

        //protected var lineThicknessDP: IList = new ArrayList([0,1,2,4,6,8,10]);
        
        //protected var alphaListDP: IList = new ArrayList([1,0.8,0.6,0.4,0.2,0.0]);

        protected var propertyMap: Dictionary = new Dictionary();

        private var _propertySet: Object;
        private var propertySetChanged: Boolean;

        public function get propertySet(): Object {

            return _propertySet;
        }

        public function set propertySet(value: Object): void{

            if(_propertySet == value) return;

            _propertySet =  value;
            propertySetChanged = true;
            invalidateProperties();
        }

        override protected function partAdded(name: String, instance: Object): void{

            super.partAdded(name, instance);

            switch(instance){

                case fillColorPicker:
                        (fillColorPicker as UIComponent).toolTip = "Fill color";
                        propertyMap[fillColorPicker] = ShapeProperties.PRIMARY_COLOR;
                        break;
                case lineColorPicker:
                        (lineColorPicker as UIComponent).toolTip = "Line color";
                        propertyMap[lineColorPicker] = ShapeProperties.LINE_COLOR;
                        break;
                case alphaControl:
                        alphaControl.minimum = 0.0;
                        alphaControl.maximum = 1.0;
                        alphaControl.stepSize = 0.05;
                        alphaControl.value = 1.0;
                        propertyMap[alphaControl] = ShapeProperties.ALPHA;
                        break;
                case lineThickness:
                        //lineThickness.dataProvider = lineThicknessDP;
                        lineThickness.minimum = MIN_LINE_THICKNESS;
                        lineThickness.maximum = MAX_LINE_THICKNESS;
                        propertyMap[lineThickness] = ShapeProperties.LINE_THICKNESS;
                        break;
                }
            (instance as IEventDispatcher).addEventListener(Event.CHANGE, onValueChange);
        }

        override protected function commitProperties(): void {

            super.commitProperties();

            if(propertySetChanged && propertySet){

                for( var propertyName: String in propertySet ){

                    setPropertyValue(propertyName, propertySet[propertyName]);
                }
                /*for(var control: * in propertyMap){

                    control.visible = control.includeInLayout = propertySet.hasOwnProperty(propertyMap[control]);
                }*/
                propertySetChanged = false;
            }
        }

        private function onValueChange(event: Event): void {

            var control: IVisualElement = (event.currentTarget) as IVisualElement;
            var propertyName: String = propertyMap[control];

            if(propertyName && propertySet.hasOwnProperty(propertyName)){

                propertySet[propertyName] = getPropertyValue(control);
                dispatchEvent(new Event("shapePropertyChange"));
            }
        }

        private function setPropertyValue(propertyName: String, value: *): void {

            for(var control: * in propertyMap){

                if(propertyMap[control] == propertyName){

                    if(control is ardisia.components.colorPicker.ColorPicker)
                            (control as ardisia.components.colorPicker.ColorPicker).selectedColor = value as uint;
                    else if( control is spark.components.ColorPicker)
                            (control as spark.components.ColorPicker).selectedColor = value as uint;
                    else if( control is ListBase)
                            (control as ListBase).selectedItem = value;
                    else if( control is NumericStepper)
                        (control as NumericStepper).value = value;
                    else if( control is SliderBase)
                        (control as SliderBase).value = value;
                }
            }
        }

        private function getPropertyValue(control: IVisualElement): Object {

            var value: Object;

            if(control is ardisia.components.colorPicker.ColorPicker)
                    value = (control as ardisia.components.colorPicker.ColorPicker).selectedColor;
            else if( control is spark.components.ColorPicker)
                    value = (control as spark.components.ColorPicker).selectedColor;
            else if( control is ListBase)
                  value = (control as ListBase).selectedItem;
            else if( control is NumericStepper)
                value = (control as NumericStepper).value;
            else if( control is SliderBase)
                value = (control as SliderBase).value;

            return value;
        }
    }
}
