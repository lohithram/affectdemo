/**
 * Created by lram on 17/02/2015.
 */
package com.al.pms
{

    import com.al.messages.RefreshRefDataMsg;
    import com.al.model.AnnouncementModel;
    import com.al.refData.AnnouncementRefData;
    import com.al.refData.CalendarRefData;

    import mx.collections.ArrayCollection;

    import spark.core.IContentLoader;

    public class AnnouncementsPM
    {
        [Inject]
        [Bindable]
        public var model: AnnouncementModel;

        [Inject]
        [Bindable]
        public var contentCache: IContentLoader;

        private var refData: AnnouncementRefData;

        [Init]
        public function init(): void {

            refData = new AnnouncementRefData();
            model.announcements = refData.announcements;
        }

        [MessageHandler]
        public function refreshRefData(msg: RefreshRefDataMsg): void{

            refData.readFromXML();
            model.announcements = refData.announcements;
        }
    }
}
