/**
 * Created by lram on 17/02/2015.
 */
package com.al.pms
{

    import com.al.messages.OpenApprovalMsg;
    import com.al.model.Activity;
    import com.al.model.ProjectModel;

    import mx.binding.utils.BindingUtils;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;

    import spark.core.IContentLoader;

    public class ApprovalsPM
    {
        public function ApprovalsPM() {
        }

        [Inject]
        [Bindable]
        public var model: ProjectModel;

        [Inject]
        [Bindable]
        public var contentCache: IContentLoader;

        [MessageDispatcher]
        public var dispatcher: Function;

        [Bindable]
        public var jobsCollection: ArrayCollection;

        [Bindable]
        public var searchStr: String = "";

        private var statusFilters: ArrayList;

        [Init]
        public function init(): void {

            statusFilters = new ArrayList([]);
            jobsCollection = new ArrayCollection(model.allActivities.source);
            BindingUtils.bindSetter(updateCollection, model, "allActivities");

            jobsCollection.filterFunction = jobFilterFunction;
        }

        public function updateFiler(jobStatus: String, addAction: Boolean = true): void {

            statusFilters.removeItem(jobStatus);
            if(addAction)
                statusFilters.addItem(jobStatus);

            jobsCollection.refresh();
        }

        public function openSelectedApproval(activity: Activity): void{

            dispatcher(new OpenApprovalMsg(activity));
        }

        private function updateCollection(data: Object): void {

            jobsCollection = new ArrayCollection(model.allActivities.source);
        }

        private function jobFilterFunction(item: Object): Boolean {

            var job: Activity = item as Activity;
            var includeInList: Boolean = true;

            if(searchStr){

                includeInList = (job.title && job.title.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) ||
                                (job.projectId && job.projectId.toLowerCase().indexOf(searchStr.toLowerCase()) > -1);
            }
            if(includeInList && statusFilters.length > 0) {

                for( var i: int = 0; i < statusFilters.length; ++i ) {

                    var status: String = statusFilters.getItemAt(i) as String;
                    includeInList = (job.status == status);
                    if(includeInList) break;
                }
            }
            return includeInList;
        }


    }
}
