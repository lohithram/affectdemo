package com.al.pms
{

    import com.al.messages.SaveBriefMsg;
    import com.al.model.BriefTemplatesModel;
    import com.al.model.FormField;
    import com.al.model.FormSection;
    import com.al.model.FormTemplate;
    import com.al.model.Project;
    import com.al.model.TileDataModel;

    import flash.events.EventDispatcher;
    import flash.utils.Dictionary;

    import mx.collections.ArrayCollection;

    import mx.collections.IViewCursor;
    import mx.collections.ListCollectionView;

    /**
	 * @author lram
	 * 
	 */
	public class BriefTilePM extends EventDispatcher
	{
        [Inject]
        public var tileDataModel:TileDataModel;

		[Bindable]
        [Inject]
		public var model: BriefTemplatesModel;

        [Bindable]
        public var templates:ListCollectionView;

        [Bindable]
        public var currentForm: FormTemplate;

        [MessageDispatcher]
        public var dispatcher: Function;

        public var project: Project;

        [Init]
        public function onInit(): void{

            currentForm = createNewBrief();
            templates = new ListCollectionView(model.templates);
            templates.filterFunction = templateFilter;
            templates.refresh();
        }

        protected function templateFilter(item:FormTemplate):Boolean
        {
            return !item.hidden;
        }

		public function publishBrief(): void{

            dispatcher(new SaveBriefMsg(project.id, currentForm.clone()));
		}

		public function addNewSection(): void{

            currentForm.sections.addItem(createNewSection());
		}

        public function setCurrentTemplate(template: FormTemplate): void{

            if(template.name == BriefTemplatesModel.CREATE_NEW)
                currentForm = createNewBrief();
            else
                currentForm = template.clone();
        }

        public function saveAsTemplate(): void{

            model.templates.addItem(currentForm.clone());
        }

        public function updateOrder(){

            currentForm.sections.source.sortOn("order", Array.NUMERIC);
            //currentTemplate.sections.refresh();
        }



        public function removeSection(section: FormSection): void{

            deleteSection(section.id);
		}

		public function addNewField(section: FormSection): void{

            var field: FormField = new FormField(section.id);
            field.sectionId = section.id;

            section.fields.addItem(field);
		}

        public function removeField(field: FormField): void{

            deleteField(field);
        }

        public function updateSectionOrdering(): void{

            for(var i:int = 0; i<currentForm.sections.length; ++i){

                var section: FormSection = currentForm.sections.getItemAt(i) as FormSection;
                section.order = i;
            }
        }

        public function updateFieldsOrdering(section: FormSection): void{

            var cursor: IViewCursor = section.fields.createCursor();
            var i: int = 0;
            do{

                var field:FormField = cursor.current as FormField;
                //trace(field.name);
                if(field.order != i || field.sectionId != section.id) {

                    field.order = i;
                    field.sectionId = section.id;
                }
                i++;
            }while(cursor.moveNext())
        }
        /**
         * @param section
         */

        protected var sectionMap: Dictionary = new Dictionary();

        protected function createNewBrief(): FormTemplate{

            var brief: FormTemplate = new FormTemplate("New Brief");
            brief.sections.addItem(createNewSection());

            return brief;
        }

        private function createNewSection(): FormSection{

            var section: FormSection = new FormSection();
            section.isOpen = true;
            section.fields.addItem(new FormField(section.id));
            sectionMap[section.id] = section;

            return section;
        }

        private function deleteSection(id: String): void{

            var section: FormSection = sectionMap[id];
            if(section){

                currentForm.sections.removeItem(section);
                delete sectionMap[section.id];
            }
        }

        private function deleteField(formField: FormField): void{

            if(formField){

                var section: FormSection = sectionMap[formField.sectionId];
                if(section)
                    section.fields.removeItem(formField);
            }
        }
	}
}