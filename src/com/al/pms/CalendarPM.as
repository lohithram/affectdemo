/**
 * Created by lram on 16/02/2015.
 */
package com.al.pms
{

    import com.al.messages.RefreshRefDataMsg;
    import com.al.model.CalendarModel;
    import com.al.refData.CalendarRefData;

    import mx.collections.ArrayCollection;

    public class CalendarPM
    {
        [Inject]
        [Bindable]
        public var model: CalendarModel;

        private var refData: CalendarRefData;

        [Init]
        public function init(): void {

            refData = new CalendarRefData();
            model.calendarEvents = new ArrayCollection(refData.calendarEvents);
        }

        [MessageHandler]
        public function refreshRefData(msg: RefreshRefDataMsg): void{

            refData.readFromXML();
            model.calendarEvents.source = refData.calendarEvents;
        }

    }
}
