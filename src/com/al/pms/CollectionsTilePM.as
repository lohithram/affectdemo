/**
 * Created by lram on 17/02/2015.
 */
package com.al.pms
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.DragSourceFormats;
    import com.al.enums.TileType;
    import com.al.messages.ClipboardMessage;
    import com.al.messages.SaveBriefMsg;
    import com.al.model.Cell;
    import com.al.model.CellType;
    import com.al.model.Project;
    import com.al.util.CellUtil;
    import com.al.util.ObjectUtil;

    import flash.desktop.Clipboard;
    import flash.desktop.ClipboardFormats;
    import flash.desktop.NativeDragActions;
    import flash.desktop.NativeDragManager;
    import flash.display.InteractiveObject;
    import flash.events.NativeDragEvent;
    import flash.filesystem.File;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.collections.IList;

    import spark.core.IContentLoader;

    public class CollectionsTilePM
    {
        public var model: *;

        [Bindable]
        public var project: Project;

        [Bindable]
        public var searchTileTypes: IList = new ArrayList([TileType.BRIEF,
                                                    TileType.REVIEW,
                                                    TileType.CONTENT]);

        [Bindable]
        public var newTileTypes: ArrayCollection = new ArrayCollection([TileType.BRIEF,
                                                    TileType.REVIEW,
                                                    TileType.CONCEPT_BOARD]);

        [Bindable]
        public var collection: ArrayCollection;

        [Inject]
        [Bindable]
        public var contentCache: IContentLoader;

        [Bindable]
        public var searchStr: String = "";

        [Bindable]
        public var filterTypes: Array = [];

        private var statusFilters: ArrayList;

        [MessageDispatcher]
        public var dispatcher: Function;

        [Init]
        public function init(): void {

            statusFilters = new ArrayList([]);
            collection = new ArrayCollection();
            collection.filterFunction = jobFilterFunction;
            newTileTypes.filterFunction = filter;
        }

        public function addItemToClipboard(item:Cell):void
        {
            var tc:TileContainerDescriptor = item.getTileContainerDescriptor();
            dispatcher(new ClipboardMessage(tc));
        }

        [MessageHandler]
        public function onSaveBrief(msg: SaveBriefMsg): void{

            if(msg.projectId == project.id){

                var cell: Cell = new Cell();
                cell.project = project;
                cell.model = msg.brief;
                cell.name = msg.brief.name;
                collection.addItem(cell);
                project.briefs.addItem(msg.brief);
            }
        }

        public function setProject(value: Project): void{

            project = value;
            collection.addAll(project.assetsList);

        }

        public function updateFiler(search: String, addAction: Boolean = true): void {

            statusFilters.removeItem(search);
            if(addAction)
                statusFilters.addItem(search);

            collection.refresh();
        }

        public function onNativeDragEnter(event: NativeDragEvent, target: InteractiveObject): void {

            if(canAcceptDragItem(event.clipboard)) {

                NativeDragManager.acceptDragDrop(target);
                NativeDragManager.dropAction = NativeDragActions.LINK;
            }
        }

        public function onNativeDragDrop(event: NativeDragEvent): void
        {
            dropIt(event.clipboard);
        }

        public function dropIt(dragSource:Object):void
        {
            if(canAcceptDragItem(dragSource)){

                if(dragSource.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)){

                    var fileList: Array = dragSource is Clipboard ? dragSource.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array : dragSource.dataForFormat(ClipboardFormats.FILE_LIST_FORMAT) as Array;
                    for each(var file: File in fileList)
                        addAssetForURL(file.url);
                }
                else if(dragSource.hasFormat(ClipboardFormats.URL_FORMAT)){

                    var fileUrl: String = dragSource is Clipboard ? dragSource.getData(ClipboardFormats.URL_FORMAT) as String : dragSource.dataForFormat(ClipboardFormats.URL_FORMAT) as String;
                    addAssetForURL(fileUrl);
                }if(dragSource.hasFormat( DragSourceFormats.TILE_DESCRIPTOR )){

                    var tileDesc: TileContainerDescriptor = dragSource is Clipboard ?
                            dragSource.getData(DragSourceFormats.TILE_DESCRIPTOR) as TileContainerDescriptor : dragSource.dataForFormat(DragSourceFormats.TILE_DESCRIPTOR) as TileContainerDescriptor;
                    addAsset(tileDesc);
                }
            }
        }

        private function addAsset(tileDescriptor: TileContainerDescriptor): void{

            var cell: Cell = CellUtil.createCellFromTileDescriptor(tileDescriptor);
            project.assetsList.addItem(cell);
            collection.addItem(cell);
        }

        private function addAssetForURL(fileURL: String): void{

            var cell: Cell = CellUtil.createCellFromUrl(fileURL);
            project.assetsList.addItem(cell);
            collection.addItem(cell);
        }

        private function canAcceptDragItem(clipboard: Object): Boolean{

            if( clipboard.hasFormat(ClipboardFormats.URL_FORMAT) ||
                    clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT ) ||
                    clipboard.hasFormat(DragSourceFormats.TILE_DESCRIPTOR ) ){

                return true;
            }

            return false;
        }

        private function jobFilterFunction(item: Object): Boolean {

            var cell: * = item;
            var includeInList: Boolean = true;

            if(searchStr){

                includeInList = (cell.name && cell.name.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) ||
                                (cell.id && cell.id.toLowerCase().indexOf(searchStr.toLowerCase()) > -1);
            }
            if(includeInList && statusFilters.length > 0) {

                for( var i: int = 0; i < statusFilters.length; ++i ) {

                    var status: String = statusFilters.getItemAt(i) as String;
                    includeInList = (cell.status == status);
                    if(includeInList) break;
                }
            }
            return includeInList && filter(item);
        }

        protected function filter(item:Object):Boolean
        {
            if(filterTypes.length==0) return true;

            for each(var filterType:String in filterTypes)
            {
                var itemString: String = ObjectUtil.toString(item,["id","filePath","type"]);
                if(itemString.toUpperCase().indexOf(filterType.toUpperCase())>=0 && itemString.toUpperCase().indexOf(searchStr.toUpperCase())>=0)
                {
                    return true;
                }
            }

            return false;

        }


    }
}
