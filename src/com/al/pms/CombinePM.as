/**
 * Created by lohithram on 06/05/15.
 */
package com.al.pms {

    import com.al.combine.ICombine;
    import com.al.containers.TileContainer;
    import com.al.descriptors.ICombineDescriptor;
    import com.al.messages.CombineSelectedMsg;
    import com.al.messages.TileFocusedMessage;
    import com.al.messages.ToolSelectMessage;
    import com.al.model.CombineModel;
    import com.al.model.DisplayAreaModel;

    public class CombinePM {

        [Inject]
        [Bindable]
        public var model: CombineModel;

        [Inject]
        public var displayAreaModel: DisplayAreaModel;

        //--------------------------------------------------------------------------
        //
        //  Parsley Message Handling
        //
        //--------------------------------------------------------------------------

        [MessageHandler]
        public function onTileFocus(msg: TileFocusedMessage): void{

            model.focusedTile = msg.tileDescriptor;
        }

        [MessageHandler]
        public function onCombineSelect(msg: CombineSelectedMsg): void{

            model.selected = (model.combineDescriptor == msg.combineDescriptor);
            if(model.selected){

                displayAreaModel.currentCombine = model;
            }
        }

        [MessageHandler]
        /**
         * TODO: This should be moved to a separate PM since it is not the
         * responsibility of this PM to process tool selection
         */
        public function onToolSelect(msg: ToolSelectMessage): void{

            if( model.selected && model.focusedTile ) {
            }
        }

        //--------------------------------------------------------------------------
        //
        //  Public Methods
        //
        //--------------------------------------------------------------------------
        public function setCombineDescriptor(desc: ICombineDescriptor): void{

            model.combineDescriptor = desc;
            model.selected = (model.combineDescriptor ==
                              displayAreaModel.combines.getItemAt(displayAreaModel.mainViewIndex));

            if(model.selected)
                displayAreaModel.currentCombine = model;
        }

        //QUICK and DIRTY
        public function onCombineCreation(combine: ICombine): void{

            for( var i: int = 0; i<combine.numElements; ++i) {

                var tile: TileContainer = combine.getElementAt(i) as TileContainer;
                if(tile){

                    //QUICK and DIRTY
                    if(model.tiles && model.tiles.getItemIndex(tile.pm.tileDataModel)<0)
                    {
                        model.tiles.addItem(tile.pm.tileDataModel);
                    }
                }
            }
        }

        //QUICK and DIRTY
        public function addNewTile(tile: TileContainer): void{

            if(tile)
            {
                if(model.tiles && model.tiles.getItemIndex(tile.pm.tileDataModel)<0)
                {
                    model.tiles.addItem(tile.pm.tileDataModel);
                }
            }
        }

        //QUICK and DIRTY
        public function tileRemoved(tile: TileContainer): void{

            if(tile)
                model.tiles.removeItem(tile.pm.tileDataModel);
        }

    }
}
