/**
 * Created by lram on 17/02/2015.
 */
package com.al.pms
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.DragSourceFormats;
    import com.al.events.ContentUnitChangeEvent;
    import com.al.model.ConceptBoardTemplate;
    import com.al.model.ContentUnit;
    import com.al.model.PageContent;
    import com.al.model.TileDataModel;
    import com.al.util.FileUtils;

    import flash.desktop.Clipboard;
    import flash.desktop.ClipboardFormats;
    import flash.desktop.NativeDragActions;
    import flash.desktop.NativeDragManager;
    import flash.display.InteractiveObject;
    import flash.events.NativeDragEvent;
    import flash.filesystem.File;

    import mx.collections.ArrayCollection;
    import mx.utils.UIDUtil;

    public class ConceptBoardTilePM
    {
        private var _model:ConceptBoardTemplate;

        [Bindable]
        public function get model():ConceptBoardTemplate
        {
            return _model;
        }

        public function set model(value:ConceptBoardTemplate):void
        {
            if(value != _model)
            {
                _model = value;

                if(_model && _model.pages)
                {
                    contentUnits = model.pages;
                    if(contentUnits.length > 0)
                    {
                        setIndex(0);
                    }
                    else
                    {
                        setIndex(-1);
                    }
                }
            }
        }

        [Bindable]
        public var contentUnits: ArrayCollection = new ArrayCollection();

        [Bindable]
        public var currentContent: PageContent;

        [Bindable]
        public var currentIndex:int;

        [Inject]
        public var tileDataModel:TileDataModel;

        public function addBlankPage(): void{

            var assetUrl: String = FileUtils.resolveDemoResource("BlankPage.png");
            addAsset(assetUrl);

            goToLast();
        }

        public function goToNext():void {

            if(!contentUnits) return;

            var index:int = contentUnits.getItemIndex(currentContent);
            if (index > -1 && index < contentUnits.length - 1) {
                setIndex(++index);
            }
        }

        public function goToPrevious():void {

            if(!contentUnits) return;

            var index:int = contentUnits.getItemIndex(currentContent);
            if (index > 0 && index < contentUnits.length) {
                setIndex(--index);
            }
        }

        public function onNativeDragEnter(event: NativeDragEvent, target: InteractiveObject): void {

            if(canAcceptDragItem(event.clipboard)) {

                NativeDragManager.acceptDragDrop(target);
                NativeDragManager.dropAction = NativeDragActions.COPY;
            }
        }

        // TODO: Should be adding contentUnits rather than tileDescriptors
        public function onNativeDragDrop(event: NativeDragEvent): void {

            var clipboard: Clipboard = event.clipboard;
            if(canAcceptDragItem(clipboard)){

                if(clipboard.hasFormat( DragSourceFormats.TILE_DESCRIPTOR )){

                    var tileDesc: TileContainerDescriptor =
                            clipboard.getData(DragSourceFormats.TILE_DESCRIPTOR) as TileContainerDescriptor;
                    var assetUrl: String = tileDesc.currentContent.contentUrl;
                    addAsset(assetUrl);
                }
                else if(clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)){

                    var fileList: Array = clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array;
                    for each(var file: File in fileList)
                        addAsset(file.url);
                }
                else if(clipboard.hasFormat(ClipboardFormats.URL_FORMAT)){

                    var fileUrl: String = clipboard.getData(ClipboardFormats.URL_FORMAT) as String;
                    addAsset(fileUrl);
                }
                goToLast();
            }
        }

        private function goToLast(): void{

            setIndex(contentUnits.length-1);
        }

        /**
         * Single point entry to navigate pages
         * @param value
         */
        public function setIndex(value:Number):void {

            if (value == -1) return;

            currentIndex = value;
            currentContent = contentUnits.getItemAt(currentIndex) as PageContent;

            dispatchEvent(new ContentUnitChangeEvent(currentContent));
        }

        private function addAsset(url: String): ContentUnit{

            var page: PageContent =
                    new PageContent(UIDUtil.createUID(), contentUnits.length+1, url);
            contentUnits.addItem(page);

            return page;
        }

        private function canAcceptDragItem(clipboard: Clipboard): Boolean{

            if( clipboard.hasFormat(ClipboardFormats.URL_FORMAT) ||
                    clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT ) ||
                    clipboard.hasFormat(DragSourceFormats.TILE_DESCRIPTOR ) ||
                    clipboard.hasFormat(DragSourceFormats.ASSET_CELL_ITEM ) ){

                return true;
            }

            return false;
        }
    }
}
