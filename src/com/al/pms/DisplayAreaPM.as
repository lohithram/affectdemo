/**
 * Created by lram on 12/02/2015.
 */
package com.al.pms
{

    import com.adobe.rtc.session.IConnectSession;
    import com.al.combine.ICombine;
    import com.al.containers.DashboardContainer;
    import com.al.containers.HTMLContainer;
    import com.al.descriptors.CombineDescriptor;
    import com.al.descriptors.ICombineDescriptor;
    import com.al.descriptors.ITabDescriptor;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.CombineType;
    import com.al.enums.ContentType;
    import com.al.enums.TileType;
    import com.al.factory.ProjectFactory;
    import com.al.factory.TileContainerFactory;
    import com.al.messages.CombineClosedMsg;
    import com.al.messages.CombineSelectedMsg;
    import com.al.messages.ItemMsg;
    import com.al.messages.OpenApprovalMsg;
    import com.al.messages.AddCombineMsg;
    import com.al.messages.OpenProjectMessage;
    import com.al.messages.RefreshRefDataMsg;
    import com.al.messages.UtilityPanelCloseMsg;
    import com.al.messages.DrawingPanelCloseMsg;
    import com.al.messages.DrawingPanelOpenMsg;
    import com.al.model.Cell;
    import com.al.model.CellType;
    import com.al.model.CombineModel;
    import com.al.model.DisplayAreaModel;
    import com.al.model.PageContent;
    import com.al.model.Project;
    import com.al.model.TileContentDescriptor;
    import com.al.util.FileUtils;
    import com.al.util.SizeUtil;
    import com.al.views.WhiteBoardView;

    import flash.events.Event;
    import flash.events.IEventDispatcher;
    import flash.net.URLRequest;

    import mx.collections.ArrayCollection;

    import mx.collections.ArrayList;
    import mx.core.ClassFactory;
    import mx.core.FlexGlobals;
    import mx.core.IVisualElementContainer;
    import mx.events.StyleEvent;
    import mx.managers.PopUpManager;
    import mx.utils.URLUtil;

    import spark.components.Alert;
    import spark.components.Application;

    public class DisplayAreaPM
    {

        private static const NORMAL_STATE: String = "normal";
        private static const CLOSED_STATE: String = "closed";
        private static const OPEN_STATE: String = "open";

        [Bindable]
        [Inject]
        public var model: DisplayAreaModel;

        [Bindable]
        public var utilityPanelIndex: int = 0;

        [MessageDispatcher]
        public var dispatcher: Function;

        [Inject]
        public var connectSession: IConnectSession;

        [Bindable]
        public var utilityPanelWidth: Number = SizeUtil.UP_MIN_WIDTH;

        [Bindable]
        public var displayState: String = NORMAL_STATE;


        /**
         * TODO: This is the only exception of referencing
         * UIElement in PM
         */
        public var viewStack: IVisualElementContainer;

        private var combineCounter: int = -1;

        [Init]
        public function init(): void{

            model.combines = new ArrayList();
            loadCombines();
//          createCombines();
            showCombine(0);

            displayState = CLOSED_STATE;
			//TODO: Move this to higher on in the parsley context tree, like bootstrap
			connectSession.login();
        }

        public function setUtilityPanelIndex(index: int): void{

            var prevIndex: int = utilityPanelIndex;
            utilityPanelIndex = index;

            if(prevIndex == 0 && utilityPanelIndex > 0)
                dispatcher(new DrawingPanelCloseMsg());
            else if (utilityPanelIndex == 0)
                dispatcher(new DrawingPanelOpenMsg());
        }

        //--------------------------------------------------------------------------
        //
        //  Handling Parsley Messages
        //
        //--------------------------------------------------------------------------
        //TODO: Add Message tag when the parsley message is created
        public function onCombineDisplayed(msg: Object): void{

            model.currentCombine = msg.combine as CombineModel;
        }

        [MessageHandler]
        public function openCombine(msg: AddCombineMsg): void{

            model.combines.addItem(msg.combineDescriptor);
            showCombine(model.combines.getItemIndex(msg.combineDescriptor));
        }

        [MessageHandler]
        public function openProject(msg: OpenProjectMessage): void{

            var combine: ICombineDescriptor =
                    ProjectFactory.createProjectCombine(msg.project, ++combineCounter);
            model.combines.addItem(combine);

            // Show the project
            showCombine(model.combines.length-1);
        }

        [MessageHandler]
        public function openApproval(msg: OpenApprovalMsg): void{

            var tileDesc: TileContainerDescriptor =
                    TileContainerFactory.createTileDescriptorForPreview(msg.activity.preview);
            tileDesc.title = msg.activity.title;

            var combine: ICombineDescriptor =
                    ProjectFactory.createCombineForTileDesc(tileDesc, ++combineCounter);
            model.combines.addItem(combine);

            //Open the approval
            showCombine(model.combines.length-1);
        }

        //--------------------------------------------------------------------------
        //
        //  Public Methods
        //
        //--------------------------------------------------------------------------
        public function toggleUtilityPanel(utilityBarIndex: int): void{

            if (displayState == CLOSED_STATE) {
                displayState = OPEN_STATE;
            }
            else {
                if (-1 == utilityBarIndex) {

                    displayState = CLOSED_STATE;
                    dispatcher(new UtilityPanelCloseMsg());
                }
            }
        }

        public function closeCombine(combine: ICombineDescriptor): void{

            if(!combine) return;
            if(model.combines.length < 2) return;

            var index: int = model.combines.getItemIndex(combine);
            if( index >= 0){

                model.combines.removeItem(combine);
                if(combine == model.currentCombine){

                    // switch to a combine which takes the same index as the currently closed one
                    showCombine( index < model.combines.length ? index : model.combines.length-1);
                }
                dispatcher(new CombineClosedMsg(combine));
            }
        }

        public function showCombine(combineIndex: int): void {

            model.mainViewIndex = combineIndex;
            dispatcher(new CombineSelectedMsg( model.combines.getItemAt(combineIndex) as CombineDescriptor));
        }

        public function refreshData(): void{

            dispatcher(new RefreshRefDataMsg());
        }

        public function getCombineAt(index: int): ICombine {

            return viewStack.getElementAt(index) as ICombine;
        }

        public function addNewCombine(tileDescriptors: Array): void {

            var combine: ICombineDescriptor = new CombineDescriptor(model.combines.length);
			if(tileDescriptors.length > 1){
				combine.tileContainers = new ArrayList(tileDescriptors);
			}
			else if(tileDescriptors.length == 1){
				
				var desc: TileContainerDescriptor = tileDescriptors[0] as TileContainerDescriptor;
				var clonedDesc: TileContainerDescriptor = desc.clone();
				clonedDesc.reducedX = clonedDesc.reducedY = 0;
				clonedDesc.reducedWidth = clonedDesc.reducedHeight = 100;
				combine.tileContainers = new ArrayList([clonedDesc]);
			}
			
            model.combines.addItem(combine);
        }

        public function openURLAsNewCombine(urlRequest: URLRequest): void{

            if(urlRequest && urlRequest.url){

                var tileDescriptor: TileContainerDescriptor = createTileDescriptor(0, 0, 100,100, urlRequest.url, HTMLContainer);
                var combine: ICombineDescriptor = new CombineDescriptor(model.combines.length);
                combine.tileContainers = new ArrayList([tileDescriptor]);

                model.combines.addItem(combine);
            }
        }

        private var cssList: Array = ["css/funky.swf", "css/london.swf", "css/stockholm.swf"];
		private var cssFile: String 
		private var count: int = 0;
		
		/**
		 * First unloads previously loaded css file and loads the new one next in line.
		 * 
		 */
		public function loadCSSFile(): void {
			
			
			if(FlexGlobals.topLevelApplication){
				
				var application: Application = FlexGlobals.topLevelApplication as Application;
				if(cssFile)
					application.styleManager.unloadStyleDeclarations(cssFile, false);
				
				cssFile = cssList[count++ % 3];
				
				var event: IEventDispatcher = application.styleManager.loadStyleDeclarations( cssFile );
				event.addEventListener(StyleEvent.PROGRESS, onProgress);
				event.addEventListener(StyleEvent.COMPLETE, onComplete);
			}
		}

        /**
         * Removes a combine corresponding to the tileDescriptor
         * @param tileDescriptor
         */
        public function removeCombine(tabDescriptor: ITabDescriptor): void {

            var index: int = model.combines.getItemIndex(tabDescriptor);
            if(index >= 0)
                model.combines.removeItemAt(index);
        }

        //--------------------------------------------------------------------------
        //
        //  Private
        //
        //--------------------------------------------------------------------------
		private var alert: Alert;
		private static var tileConentCounter: uint = 0;
		
		private function onProgress(event: StyleEvent):void {
			
			if(alert)
				PopUpManager.removePopUp(alert);
			alert = Alert.show("Loading " + cssFile + " - " + Math.floor(event.bytesLoaded/event.bytesTotal*100)+"%");
		}	
		
		private function onComplete(event:Event):void {
			
			if(alert)
				PopUpManager.removePopUp(alert);
			Alert.show(cssFile + " succesfully loaded and applied");
			(event.target as IEventDispatcher).removeEventListener(StyleEvent.PROGRESS, onProgress);
			(event.target as IEventDispatcher).removeEventListener(StyleEvent.COMPLETE, onComplete);	
		}

        private function loadCombines():void
        {
            var xml:XML = FileUtils.loadRefDataXML("Combines.xml");
            if (!xml) return;

            for each(var combineXML:XML in xml.combine)
            {
                var closable:Boolean = String(combineXML.closable).toUpperCase() == "FALSE" || String(combineXML.closable).toUpperCase() == "NO" ? false:true;

                var combine: ICombineDescriptor = new CombineDescriptor(++combineCounter, combineXML.title, closable);

                if (String(combineXML.type).toUpperCase() == CombineType.DASHBOARD.toUpperCase())
                {
                    combine.combineClass = DashboardContainer;
                }
                else
                {
                    if(String(combineXML.type).toUpperCase() == CombineType.PROJECT.toUpperCase() ) {

                        var project:Project = ProjectFactory.createNewProject();
                        project.title = combineXML.title;
                    }

                    for each(var tileXML:XML in combineXML.tiles.tile)
                    {
                        var type:Class;

                        switch(String(tileXML.type).toUpperCase())
                        {
                            case TileType.HTML.toUpperCase():
                                type = HTMLContainer;
                                break;

                            case TileType.WB.toUpperCase():
                                type = WhiteBoardView;
                                break

                            default:
                                type = null;
                        }

                        var td:TileContainerDescriptor;

                        if(String(tileXML.type).toUpperCase() == TileType.PROJECT_ASSETS.toUpperCase() ||
                                String(tileXML.type).toUpperCase() == TileType.PROJECT_DETAILS.toUpperCase() ||
                                String(tileXML.type).toUpperCase() == TileType.ACTIVITIES.toUpperCase() ||
                                String(tileXML.type).toUpperCase() == TileType.REPORT.toUpperCase() ||
                                String(tileXML.type).toUpperCase() == TileType.TEAM.toUpperCase())
                        {
                            if(String(tileXML.type).toUpperCase() == TileType.PROJECT_ASSETS.toUpperCase())
                            {
                                project.assetsList = createPredefinedAssets(project);
                            }

                            td = TileContainerFactory.createTileDescriptor(tileXML.type, tileXML.reducedX, tileXML.reducedY, tileXML.reducedWidth, tileXML.reducedHeight, project)
                        }
                        else
                        {
                            td = createTileDescriptor(tileXML.reducedX, tileXML.reducedY, tileXML.reducedWidth, tileXML.reducedHeight,tileXML.url,type);
                            if(String(tileXML.type).toUpperCase() == TileType.MULTI_PAGE.toUpperCase())
                            {
                                var content: TileContentDescriptor = new TileContentDescriptor("TileContent::"+String(++tileConentCounter));
                                content.contentType = ContentType.MULTI_PAGE;

                                content.contentUnits = new ArrayCollection();

                                var pageCount: int = 0;
                                for each(var pageXML:XML in tileXML.pages.page)
                                {
                                    var pageUrl: String = FileUtils.resolveDemoResource(pageXML.url);
                                    content.contentUnits.addItem(new PageContent(String(++pageCount), pageCount, pageUrl ));
                                }
                                content.contentUrl = content.contentUnits.getItemAt(0).pageUrl;

                                td.currentContent = content;
                                td.contents = new Array(content);
                            }
                        }

                        combine.tileContainers.addItem(td);
                    }
                }

                model.combines.addItem(combine);

            }
        }

        private function createPredefinedAssets(project:Project):ArrayCollection
        {
            var result:ArrayCollection = new ArrayCollection();
            var projectCell : Cell = new Cell();
            projectCell.type = CellType.PROJECT_DETAILS;
            projectCell.project = project;
            projectCell.name = "Project Details";
            result.addItem(projectCell);

            var activitiesCell : Cell = new Cell();
            activitiesCell.project = project;
            activitiesCell.type = CellType.ACTIVITIES;
            activitiesCell.name = "Activities and Estimates ["+project.id+"]";
            result.addItem(activitiesCell);

            var teamCell : Cell = new Cell();
            teamCell.project = project;
            teamCell.type = CellType.TEAM;
            teamCell.name = "Team ["+project.id+"]";
            result.addItem(teamCell);

            var reportCell : Cell = new Cell();
            reportCell.project = project;
            reportCell.type = CellType.REPORT;
            reportCell.name = "Actual + Budget Activities ["+project.id+"]";
            result.addItem(reportCell);

            return result;

        }

        private function createTileDescriptor(reducedX: Number, reducedY: Number,
                                              reducedWidth: Number, reducedHeight: Number,
                                              contentUrl: String = null,
											  contentClass: Class = null): TileContainerDescriptor {

            var tileType:String = URLUtil.isHttpURL(contentUrl) && !FileUtils.isImageUrl(contentUrl) ? TileType.HTML : TileType.CONTENT;

            var desc: TileContainerDescriptor =
                    TileContainerFactory.createTileDescriptor(tileType,
                                                              reducedX, reducedY, reducedWidth, reducedHeight);
            desc.title = contentUrl;

			//contentDescriptor
			var contentModel: TileContentDescriptor =
                    new TileContentDescriptor("TileContent::"+String(++tileConentCounter));
			desc.currentContent = contentModel;
			if(contentClass)
				contentModel.contentFactory = new ClassFactory(contentClass);
			if(contentUrl) {

                if(URLUtil.isHttpURL(contentUrl))
                    contentModel.contentUrl = contentUrl;
                else
                    contentModel.contentUrl = FileUtils.resolveDemoResource(contentUrl);

                contentModel.contentType = ContentType.SINGLE_DOCUMENT;
            }
			
            return desc;
        }

        [MessageHandler(scope="global",selector="projectTitle")]
        public function onTitleModified(msg:ItemMsg):void
        {

        }
    }
}
