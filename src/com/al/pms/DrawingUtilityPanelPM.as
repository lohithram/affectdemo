/**
 * Created by lohithram on 01/05/15.
 */
package com.al.pms {

    import com.adobe.coreUI.controls.whiteboardClasses.ToolBarDescriptors.WBShapeToolBarDescriptor;
    import com.adobe.coreUI.controls.whiteboardClasses.WBShapeDescriptor;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBArrowShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBMarkerShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBSimpleShape;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBSimpleShapeFactory;
    import com.adobe.coreUI.controls.whiteboardClasses.shapes.WBTextShapeFactory;
    import com.al.messages.DrawingPanelCloseMsg;
    import com.al.messages.DrawingPanelOpenMsg;
    import com.al.messages.ToolSelectMessage;
    import com.al.messages.UtilityPanelCloseMsg;
    import com.al.model.DrawingModel;
    import com.al.model.DrawingToolsDescriptorModel;
    import com.al.model.TextFormatModel;
    import com.al.renderers.ToolItemRenderer;

    import flash.events.Event;
    import flash.events.EventDispatcher;

    public class DrawingUtilityPanelPM extends EventDispatcher {

        [Inject]
        [Bindable]
        public var drawingModel: DrawingModel;

        [Inject]
        [Bindable]
        public var textFormatModel: TextFormatModel;

        [MessageDispatcher]
        public var dispatcher: Function;

        [Bindable]
        public var selectedShape: WBShapeDescriptor;

        public var currentDrawingSelection: ToolItemRenderer;

        [Init]
        public function init(): void {

        }


        //--------------------------------------------------------------------------
        //
        //  Public methods
        //
        //--------------------------------------------------------------------------
        [MessageHandler]
        public function utilityPanelClosed(message: UtilityPanelCloseMsg): void {

            updateContext(null);
            dispatchEvent(new Event("drawingPanelClosed"));
            dispatcher(new ToolSelectMessage(new Object()));
        }

        [MessageHandler]
        public function drawingPanelClosed(message: DrawingPanelCloseMsg): void {

            updateContext(null);
            dispatchEvent(new Event("drawingPanelClosed"));
            dispatcher(new ToolSelectMessage(new Object()));
        }

        [MessageHandler]
        public function drawingPanelOpen(message: DrawingPanelOpenMsg): void {

            dispatchEvent(new Event("drawingPanelOpened"));
        }

        public function onToolSelect(toolDescriptor: Object, selected: Boolean): void{

            updateContext(toolDescriptor, selected);
            dispatcher(new ToolSelectMessage(selected ? toolDescriptor : null));
        }

        public function squareMaskChange(value: Boolean): void{

            if(value){
                drawingModel.roundMaskSelected = false;
            }
            else {
                drawingModel.squareMaskSelected = true;
            }
        }

        public function roundMaskChange(value: Boolean): void{

            if(value){
                drawingModel.squareMaskSelected = false;
            }
            else {
                drawingModel.roundMaskSelected = true;
            }
        }

        // Updates the selection of tools
        public function updateContext(toolDescriptor: Object, selected: Boolean = false): void {

            drawingModel.annotationToolSelected =
                    toolDescriptor && toolDescriptor.hasOwnProperty("toolName") &&
                    toolDescriptor.toolName == DrawingToolsDescriptorModel.ANNOTATE && selected;
            drawingModel.maskToolSelected =
                    toolDescriptor && toolDescriptor.hasOwnProperty("toolName") &&
                    toolDescriptor.toolName == DrawingToolsDescriptorModel.MASK && selected;
            drawingModel.zoomInSelected =
                    toolDescriptor && toolDescriptor.hasOwnProperty("toolName") &&
                    toolDescriptor.toolName == DrawingToolsDescriptorModel.ZOOM_IN && selected;
            drawingModel.zoomOutSelected =
                    toolDescriptor && toolDescriptor.hasOwnProperty("toolName") &&
                    toolDescriptor.toolName == DrawingToolsDescriptorModel.ZOOM_OUT && selected;
            drawingModel.exactFitSelected =
                    toolDescriptor && toolDescriptor.hasOwnProperty("toolName") &&
                    toolDescriptor.toolName == DrawingToolsDescriptorModel.EXACT_FIT && selected;

            drawingModel.selectToolSelected =
                    (toolDescriptor is WBShapeToolBarDescriptor) &&
                    (toolDescriptor as WBShapeToolBarDescriptor).shapeFactory == null && selected;
            drawingModel.showTextFormatTools =
                    (toolDescriptor is WBShapeToolBarDescriptor) &&
                    (toolDescriptor as WBShapeToolBarDescriptor).shapeFactory is WBTextShapeFactory && selected;
            drawingModel.shapeToolsSelected =
                    (toolDescriptor as WBShapeToolBarDescriptor) &&
                    ( (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBArrowShapeFactory.ARROW_HEAD ||
                            (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBArrowShapeFactory.NO_ARROW_HEAD ||
                            (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBSimpleShape.ELLIPSE ||
                            (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBSimpleShape.RECTANGLE ||
                            (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBSimpleShape.ROUNDED_RECTANGLE ||
                            (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBMarkerShapeFactory.HIGHLIGHTER ||
                            (toolDescriptor as WBShapeToolBarDescriptor).shapeData == WBSimpleShapeFactory.HIGHLIGHT_AREA
                    ) && selected;
        }

    }
}
