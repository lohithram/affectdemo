/**
 * Created by lohithram on 06/05/15.
 */
package com.al.pms {

    import com.al.events.ContentUnitChangeEvent;
    import com.al.model.IContentUnit;
    import com.al.model.TileContainerModel;
    import com.al.model.WebPage;

    import flash.display.BitmapData;

    import mx.collections.ArrayList;
    import mx.utils.HashUtil;

import themes.affect.utils.IconUtils;

import themes.affect.utils.IconUtils;

public class HTMLContainerPM {

        [Bindable]
        public var currentUrl: String = "";

        [Bindable]
        [ArrayElementType("com.al.model.WebPage")]
        public var historyList: ArrayList = new ArrayList();

        [Bindable]
        public var tileContainerModel: TileContainerModel;

        [Bindable]
        public var resolutions: ArrayList =
                new ArrayList([{label: IconUtils.Desktop, mode:"Desktop", width:"1200", height:"100%"},
                                {label: IconUtils.Mobile, mode:"Mobile", width:"320", height:"100%"}]);

        private var _currentPage: WebPage;

        [Bindable]
        public function get currentPage(): WebPage
        {
            return _currentPage;
        }

        public function set currentPage(value: WebPage):void
        {
            if(value != _currentPage)
            {
                _currentPage = value;
                currentUrl = _currentPage ? _currentPage.url : "";

                dispatchEvent(new ContentUnitChangeEvent(_currentPage));
            }
        }

        public function onHistorySelect(item: WebPage): void{

            currentPage = item;
        }

        public function saveCurrentPage(): void{

            if(currentPage && !getHistoryForURL(currentPage.url)) {

                historyList.addItem(currentPage);
            }
        }

        public function nextPage(): void{

            var currentIndex: int = historyList.getItemIndex(currentPage);
            if(currentIndex < historyList.length-1){

                currentPage = historyList.getItemAt(++currentIndex) as WebPage;
            }
        }

        public function prevPage(): void{

            var currentIndex: int = historyList.getItemIndex(currentPage);
            if(currentIndex > 0){

                currentPage = historyList.getItemAt(--currentIndex) as WebPage;
            }
        }

        public function setCurrentPage(url: String, snapShot: BitmapData): void{

            var history: WebPage = getHistoryForURL(url);
            if(history) {

                history.snapShot = snapShot;
                currentPage = history;
            }
            else{

                var id: String = String(HashUtil.apHash(url));
                currentPage = new WebPage(id, url, snapShot);
            }
        }

        private function getHistoryForURL(url: String): WebPage{

            var found: Boolean;
            for each( var history: WebPage in historyList.source){

                if(history.url == url){

                    found = true;
                    break;
                }
            }

            return found ? history : null;
        }
    }
}
