/**
 * Created by lram on 12/02/2015.
 */
package com.al.pms
{

    import com.al.combine.ICombine;
    import com.al.messages.RefreshRefDataMsg;
    import com.al.model.TabModel;

    import mx.core.IVisualElementContainer;

    public class MainViewPM
    {

        [MessageDispatcher]
        public var dispatcher: Function;


        //--------------------------------------------------------------------------
        //
        //  Methods
        //
        //--------------------------------------------------------------------------
        public function refreshData(): void{

            dispatcher(new RefreshRefDataMsg());
        }



    }
}
