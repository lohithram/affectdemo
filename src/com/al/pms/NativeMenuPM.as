/**
 * Created by lram on 12/02/2015.
 */
package com.al.pms
{

    import com.al.messages.CombineClosedMsg;
    import com.al.messages.CreateNewProjectMsg;
    import com.al.messages.AddCombineMsg;

    import flash.utils.Dictionary;

    import mx.collections.XMLListCollection;

    import mx.controls.FlexNativeMenu;
    import mx.core.FlexGlobals;

    import mx.events.FlexNativeMenuEvent;

    import spark.components.WindowedApplication;


    public class NativeMenuPM
    {

        [MessageDispatcher]
        public var dispatcher: Function;

        [Bindable]
        [Embed("/resources/nativeMenus.xml", mimeType="text/xml")]
        protected var menuXML: Class;

        private var combinesDictionary: Dictionary = new Dictionary();

        private function get application(): WindowedApplication{

            return FlexGlobals.topLevelApplication as WindowedApplication;
        }

        [Init]
        public function onInit(): void{

            var nativeMenu: FlexNativeMenu = new FlexNativeMenu();
            nativeMenu.dataProvider = menuXML.data;
            nativeMenu.keyEquivalentField = "@key";
            nativeMenu.labelField = "@label";
            nativeMenu.mnemonicIndexField
            nativeMenu.showRoot = false;

            application.menu = nativeMenu;
            nativeMenu.addEventListener(FlexNativeMenuEvent.MENU_SHOW, handleMenuShow);
            nativeMenu.addEventListener(FlexNativeMenuEvent.ITEM_CLICK, handleMenuSelect);

            processMenu(nativeMenu);
        }

        //--------------------------------------------------------------------------
        //
        //  Methods
        //
        //--------------------------------------------------------------------------
        private static const RECENT_ITEM: String = "RECENT_ITEM";
        [MessageHandler]
        public function onCombineClose(msg: CombineClosedMsg): void{

            if(combinesDictionary[msg.combineDescriptor.title]){

                combinesDictionary[msg.combineDescriptor.title] = msg.combineDescriptor;
                return;
            }
            else {
                var xml:XML = <menutiem/>;
                xml.@itemType = RECENT_ITEM;
                xml.@itemCode = msg.combineDescriptor.title;
                xml.@label = msg.combineDescriptor.title;
                combinesDictionary[msg.combineDescriptor.title] = msg.combineDescriptor;

                recentNode.appendChild(xml);
                application.menu.dataDescriptor.setEnabled(recentNode, true);
            }
        }

        //--------------------------------------------------------------------------
        //
        //  Event handlers
        //
        //--------------------------------------------------------------------------
        private function handleMenuShow(event: FlexNativeMenuEvent): void{

            //trace(event.label);
        }

        private function handleMenuSelect(event: FlexNativeMenuEvent): void{

            if(event.item && event.item.@itemType == RECENT_ITEM){

                var itemCode: String = event.item.@itemCode;
                if(combinesDictionary[itemCode])
                    dispatcher(new AddCombineMsg(combinesDictionary[itemCode]));
            }
            else if(event.label.indexOf("Quit") == 0)
                application.stage.nativeWindow.close();
            else if(event.label.indexOf("Project") == 0)
                dispatcher(new CreateNewProjectMsg());
        }

        //--------------------------------------------------------------------------
        //
        //  Private functions
        //
        //--------------------------------------------------------------------------
        private var fileNode: XML;
        private var recentNode: XML;

        private function processMenu(menu: FlexNativeMenu): void{

            var menuDataProvider: XMLListCollection = menu.dataProvider as XMLListCollection;
            var rootXML: XML = menuDataProvider.getItemAt(0) as XML;
            for each(var xml: XML in rootXML.children()) {
                if (xml.@label == "File") {

                    fileNode = xml;
                    for each(xml in fileNode.children()){
                        if (xml.@label == "Open Recent") {

                            recentNode = xml;
                            break;
                        }
                    }
                }
            }
        }
    }
}
