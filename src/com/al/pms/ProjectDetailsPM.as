package com.al.pms
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.DragSourceFormats;
    import com.al.messages.ItemMsg;
    import com.al.model.Cell;
    import com.al.model.Project;
    import com.al.model.Task;
    import com.al.model.UserCell;
    import com.al.util.CellUtil;

    import flash.events.EventDispatcher;

    import mx.collections.ArrayList;
    import mx.core.DragSource;
    import mx.events.DragEvent;

    /**
	 * @author lram
	 * 
	 */
	public class ProjectDetailsPM extends EventDispatcher {

        [MessageDispatcher]
        public var dispatcher: Function;

        [Bindable]
        public var project: Project;

        [Bindable]
        public var projectTypes: ArrayList = new ArrayList(['TYPE']);

        [Bindable]
        public var mediaTypes: ArrayList = new ArrayList(['TYPE']);

        [Bindable]
        public var publicationTypes: ArrayList = new ArrayList();

        [Bindable]
        public var sizesList: ArrayList = new ArrayList(['a','b','c','d']);



        public function onDragDrop(event: DragEvent): void{

            var droppedItem: Object;
            var dragSource: DragSource = event.dragSource;
            if(dragSource.hasFormat(DragSourceFormats.USER_CELL))
                droppedItem = dragSource.dataForFormat(DragSourceFormats.USER_CELL) as Cell;

            else if(dragSource.hasFormat(DragSourceFormats.TASK_CELL))
                droppedItem = dragSource.dataForFormat(DragSourceFormats.TASK_CELL) as Cell;

            else if(dragSource.hasFormat(DragSourceFormats.ASSET_CELL))
                droppedItem = dragSource.dataForFormat(DragSourceFormats.ASSET_CELL);

            else if(dragSource.hasFormat(DragSourceFormats.TILE_DESCRIPTOR)) {

                droppedItem = dragSource.dataForFormat(DragSourceFormats.TILE_DESCRIPTOR);
                droppedItem = CellUtil.createCellFromTileDescriptor( droppedItem as TileContainerDescriptor );
            }

            if(droppedItem is UserCell)
                project.administrators.addItem(droppedItem);
        }

        public function notifyTitleChange(title:String):void
        {
            dispatcher(new ItemMsg(ItemMsg.PROJECT_TITLE,title));
        }
    }
}