/**
 * Created by lram on 11/02/2015.
 */
package com.al.pms
{

    import ardisia.charts.radar.data.RadarAxis;
    import ardisia.components.timeline.dataTypes.TimelineSeries;

    import com.al.factory.ProjectFactory;
    import com.al.messages.CreateNewProjectMsg;
    import com.al.messages.OpenProjectMessage;
    import com.al.model.Activity;
    import com.al.model.Project;
    import com.al.model.ProjectModel;

    import flash.events.Event;
    import flash.utils.Dictionary;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;

    import spark.core.IContentLoader;

    public class ProjectViewPM
    {
        public static const ID_PROPERTY: String = "id";
        public static const TITLE_PROPERTY: String = "title";
        public static const SERIES_FIELD: String = "projectId";

        [Inject]
        [Bindable]
        public var jobModel: ProjectModel;

        [Inject]
        [Bindable]
        public var contentCache: IContentLoader;

        [MessageDispatcher]
        public var dispatcher: Function;

        [Bindable]
        public var rowHeight: Number = 24;

        [Bindable]
        public var showDetails: Boolean;

        [Bindable]
        public var seriesList: ArrayList = new ArrayList();

        [Bindable]
        public var timelines: ArrayList = new ArrayList();

        [Bindable]
        public var titleField: String;

        [Bindable]
        public var axes: Array;

        [Bindable]
        public var radialData: ArrayCollection;

        [Init]
        public function init(): void{

            titleField = TITLE_PROPERTY;
            selectedActivityIndex = -1;
            selectedProjectIndex = -1;

            generateTimelines();
            //add Listeners to project activities
            //if they are edited elsewhere.. we need to redraw the timeline
            for each(var project: Project in jobModel.projectList.source)
                project.activities.addEventListener(CollectionEvent.COLLECTION_CHANGE, onActivitiesChanged);
        }

        //-----------------------------------------------------------------
        //
        //              Properties
        //
        //-----------------------------------------------------------------

        //-----------------------------------------------------------------
        // selectedProjectIndex
        //-----------------------------------------------------------------
        private var _selectedProjectIndex: int;
        public function set selectedProjectIndex(value: int): void{

            if(value != _selectedProjectIndex){

                _selectedProjectIndex = value;
                if(_selectedProjectIndex > 0){

                    if(selectedProject.activities.length > 0) {
                        // if there is atleast one activity, get the first activity of the project
                        var firstActivity:Activity = selectedProject.activities.getItemAt(0) as Activity;
                    }
                    selectedActivityIndex = jobModel.allActivities.getItemIndex(firstActivity);
                }else{
                    selectedActivityIndex = _selectedProjectIndex;
                }

                updateRadarChart();
                dispatchEvent(new Event('indexChanged'));
            }
        }

        [Bindable('indexChanged')]
        public function get selectedProjectIndex(): int{
            return _selectedProjectIndex;
        }

        //-----------------------------------------------------------------
        // selectedActivityIndex
        //-----------------------------------------------------------------
        private var _selectedActivityIndex: int;
        public function set selectedActivityIndex(value: int): void{

            if(value != _selectedActivityIndex){

                _selectedActivityIndex = value;
                onTimelineIndexChange(_selectedActivityIndex);
                dispatchEvent(new Event('timelineIndexChanged'));
            }
        }

        [Bindable('timelineIndexChanged')]
        public function get selectedActivityIndex(): int{
            return _selectedActivityIndex;
        }

        //-----------------------------------------------------------------
        // selectedProject
        //-----------------------------------------------------------------
        [Bindable('indexChanged')]
        public function get selectedProject(): Project{

            return selectedProjectIndex > -1 ?
                    jobModel.projectList.getItemAt(selectedProjectIndex) as Project : null;
        }

        //-----------------------------------------------------------------
        //
        //              Public methods
        //
        //-----------------------------------------------------------------
        [MessageHandler]
        public function handleNewProjectCreation(msg: CreateNewProjectMsg): void{

            createNewProject();
        }

        public function updateRadarChart(): void{

            if(selectedProject) {
                generateAxes(selectedProject.activities.source);
                generateChartDataProvider(selectedProject.activities.source);
            }
        }

        public function createNewProject(): void{

            var project: Project = ProjectFactory.createNewProject();
            project.activities.addEventListener(CollectionEvent.COLLECTION_CHANGE, onActivitiesChanged);

            jobModel.projectList.addItem(project);
            generateTimelines();
            generateUniqueIndexMapping();
            selectedProjectIndex = jobModel.projectList.length - 1;

            dispatcher(new OpenProjectMessage(project));
        }

        public function openSelectedProject(): void{

            dispatcher(new OpenProjectMessage(selectedProject));
        }

        public function toggleTitleField(): void{

            titleField = (titleField == ID_PROPERTY) ? TITLE_PROPERTY : ID_PROPERTY;
            dispatchEvent(new Event('titleFieldChanged'));
        }

        [Bindable('titleFieldChanged')]
        public function getTitle(proj: Project): String{

            return proj[titleField];
        }

        public function toggleDetailsView(): void{

            showDetails = !showDetails;
        }

        public function showDetailsView(): void{

            showDetails = (selectedProject != null);
        }

        public function onTimelineIndexChange(index: int): void{

            if(index <= 0 || jobModel.projectList.length == jobModel.allActivities.length){

                selectedProjectIndex = index;
            }
            else{

                var selectedActivity: Activity = jobModel.allActivities.getItemAt(index) as Activity;
                if(selectedActivity){

                    selectedProjectIndex = findUniqueIndex(selectedActivity.projectId);
                }
            }
        }

        public function goToPrevProject(): void{

            if(selectedProjectIndex > 0){
                --selectedProjectIndex;
            }

        }

        public function goToNextProject(): void{

            if(selectedProjectIndex < jobModel.projectList.length-1){
                ++selectedProjectIndex;
            }

        }

        //-----------------------------------------------------------------
        //
        //              Private methods
        //
        //-----------------------------------------------------------------
        private function generateAxes(activities: Array):void {

            var array:Array = [];

            for each(var activity:Activity in activities) {

                if (activity.title) {

                    var axis:RadarAxis = new RadarAxis();
                    axis.categoryField = activity.id;
                    axis.displayName = activity.title + " (" + activity.unit + ")";
                    axis.min = 0;
                    axis.max = Math.max(Number(activity.totalActual), Number(activity.totalPlanned));
                    axis.max *= 1.16;
                    array.push(axis);
                }
            }
            // Chart freezes if there is less than two axis
            axes = array.length > 1 ? array : null;
        }

        private function generateChartDataProvider(activities: Array):void {

            var plannedData:Object = new Object();
            plannedData.name = "planned";
            var actualData:Object = new Object();
            actualData.name = "actual";

            for each(var activity:Activity in activities) {

                //if(activity.totalPlanned)
                    plannedData[activity.id] = activity.totalPlanned;
                //if(activity.totalActual)
                    actualData[activity.id] = activity.totalActual;
            }

            radialData = new ArrayCollection([plannedData, actualData]);
        }

        private function onActivitiesChanged(event: CollectionEvent): void{

            if(event.kind == CollectionEventKind.ADD){

                for each(var activity: Activity in event.items)
                    jobModel.allActivities.addItem(activity);
            }
            else if(event.kind == CollectionEventKind.REMOVE){

                for each(var activity: Activity in event.items)
                    jobModel.allActivities.removeItem(activity);
            }
            else if(event.kind == CollectionEventKind.UPDATE){

                // This forces the timeline to be rebuilt / redrawn
                jobModel.allActivities.dispatchEvent(new CollectionEvent(CollectionEvent.COLLECTION_CHANGE));
            }
        }

        protected function generateTimelines(): void{

            var seriesArray: Array = [];
            for( var i: int=0; i<jobModel.projectList.length; ++i){

                var job: Project = jobModel.projectList.getItemAt(i) as Project;
                var timeline: TimelineSeries = new TimelineSeries();
                timeline.hideLabel = true;
                timeline.fieldValue = job.id;

                seriesArray.push(timeline);
            }
            seriesList = new ArrayList(seriesArray);
        }

        private var uniqueIndexMap: Dictionary;

        private function findUniqueIndex(projId: String): int{

            var index: int = -1;
            if(!uniqueIndexMap)
                generateUniqueIndexMapping();
            if(uniqueIndexMap[projId] != null)
                index = uniqueIndexMap[projId];

            return index;
        }

        /**
         * Builds a mapping dictionary which is used to find
         * index of job item on the far left when user selects
         * an item in the timeline.
         */
        private function generateUniqueIndexMapping(): void{

            uniqueIndexMap = new Dictionary(true);

            for(var i: int =0; i<jobModel.projectList.length; ++i){

                var job: Project = jobModel.projectList[i] as Project;
                uniqueIndexMap[job.id] = i;
            }
        }

    }
}
