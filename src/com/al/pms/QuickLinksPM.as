/**
 * Created by lram on 16/02/2015.
 */
package com.al.pms
{

    import com.al.messages.RefreshRefDataMsg;
    import com.al.model.CalendarModel;
    import com.al.model.LinksModel;
    import com.al.refData.CalendarRefData;
    import com.al.refData.LinksRefData;

    import mx.collections.ArrayCollection;

    public class QuickLinksPM
    {
        [Inject]
        [Bindable]
        public var model: LinksModel;

        private var refData: LinksRefData;

        [Init]
        public function init(): void {

            refData = new LinksRefData();
            model.links.source = refData.links;
        }

        [MessageHandler]
        public function refreshRefData(msg: RefreshRefDataMsg): void{

            refData.readFromXML();
            model.links.source = refData.links;
        }

    }
}
