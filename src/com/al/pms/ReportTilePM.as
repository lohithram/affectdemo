/**
 * Created by lram on 16/02/2015.
 */
package com.al.pms {

    import ardisia.charts.radar.data.RadarAxis;

    import com.al.messages.PublishActivitiesMsg;

    import com.al.model.Activity;
    import com.al.model.ActivityModel;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;

    public class ReportTilePM {

        [Bindable]
        public var model: ActivityModel;

        [Bindable]
        public var axes: Array;

        [Bindable]
        public var radialData: ArrayCollection;

        [Bindable]
        public var historicalData: ArrayList;

        [Bindable]
        public var lastEditedDate: Date;

        [Bindable]
        public var displayedDate: Date;

        [Bindable]
        public var currentIndex: int;

        [Bindable]
        public var chartTitle: String;

        [Init]
        public function init():void {

            model = new ActivityModel();
            historicalData = new ArrayList();
            model.activities.addEventListener(CollectionEvent.COLLECTION_CHANGE, onChange);
        }

        [MessageHandler]
        public function publishActivities(msg: PublishActivitiesMsg):void {

            if(model.projectId != msg.projectId) return;

            model.activities = new ArrayList(msg.activities);

            if (model.activities.length > 1) {

                generateAxes(model.activities.source);
                generateDataProvider(model.activities.source);

                chartTitle = "";
                currentIndex = historicalData.length - 1;
            }
        }

        public function showHistoricalData(index: int):void {

            var activitiesData: PublishedData =
                    historicalData.getItemAt(currentIndex) as PublishedData;
            activitiesData.title = chartTitle;

            if (index > -1 && index < historicalData.length) {

                activitiesData = historicalData.getItemAt(index) as PublishedData;
                chartTitle = activitiesData.title;
                radialData = activitiesData.chartData;
                displayedDate = activitiesData.timestamp;

                generateAxes(activitiesData.activities);
                currentIndex = index;
            }
        }

        public function getEditedDate(index: int): Date{

            if (index > -1 && index < historicalData.length) {

                var activitiesData:PublishedData =
                        historicalData.getItemAt(index) as PublishedData;
                return activitiesData.timestamp;
            }

            return null;
        }

        private function onChange(event: CollectionEvent):void {

            if (event.kind == CollectionEventKind.UPDATE ||
                    event.kind == CollectionEventKind.REFRESH ||
                    event.kind == CollectionEventKind.RESET) {

                // Currently nothing to be done.
                // Left this event handler, incase if we need to do real time Activity Label and unit updates
                // on the chart
            }
        }

        private function generateAxes(activities: Array):void {

            var array:Array = [];

            for each(var activity:Activity in activities) {

                if (activity.title) {

                    var axis:RadarAxis = new RadarAxis();
                    axis.categoryField = activity.id;
                    axis.displayName = activity.title + " (" + activity.unit + ")";
                    axis.min = 0;
                    axis.max = Math.max(Number(activity.totalActual), Number(activity.totalPlanned));
                    axis.max *= 1.16;
                    array.push(axis);
                }
            }
            axes = array;
        }

        private function generateDataProvider(activities: Array):void {

            var plannedData:Object = new Object();
            plannedData.name = "planned";
            var actualData:Object = new Object();
            actualData.name = "actual";

            for each(var activity:Activity in activities) {

                plannedData[activity.id] = activity.totalPlanned;
                actualData[activity.id] = activity.totalActual;
            }

            radialData = new ArrayCollection([plannedData, actualData]);
            displayedDate = lastEditedDate = new Date();

            var activitiesData: PublishedData = new PublishedData();
            activitiesData.chartData = radialData;
            activitiesData.timestamp = lastEditedDate;
            activitiesData.activities = activities.slice();
            historicalData.addItem(activitiesData);
        }
    }
}

class PublishedData{

    public var chartData: *;

    public var timestamp: Date;

    public var activities: Array;

    public var title: String;
}
