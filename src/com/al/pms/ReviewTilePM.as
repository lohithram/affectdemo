package com.al.pms
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.DragSourceFormats;
    import com.al.model.Activity;
    import com.al.model.Cell;
    import com.al.model.Task;
    import com.al.model.UserCell;
    import com.al.util.CellUtil;

    import flash.events.EventDispatcher;

    import mx.collections.ArrayList;
    import mx.core.DragSource;
    import mx.events.DragEvent;

    /**
	 * @author lram
	 * 
	 */
	public class ReviewTilePM extends EventDispatcher {

        [Bindable]
        public var assetsList: ArrayList = new ArrayList();

        [Bindable]
        public var reviewersList: ArrayList = new ArrayList();

        [Bindable]
        public var activitiesList: ArrayList = new ArrayList();


        public function onDragDrop(event: DragEvent): void{

            var droppedItem: Object;
            var dragSource: DragSource = event.dragSource;
            if(dragSource.hasFormat(DragSourceFormats.USER_CELL))
                droppedItem = dragSource.dataForFormat(DragSourceFormats.USER_CELL) as Cell;

            else if(dragSource.hasFormat(DragSourceFormats.TASK_CELL))
                droppedItem = dragSource.dataForFormat(DragSourceFormats.TASK_CELL) as Cell;

            else if(dragSource.hasFormat(DragSourceFormats.ASSET_CELL))
                droppedItem = dragSource.dataForFormat(DragSourceFormats.ASSET_CELL);

            else if(dragSource.hasFormat(DragSourceFormats.TILE_DESCRIPTOR)) {

                droppedItem = dragSource.dataForFormat(DragSourceFormats.TILE_DESCRIPTOR);
                droppedItem = CellUtil.createCellFromTileDescriptor( droppedItem as TileContainerDescriptor );
            }

            if(droppedItem is UserCell) {

                reviewersList.addItem(droppedItem);
            }
            else if(droppedItem is Task){

                addTask(droppedItem as Task);
            }
            else{

                assetsList.addItem(droppedItem);
            }
        }

        private function addTask(task: Task): void{

            var activity: Activity;
            for each(var existingActivity: Activity in activitiesList.source){

                if(existingActivity.title == task.activity.title){

                    activity = existingActivity;
                    break;
                }
            }
            if(!activity) {
                activity = task.activity.clone();
                activitiesList.addItem(activity);
            }

            activity.tasks.addItem(task);
        }
    }
}