/**
 * Created by synesis on 23/04/2015.
 */
package com.al.pms
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.messages.ClipboardMessage;
    import com.al.messages.TileFocusedMessage;
    import com.al.model.DrawingModel;
    import com.al.model.TileContainerModel;
    import com.al.model.TileDataModel;
    import com.al.model.TileMenuItem;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.net.FileReference;
    import flash.net.URLRequest;

    import mx.binding.utils.BindingUtils;

    import mx.collections.ArrayList;
    import mx.collections.IList;

    import themes.affect.utils.IconUtils;

    public class TileContainerPM extends EventDispatcher
    {
        [Inject]
        [Bindable]
        public var drawingModel: DrawingModel;

        [Inject]
        [Bindable]
        public var tileDataModel: TileDataModel;

        [Bindable]
        public var tileContainerModel: TileContainerModel;

        [Bindable]
        public var tileDescriptor: TileContainerDescriptor;

        [Bindable]
        public var menuItems: IList;

        [MessageDispatcher]
        public var dispatcher: Function;

        [Init]
        public function init(): void {

            BindingUtils.bindSetter(onTileContainerModelChange, this, 'tileContainerModel');
        }

        //--------------------------------------------------------------------------
        //
        //  Parsley Message Handling
        //
        //--------------------------------------------------------------------------
        //Todo: Add parsley Message tag
        public function onAddData(msg: Object): void{

            // Left as a place holder for tile contents to hook up data
        }

        // --------------------------------------------------------------------------
        //
        //  Public methods
        //
        //--------------------------------------------------------------------------
        public function setTileContainerModel(value: TileContainerModel): void{

            tileContainerModel = value;
            tileContainerModel.tileDataModel = tileDataModel;

        }

        public function onTileFocused(): void{

            dispatcher(new TileFocusedMessage(tileContainerModel));
        }

        public function handleMenuSelection(menuCode: String): void {

            switch (menuCode){

                case IconUtils.Collaboration:
                    toggleCollaboration();
                    break;
                case IconUtils.Download:
                    var file: FileReference = new FileReference();
                    file.download(new URLRequest("http://www.affectivelogic.com"), "Content.jpg");
                    break;
                case IconUtils.Close:
                    dispatchEvent(new Event("removeTile"));
                    break;
                case IconUtils.ViewExpand:
                    dispatchEvent(new Event("toggleFullScreen"));
                    break;
                case IconUtils.Collections:
                    dispatcher(new ClipboardMessage(tileDescriptor));
                    break;
            }

        }

        public function exitFullScreen(): void{

            //find the full screen menu item
            for each(var menuItem: TileMenuItem in menuItems.toArray()){

                if(menuItem.iconCode == IconUtils.ViewExpand){

                    menuItem.isToggled = false;
                    break;
                }
            }
        }

        public function toggleCollaboration(): void {

            tileContainerModel.isCollaborationEnabled = !tileContainerModel.isCollaborationEnabled;
        }

        private function onTileContainerModelChange(newValue: Object): void{

            menuItems = new ArrayList();

            if(tileContainerModel && tileContainerModel.canCollaborate)
                menuItems.addItem(createMenuItem(IconUtils.Collaboration, "Toggle collaboration", true));
            menuItems.addItem(createMenuItem(IconUtils.Download, "Download Asset File"));
            menuItems.addItem(createMenuItem(IconUtils.Collections, "Add to Lightbox"));
            menuItems.addItem(createMenuItem(IconUtils.ViewExpand, "Presentation Mode", true));
            //menuItems.addItem(createMenuItem(AwesomeUtils.fa_trash, "Permanently delete Tile"));
            menuItems.addItem(createMenuItem(IconUtils.Close, "Remove from this combine"));
        }


        private function createMenuItem(
                iconCode: String, labelDisplay: String = "", canToggle: Boolean = false ): TileMenuItem {

            var item: TileMenuItem = new TileMenuItem();
            item.iconCode = iconCode;
            item.displayLabel = labelDisplay;
            item.canToggle = canToggle;

            return item;
        }
    }
}
