/**
 * Created by lram on 12/02/2015.
 */
package com.al.pms
{

    import com.al.containers.TileContainer;
    import com.al.enums.TileDataFormats;
    import com.al.model.DisplayAreaModel;
    import com.al.model.TileDataModel;

    import flash.utils.Dictionary;

    import mx.binding.utils.BindingUtils;

    public class UtilityPanelPM
    {

        [Bindable]
        [Inject]
        public var model: DisplayAreaModel;

        [Init]
        public function init(): void{

            BindingUtils.bindSetter(onCombineChange, model, "currentCombine");
        }

        //--------------------------------------------------------------------------
        //
        //  Parsley Message Handling
        //
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //
        //  Public Methods
        //
        //--------------------------------------------------------------------------


        //--------------------------------------------------------------------------
        //
        //  Private
        //
        //--------------------------------------------------------------------------
        private var tileDataFormats: Array = [TileDataFormats.ANNOTATIONS];

        // QUICK AND DIRTY: Shortcut employed to collect all data from the tiles
        private function onCombineChange(newValue: Object): void{

            if(!model.currentCombine) return;

            var allTilesData: Dictionary = new Dictionary();
            for( var i: int = 0; i<model.currentCombine.tiles.length; ++i){

                var tile: TileDataModel = model.currentCombine.tiles.getItemAt(i) as TileDataModel;
                for each( var dataFormat: String in tileDataFormats ) {

                    var tileData:Object = tile.getTileData(dataFormat);
                    if (!allTilesData[dataFormat])
                        allTilesData[dataFormat] = [];
                    allTilesData[dataFormat].push(tileData);
                }
            }

        }
    }
}
