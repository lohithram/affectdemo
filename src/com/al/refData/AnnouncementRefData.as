/**
 * Created by lram on 10/02/2015.
 */
package com.al.refData
{

    import com.al.model.Announcement;
    import com.al.model.MemberInfo;
    import com.al.util.DateUtils;
    import com.al.util.FileUtils;


    import mx.collections.ArrayList;
    import mx.utils.URLUtil;


    public class AnnouncementRefData
    {
        public var announcements: ArrayList = new ArrayList();


        public function AnnouncementRefData(){

            readFromXML();
            if(announcements.length == 0)
                loadDefaults();
        }

        public function readFromXML(): void{

            var announcementsArray: Array = [];
            var xml: XML = FileUtils.loadRefDataXML("AnnouncementsData.xml");

            if(!xml)
                return;

            for each( var announcementXML: XML in xml.announcement ){

                var announcement: Announcement = createAnnouncement(announcementXML.id,
                                        announcementXML.name,
                                        announcementXML.avatar,
                                        announcementXML.content,
                                        Number(announcementXML.startDate),
                                        Number(announcementXML.hour));

                announcementsArray.push(announcement);
            }
            announcements.source = announcementsArray;
        }


        private function loadDefaults(): void{

            announcements.source = [
                /*
                createAnnouncement("1", "Biel Baraut", "Biel.jpg", "Lorem Ipsum", -5, 1)
                */
            ];
        }

        private function createAnnouncement(id: String, name: String,
                                            avatar: String,
                                            content: String,
                                            dateOffset: Number,
                                            hour: Number ): Announcement {

            var avatarURL: String = resolveResource(avatar);
            var announcement: Announcement = new Announcement();
            var date: Date = DateUtils.addDays(new Date(), dateOffset);
            var member: MemberInfo = new MemberInfo(name, "", avatarURL);

            date.hours = hour;
            announcement.timestamp = date;
            announcement.content = content;
            announcement.member = member;

            return announcement;
        }


        private function resolveResource(resource: String): String {

            var isExternal: Boolean =  URLUtil.isHttpURL(resource) || URLUtil.isHttpsURL(resource);
            var resourceURL: String = isExternal ? resource : FileUtils.resolveDemoResource(resource);

            return resourceURL;
        }
    }
}
