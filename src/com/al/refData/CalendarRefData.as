/**
 * Created by lram on 16/02/2015.
 */
package com.al.refData
{

    import ardisia.scheduling.dataTypes.CalendarDefinitionData;
    import ardisia.scheduling.dataTypes.SchedulingData;

    import com.al.util.DateUtils;

    import com.al.util.FileUtils;

    public class CalendarRefData
    {

        public var calendarEvents: Array;

        private var workCalendar: CalendarDefinitionData;

        public function CalendarRefData() {

            workCalendar = new CalendarDefinitionData();
            workCalendar.title = "Work";
            workCalendar.color = 0x662C91;

            readFromXML();
            if(!calendarEvents || calendarEvents.length <= 0)
                generateCalendarData();
        }

        public function readFromXML(): void{

            var calendarEvents: Array = [];
            var xml: XML = FileUtils.loadRefDataXML("CalendarData.xml");

            if(!xml)
                return;

            var today: Date = new Date();

            for each( var eventXML: XML in xml.event ){

                var date: Number = Number(eventXML.date);

                var event: SchedulingData = new SchedulingData(eventXML.id);
                event.calendar = workCalendar;
                event.summary = eventXML.title;
                event.location = eventXML.location;
                event.description = eventXML.description;
                event.allDay = eventXML.allDay == 'true' ? true : false;
                if(event.allDay) {
                    event.dtStart = DateUtils.addDays(today, date);
                    event.dtEnd = DateUtils.addDays(today, date+1);
                }
                else {
                    // timed
                    var hour:int = Number(eventXML.hour);
                    var duration:int = Number(eventXML.duration);

                    event.dtStart = new Date(today.fullYear, today.month, today.date + date, hour);
                    event.dtEnd = new Date(today.fullYear, today.month, today.date + date, duration + hour);
                }

                calendarEvents.push(event);
            }

            this.calendarEvents = calendarEvents;
        }

        protected function generateCalendarData(): void {

            var today:Date = new Date();
            var items:Array = [];

            for (var i:int = 0; i < 60; i++)
            {
                // all day event
                var newItem:SchedulingData = generateCalendarEvent(true);

                var day:int = Math.random() * 77;
                newItem.dtStart = new Date(today.fullYear, today.month - 1,
                        today.date + day);
                newItem.dtEnd = new Date(today.fullYear, today.month - 1,
                        today.date + day + 1);
                newItem.allDay = true;
                items.push(newItem);

                // timed
                newItem = generateCalendarEvent(false);

                day = Math.random() * 90;
                var hour:int = 8 + Math.random() * 7;
                var duration:int = 1 + Math.random() * 5;
                newItem.dtStart = new Date(today.fullYear, today.month - 1,
                        today.date + day, hour);
                newItem.dtEnd = new Date(today.fullYear, today.month - 1,
                        today.date + day, duration + hour);
                newItem.allDay = false;
                items.push(newItem);
            }

            calendarEvents = items;
        }

        protected function generateCalendarEvent(allDay:Boolean = false):SchedulingData
        {
            var rand:int = int(Math.random() * 100);
            var schedulingData:SchedulingData = new SchedulingData();
            schedulingData.calendar = workCalendar;

            // allday
            if (allDay)
            {
                if (rand < 10)
                {
                    schedulingData.summary = "Corporate retreat";
                    schedulingData.location = "Woods";
                    schedulingData.description = "Falling exercises.";
                }
                else if (rand < 20)
                {
                    schedulingData.summary = "Conference";
                    schedulingData.location = "conference center";
                    schedulingData.description = "New techniques in our business.";
                }
                else if (rand < 30)
                {
                    schedulingData.summary = "Budget Report";
                    schedulingData.description = "Try to finish.";
                }
                else if (rand < 40)
                {
                    schedulingData.summary = "Sales Report";
                    schedulingData.description = "Try to finish.";
                }
                else if (rand < 50)
                {
                    schedulingData.summary = "Jane in Accounting - Birthday";
                    schedulingData.description = "Tell her happy birthday.";
                }
                else if (rand < 60)
                {
                    schedulingData.summary = "Taco Day In Cafeteria";
                    schedulingData.location = "cafeteria";
                    schedulingData.description = "Skip bagged lunch.";
                }
                else if (rand < 70)
                {
                    schedulingData.summary = "Fill Out Evaluation Forms";
                }
                else if (rand < 80)
                {
                    schedulingData.summary = "Pickup Paycheck from HR";
                }
                else if (rand < 90)
                {
                    schedulingData.summary = "Train Intern";
                    schedulingData.location = "third floor";
                    schedulingData.description = "Find time to train in the new intern.";
                }
                else if (rand <= 100)
                {
                    schedulingData.summary = "Update Dashboard";
                }
            }
            // timed
            else
            {
                if (rand < 10)
                {
                    schedulingData.summary = "Meeting";
                    schedulingData.location = "Conference Room A";
                    schedulingData.description = "Bill will be presenting q1 financials.";
                }
                else if (rand < 20)
                {
                    schedulingData.summary = "Downtown Meeting";
                    schedulingData.location = "Downtown";
                }
                else if (rand < 30)
                {
                    schedulingData.summary = "Warehouse Meeting";
                    schedulingData.location = "Warehouse";
                }
                else if (rand < 40)
                {
                    schedulingData.summary = "Call sales rep";
                    schedulingData.location = "Conference room";
                }
                else if (rand < 50)
                {
                    schedulingData.summary = "Pick up consultant";
                    schedulingData.location = "Airport";
                    schedulingData.description = "New consultant in town.";
                }
                else if (rand < 60)
                {
                    schedulingData.summary = "Continuing Education Class";
                    schedulingData.location = "Rm. 335";
                    schedulingData.description = "CLE #101";
                }
                else if (rand < 70)
                {
                    schedulingData.summary = "Talk to HR";
                    schedulingData.description = "Want more money";
                }
                else if (rand < 80)
                {
                    schedulingData.summary = "Catch up on Emails";
                }
                else if (rand < 90)
                {
                    schedulingData.summary = "Visit Factory Floor";
                    schedulingData.location = "Factory Floor";
                    schedulingData.description = "Look into production glitches.";
                }
                else if (rand <= 100)
                {
                    schedulingData.summary = "Distribution Center";
                    schedulingData.location = "Check in.";
                }
            }

            return schedulingData;
        }
    }
}
