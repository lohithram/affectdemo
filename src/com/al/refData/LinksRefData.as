/**
 * Created by lram on 16/02/2015.
 */
package com.al.refData
{

    import ardisia.scheduling.dataTypes.CalendarDefinitionData;
    import ardisia.scheduling.dataTypes.SchedulingData;

    import com.al.model.Link;

    import com.al.model.LinkGroup;

    import com.al.util.DateUtils;

    import com.al.util.FileUtils;

    import mx.collections.ArrayList;

    public class LinksRefData
    {

        public var links: Array;

        public function LinksRefData() {

             readFromXML();
            if(!links || links.length <= 0)
                loadDefaults();
        }

        public function readFromXML(): void{

            var links: Array = [];
            var xml: XML = FileUtils.loadRefDataXML("QuickLinksData.xml");

            if(!xml)
                return;

            for each( var groupXML: XML in xml.group ){

                var linkGroup: LinkGroup = createLinkGroup(groupXML);
                links.push(linkGroup);
            }

            this.links = links;
        }

        private function createLinkGroup(groupXML: XML): LinkGroup{

            var linkGroup: LinkGroup = new LinkGroup(groupXML.@name);
            linkGroup.childrenGroups = new ArrayList();
            linkGroup.links = new ArrayList();

            for each( var linkXML: XML in groupXML.link ){

                var link: Link = new Link(linkXML.@name, linkXML.toString());
                linkGroup.links.addItem(link);
            }

            for each( var subGroupXML: XML in groupXML.group ){

                var subGroup: LinkGroup = createLinkGroup(subGroupXML);
                linkGroup.childrenGroups.addItem(subGroup);
            }

            return linkGroup;

        }

        private function loadDefaults(): void{

            links.source = [
                /*
                 createAnnouncement("1", "Biel Baraut", "Biel.jpg", "Lorem Ipsum", -5, 1)
                 */
            ];
        }

    }
}
