/**
 * Created by lram on 10/02/2015.
 */
package com.al.refData
{

    import com.al.enums.ActivityStatus;
    import com.al.model.Activity;
    import com.al.model.ImageInfo;
    import com.al.model.MemberInfo;
    import com.al.model.Project;
    import com.al.util.FileUtils;

    import flash.utils.Dictionary;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.utils.URLUtil;

    public class TimeLineRefData
    {


        // unique list of job ids
        public var projects: ArrayCollection = new ArrayCollection();

        public var allActivities: ArrayCollection = new ArrayCollection();

        public function TimeLineRefData(){

            readFromXML();
            if(allActivities.length == 0)
                loadDefaults();
        }

        public function readFromXML(): void{

            var activities: Array = [];
            var xml: XML = FileUtils.loadRefDataXML("ProjectsData.xml");

            if(!xml)
                return;

            for each( var activityXML: XML in xml.activity ){

                var activity: Activity = createActivity(activityXML.projectId,
                                        activityXML.title,
                                        Number(activityXML.startDate),
                                        Number(activityXML.endDate),
                                        activityXML.status,
                                        activityXML.campaign,
                                        activityXML.product,
                                        activityXML.deliverable,
                                        activityXML.brand,
                                        activityXML.unit,
                                        activityXML.totalPlanned,
                                        activityXML.totalActual
                                        );

                if(activityXML.preview != null){
                    activity.preview = [];
                    for each(var imageXML: XML in activityXML.preview.image) {

                        var imageURL: String = resolveResource(imageXML.toString());
                        var imageInfo: ImageInfo = new ImageInfo(imageURL);
                        activity.preview.push(imageInfo);
                    }
                }

                activity.logs = new ArrayList();
                if(activityXML.logs != null){
                    for each(var logXML: XML in activityXML.logs.log){
                        activity.logs.addItem(logXML.toString());
                    }
                }

                activity.members = new ArrayList();
                if(activityXML.members != null){
                    for each(var memberXML: XML in activityXML.members.member){

                        var avatarURL: String = resolveResource(memberXML.avatar);
                        activity.members.addItem(new MemberInfo(memberXML.name, memberXML.role, avatarURL));
                    }
                }
                activities.push(activity);
            }

            allActivities.source = activities;
            createProjectHierarchy();
        }

        private function resolveResource(resource: String): String {

            var isExternal: Boolean =  URLUtil.isHttpURL(resource) || URLUtil.isHttpsURL(resource);
            var resourceURL: String = isExternal ? resource : FileUtils.resolveDemoResource(resource);

            return resourceURL;
        }

        private function loadDefaults(): void{

            allActivities.source = [
                createActivity("174 15 A90475", "Lorem Ipsum", 0, 2, ActivityStatus.PROGRESS),
                createActivity("174 15 A90477", "Dolor sit amet", 2, 5, ActivityStatus.NEW),
                createActivity("C77 13 A90364", "Onsectetur adipiscing elit", 0, 2, ActivityStatus.PENDING),
                createActivity("174 15 A90479", "Lorem Ipsum", 3, 8, ActivityStatus.APPROVED),
                createActivity("174 15 A90479", "Lorem Ipsum2", 10, 12, ActivityStatus.NEW),
                createActivity("174 15 A90484", "Lorem Ipsum", -5, 1, ActivityStatus.REJECTED),
                createActivity("A29 05 A91259", "Lorem Ipsum", -8, 11, ActivityStatus.PROGRESS),
                createActivity("A29 05 A91265", "Lorem Ipsum", 0, 7, ActivityStatus.PROGRESS),
                createActivity("A29 05 A91267", "Lorem Ipsum", 2, 6, ActivityStatus.NEW),
                createActivity("A29 05 A91273", "Lorem Ipsum", -2, 1, ActivityStatus.PROGRESS),
                createActivity("C77 13 A90369", "Lorem Ipsum", -3, 4, ActivityStatus.APPROVED),
                createActivity("C77 13 A90372", "Lorem Ipsum", -4, 2, ActivityStatus.APPROVED),
                createActivity("C77 13 A90372", "Lorem Ipsum", 4, 7, ActivityStatus.PENDING),
                createActivity("C77 13 A90372", "Lorem Ipsum", 8, 9, ActivityStatus.NEW),
                createActivity("C77 13 A90374", "Lorem Ipsum", -5, 1, ActivityStatus.APPROVED),
                createActivity("C77 13 A90375", "Lorem Ipsum", -10, 2, ActivityStatus.PENDING)
            ];
            createProjectHierarchy();
        }

        private function createProjectHierarchy(): void{

            var projs: Array = [];
            var projectIdDictionary: Dictionary = new Dictionary(true);

            for each(var activity: Activity in allActivities){

                var project: Project = projectIdDictionary[activity.projectId] as Project;
                if(!project){

                    project = new Project(activity.projectId);
                    project.title = activity.title;
                    project.status = activity.status;
                    project.brand = activity.brand;
                    project.product = activity.product;
                    project.preview = activity.preview;
                    project.campaign = activity.campaign;
                    project.deliverable = activity.deliverable;
                    //project.firstActivity = activity;
                    project.activities = new ArrayList([activity]);

                    projs.push(project);
                    projectIdDictionary[activity.projectId] = project;
                }
                else{

                    project.activities.addItem(activity);
                }
            }

            projects.source = projs;
        }

        private function createActivity(id: String, title: String,
                                   startDateOffset: Number,
                                   endDateOffset: Number,
                                   status: String,
                                    campaign: String = "Test",
                                    product: String = "Test",
                                    deliverable: String = "Test",
                                    brand: String = "Test",
                                    unit: String = "",
                                    totalPlanned: String = "1000",
                                    totalActual: String = "100"): Activity{

            var job: Activity = new Activity(id);
            var endDate: Date = new Date();
            var startDate: Date = new Date();

            endDate.date += endDateOffset;
            startDate.date += startDateOffset;

            job.title = title;

            job.startDate = startDate;
            job.completionDate = endDate;
            job.status = status;

            job.campaign = campaign;
            job.product = product;
            job.deliverable = deliverable
            job.brand = brand;
            job.unit = unit;
            job.totalActual = totalActual;
            job.totalPlanned = totalPlanned;

            return job;
        }

    }
}
