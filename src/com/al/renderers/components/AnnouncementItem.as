package com.al.renderers.components
{
    import com.al.model.Announcement;


    import spark.components.Image;
    import spark.components.supportClasses.TextBase;

    import com.al.util.DateUtils;

    public class AnnouncementItem extends CacheContentItemRendererComponent
    {
        [SkinPart(required="false")]
        public var image:Image;

        [SkinPart(required="false")]
        public var title:TextBase;

        [SkinPart(required="false")]
        public var content:TextBase;

        [SkinPart(required="false")]
        public var dateField:TextBase;


        private function getFormattedDate(date: Date): String
        {
            return date ? DateUtils.format(date, "DD MMM YYYY - HH:NN"):"";
        }

        [Bindable("contentCacheChange")]
        public function updateImageCache():void
        {
            if(image)
            {
                image.contentLoader = contentCache;
            }
        }


        override protected function updateControls():void
        {
            var announcement:Announcement = data as Announcement;
            if(image)
            {
                image.source = announcement && announcement.member ? announcement.member.avatar : "";
                image.contentLoader = contentCache;
            }

            if(title)
            {
                title.text = announcement && announcement.member ? announcement.member.name : "";
            }

            if(content)
            {
                content.text = announcement ? announcement.content : "";
            }

            if(dateField)
            {
                dateField.text = announcement ? getFormattedDate(announcement.timestamp) : "";
            }
        }
    }
}
