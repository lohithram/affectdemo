/**
 * Created by abhisekpaul on 27/05/15.
 */
package com.al.renderers.components
{
    import com.al.components.AssetPreview;
    import com.al.model.Activity;
    import com.al.util.DateUtils;

    import spark.components.supportClasses.TextBase;

    public class ApprovalItem extends CacheContentItemRendererComponent
    {
        [SkinPart(required="false")]
        public var assetPreview:AssetPreview;

        [SkinPart(required="false")]
        public var title:TextBase;

        [SkinPart(required="false")]
        public var jobId:TextBase;

        [SkinPart(required="false")]
        public var designation:TextBase;

        [SkinPart(required="false")]
        public var fileField:TextBase;

        [SkinPart(required="false")]
        public var dueDate:TextBase;

        [Bindable]
        public var isLast:Boolean;

        private function getFormattedDate(date: Date): String {

            return DateUtils.format(date, "DD MMM YYYY");
        }

        [Bindable("contentCacheChange")]
        public function updateImageCache():void
        {
            if(assetPreview)
            {
                assetPreview.contentCache = contentCache;
            }
        }

        override protected function updateControls():void
        {
            var job:Activity = data as Activity;
            if(assetPreview)
            {
                assetPreview.previewData = job ? job.preview : null;
                assetPreview.contentCache = contentCache;
            }

            if(title)
            {
                title.text = job ? job.title : "";
            }

            if(jobId)
            {
                jobId.text = job ? job.projectId : "";

            }

            if(designation)
            {
                designation.text = "Moderator";
            }

            if(fileField)
            {
                fileField.text = "filename.pdf";
            }

            if(dueDate)
            {
                dueDate.text = job ? getFormattedDate(job.completionDate) : "";
            }
        }

    }
}
