/**
 * Created by abhisekpaul on 27/05/15.
 */
package com.al.renderers.components
{
    import flash.events.Event;

    import spark.core.IContentLoader;

    public class CacheContentItemRendererComponent extends ItemRendererComponentBase
    {
        private var _contentCache:IContentLoader;

        [Bindable("contentCacheChange")]
        public function get contentCache():IContentLoader
        {
            return _contentCache;
        }

        public function set contentCache(value:IContentLoader):void
        {
            if(value != _contentCache)
            {
                _contentCache = value;
                dispatchEvent(new Event("contentCacheChange"));

            }
        }
    }
}
