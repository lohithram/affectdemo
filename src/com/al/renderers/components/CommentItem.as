/**
 * Created by abhisekpaul on 28/05/15.
 */
package com.al.renderers.components
{
    import com.al.collaboration.annotation.model.Comment;
    import com.al.util.DateUtils;

    import spark.components.Image;
    import spark.components.supportClasses.TextBase;

    public class CommentItem extends CacheContentItemRendererComponent
    {
        [SkinPart(required="false")]
        public var image:Image;

        [SkinPart(required="false")]
        public var userName:TextBase;

        [SkinPart(required="false")]
        public var timestamp:TextBase;

        [SkinPart(required="false")]
        public var description:TextBase;

        [Bindable]
        [Embed(source="/images/avatar.png")]
        private var iconClass: Class;

        private function getFormattedDate(date: Date): String
        {

            return DateUtils.format(date, "DD MMM YYYY - HH:NN");
        }

        [Bindable("contentCacheChange")]
        public function updateImageCache():void
        {
            if(image)
            {
                image.contentLoader = contentCache;
            }
        }

        override protected function updateControls():void
        {
            var comment:Comment = data as Comment;
            if(image)
            {
                image.source = iconClass;
                image.contentLoader = contentCache;
            }

            if(userName)
            {
                userName.text = comment ? comment.userName : "";
            }

            if(timestamp)
            {
                timestamp.text = comment ? getFormattedDate(comment.timestamp) : "";
            }

            if(description)
            {
                description.text = comment ? comment.commentString : "";
            }
        }
    }
}
