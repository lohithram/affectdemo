/**
 * Created by abhisekpaul on 28/05/15.
 */
package com.al.renderers.components
{

    import com.al.components.EditableLabel;
    import com.al.model.FormField;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import spark.components.supportClasses.ButtonBase;
    import spark.components.supportClasses.SkinnableTextBase;
    import spark.components.supportClasses.TextBase;
    import spark.events.TextOperationEvent;

    [Event(name='fieldUpdate', type='flash.events.Event')]
    [Event(name='removeField', type='flash.events.Event')]
    public class FormFieldItem extends ItemRendererComponentBase
    {
        [SkinPart(required="false")]
        public var delIcon:ButtonBase;

        [SkinPart(required="false")]
        public var editableLabel:EditableLabel;

        [SkinPart(required="false")]
        public var txtValue:SkinnableTextBase;

        [SkinPart(required="false")]
        public var infoLabel:TextBase;

        protected function removeThisField(event:Event):void
        {
            dispatchEvent(new Event("removeField"));
        }

        protected function onTxtValueChange(event:TextOperationEvent):void
        {
            var formField:FormField = data as FormField;
            if(formField)
            {
                formField.value = txtValue.text;
            }

            dispatchEvent(new Event("fieldUpdate"));
        }

        protected function onEditableLabelChange(event:TextOperationEvent):void
        {
            var formField:FormField = data as FormField;
            if(formField)
            {
                formField.name = editableLabel.text;
            }
            dispatchEvent(new Event("fieldUpdate"));
        }

        override protected function updateControls():void
        {
            var formField:FormField = data as FormField;

            if(editableLabel)
            {
                editableLabel.text = formField ? formField.name : "";
            }

            if(txtValue)
            {
                txtValue.text = formField ? formField.value : "";
            }

            if(infoLabel)
            {
                infoLabel.text = "";
            }
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName,instance);

            if(delIcon)
            {
                delIcon.addEventListener(MouseEvent.CLICK, removeThisField);
            }

            if(editableLabel)
            {
                editableLabel.addEventListener(TextOperationEvent.CHANGE, onEditableLabelChange);
            }

            if(txtValue)
            {
                txtValue.addEventListener(TextOperationEvent.CHANGE, onTxtValueChange);
            }
        }



        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName,instance);

            if(delIcon)
            {
                delIcon.removeEventListener(MouseEvent.CLICK, removeThisField);
            }

            if(editableLabel)
            {
                editableLabel.removeEventListener(TextOperationEvent.CHANGE, onEditableLabelChange);
            }

            if(txtValue)
            {
                txtValue.removeEventListener(TextOperationEvent.CHANGE, onTxtValueChange);
            }
        }
    }
}
