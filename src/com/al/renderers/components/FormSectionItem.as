/**
 * Created by abhisekpaul on 28/05/15.
 */
package com.al.renderers.components
{

    import com.al.collaboration.annotation.model.Annotation;
    import com.al.collaboration.annotation.model.Comment;
    import com.al.components.TaskList;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.dragging.DropStrategy;
    import com.al.enums.DragSourceFormats;
    import com.al.model.Activity;
    import com.al.model.Cell;
    import com.al.model.FormSection;
    import com.al.model.Task;
    import com.al.util.CellUtil;
    import com.al.util.EventUtils;

    import flash.desktop.Clipboard;
    import flash.desktop.ClipboardFormats;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.events.NativeDragEvent;
    import flash.filesystem.File;

    import mx.events.DragEvent;
    import mx.managers.DragManager;
    import mx.utils.UIDUtil;

    import spark.components.DataGrid;
    import spark.components.Group;
    import spark.components.List;
    import spark.components.supportClasses.ButtonBase;

    [Event(name='addField', type='flash.events.Event')]
    [Event(name='removeSection', type='flash.events.Event')]
    [Event(name='fieldOrderChange', type='flash.events.Event')]
    public class FormSectionItem extends ItemRendererComponentBase
    {
        [SkinPart(required="false")]
        public var fieldsList: List;

        [SkinPart(required="false")]
        public var tasksList: TaskList;

        [SkinPart(required="false")]
        public var assetsList: List;

        [SkinPart(required="false")]
        public var peopleList: List;

        [SkinPart(required="false")]
        public var instructionsList: List;

        [SkinPart(required="false")]
        public var dropArea: Group;

        [SkinPart(required="false")]
        public var addBtn: ButtonBase;

        [SkinPart(required="false")]
        public var deleteSectionBtn: ButtonBase;

        protected var formSectionId: String;

        private var acceptedFormats: Array = [DragSourceFormats.ASSET_CELL_ITEM,
                                                DragSourceFormats.USER_CELL,
                                                DragSourceFormats.TASK_CELL,
                                                DragSourceFormats.ASSET_CELL,
                                                DragSourceFormats.COMMENT_CELL,
                                                ClipboardFormats.URL_FORMAT,
                                                ClipboardFormats.FILE_LIST_FORMAT,
                                                DragSourceFormats.TILE_DESCRIPTOR];

        public function FormSectionItem(){

            formSectionId = UIDUtil.createUID().substr(0, 12);
        }

        protected function get section(): FormSection{

            return data as FormSection;
        }

        protected function onAddBtnClick(event:MouseEvent):void
        {
            dispatchEvent(new Event('addField'));
        }

        protected function onDeleteBtnClick(event:MouseEvent):void {

            dispatchEvent(new Event('removeSection'));
        }

        /**
         * TODO: Drag rejects should be based on DragFomrat.
         *       Add formats to drag source and use it instead.
         * @param event
         */
        protected function onDragEnter(event:DragEvent):void {

            if (event.dragInitiator == owner) {
                event.preventDefault();
            }
            else if(event.dragInitiator is DataGrid){

                //Not to perform default operation for comments
                event.preventDefault();
            }
        }

        protected function onRelatedItemsDragEnter(event: NativeDragEvent):void {

            if(event.currentTarget != event.target) return;

            var canDrop: Boolean;
            var clipboard: Clipboard = event.clipboard;
            if( DropStrategy.canAcceptClipboard(clipboard, acceptedFormats) ){

                canDrop = true;
            }
            if(!canDrop && clipboard.hasFormat(DragSourceFormats.ITMES_BY_INDEX)){

                var item: Vector.<Object> =
                        clipboard.getData(DragSourceFormats.ITMES_BY_INDEX) as Vector.<Object>;
                canDrop = item[0] is Comment;
            }

            if(canDrop){

                DragManager.acceptDragDrop(dropArea);
                DragManager.showFeedback(DragManager.LINK);

                dropArea.mouseChildren = false;
                dropArea.addEventListener(NativeDragEvent.NATIVE_DRAG_OVER, onRelatedItemsDragOver);
                dropArea.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onRelatedItemsDragDrop);
                dropArea.addEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onRelatedItemsDragExit);
            }
        }

        protected function onRelatedItemsDragOver(event: NativeDragEvent):void {

            DragManager.showFeedback(DragManager.LINK);
        }

        protected function onRelatedItemsDragExit(event: NativeDragEvent):void {

            if(event.currentTarget != event.target) return;

            cleanUp();
        }

        protected function cleanUp(): void{

            dropArea.mouseChildren = true;
            dropArea.removeEventListener(NativeDragEvent.NATIVE_DRAG_EXIT, onRelatedItemsDragExit);
            dropArea.removeEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onRelatedItemsDragDrop);
            dropArea.removeEventListener(NativeDragEvent.NATIVE_DRAG_OVER, onRelatedItemsDragOver);
        }

        protected function onRelatedItemsDragDrop(event: NativeDragEvent):void {

            var comment: Comment;
            var droppedItem: Object;
            var clipboard: Clipboard = event.clipboard;
            if(clipboard.hasFormat(DragSourceFormats.USER_CELL)) {

                droppedItem = clipboard.getData(DragSourceFormats.USER_CELL) as Cell;
                section.people.addItem(droppedItem);
            }
            else if(clipboard.hasFormat(DragSourceFormats.TASK_CELL)) {

                droppedItem = clipboard.getData(DragSourceFormats.TASK_CELL) as Cell;
                addTask(droppedItem as Task);
            }
            else if(clipboard.hasFormat(DragSourceFormats.COMMENT_CELL)) {

                comment = clipboard.getData(DragSourceFormats.COMMENT_CELL) as Comment;

                var annotation: Annotation = getNewAnnotation(comment);
                section.instructions.addItem(annotation);
                addRelatedAssetFor(annotation);
            }
            else if(clipboard.hasFormat(DragSourceFormats.ASSET_CELL)) {

                droppedItem = clipboard.getData(DragSourceFormats.ASSET_CELL);
                section.relatedAssets.addItem(droppedItem);
            }
            else if(clipboard.hasFormat(DragSourceFormats.TILE_DESCRIPTOR)) {

                droppedItem = clipboard.getData(DragSourceFormats.TILE_DESCRIPTOR);
                droppedItem = CellUtil.createCellFromTileDescriptor( droppedItem as TileContainerDescriptor );
                section.relatedAssets.addItem(droppedItem);
            }
            else if(clipboard.hasFormat(DragSourceFormats.ITMES_BY_INDEX)){

                var item: Vector.<Object> =
                        clipboard.getData(DragSourceFormats.ITMES_BY_INDEX) as Vector.<Object>;
                comment = item[0] as Comment;
                if(comment) {
                    var annotation:Annotation = getNewAnnotation(comment);
                    section.instructions.addItem(annotation);
                    addRelatedAssetFor(annotation);
                }
            }else if(clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)){

                var fileList: Array = clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array;
                for each(var file: File in fileList)
                    addAsset(file.url);
            }
            else if(clipboard.hasFormat(ClipboardFormats.URL_FORMAT)){

                var fileUrl: String = clipboard.getData(ClipboardFormats.URL_FORMAT) as String;
                addAsset(fileUrl);
            }

            cleanUp();
        }

        private function addTask(task: Task): void{

            var activity: Activity;
            for each(var existingActivity: Activity in section.activities.source){

                if(existingActivity.title == task.activity.title){

                    activity = existingActivity;
                    break;
                }
            }
            if(!activity) {
                activity = task.activity.clone();
                section.activities.addItem(activity);
            }

            activity.tasks.addItem(task);
        }

        /**
         * 1. Create a new annotation for a new collaboration
         * 2. Create a new tileDescriptor
         *
         * @param comment
         */
        private function addRelatedAssetFor(annotation: Annotation): void{

            var assetCell: Cell;
            for each(var relatedAsset: Cell in section.relatedAssets.source){

                if(relatedAsset.filePath == annotation.tileDescriptor.currentContent.contentUrl){

                    assetCell = relatedAsset;
                    break;
                }
            }
            if(!assetCell) {

                assetCell = CellUtil.createCellFromTileDescriptor(annotation.tileDescriptor);
                assetCell.id = UIDUtil.createUID().substr(0, 12);
                section.relatedAssets.addItem(assetCell);
            }
            if(!assetCell.pendingAnnotations)
                assetCell.pendingAnnotations = new Array();
            assetCell.pendingAnnotations.push(annotation);
        }

        private function addAsset(fileURL: String): void{

            var cell: Cell = CellUtil.createCellFromUrl(fileURL);
            section.relatedAssets.addItem( cell );
            return;
        }

        private function getNewAnnotation(comment: Comment): Annotation{

            var annotation: Annotation = new Annotation();
            annotation.reducedX = comment.annotation.reducedX;
            annotation.reducedY = comment.annotation.reducedY;
            annotation.contentUnitId = comment.annotation.contentUnitId;
            annotation.conversationId = section.instructions.length+1;
            annotation.annotationText = comment.commentString;
            annotation.tileDescriptor = comment.annotation.tileDescriptor;
            return annotation;
        }

        /**
         * TODO: All drag feedbacks should be through model rather
         * than bubbling events
         * @param event
         */
        protected function onDragDrop(event:DragEvent):void {
x
            event.dragInitiator.addEventListener(DragEvent.DRAG_COMPLETE, onDragComplete);
        }

        protected function onDragComplete(event:DragEvent):void {

            EventUtils.removeListener(event, onDragComplete);
            dispatchEvent(new Event('fieldOrderChange'));
        }


        override protected function updateControls():void
        {
            if(fieldsList) {
                fieldsList.dataProvider = section ? section.fields : null;
            }
            if(assetsList) {
                assetsList.dataProvider = section ? section.relatedAssets : null;
            }
            if(peopleList) {
                peopleList.dataProvider = section ? section.people : null;
            }
            if(tasksList) {
                tasksList.activities = section ? section.activities : null;
            }
            if(instructionsList) {
                instructionsList.dataProvider = section ? section.instructions : null;
            }

        }

        override protected function childrenCreated(): void{

            super.childrenCreated();
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName,instance);

            if(instance == addBtn)
            {
                addBtn.addEventListener(MouseEvent.CLICK, onAddBtnClick);
            }
            else if(instance == deleteSectionBtn)
            {
                deleteSectionBtn.addEventListener(MouseEvent.CLICK, onDeleteBtnClick);
            }
            else if(instance == fieldsList)
            {
                fieldsList.addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
            }
            else if(instance == dropArea)
            {
                dropArea.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onRelatedItemsDragEnter);
            }
        }

        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName,instance);
            if(instance == addBtn)
            {
                addBtn.removeEventListener(MouseEvent.CLICK, onAddBtnClick);
            }
            else if(instance == deleteSectionBtn)
            {
                deleteSectionBtn.removeEventListener(MouseEvent.CLICK, onDeleteBtnClick);
            }
            else if(instance == fieldsList)
            {
                fieldsList.removeEventListener(DragEvent.DRAG_ENTER,onDragEnter);
            }
            else if(instance == dropArea)
            {
                dropArea.removeEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onRelatedItemsDragEnter);
            }
        }
    }
}
