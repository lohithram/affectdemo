package com.al.renderers.components
{
    import flash.events.Event;

    import spark.components.supportClasses.SkinnableComponent;

/*
    [SkinState("normal")]
    [SkinState("hovered")]
    [SkinState("selected")]
    [SkinState("disabled")]
    [SkinState("dragging")]
    */

    public class ItemRendererComponentBase extends SkinnableComponent
    {
        private var dataChanged:Boolean = false;

        private var _data:Object;

        [Bindable("dataChange")]
        public function get data():Object
        {
            return _data;
        }

        public function set data(value:Object):void
        {
            if(value != _data)
            {
                _data = value;
                dataChanged = true;
                invalidateProperties();
                dispatchEvent(new Event("dataChange"));
            }
        }

        private var _rendererState:String = "normal";

        [Bindable("rendererStateChange")]
        public function get rendererState():String
        {
            return _rendererState;
        }

        public function set rendererState(value:String):void
        {
            if(value != _rendererState)
            {
                _rendererState = value;
                dispatchEvent(new Event("rendererStateChange"));
                invalidateSkinState();
            }
        }

        protected function updateControls():void
        {
            // this function is invoked after every data update or when a skin part is added to it.
            // this one must be overridden to react to data being given to the rendererer.
        }

        override protected function getCurrentSkinState():String
        {
            return _rendererState;

        }

        override protected function commitProperties():void
        {

            super.commitProperties();
            if(dataChanged)
            {
                dataChanged = false;
                updateControls();
            }
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName,instance);
            updateControls();
        }
    }
}
