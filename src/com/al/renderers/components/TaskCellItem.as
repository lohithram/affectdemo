package com.al.renderers.components
{
    import com.al.containers.NoteContainer;
    import com.al.dragging.StartDraggingStrategy;
    import com.al.enums.DragSourceFormats;
    import com.al.events.StartDraggingEvent;
    import com.al.model.Task;
    import com.al.model.UserCell;
    import com.al.renderers.components.*;
    import com.al.util.SnapshotUtil;
    import com.al.utility.panels.common.CellItem;

    import flash.events.Event;
    import flash.utils.Dictionary;

    import mx.binding.utils.BindingUtils;

    import mx.binding.utils.ChangeWatcher;

    import mx.core.DragSource;
    import mx.events.FlexEvent;
    import mx.managers.DragManager;

    import spark.components.Image;
    import spark.components.supportClasses.SkinnableTextBase;
    import spark.components.supportClasses.TextBase;

    public class TaskCellItem extends com.al.utility.panels.common.CellItem
    {

        [SkinPart(required="false")]
        public var budgetedDisplay:TextBase;

        [SkinPart(required="false")]
        public var budgetedInput:SkinnableTextBase;


        [SkinPart(required="false")]
        public var actualDisplay:TextBase;

        [SkinPart(required="false")]
        public var actualInput:SkinnableTextBase;

        private var budgetedCW:ChangeWatcher;
        private var actualCW:ChangeWatcher;

        override protected function enableDragging():void
        {
            super.enableDragging();
        }

        override public function set data(value:Object):void
        {
            super.data = value;
        }

        override protected function updateControls():void
        {
            super.updateControls();
            var cell:Task = data as Task;

            if(budgetedDisplay)
            {
                budgetedDisplay.text = cell ? cell.budgeted : "";
            }

            if(budgetedInput)
            {
                budgetedInput.text = cell ? cell.budgeted :"";

                if(cell && !budgetedCW)
                {
                    budgetedCW = BindingUtils.bindProperty(budgetedInput,"text",cell,"budgeted");
                }
            }

            if(actualDisplay)
            {
                actualDisplay.text = cell ? cell.actual : "";
            }

            if(actualInput)
            {
                actualInput.text = cell ? cell.actual :"";

                if(cell && !actualCW)
                {
                    actualCW = BindingUtils.bindProperty(actualInput,"text",cell,"actual");
                }
            }
        }

        override protected function getAdditionalSourceFormats():Dictionary
        {
            var result:Dictionary = super.getAdditionalSourceFormats();

            if(!result)
            {
                result = new Dictionary();
            }

            result[DragSourceFormats.TASK_CELL] = data;
            return result;

        }


        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName,instance);

            if(budgetedInput && budgetedCW)
            {
                budgetedCW.unwatch();
            }

            if(actualInput && actualCW)
            {
                actualCW.unwatch();
            }
        }

    }
}
