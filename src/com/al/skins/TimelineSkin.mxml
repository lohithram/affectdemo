<?xml version="1.0" encoding="utf-8"?>
<!--

////////////////////////////////////////////////////////////////////////////////
//	
//	Copyright 2014 Ardisia Labs LLC. All Rights Reserved.
//
//	This file is licensed under the Ardisia Component Library License. 
//
//	Only license holders are entitled to use this file subject to the  
//	conditions of the license. All other uses are expressly forbidden. Visit 
//	http://www.ardisialabs.com to view and purchase a license.
//
//	Apache Flex's source code notices are reproduced below.
//
// 	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//
//  Licensed to the Apache Software Foundation (ASF) under one or more
//  contributor license agreements.  See the NOTICE file distributed with
//  this work for additional information regarding copyright ownership.
//  The ASF licenses this file to You under the Apache License, Version 2.0
//  (the "License"); you may not use this file except in compliance with
//  the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////

-->
<s:Skin xmlns:fx="http://ns.adobe.com/mxml/2009" 
		xmlns:s="library://ns.adobe.com/flex/spark" 
		xmlns:mx="library://ns.adobe.com/flex/mx" 
		xmlns:timebarContainer="ardisia.components.timebarContainer.*" 
		xmlns:dataTip="ardisia.components.dataTip.*"
		xmlns:itemRenderers="ardisia.components.timebarContainer.themes.itemRenderers.*" 
		xmlns:supportClasses="ardisia.supportClasses.*"
		xmlns:dataTypes="ardisia.components.timebarContainer.dataTypes.*">
	
	<!-- host component -->
	<fx:Metadata>
		[HostComponent("ardisia.components.timeline.Timeline")]
	</fx:Metadata>
	
	<!-- scripts -->
	<fx:Script>
		<![CDATA[
			
			//----------------------------------
			//  variables
			//----------------------------------
			
			protected var stylesChanged:Boolean;
			
			protected var _borderVisible:Boolean;
			
			protected var _borderColor:Number = 0;
			
			protected var _borderAlpha:Number = 0;
			
			protected var _backgroundColor:Number = 0;
			
			protected var _backgroundAlpha:Number = 0;
			
			//----------------------------------
			//  overridden methods
			//----------------------------------
			
			override protected function updateDisplayList(unscaledWidth:Number, 
														  unscaledHeight:Number):void
			{
				// add syle properties
				if (_borderVisible)
				{
					stroke.color = _borderColor;
					stroke.alpha = _borderAlpha;
				}
				else
				{
					stroke.alpha = 0
				}
				
				fill.color = _backgroundColor;
				fill.alpha = _backgroundAlpha;
				
				super.updateDisplayList(unscaledWidth, unscaledHeight);
			}
			
			override protected function commitProperties():void
			{
				super.commitProperties();
				
				if (stylesChanged)
				{
					stylesChanged = false;
					
					_borderColor = getStyle("borderColor");
					_borderAlpha = getStyle("borderAlpha");
					_backgroundColor = getStyle("backgroundColor");
					_backgroundAlpha = getStyle("backgroundAlpha");
					_borderVisible = getStyle("borderVisible");
					
					invalidateDisplayList();
				}
			}
			
			override public function styleChanged(styleProp:String):void
			{
				super.styleChanged(styleProp);
				
				stylesChanged = true;
				invalidateProperties();
			}
			
		]]>
	</fx:Script>
	
	<!-- declarations -->
	<fx:Declarations>
		
		<!-- skin part -->
		<!--- @optional -->
		<fx:Component id="dataTipFactoryPart">
			<dataTip:DataTip />
		</fx:Component>
		
		<!-- skin part -->
		<!--- @required -->
		<fx:Component id="headerItemRendererFactoryPart">
			<itemRenderers:TimebarContainerHeaderItemRenderer />
		</fx:Component>
		
		<!-- skin part -->
		<!--- @required -->
		<fx:Component id="itemRendererFactoryPart">
			<itemRenderers:TimebarContainerItemRenderer />
		</fx:Component>
		
		<!-- skin part -->
		<!--- @required -->
		<fx:Component id="seriesLabelRendererFactoryPart">
			<supportClasses:BasicItemRenderer implements="ardisia.components.timeline.interfaces.ITimelineSeriesLabelRenderer">
				<fx:Script>
					<![CDATA[
						import ardisia.managers.cursorManager.CursorPriority;
						import ardisia.managers.cursorManager.DefaultCursors;
						import ardisia.managers.cursorManager.CursorManager;
						
						//----------------------------------
						//  properties
						//----------------------------------
						
						private var _collapsed:Boolean;
						public function get collapsed():Boolean
						{
							return _collapsed;
						}
						public function set collapsed(value:Boolean):void
						{
							if (_collapsed == value)
								return;
							_collapsed = value;
							
							invalidateRendererState();
						}
						
						private var _label:String;
						override public function get label():String
						{
							return _label;
						}
						override public function set label(value:String):void
						{
							_label = value;
							
							labelDisplay.text = _label;
							
							invalidateRendererState();
						}
						
						public function get toggleCollapseButton():DisplayObject
						{
							return collapseButton;
						}
						
						//----------------------------------
						//  overridden methods
						//----------------------------------
						
						override protected function getCurrentRendererState():String
						{
							if (collapsed && hasState("normal"))    
								return "normal";
							
							return "normal";
						}
						
					]]>
				</fx:Script>
				
				<!-- states -->
				<supportClasses:states>
					<s:State name="normal"/>
					<s:State name="collapsed"/>
				</supportClasses:states>
				
				<s:HGroup left="10" right="10" top="0" bottom="10"
						  verticalAlign="middle">
					<s:Group id="collapseButton"
							 left="10" top="0"
							 height="9" width="9"
							 rollOver="collapseFill.color=0xCCCCCC;CursorManager.setCursor(DefaultCursors.BUTTON, CursorPriority.MEDIUM, 'timelineCollapseCursorGroup', false, systemManager);"
							 rollOut="collapseFill.color=0xFFFFFF;CursorManager.removeGroup('timelineCollapseCursorGroup');">
						<s:Rect top="0" right="0" bottom="0" left="0">
							<s:stroke>
								<s:SolidColorStroke />
							</s:stroke>
							<s:fill>
								<s:SolidColor id="collapseFill"
											  color="#FFFFFF" />
							</s:fill>
						</s:Rect>
						<s:Line horizontalCenter="0" 
								includeIn="collapsed"
								top="2" bottom="2">
							<s:stroke>
								<s:SolidColorStroke caps="square"/>
							</s:stroke>
						</s:Line>
						<s:Line verticalCenter="0" 
								left="2" right="2">
							<s:stroke>
								<s:SolidColorStroke caps="square"/>
							</s:stroke>
						</s:Line>
					</s:Group>
					<s:Label id="labelDisplay"
							 paddingTop="3"
							 fontWeight="bold"
							 color="#000000"
							 fontSize="12" />
					<s:Line right="0" left="0">
						<s:stroke>
							<s:SolidColorStroke weight="1"
												color="#D6D4D4" />
						</s:stroke>
					</s:Line>
					
				</s:HGroup>
				
			</supportClasses:BasicItemRenderer>
		</fx:Component>
		
		<!-- skin part -->
		<!--- @required -->
		<fx:Component id="seriesItemRendererFactoryPart">
			<supportClasses:BasicItemRenderer implements="ardisia.components.timeline.interfaces.ITimelineSeriesItemRenderer"
											  minWidth="9" minHeight="9" maxWidth="10000000"
											  depth.point="2" depth.range="1" depth.selectedPoint="3" depth.selectedRange="3">
				<fx:Script>
					<![CDATA[
						
						//----------------------------------
						//  variables
						//----------------------------------
						
						private var series:String;
						
						private var type:String = "point";
						
						//----------------------------------
						//  properties
						//----------------------------------
						
						private var _dateBegin:Date;
						public function get dateBegin():Date
						{
							return _dateBegin;
						}
						public function set dateBegin(value:Date):void
						{
							_dateBegin = value;
						}
						
						private var _dateEnd:Date;
						public function get dateEnd():Date
						{
							return _dateEnd;
						}
						public function set dateEnd(value:Date):void
						{
							_dateEnd = value;
							
							type = _dateEnd ? "range" : "point";
						}
						
						private var _itemIndex:int;
						override public function get itemIndex():int
						{
							return _itemIndex;
						}
						override public function set itemIndex(value:int):void
						{
							_itemIndex = value;
						}
						
						private var _selected:Boolean;
						override public function get selected():Boolean
						{
							return _selected;
						}
						override public function set selected(value:Boolean):void
						{
							if (_selected == value)
								return;
							_selected = value;
							
							invalidateRendererState();
						}
						
						override public function set data(value:Object):void
						{
							if (value == data)
								return;
							
							super.data = value;
							
							series = value ? value.series : "";
							
							invalidateRendererState();
							validateNow();
						}
						
						//----------------------------------
						//  overridden methods
						//----------------------------------
						
						override protected function getCurrentRendererState():String
						{
							
							if (_selected && type == "range")
								return "selectedRange";
							
							if (_selected && type == "point")
								return "selectedPoint";
							
							if (type == "point")
								return "point";
							
							return "range";
						}
						
					]]>
				</fx:Script>
				
				<!-- states -->
				<supportClasses:states>
					<s:State name="point" stateGroups="pointGroup" />
					<s:State name="range" stateGroups="rangeGroup" />
					<s:State name="selectedPoint" stateGroups="pointGroup, selectedGroup" />
					<s:State name="selectedRange" stateGroups="rangeGroup, selectedGroup" />
				</supportClasses:states>
				
				<!--- remember to increase maxWidth to a huge number -->
				<s:Rect includeIn="rangeGroup" 
						top="0" right="0" bottom="0" left="0"
						maxWidth="1000000">
					<s:fill>
						<s:SolidColor color="#D92525"/>
					</s:fill>
					<s:stroke>
						<s:SolidColorStroke color="#000000" color.selectedGroup="{outerDocument.getStyle('selectionColor')}" 
											weight="2"/>
					</s:stroke>
				</s:Rect>
				
				<s:Rect includeIn="pointGroup" 
						width="16" height="11"
						verticalCenter="0">
					<s:fill>
						<s:SolidColor color="#DFDFDF"/>
					</s:fill>
					<s:stroke>
						<s:SolidColorStroke color="0" color.selectedGroup="{outerDocument.getStyle('selectionColor')}"
											pixelHinting="true"
											caps="square"
											joints="miter"
											weight="2"/>
					</s:stroke>
				</s:Rect>
				
			</supportClasses:BasicItemRenderer>
		</fx:Component>
		
		<!-- skin part -->
		<!--- @required -->
		<fx:Component id="hScrollBarFactoryPart">
			<timebarContainer:TimebarContainerHScrollBar />
		</fx:Component>
		
		
		<!-- skin part -->
		<!--- @required -->
		<fx:Component id="vScrollBarFactoryPart">
			<s:VScrollBar />
		</fx:Component>
		
		<!-- skin part -->
		<!--- @optional -->
		<fx:Array id="defaultIntervalModesPart">
			<dataTypes:IntervalMode name="decaMillenium" years="10000" />
			<dataTypes:IntervalMode name="pentaMillenium" years="5000" />
			<dataTypes:IntervalMode name="biMillenium" years="2000" />
			<dataTypes:IntervalMode name="millenium" years="1000" />
			<dataTypes:IntervalMode name="halfMillenium" years="500" />
			<dataTypes:IntervalMode name="century" years="100" />
			<dataTypes:IntervalMode name="halfCentury" years="50" />
			<dataTypes:IntervalMode name="quarterCentury" years="25" />
			<dataTypes:IntervalMode name="decade" years="10" />
			<dataTypes:IntervalMode name="halfDecade" years="5" />
			<dataTypes:IntervalMode name="year" years="1" />
			<dataTypes:IntervalMode name="halfYear" months="6" />
			<dataTypes:IntervalMode name="quarter" months="3" />
			<dataTypes:IntervalMode name="month" months="1" />
			<dataTypes:IntervalMode name="week" days="7" />
			<dataTypes:IntervalMode name="day" days="1" />
			<dataTypes:IntervalMode name="hours" hours="1" />
			<dataTypes:IntervalMode name="thirtyMinutes" minutes="30" />
			<dataTypes:IntervalMode name="tenMinutes" minutes="10" />
			<dataTypes:IntervalMode name="minutes" minutes="1" />
			<dataTypes:IntervalMode name="thirtySeconds" seconds="30" />
			<dataTypes:IntervalMode name="fifteenSeconds" seconds="15" />
			<dataTypes:IntervalMode name="seconds" seconds="1" />
		</fx:Array>
		
	</fx:Declarations>
	
	<!-- states -->
	<s:states>
		<s:State name="disabled" />
		<s:State name="normal" />
	</s:states>
	
	<!--- border & background -->
	<s:Rect top="0" right="0" bottom="0" left="0">
		<s:stroke>
			<s:SolidColorStroke id="stroke"
								weight="1"
								joints="miter"/>
		</s:stroke>
		<s:fill>
			<s:SolidColor id="fill"/>
		</s:fill>
	</s:Rect>
	
	<!-- skin part -->
	<!--- @required -->
	<s:Group id="contentGroup"
			 minWidth="0" minHeight="0"/>
	
</s:Skin>
