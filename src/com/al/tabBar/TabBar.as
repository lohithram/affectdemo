/**
 * Created by lram on 02/03/2015.
 */
package com.al.tabBar
{

    import spark.components.TabBar;

    [Style(name="buttonWidth", type="Number", inherit="no")]
    [Style(name="buttonHeight", type="Number", inherit="no")]

    public class TabBar extends spark.components.TabBar{

        public function TabBar() {
        }

        //--------------------------------------------------------------------------
        //
        //  Overrides
        //
        //--------------------------------------------------------------------------


        //--------------------------------------------------------------------------
        //
        //  Privates
        //
        //--------------------------------------------------------------------------


    }
}
