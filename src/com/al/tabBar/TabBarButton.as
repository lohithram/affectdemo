/**
 * Created by lram on 02/03/2015.
 */
package com.al.tabBar
{

    import com.al.components.EditableLabel;
    import com.al.descriptors.ICombineDescriptor;
    import com.al.dragging.StartDraggingStrategy;
    import com.al.events.TabCloseEvent;

    import flash.events.Event;

    import flash.events.MouseEvent;

    import mx.events.FlexEvent;

    import spark.components.ButtonBarButton;
    import spark.components.supportClasses.ButtonBase;

    [Event(name="close", type="flash.events.Event")]

    public class TabBarButton extends ButtonBarButton
    {
        [SkinPart]
        public var closeButton: ButtonBase;

        [SkinPart]
        public var editableLabel: EditableLabel;

        public function TabBarButton() {

            super();
            mouseChildren = true;
			StartDraggingStrategy.dragWatch(this);
            this.doubleClickEnabled = true;
            this.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick );
        }

        override public function set data(value:Object):void
        {
            super.data = value;

            if(value && editableLabel)
            {
                editableLabel.text = label;
            }
        }

        public function get combineDescriptor(): ICombineDescriptor{

            return data as ICombineDescriptor;
        }

        protected function onDoubleClick(event:MouseEvent):void
        {
            if(editableLabel)
            {
                editableLabel.editFieldLabel();
            }
        }

        override protected function partAdded(partName:String, instance:Object):void {

            super.partAdded(partName, instance);

            if(instance == closeButton)
            {

                closeButton.addEventListener(MouseEvent.CLICK, function(event){

                    dispatchEvent(new Event(Event.CLOSE));
                    dispatchEvent(new TabCloseEvent(data, true));
                })
            }
            else if(instance == editableLabel)
            {
                editableLabel.text = label;
                editableLabel.mouseEnabled = false;
                editableLabel.mouseChildren = false;
            }
        }

        override protected function commitProperties(): void{

            super.commitProperties();
            if(closeButton){

                closeButton.visible = combineDescriptor && combineDescriptor.canClose;
                closeButton.includeInLayout = combineDescriptor && combineDescriptor.canClose;
            }
        }

        //--------------------------------------------------------------------------
        //
        //  Privates
        //
        //--------------------------------------------------------------------------
    }
}
