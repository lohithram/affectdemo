/**
 * Created by lram on 24/02/2015.
 */
package com.al.tile
{

    import flash.display.IBitmapDrawable;
    
    import mx.core.IFlexDisplayObject;
    import mx.core.ILayoutElement;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;
    import mx.core.IVisualElementContainer;

    /**
     * This interface exposes dimensions of a visual element reduced to a parent container of size 100x100 pixels.
     * In other words the properties of this interface gives the bounds of the visual element mapped to the co-ordinates of
     * a reference parent of 100x100 width and height.
     *
     * Every Tile element that expects itself to be added to a combine should
     * implement this interface so that it can be properly sized and laid out.
     */

    public interface ICombineElement extends IVisualElement
    {
        function get visualIndex(): int;
        function set visualIndex(value: int): void;

        function get reducedX(): Number;
        function set reducedX(value: Number): void;

        function get reducedY(): Number;
        function set reducedY(value: Number): void;

        function get reducedWidth(): Number;
        function set reducedWidth(value: Number): void;

        function get reducedHeight(): Number;
        function set reducedHeight(value: Number): void;
		
		function get includeInSizing(): Boolean;
		
		/*function get roundedX(): Number;
		function get roundedY(): Number;
		function get roundedRight(): Number;
		function get roundedBottom(): Number;*/
		
		function get selected(): Boolean;
		function set selected(value: Boolean): void;

    }
}
