/**
 * Created by lram on 24/02/2015.
 */
package com.al.tile
{

    import com.al.combine.Corner;
    
    import flash.utils.Dictionary;
    
    import mx.core.IVisualElement;

    /**
     *
     */

    public interface IResizeHandle extends ICombineElement
    {
		function get adjoiningTiles(): Dictionary;

		function get corner(): Corner;
		
		function get orientation(): String;
		
		function set orientation(value: String): void;

    }
}
