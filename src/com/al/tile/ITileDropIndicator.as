/**
 * Created by lram on 24/02/2015.
 */
package com.al.tile
{

    import flash.display.IBitmapDrawable;

    import mx.core.IFlexDisplayObject;
    import mx.core.ILayoutElement;
    import mx.core.IUIComponent;
    import mx.core.IVisualElement;

    /**
     * This interface exposes dimensions of a visual element reduced to a parent container of size 100x100 pixels.
     * In other words the properties of this interface gives the bounds of the visual element mapped to the co-ordinates of
     * a reference parent of 100x100 width and height.
     *
     * Every Tile element that expects itself to be added to a combine should
     * implement this interface so that it can be properly sized and laid out.
     */

    public interface ITileDropIndicator extends ICombineElement
    {
        function get side(): String;
        function set side(value: String): void;

        function get relatedTile(): ICombineElement;
        function set relatedTile(value: ICombineElement): void;

        function getDropAction(): String;
    }
}
