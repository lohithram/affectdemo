package com.al.tiles.team {

    import com.al.model.ApplicationModel;
    import com.al.model.Task;
    import com.al.model.UserCell;
    import com.al.util.CollectionUtil;
    import com.al.util.DataUtils;
    import com.al.util.FileUtils;
    import com.al.util.ObjectUtil;

    import mx.collections.ArrayCollection;

    public class TeamTilePM {

        [Bindable]
        public var users:ArrayCollection = new ArrayCollection();

        [Bindable]
        public var tasks:ArrayCollection = new ArrayCollection();

        [Init]
        public function init():void
        {
            loadUsers();

            users.filterFunction = userFilterFunction;
            tasks.filterFunction = taskFilterFunction;
        }

        [Inject]
        [Bindable]
        public var appModel:ApplicationModel;

        private var userFilterText:String="";

        private var taskFilterText:String="";

        public function updateTasks(selectedUsers:Array):void
        {
            tasks.removeAll();
            for each(var userCell:UserCell in selectedUsers)
            {
                for each(var task:Task in userCell.tasks)
                {
                    var found:Task = CollectionUtil.findInArray(tasks.source,"id",task.id) as Task;
                    if(!found)
                    {
                        tasks.addItem(task);
                    }
                }
            }
        }

        public function addTaskToUsers(itasks:Array,selectedUsers:Array):void
        {
            if(tasks && tasks.length>0)
            {
                for each(var user:UserCell in selectedUsers)
                {
                    if(!user.tasks) user.tasks = new ArrayCollection();

                    for each(var task:Task in itasks)
                    {
                        var found:Task = CollectionUtil.findInArray(tasks.source, "id", task.id) as Task;
                        if (!found)
                        {
                            tasks.addItem(task);
                        }

                        var found2= CollectionUtil.findInArray(user.tasks.source,"id",task.id) as Task;
                        if(!found2)
                        {
                            user.tasks.addItem(task);
                        }
                    }
                }
            }
        }

        public function filterUser(txt:String):void
        {
            userFilterText = txt;
            users.refresh();
        }

        protected function userFilterFunction(item:Object):Boolean
        {
            if(!userFilterText || userFilterText=="") return true;

            var itemString: String = ObjectUtil.toString(item,["id","filePath","type"]);
            if(itemString.toUpperCase().indexOf(userFilterText.toUpperCase())<0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public function filterTask(txt:String):void
        {
            taskFilterText = txt;
            tasks.refresh();
        }

        protected function taskFilterFunction(item:Object):Boolean
        {
            if(!taskFilterText || taskFilterText=="") return true;

            var itemString: String = ObjectUtil.toString(item,["id","filePath","type"]);
            if(itemString.toUpperCase().indexOf(taskFilterText.toUpperCase())<0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private function loadUsers():void
        {
            var xml:XML = FileUtils.loadRefDataXML("Teams.xml");

            if (!xml) return;

            for each(var userXML:XML in xml.item)
            {
                var userCell:UserCell = new UserCell();

                DataUtils.populateCellItemInstance(userCell,userXML.id, userXML.type, userXML.name, userXML.file,
                        userXML.content, userXML.startDate, userXML.modifiedDate, userXML.modifiedBy,
                        userXML.projectId);

                var itasks:ArrayCollection = new ArrayCollection();
                for each(var taskXML:XML in userXML.tasks.id)
                {
                    itasks.addItem(String(taskXML));
                }

                populateUserInformation(userCell,userXML.id, userXML.name, userXML.file,
                        userXML.position, userXML.location, userXML.username, itasks) as UserCell;

                users.addItem(userCell);

            }
        }

        private function populateUserInformation(userCell:UserCell,id:String, name:String, image:String, position:String,
                                                 location:String,username:String, taskIds:ArrayCollection):void
        {
            var imageURL:String = DataUtils.resolveResource(image);
            userCell.id = id;
            userCell.name = name;
            userCell.username = username;
            userCell.filePath = imageURL;
            userCell.location = location;
            userCell.position = position;

            for each(var taskId:String in taskIds)
            {
                var itask:Task = CollectionUtil.findInArray(appModel.tasks.source,"id",taskId) as Task;
                if(itask)
                {
                    if(!userCell.tasks) userCell.tasks = new ArrayCollection();
                    userCell.tasks.addItem(itask);
                }
            }
        }

    }
}