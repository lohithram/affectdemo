
package com.al.util
{

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.factory.CellFactory;
    import com.al.model.Cell;
    import com.al.model.Task;
    import com.al.model.UserCell;
    import com.al.renderers.TaskCellItemRenderer;
    import com.al.renderers.UserItemRenderer;
    import com.al.utility.panels.common.CellItemRenderer;

    import mx.core.ClassFactory;
    import mx.core.IFactory;

    public class CellUtil
    {
        public static function createCellFromUrl(contentUrl: String): Cell {

            var cell: Cell = new Cell();
            cell.filePath = contentUrl;
            cell.name = ContentUtil.determineContentTitle(contentUrl);

            return cell;
        }

        public static function createCellFromTileDescriptor(descriptor: TileContainerDescriptor): Cell {

            var cell: Cell = CellFactory.getCellFor(descriptor);
            cell.name = descriptor.title ? descriptor.title : cell.name;

            if(descriptor.currentContent) {

                cell.id = descriptor.currentContent.id;
                cell.filePath = descriptor.currentContent.contentUrl ? descriptor.currentContent.contentUrl : descriptor.contentUrl;
                cell.bitmapData = descriptor.currentContent.bitMapInfo;

                cell.name = ContentUtil.determineContentTitle(cell.filePath);
                cell.pendingAnnotations = descriptor.currentContent.pendingAnnotations;
            }

            return cell;
        }

        public static function getItemRenderer(data:Object,
                                               width: Number = SizeUtil.CELL_MAX_THUMBNAIL_WIDTH,
                                               height: Number = SizeUtil.CELL_MAX_THUMBNAIL_HEIGHT):IFactory {

            if (data is UserCell) {

                return new ClassFactory(UserItemRenderer);
            }
            else if (data is Task) {

                return new ClassFactory(TaskCellItemRenderer);
            }
            else{

                var factory: ClassFactory = new ClassFactory(CellItemRenderer);
                factory.properties = {draggable: true, width: width, height: height};
                return factory;
            }

            return null;
        }
    }
}
