/**
 * Created by abhisekpaul on 12/06/15.
 */
package com.al.util
{
    import mx.collections.ArrayCollection;
    import mx.utils.ArrayUtil;

    public class CollectionUtil
    {
        private static function findFunction(propertyName:String,propertyValue:*):Function
        {
            return function( element : *, index : int, array : Array ) : Boolean
            {
                return element[propertyName] == propertyValue;
            }
        }
        public static function findInCollection(collection:ArrayCollection, propertyName:String, propertyValue:*):Object
        {
            var filterfunction:Function=findFunction(propertyName,propertyValue);
            var matches : Array = collection.source.filter( filterfunction );
            return ( matches.length > 0 ? matches[0] : null );
        }
        public static function findInArray(collection:Array, propertyName:String, propertyValue:*):Object
        {
            var filterfunction:Function=findFunction(propertyName,propertyValue);
            var matches : Array = collection.filter( filterfunction );
            return ( matches.length > 0 ? matches[0] : null );
        }

        public static function findMatchesInCollection(collection:ArrayCollection, propertyName:String, propertyValue:*):Array
        {
            var filterfunction:Function=findFunction(propertyName,propertyValue);
            var matches : Array = collection.source.filter( filterfunction );
            return ( matches.length > 0 ? matches : null );
        }

        public static function findIndexInCollection(collection:ArrayCollection, propertyName:String, propertyValue:*):int
        {
            var filterfunction:Function=findFunction(propertyName,propertyValue);
            var matches : Array = collection.source.filter( filterfunction );
            return ( matches.length > 0 ? collection.getItemIndex(matches[0]): -1 );

        }
        public static function findIndexInArray(collection:Array, propertyName:String, propertyValue:*):int
        {
            var filterfunction:Function=findFunction(propertyName,propertyValue);
            var matches : Array = collection.filter( filterfunction );
            return ( matches.length > 0 ? ArrayUtil.getItemIndex(matches[0],collection): -1 );


        }
        public static function convertObject(source:*,typeClass:Class):*
        {
            var target:* = new typeClass();
            for (var prop:String in source)
            {
                if(target.hasOwnProperty(prop))
                {
                    target[prop]=source[prop];
                }
            }
            return target;

        }


        public static function numericalParseFunction(item:Object):Number
        {
            return parseFloat(item as String);
        }
    }
}
