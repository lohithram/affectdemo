
package com.al.util
{

    import com.al.model.PageContent;
    import com.al.model.TileContentDescriptor;

    import flash.filesystem.File;

    import mx.collections.ArrayCollection;

    import mx.collections.ArrayList;
    import mx.utils.UIDUtil;
    import mx.utils.URLUtil;

    public class ContentUtil
    {
        public static function determineContentTitle(contentURL: String): String {

            var title: String;

            if(URLUtil.isHttpURL(contentURL))
                title = contentURL;
            else if(contentURL)
                title = new File(contentURL).name;

            return title;
        }

        public static function createContentDescriptorForAssetURL(contentURL: String): TileContentDescriptor {

            var contentDescriptor: TileContentDescriptor = new TileContentDescriptor(UIDUtil.createUID());
            contentDescriptor.contentUrl = contentURL;

            contentDescriptor.contentUnits = new ArrayCollection();

            return contentDescriptor;
        }
    }
}
