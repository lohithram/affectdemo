/**
 * Created by lram on 16/02/2015.
 */
package com.al.util
{

    import flash.display.DisplayObject;

    import mx.core.IVisualElement;

    import org.spicefactory.parsley.flex.FlexContextBuilder;

    public class ContextUtil
    {

        public static function buildContext(configClass: Class, viewRoot: DisplayObject, dependencies: Array = null): void{

            if(!viewRoot) null;

            new BuildContext(configClass, viewRoot, dependencies);
        }

    }
}

import com.al.events.DependenciesSatisfiedEvent;
import com.al.model.PropertyDependency;
import com.al.util.EventUtils;

import flash.display.DisplayObject;
import flash.events.Event;

import org.spicefactory.parsley.core.context.Context;
import org.spicefactory.parsley.core.events.ContextEvent;

import org.spicefactory.parsley.flex.FlexContextBuilder;

class BuildContext{

    public function BuildContext(configClass: Class, viewRoot: DisplayObject, dependencies: Array){

        this.viewRoot = viewRoot;
        this.configClass = configClass;
        this.dependencies = dependencies;

        if(viewRoot.stage)
            buildContext();
        else
            viewRoot.addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
    }


    private var configClass: Class;

    private var viewRoot: DisplayObject;

    private var dependencies: Array;


    private function onAddToStage(event:Event):void {

        EventUtils.removeListener(event, onAddToStage);

        buildContext();
    }

    private function buildContext(): void{

        var context: Context = FlexContextBuilder.build(configClass, viewRoot);

        if(context.configured)
            injectDependencies(context);
        else
            context.addEventListener(ContextEvent.CONFIGURED, onContextConfigured);
    }

    private function onContextConfigured(event: ContextEvent): void{

        EventUtils.removeListener(event, onContextConfigured);
        injectDependencies(event.target as Context);
    }

    private  function injectDependencies(context: Context): void{

        for each( var dependency: PropertyDependency in dependencies){

            if(dependency)
                viewRoot[dependency.propertyName] = context.getObjectByType(dependency.type);
        }
        viewRoot.dispatchEvent(new DependenciesSatisfiedEvent());
    }
}
