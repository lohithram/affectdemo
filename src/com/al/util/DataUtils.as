/**
 * Created by abhisekpaul on 08/06/15.
 */
package com.al.util
{
    import com.al.model.Cell;
    import com.al.model.CellType;
    import com.al.model.ConceptBoardTemplate;
    import com.al.model.FormTemplate;
    import com.al.model.PageContent;

    import mx.collections.ArrayCollection;
    import mx.utils.URLUtil;

    import com.al.model.Cell;

    public class DataUtils
    {
        public static function convertXMLToType(type:Class, xml:XML):Object
        {
            var obj:Object = new type();
            for each(var prop:XML in xml.children())
            {
                obj[prop.name()] = prop;
            }

            return obj;
        }

        public static function resolveResource(resource:String):String
        {

            var isExternal:Boolean = URLUtil.isHttpURL(resource) || URLUtil.isHttpsURL(resource);
            var resourceURL:String = isExternal ? resource : FileUtils.resolveDemoResource(resource);

            return resourceURL;
        }

        public static function populateCellItemInstance(cell:Cell, id:String, type:String,
                                                        name:String, image:String, content:String, startDate:String, modifiedDate:String,
                                                        modifiedBy:String, projectId:String, collections:ArrayCollection = null,
                                                        tags:ArrayCollection = null, endDate:String = "", assetType:String = "", templateName:String = "",templates:ArrayCollection=null,
                                                        conceptBoardTemplates:ArrayCollection=null, pagesXML:XMLList = null):void
        {
            var imageURL:String = DataUtils.resolveResource(image);
            cell.startDate = startDate;
            cell.modifiedDate = modifiedDate;
            cell.modifiedBy = modifiedBy;
            cell.id = id;
            cell.name = name;
            cell.filePath = imageURL;
            cell.content = content;
            cell.type = type;
            cell.projectId = projectId;
            cell.collections = collections;
            cell.tags = tags;
            cell.endDate = endDate;
            cell.assetType = assetType;
            cell.templateName = templateName;

            if(cell.type == CellType.BRIEF && templates)
            {
                var template:FormTemplate = CollectionUtil.findInArray(templates.source,"name",cell.templateName) as FormTemplate;
                cell.model  = template;
            }
            else if(cell.type == CellType.CONCEPT && conceptBoardTemplates)
            {
                var conceptBoardTemplate:ConceptBoardTemplate = CollectionUtil.findInArray(conceptBoardTemplates.source,"name",cell.templateName) as ConceptBoardTemplate;
                cell.model  = conceptBoardTemplate;
            }

            cell.pages = parsePagesFormTemplate(pagesXML);
        }

        public static function parsePagesFormTemplate(pagesXML: XMLList): ArrayCollection{

            var result:ArrayCollection = new ArrayCollection();

            var pageCount: int = 0;
            for each(var pageXML:XML in pagesXML)
            {
                var pageUrl: String = FileUtils.resolveDemoResource(pageXML.url);
                result.addItem(new PageContent(String(++pageCount), pageCount, pageUrl ));
            }
            return result;
        }


    }


}
