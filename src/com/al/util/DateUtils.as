/**
 * Created by lram on 16/02/2015.
 */
package com.al.util
{

    import ardisia.utils.DateUtils;

    public class DateUtils
    {
        public static function isToday(date: Date): Boolean{

            var today: Date = new Date();
            return date && date.date == today.date && date.month == today.month && date.fullYear == today.fullYear;
        }

        public static function addDays(date: Date, numberOfDays: Number): Date{

            return ardisia.utils.DateUtils.addDays(date, numberOfDays);
        }

        public static function format(date: Date, formatString: String = "DD MMM YYYY - HH:NN"): String{

            return ardisia.utils.DateUtils.format(date, formatString);
        }

        public static function getShortDateFormat(date: Date): String{

            return ardisia.utils.DateUtils.format(date, "DD/MM/YYYY");
        }
    }
}
