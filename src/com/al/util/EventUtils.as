/**
 * Created by lram on 09/02/2015.
 */
package com.al.util
{

    import flash.events.Event;
    import flash.events.IEventDispatcher;

    /**
     *  Contains utility methods to use with flex/flash events
     */
    public class EventUtils
    {
        public static function removeListener(event: Event, listener: Function): void{

            if(!event || listener == null)
                throw new ArgumentError("Arguments to EventUtils.removeListener() cannot be null");

            if(event.currentTarget is IEventDispatcher)
                (event.currentTarget as IEventDispatcher).removeEventListener(event.type, listener);
        }
    }
}
