/**
 * Created by lram on 12/02/2015.
 */
package com.al.util
{

    import flash.events.Event;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.net.FileReference;

    public class FileUtils
    {
        private static const DEMO_DIRECTORY_NAME: String = "demo";

        public function FileUtils() {
        }

        public static function loadRefDataXML(fileName: String): XML{

            var xmlFile: File =
                    File.userDirectory.resolvePath(DEMO_DIRECTORY_NAME).resolvePath(fileName);

            return readXML(xmlFile);
        }

        /**
         * Returns the url for the file in the local system in the demo folder
         * @return
         */
        public static function resolveDemoResource(fileName: String): String {

            var resourceFile: File =
                    File.userDirectory.resolvePath(DEMO_DIRECTORY_NAME).resolvePath(fileName);

            return resourceFile.url;
        }
		
        /**
         * Returns the url for the file in the local system in the demo folder
         * @return
         */
        public static function resolveImageResource(fileName: String): String {

            var resourceFile: File =
                    File.applicationDirectory.resolvePath("images").resolvePath(fileName);

            return resourceFile.url;
        }

        public static function isImageUrl(fileName:String):Boolean
        {
            var imageExtensions:Array = [".jpg",".jpeg",".jfif",".png",".gif",".svg",".bmp",".tif",".tiff",".webp"];
            for each(var extension:String in imageExtensions)
            {
                if(fileName.indexOf(extension)>=0)
                {
                    return true;
                }
            }

            return false;

        }
		//--------------------------------------------------------------------------
		//
		//  Private
		//
		//--------------------------------------------------------------------------

        private static function readXML(xmlFile: File): XML{

            var xmlData: XML;
            var fileStream: FileStream = new FileStream();
            try
            {
                fileStream.open(xmlFile, FileMode.READ);
                xmlData = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
            }
            catch(e:Error) {
                //trace("[ERROR]: "+e.message);
            }
            finally{
                fileStream.close();
            }

            return xmlData;
        }
    }
}
