/**
 * Created by abhisekpaul on 09/06/15.
 */
package com.al.util
{
    import flash.utils.describeType;

    public class ObjectUtil
    {
       public static function toString(obj:Object,exceptions:Array=null,delimiter:String = " "):String
       {
           if(obj is String || obj is int || obj is Number) return String(obj);

           var ret:Array = getProperties(obj, exceptions);

           return ret.join(delimiter);
       }

        public static function getProperties(_obj : *, exceptions:Array=null, onlyValues:Boolean = true) : Array
        {
            var _description : XML = describeType(_obj);
            var _properties : Array = new Array();

            updateAccessors(_obj,_properties,_description,onlyValues, exceptions);
            updateVariables(_obj,_properties,_description,onlyValues, exceptions);
            return _properties;
        }

        private static function updateAccessors(obj : *, properties:Array,description:XML, onlyValues:Boolean, exceptions:Array=null):void
        {
            for each (var prop:XML in description.accessor)
            {
                if(exceptions && exceptions.indexOf(String(prop.@name)) < 1)
                {
                    if(onlyValues)
                    {
                        try
                        {
                            properties.push(obj[String(prop.@name)]);
                        }
                        catch (e : Error)
                        {
                            properties.push("");
                        }
                    }
                    else
                    {
                        var _property : Object = new Object();
                        _property.name = String(prop.@name);
                        _property.type = String(simpleType(prop.@type));
                        _property.access = String(prop.@access);
                        _property.declaredBy = String(prop.@declaredBy);
                        try
                        {
                            _property.value = obj[_property.name];
                        }
                        catch (e : Error)
                        {
                            _property.value = "";
                        }
                        properties.push(_property)
                        properties.sortOn("name");
                    }
                }
            }
        }

        private static function updateVariables(obj : *, properties:Array,description:XML, onlyValues:Boolean, exceptions:Array=null):void
        {
            for each (var prop:XML in description.variable)
            {
                if(exceptions && exceptions.indexOf(String(prop.@name)) < 1)
                {
                    if(onlyValues)
                    {
                        try
                        {
                            properties.push(obj[String(prop.@name)]);
                        }
                        catch (e : Error)
                        {
                            properties.push("");
                        }
                    }
                    else
                    {
                        var _property : Object = new Object();
                        _property.name = String(prop.@name);
                        _property.type = String(simpleType(prop.@type));
                        _property.access = String(prop.@access);
                        _property.declaredBy = String(prop.@declaredBy);
                        try
                        {
                            _property.value = obj[_property.name];
                        }
                        catch (e : Error)
                        {
                            _property.value = "";
                        }
                        properties.push(_property);
                        properties.sortOn("name");
                    }
                }
            }
        }

        private static function simpleType(_type : String) : String
        {
            var lastIndex : int = _type.lastIndexOf("::");
            _type = lastIndex > 0 ? _type.substr(lastIndex + 2) : _type;
            return _type;
        }

        public static function cloneArray(source:Array):Array
        {
            return source ? source.slice(0) : null;

        }
    }
}
