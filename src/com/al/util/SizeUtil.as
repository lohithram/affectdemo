/**
 * Created by abhisekpaul on 03/06/15.
 */
package com.al.util
{
    public class SizeUtil
    {
        public static const CELL_PERCENT_WIDTH:Number = 20;

        public static const CELL_MIN_LIST_HEIGHT:Number = 40;

        public static const CELL_MIN_THUMBNAIL_HEIGHT:Number = 82;

        public static const CELL_MAX_THUMBNAIL_HEIGHT:Number = 144;

        public static const CELL_MAX_THUMBNAIL_WIDTH:Number = 240;

        public static const CELL_MIN_WIDTH:Number = 240;

        public static const CELL_MED_WIDTH:Number = 510;

        public static const CELL_MAX_WIDTH:Number = 900;

        public static const CELL_THRESHOLD_WIDTH:Number = 10;

        public static const CELL_THRESHOLD_HEIGHT:Number = 41;

        public static const UP_MIN_WIDTH:Number = CELL_MIN_WIDTH + 70;

        public static function getZoomHeight(zoomFactor:Number,min:Number,max:Number):Number
        {
            var heightFactor:Number = (max - min)/100;

           return min + (heightFactor * zoomFactor);
        }
    }
}
