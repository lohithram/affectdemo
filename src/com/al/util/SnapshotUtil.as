package com.al.util
{
	import flash.display.BitmapData;
	
	import mx.core.IUIComponent;
	
	import spark.components.Image;
	import spark.utils.BitmapUtil;

	public class SnapshotUtil
	{
		public static function getSnapShot(component: IUIComponent, width: Number=400, height: Number=300): Image {
			
			var bitmapData: BitmapData = BitmapUtil.getSnapshot(component);
			var image: Image = new Image();
			image.source = bitmapData;
			image.width = width;
			image.height = height;
			
			return image;
		}
	}
}