/**
 * Created by abhisekpaul on 18/06/15.
 */
package com.al.util
{
    public class TransitionUtil
    {

        public static const EXPAND_DURATION: Number = 300;
        public static const COLLAPSE_DURATION: Number = 200;

        public static const UTILITY_EXPAND_DURATION: Number = 450;
        public static const UTILITY_COLLAPSE_DURATION: Number = 250;

        // HANDS OFF THESE COMBINE PARAMETERS
        public static const SHRINK_TIME: Number = 600;
    }
}
