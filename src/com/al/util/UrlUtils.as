/**
 * Created by abhisekpaul on 18/06/15.
 */
package com.al.util
{
    import mx.utils.URLUtil;

    public class UrlUtils
    {
        public static function getQualifiedUrl(txt:String):String
        {
            var result = txt;
            var isHttp:Boolean = URLUtil.isHttpURL(txt);
            if(!isHttp)
            {
                result="http://" + result;

            }

            return result;
        }
    }
}
