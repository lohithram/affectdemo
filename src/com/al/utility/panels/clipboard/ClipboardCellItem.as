package com.al.utility.panels.clipboard
{
    import air.update.utils.FileUtils;

    import com.al.containers.NoteContainer;
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.ContentType;
    import com.al.events.ItemEvent;
    import com.al.utility.panels.common.CellItem;

    import flash.events.Event;
    import flash.filesystem.File;
    import flash.net.FileReference;
    import flash.net.URLRequest;

    import mx.utils.URLUtil;

    import spark.components.supportClasses.TextBase;
    import spark.events.TextOperationEvent;

    public class ClipboardCellItem extends CellItem
    {
        [SkinPart(required="false")]
        public var noteContainer:NoteContainer;

        [SkinPart(required="false")]
        public var labelDisplay:TextBase;

        protected function onContentTextChange(event:Event):void
        {
            if(labelDisplay)
            {
                var ncText:String = noteContainer.ta.text ? noteContainer.ta.text : "";
                labelDisplay.text = "Note: " + noteContainer.ta.text;
            }
        }

        override protected function onDownloadClick(event:Event):void
        {
            dispatchEvent(new ItemEvent(ItemEvent.DOWNLOAD,data, true));

            var tc:TileContainerDescriptor = data as TileContainerDescriptor;
            var fileRef:FileReference;
            if(tc && tc.currentContent.contentType == ContentType.NOTE)
            {
                fileRef = new FileReference();
                fileRef.save(tc.currentContent.text,"note.txt");

            }
            else if(tc && tc.currentContent.contentUrl)
            {
                if(URLUtil.isHttpURL(tc.currentContent.contentUrl))
                {
                    fileRef = new FileReference();
                    var fileName:String = FileUtils.getFilenameFromURL(tc.currentContent.contentUrl);
                    fileRef.download(new URLRequest(tc.currentContent.contentUrl),fileName);

                }
                else if(tc.currentContent.contentUrl)
                {
                    var file:File = new File(tc.currentContent.contentUrl);
                    file.addEventListener(Event.COMPLETE, onFileLoaded);
                    file.load();
                }
            }

        }

        private function onFileLoaded(e:Event):void {
            var fileRef:FileReference = new FileReference();
            fileRef.save(e.target.data,e.target.name);
        }

        override protected function updateControls():void
        {
            var tc:TileContainerDescriptor = data as TileContainerDescriptor;

            if(noteContainer)
            {
                noteContainer.contentDescriptor = tc.currentContent;
                noteContainer.ta.addEventListener(TextOperationEvent.CHANGE, onContentTextChange);
                onContentTextChange(null);
            }

            if(imageDisplay)
            {
                imageDisplay.source = tc ? tc.currentContent.contentUrl : "";
            }

            if(labelDisplay)
            {
                if(tc && tc.currentContent && tc.currentContent.contentType == ContentType.NOTE)
                {
                    var ntx:String = tc.currentContent.text ? tc.currentContent.text : "";
                    labelDisplay.text = "Note: " + ntx;
                }
                else
                {
                    labelDisplay.text = tc ? tc.title : "";
                }

            }
        }
    }
}
