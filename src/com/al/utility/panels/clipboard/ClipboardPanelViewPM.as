/**
 * Created by abhisekpaul on 01/06/15.
 */
package com.al.utility.panels.clipboard
{
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.ContentType;
    import com.al.enums.TileType;
    import com.al.factory.TileContainerFactory;
    import com.al.messages.ClipboardMessage;
    import com.al.model.TileContentDescriptor;
    import com.al.model.UtilityGroup;
    import com.al.util.CollectionUtil;
    import com.al.utility.panels.common.UtilityPanelViewPM;

    import mx.collections.ArrayCollection;
    import mx.utils.UIDUtil;

    public class ClipboardPanelViewPM extends UtilityPanelViewPM
    {

        [Init]
        override public function init():void
        {
            super.init();
            var defGroup:UtilityGroup = new UtilityGroup();
            defGroup.items = new ArrayCollection();
            defGroup.isDefault = true;
            addItemToGroup(defGroup);
        }

        public function createGroup():void
        {
            var group:UtilityGroup = new UtilityGroup();
            group.title ="New collection";
            group.items = new ArrayCollection();
            addItemToGroup(group);
        }

        public function deleteGroup(item:*):void
        {
            groups.removeItem(item);
        }

        public function addNote(item:*):void
        {
            var td:TileContainerDescriptor = TileContainerFactory.createTileDescriptorForType(TileType.CONTENT);
            td.currentContent = new TileContentDescriptor(UIDUtil.createUID());
            td.currentContent.contentType = ContentType.NOTE;
            item.contents.addItem(td);
        }

        public function addCell(items:ArrayCollection,item:TileContainerDescriptor):void
        {
            var found:Object = CollectionUtil.findInArray(items.source,"contentId",item.contentId);
            if(!found)
            {
                items.addItem(item);
            }
        }

        [MessageHandler(scope="global")]
        public function handleClipboardAdd(msg:ClipboardMessage):void
        {
            addCell(groups.getItemAt(0).contents,msg.tileDescriptor);
        }
    }
}
