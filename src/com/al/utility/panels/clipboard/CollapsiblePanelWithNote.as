/**
 * Created by abhisekpaul on 01/06/15.
 */
package com.al.utility.panels.clipboard
{
    import com.al.components.CollapsiblePanel;

    import flash.events.Event;

    import flash.events.MouseEvent;

    import spark.components.supportClasses.ButtonBase;

    [Event(name="addNote", type="flash.events.Event")]
    public class CollapsiblePanelWithNote extends CollapsiblePanel
    {
        [SkinPart(required="false")]
        public var noteBtn:ButtonBase;

        [Bindable]
        public var contentData:*;

        protected function onNoteAdded(event:MouseEvent):void
        {
            dispatchEvent(new Event("addNote"));
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName,instance);
            if(instance == noteBtn)
            {
                noteBtn.addEventListener(MouseEvent.CLICK, onNoteAdded);
            }
        }

        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName,instance);
            if(instance == noteBtn)
            {
                noteBtn.removeEventListener(MouseEvent.CLICK, onNoteAdded);
            }
        }
    }
}
