package com.al.utility.panels.collaboration
{
    import com.al.collaboration.annotation.model.Comment;
    import com.al.enums.DragSourceFormats;
    import com.al.util.DateUtils;
    import com.al.utility.panels.common.CellItem;

    import flash.events.Event;
    import flash.utils.Dictionary;

    import mx.events.FlexEvent;

    import spark.components.supportClasses.TextBase;

    public class CollaborationCellItem extends CellItem
    {
        [SkinPart(required="false")]
        public var conversationField:TextBase;

        [SkinPart(required="false")]
        public var postedField:TextBase;

        [SkinPart(required="false")]
        public var userField:TextBase;

        [SkinPart(required="false")]
        public var typeField:TextBase;

        [SkinPart(required="false")]
        public var descriptionField:TextBase;

        [Bindable]
        public var status:String;


        public function CollaborationCellItem()
        {
            this.addEventListener(FlexEvent.INITIALIZE, onInit);
        }

        protected function onInit(event:Event):void
        {
        }

        override protected function getAdditionalSourceFormats():Dictionary
        {
            var result:Dictionary = new Dictionary();
            result[DragSourceFormats.COMMENT_CELL] = data;

            return result;

        }

        override protected function updateControls():void
        {
            var comment:Comment = data as Comment;

            if(conversationField)
            {
                conversationField.text = comment ? String(comment.conversationId):"";
            }

            if(postedField)
            {
                postedField.text = comment ? DateUtils.format(new Date(comment.timestamp)) : "";
            }

            if(userField)
            {
                userField.text = comment ? comment.userName : "";
            }

            if(typeField)
            {

            }

            if(descriptionField)
            {
                descriptionField.text = comment ? comment.commentString : "";
            }
        }
    }
}
