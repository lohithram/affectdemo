package com.al.utility.panels.collaboration
{
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.enums.ContentType;
    import com.al.enums.TileDataFormats;
    import com.al.enums.TileType;
    import com.al.factory.TileContainerFactory;
    import com.al.messages.ClipboardMessage;
    import com.al.messages.CombineSelectedMsg;
    import com.al.model.DisplayAreaModel;
    import com.al.model.TileContentDescriptor;
    import com.al.model.TileDataModel;
    import com.al.model.UtilityGroup;
    import com.al.utility.panels.common.UtilityPanelViewPM;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.utils.UIDUtil;

    public class CollaborationPanelViewPM
    {
        [Bindable]
        [ArrayElementType("com.al.model.TileDataModel")]
        public var groups:ArrayList;

        [Inject]
        public var displayAreaModel:DisplayAreaModel;

        [Init]
        public function init()
        {
            if(displayAreaModel.currentCombine)
            {
                groups = getCollaborateTiles();
            }
        }

        public function createGroup():void
        {
            var group:UtilityGroup = new UtilityGroup();
            group.title ="New group";
            group.items = new ArrayCollection();
            groups.addItem(group);
        }


        public function addCell(items:ArrayCollection,item:TileContainerDescriptor):void
        {
            if(items.getItemIndex(item)<0)
            {
                items.addItem(item);
            }
        }

        [MessageHandler]
        public function onCombineSelect(msg: CombineSelectedMsg): void
        {
            if(displayAreaModel.currentCombine)
            {
                groups = getCollaborateTiles();
            }
        }

        private function getCollaborateTiles():ArrayList
        {
            if(! displayAreaModel.currentCombine.tiles) return null;

            var result:ArrayList = new ArrayList();
            for each (var tileDataModel:TileDataModel in displayAreaModel.currentCombine.tiles.source)
            {
                var td:TileContainerDescriptor = tileDataModel.getTileData(TileDataFormats.TILE_DESCRIPTOR);
                if(td.tileType == TileType.CONTENT || td.tileType == TileType.HTML)
                {
                    result.addItem(tileDataModel);
                }
            }

            return result;
        }

    }
}
