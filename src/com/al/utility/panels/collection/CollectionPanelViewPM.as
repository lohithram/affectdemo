/**
 * Created by abhisekpaul on 01/06/15.
 */
package com.al.utility.panels.collection
{
    import com.al.descriptors.TileContainerDescriptor;
    import com.al.messages.ClipboardMessage;
    import com.al.model.Cell;
    import com.al.model.Project;
    import com.al.model.UtilityProject;
    import com.al.util.DataUtils;
    import com.al.util.FileUtils;
    import com.al.utility.panels.common.UtilityPanelViewPM;

    import mx.collections.ArrayCollection;
    import mx.utils.UIDUtil;

    public class CollectionPanelViewPM extends UtilityPanelViewPM
    {
        [Init]
        override public function init():void
        {
            super.init();
            loadItems();
        }

        [MessageDispatcher]
        public var dispatcher: Function;

        public function addItemToClipboard(item:Cell):void
        {
            var tc:TileContainerDescriptor = item.getTileContainerDescriptor();
            dispatcher(new ClipboardMessage(tc));
        }

        private function loadItems():void
        {
            var xml:XML = FileUtils.loadRefDataXML("CreateItems.xml");

            if (!xml)
                return;

            for each(var projectXML:XML in xml.group)
            {
                var utilityProject:UtilityProject = new UtilityProject();
                utilityProject.title = projectXML.title;
                utilityProject.items = new ArrayCollection();
                for each(var relatedXML:XML in projectXML.items.item)
                {
                    var collections:ArrayCollection = new ArrayCollection();
                    var tags:ArrayCollection = new ArrayCollection();

                    for each(var tagXML:XML in relatedXML.tags.tag)
                    {
                        tags.addItem(String(tagXML));
                    }

                    for each(var colXML:XML in relatedXML.collections.collection)
                    {
                        collections.addItem(String(colXML));
                    }

                    var cell:Cell = new Cell();
                    var project:Project = new Project(UIDUtil.createUID());
                    cell.project = project;


                    DataUtils.populateCellItemInstance(cell,relatedXML.id, relatedXML.type, relatedXML.name, relatedXML.file,
                            relatedXML.content, relatedXML.startDate, relatedXML.modifiedDate, relatedXML.modifiedBy,
                            relatedXML.projectId,collections,tags,relatedXML.endDate,relatedXML.assetType, relatedXML.templateName, briefModel.templates,
                    conceptBoardModel.templates, relatedXML.pages.page);

                    utilityProject.items.addItem(cell);
                }
                addItemToGroup(utilityProject);
            }
        }

        public function addCell(items:ArrayCollection, item:TileContainerDescriptor):void
        {
            if (items.getItemIndex(item) < 0)
            {
                items.addItem(item);
            }
        }
    }
}
