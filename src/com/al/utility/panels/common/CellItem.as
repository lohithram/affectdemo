package com.al.utility.panels.common
{
    import air.update.utils.FileUtils;

    import com.al.descriptors.TileContainerDescriptor;
    import com.al.dragging.StartDraggingStrategy;
    import com.al.enums.DragSourceFormats;
    import com.al.events.ItemEvent;
    import com.al.events.StartDraggingEvent;
    import com.al.model.Cell;
    import com.al.renderers.components.*;
    import com.al.util.DateUtils;
    import com.al.util.SnapshotUtil;

    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.filesystem.File;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.utils.Dictionary;

    import mx.controls.DateField;
    import mx.core.DragSource;
    import mx.events.CalendarLayoutChangeEvent;
    import mx.events.DragEvent;
    import mx.managers.DragManager;
    import mx.utils.URLUtil;

    import spark.components.Image;
    import spark.components.supportClasses.ButtonBase;
    import spark.components.supportClasses.SkinnableTextBase;
    import spark.components.supportClasses.TextBase;
    import spark.events.TextOperationEvent;

    import com.al.util.FileUtils;

    [Event(name='download', type='com.al.events.ItemEvent')]
    [Event(name='clipboard', type='com.al.events.ItemEvent')]
    [Event(name='remove', type='com.al.events.ItemEvent')]
    [Event(name='moveItemComplete', type='com.al.events.ItemEvent')]
    public class CellItem extends ItemRendererComponentBase
    {
        [SkinPart(required="false")]
        public var idDisplay:TextBase;

        [SkinPart(required="false")]
        public var imageDisplay:Image;

        [SkinPart(required="false")]
        public var nameDisplay:TextBase;

        [SkinPart(required="false")]
        public var nameInput:SkinnableTextBase;

        [SkinPart(required="false")]
        public var startDateDisplay:TextBase;

        [SkinPart(required="false")]
        public var startDateInput:DateField;

        [SkinPart(required="false")]
        public var endDateDisplay:TextBase;

        [SkinPart(required="false")]
        public var endDateInput:DateField;

        [SkinPart(required="false")]
        public var modifiedDateDisplay:TextBase;

        [SkinPart(required="false")]
        public var modifiedByDisplay:TextBase;

        [SkinPart(required="false")]
        public var contentDisplay:TextBase;

        [SkinPart(required="false")]
        public var contentInput:SkinnableTextBase;

        [SkinPart(required="false")]
        public var downloadBtn:ButtonBase;

        [SkinPart(required="false")]
        public var removeBtn:ButtonBase;

        [SkinPart(required="false")]
        public var createClipboardBtn:ButtonBase;

        private var _draggable:Boolean;
        [Bindable]
        public function get draggable():Boolean
        {
            return _draggable;
        }

        public function set draggable(value:Boolean):void
        {
            if (value != _draggable)
            {
                _draggable = value;

                disableDragging();

                enableDragging();
            }
        }

        override protected function updateControls():void
        {
            super.updateControls();
            var cell:Cell = data as Cell;

            if (imageDisplay)
            {
                var imageSource:Object;
                if(cell)
                {
                    if(URLUtil.isHttpURL(cell.filePath) && !com.al.util.FileUtils.isImageUrl(cell.filePath))
                    {
                        imageSource = cell.bitmapData;
                    }
                    else
                    {
                        imageSource = cell.filePath ? cell.filePath : "";
                    }

                }
                imageDisplay.source = imageSource;
            }

            if (idDisplay)
            {
                idDisplay.text = cell ? cell.id : "";
            }

            if (nameDisplay)
            {
                nameDisplay.text = cell ? cell.name : "";
            }

            if (nameInput)
            {
                nameInput.text = cell ? cell.name : "";
            }

            if (startDateDisplay)
            {
                startDateDisplay.text = cell ? cell.startDate : "";
            }

            if (startDateInput)
            {
                startDateInput.selectedDate = cell ? cell.startDateRaw : null;
            }

            if (endDateDisplay)
            {
                endDateDisplay.text = cell ? cell.endDate : "";
            }

            if (endDateInput)
            {
                endDateInput.selectedDate = cell ? cell.endDateRaw : null;
            }

            if (modifiedDateDisplay)
            {
                modifiedDateDisplay.text = cell ? cell.modifiedDate : "";
            }

            if (modifiedByDisplay)
            {
                modifiedByDisplay.text = cell ? cell.modifiedBy : "";
            }

            if (contentDisplay)
            {
                contentDisplay.text = cell ? cell.content : "";
            }

            if (contentInput)
            {
                contentInput.text = cell ? cell.content : "";
            }
        }

        protected function getAdditionalSourceFormats():Dictionary
        {
            //this method needs to be overwritten by derived classes to include additional source formats.
            var result:Dictionary = new Dictionary();
            var tcd:TileContainerDescriptor = data is ITileContainerDescriptorProvider ?
                    ITileContainerDescriptorProvider(data).getTileContainerDescriptor() : null;
            result[DragSourceFormats.TILE_DESCRIPTOR] = tcd;
            result[DragSourceFormats.ASSET_CELL_ITEM] = this;
            result[DragSourceFormats.ASSET_CELL] = data;

            return result;

        }

        protected function enableDragging():void
        {
            if (draggable)
            {
                StartDraggingStrategy.dragWatch(this);
                addEventListener(StartDraggingEvent.START_DRAGGING, onStartDragging);
            }
        }

        private function disableDragging():void
        {
            StartDraggingStrategy.unWatch(this);
            removeEventListener(StartDraggingEvent.START_DRAGGING, onStartDragging);
        }

        protected function onStartDragging(event:StartDraggingEvent):void
        {
            var dragSource:DragSource = new DragSource();
            var sourceFormats:Dictionary = getAdditionalSourceFormats();

            for (var sourceFormat:String in sourceFormats)
            {
                dragSource.addData(sourceFormats[sourceFormat], sourceFormat);
            }

            DragManager.doDrag(this, dragSource, event.mouseEvent, SnapshotUtil.getSnapShot(this, width, height));
            addEventListener(DragEvent.DRAG_COMPLETE, onDragComplete);
        }

        protected function onDragComplete(event:DragEvent):void
        {

            if(event.action == DragManager.MOVE)
            {
                dispatchEvent(new ItemEvent(ItemEvent.ITEM_MOVE, data, true));

            }

            removeEventListener(DragEvent.DRAG_COMPLETE, onDragComplete);
        }

        protected function onContentInputChange(event: TextOperationEvent):void
        {
            var cell:Cell = data as Cell;
            cell.content = contentInput.text;

            if(contentDisplay)
            {
                contentDisplay.text = cell.content;
            }
        }

        protected function onStartDateChange(event:CalendarLayoutChangeEvent):void
        {
            var cell:Cell = data as Cell;
            cell.startDateRaw = startDateInput.selectedDate;
            cell.startDate = DateUtils.format(startDateInput.selectedDate,"DD/MM/YYYY");

            if(startDateDisplay)
            {
                startDateDisplay.text = cell.startDate;
            }
        }

        protected function onEndDateChange(event:CalendarLayoutChangeEvent):void
        {
            var cell:Cell = data as Cell;
            cell.endDateRaw = endDateInput.selectedDate;
            cell.endDate = DateUtils.format(endDateInput.selectedDate,"DD/MM/YYYY");

            if(endDateDisplay)
            {
                endDateDisplay.text = cell.endDate;
            }
        }

        protected function onNameInputChange(event:TextOperationEvent):void
        {
            var cell:Cell = data as Cell;
            cell.name = nameInput.text;

            if(nameDisplay)
            {
                nameDisplay.text = cell.name;
            }
        }

        protected function onDownloadClick(event:Event):void
        {
            dispatchEvent(new ItemEvent(ItemEvent.DOWNLOAD,data, true));
            if(data is Cell && data.filePath)
            {
                var cell:Cell = data as Cell;
                if(URLUtil.isHttpURL(cell.filePath))
                {
                    var fileRef:FileReference = new FileReference();
                    var fileName:String = air.update.utils.FileUtils.getFilenameFromURL(cell.filePath);
                    fileRef.download(new URLRequest(cell.filePath),fileName);

                }
                else if(cell.filePath)
                {
                    var file:File = new File(cell.filePath);
                    file.addEventListener(Event.COMPLETE, onFileLoaded);
                    file.load();
                }
            }
        }

        private function onFileLoaded(e:Event):void {
            var fileRef:FileReference = new FileReference();
            fileRef.save(e.target.data,e.target.name);
        }

        protected function onCreateClipboardClick(event:Event):void
        {
            dispatchEvent(new ItemEvent(ItemEvent.CLIPBOARD, data, true));
        }

        protected function onRemoveClick(event:Event):void
        {
            dispatchEvent(new ItemEvent(ItemEvent.REMOVE, data, true));
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName, instance);

            if (instance == nameInput)
            {
                nameInput.addEventListener(TextOperationEvent.CHANGE, onNameInputChange)
            }
            else if (instance == startDateInput)
            {
                startDateInput.addEventListener(CalendarLayoutChangeEvent.CHANGE, onStartDateChange)
            }
            else if (instance == endDateInput)
            {
                endDateInput.addEventListener(CalendarLayoutChangeEvent.CHANGE, onEndDateChange)
            }
            else if (instance == contentInput)
            {
                contentInput.addEventListener(TextOperationEvent.CHANGE, onContentInputChange)
            }
            else if (instance == downloadBtn)
            {
                downloadBtn.addEventListener(MouseEvent.CLICK, onDownloadClick)
            }
            else if (instance == createClipboardBtn)
            {
                createClipboardBtn.addEventListener(MouseEvent.CLICK, onCreateClipboardClick)
            }
            else if (instance == removeBtn)
            {
                removeBtn.addEventListener(MouseEvent.CLICK, onRemoveClick)
            }
        }

        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName, instance);

            if (instance == nameInput)
            {
                nameInput.removeEventListener(TextOperationEvent.CHANGE, onNameInputChange)
            }
            else if (instance == startDateInput)
            {
                startDateInput.removeEventListener(CalendarLayoutChangeEvent.CHANGE, onStartDateChange)
            }
            else if (instance == endDateInput)
            {
                endDateInput.removeEventListener(CalendarLayoutChangeEvent.CHANGE, onEndDateChange)
            }
            else if (instance == contentInput)
            {
                contentInput.removeEventListener(TextOperationEvent.CHANGE, onContentInputChange)
            }
            else if (instance == downloadBtn)
            {
                downloadBtn.removeEventListener(MouseEvent.CLICK, onDownloadClick)
            }
            else if (instance == createClipboardBtn)
            {
                createClipboardBtn.removeEventListener(MouseEvent.CLICK, onCreateClipboardClick)
            }
            else if (instance == removeBtn)
            {
                removeBtn.removeEventListener(MouseEvent.CLICK, onRemoveClick)
            }
        }
    }
}
