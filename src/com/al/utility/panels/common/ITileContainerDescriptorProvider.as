/**
 * Created by abhisekpaul on 10/06/15.
 */
package com.al.utility.panels.common
{
    import com.al.descriptors.TileContainerDescriptor;

    public interface ITileContainerDescriptorProvider
    {
        function getTileContainerDescriptor():TileContainerDescriptor;
    }
}
