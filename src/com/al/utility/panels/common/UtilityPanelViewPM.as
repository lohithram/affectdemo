package com.al.utility.panels.common
{
    import com.al.containers.ConceptBoard;
    import com.al.interfaces.IUtilityGroup;
    import com.al.messages.TemplateMessage;
    import com.al.model.BriefTemplatesModel;
    import com.al.model.Cell;
    import com.al.model.CellType;
    import com.al.model.ConceptBoardTemplatesModel;
    import com.al.model.ConceptBoardTemplate;
    import com.al.util.CollectionUtil;
    import com.al.util.ObjectUtil;
    import com.al.model.FormTemplate;

    import mx.collections.ArrayCollection;

    public class UtilityPanelViewPM
    {
        [Bindable]
        [ArrayElementType("com.al.interfaces.IUtilityGroup")]
        public var groups:ArrayCollection = new ArrayCollection();

        [Inject]
        public var briefModel:BriefTemplatesModel;

        [Inject]
        public var conceptBoardModel:ConceptBoardTemplatesModel;

        [Init]
        public function init():void
        {
            groups.filterFunction = groupFilter;
        }

        protected var filteredTxt:String="";

        public function filterText(txt:String):void
        {
            filteredTxt = txt;
            for each(var group:IUtilityGroup in groups.source)
            {
                group.contents.refresh();
            }

            groups.refresh();

        }

        protected function addItemToGroup(item:IUtilityGroup):void
        {
            item.contents.filterFunction = filter;
            groups.addItem(item);
        }

        protected function groupFilter(item:IUtilityGroup):Boolean
        {
            if(item.contents && item.contents.length>0)
            {
                return true;
            }
            return false;
        }

        protected function filter(item:Object):Boolean
        {
            if(filteredTxt=="") return true;

            var tokens:Array = filteredTxt.split(" ");

            var result:Boolean;

            for each (var token:String in tokens)
            {
                var itemString: String = ObjectUtil.toString(item,["id","filePath","type"]);
                if(itemString.toUpperCase().indexOf(token.toUpperCase())<0)
                {
                    return false;
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        [MessageHandler(scope="global", selector="FORM_TEMPLATE")]
        public function handleTemplates(msg: TemplateMessage): void
        {
            for each (var group:IUtilityGroup in groups)
            {
                for each(var cell:Cell in group.contents)
                {
                    if(cell.type == CellType.BRIEF && briefModel.templates)
                    {
                        var template:FormTemplate = CollectionUtil.findInArray(briefModel.templates.source,"name",cell.templateName) as FormTemplate;
                        cell.model  = template;
                    }
                }
            }
        }

        [MessageHandler(scope="global", selector="CONCEPT_TEMPLATE")]
        public function handleConceptBoardTemplates(msg: TemplateMessage): void
        {
            for each (var group:IUtilityGroup in groups)
            {
                for each(var cell:Cell in group.contents)
                {
                    if(cell.type == CellType.CONCEPT && conceptBoardModel.templates)
                    {
                        var template:ConceptBoardTemplate = CollectionUtil.findInArray(briefModel.templates.source,"name",cell.templateName) as ConceptBoardTemplate;
                        cell.model  = template;
                    }
                }
            }
        }

    }
}
