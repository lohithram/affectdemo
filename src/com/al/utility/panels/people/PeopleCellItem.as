package com.al.utility.panels.people
{
    import com.al.containers.NoteContainer;
    import com.al.dragging.StartDraggingStrategy;
    import com.al.enums.DragSourceFormats;
    import com.al.events.StartDraggingEvent;
    import com.al.model.UserCell;
    import com.al.renderers.components.*;
    import com.al.util.SnapshotUtil;
    import com.al.utility.panels.common.CellItem;

    import flash.events.Event;
    import flash.utils.Dictionary;

    import mx.core.DragSource;
    import mx.events.FlexEvent;
    import mx.managers.DragManager;

    import spark.components.Image;
    import spark.components.supportClasses.TextBase;

    public class PeopleCellItem extends com.al.utility.panels.common.CellItem
    {

        [SkinPart(required="false")]
        public var firstNameDisplay:TextBase;

        [SkinPart(required="false")]
        public var lastNameDisplay:TextBase;

        [SkinPart(required="false")]
        public var locationDisplay:TextBase;

        [SkinPart(required="false")]
        public var contactDisplay:TextBase;

        [SkinPart(required="false")]
        public var emailDisplay:TextBase;

        [SkinPart(required="false")]
        public var usernameDisplay:TextBase;

        [SkinPart(required="false")]
        public var positionDisplay:TextBase;

        override protected function updateControls():void
        {
            super.updateControls();

            var people:UserCell = data as UserCell;
            if(firstNameDisplay)
            {
                firstNameDisplay.text = people ? people.firstName : "";
            }

            if(lastNameDisplay)
            {
                lastNameDisplay.text = people ? people.lastName : "";
            }

            if(usernameDisplay)
            {
                usernameDisplay.text = people ? people.username : "";
            }

            if(contactDisplay)
            {
                contactDisplay.text = people ? people.contact : "";
            }

            if(emailDisplay)
            {
                emailDisplay.text = people ? people.email : "";
            }

            if(locationDisplay)
            {
                locationDisplay.text = people ? people.location : "";
            }

            if(positionDisplay)
            {
                positionDisplay.text = people ? people.position : "";
            }
        }

        override protected function getAdditionalSourceFormats():Dictionary
        {
            var result:Dictionary = new Dictionary();

            result[DragSourceFormats.USER_CELL] = data;
            return result;
        }

    }
}
