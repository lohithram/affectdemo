package com.al.visualElement
{
	import mx.core.IFlexDisplayObject;
	
	public interface ICombinePopup extends IFlexDisplayObject
	{
		function showPopup(): void;
		function removePopup(): void;
	}
}