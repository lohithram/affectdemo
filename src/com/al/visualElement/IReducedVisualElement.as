package com.al.visualElement
{
	import mx.core.IVisualElement;
	
	public interface IReducedVisualElement extends IVisualElement
	{
		function get reducedX(): Number;
		
		function get reducedY(): Number;
	}
}