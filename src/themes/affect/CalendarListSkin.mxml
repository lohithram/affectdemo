<?xml version="1.0" encoding="utf-8"?>
<!--

////////////////////////////////////////////////////////////////////////////////
//	
//	Copyright 2014 Ardisia Labs LLC. All Rights Reserved.
//
//	This file is licensed under the Ardisia Component Library License. 
//
//	Only license holders are entitled to use this file subject to the  
//	conditions of the license. All other uses are expressly forbidden. Visit 
//	http://www.ardisialabs.com to view and purchase a license.
//
//	Apache Flex's source code notices are reproduced below.
//
// 	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//
//  Licensed to the Apache Software Foundation (ASF) under one or more
//  contributor license agreements.  See the NOTICE file distributed with
//  this work for additional information regarding copyright ownership.
//  The ASF licenses this file to You under the Apache License, Version 2.0
//  (the "License"); you may not use this file except in compliance with
//  the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////

-->
<s:Skin xmlns:fx="http://ns.adobe.com/mxml/2009" 
		xmlns:s="library://ns.adobe.com/flex/spark" 
		xmlns:scroller="ardisia.components.scroller.*"
		xmlns:supportClasses="ardisia.supportClasses.*"
		alpha.disabled="0.5"> 
	
	<!-- host component -->
	<fx:Metadata>
		<![CDATA[ [HostComponent("com.al.components.calendarList.CalendarList")] ]]>
	</fx:Metadata>
	
	<!-- states -->
	<s:states>
		<s:State name="normal" />
		<s:State name="disabled" />
	</s:states>
	
	<!-- declarations -->
	<fx:Declarations>
		
	</fx:Declarations>
	
	<s:Rect top="0" right="0" bottom="0" left="0">
		<s:fill>
			<s:SolidColor color="0xFFFFFF" />
		</s:fill>
	</s:Rect>
	
	<scroller:Scroller id="scroller" 
					   left="2" top="0" right="0" bottom="1" 
					   minViewportInset="0" 
					   mouseWheelStepSize="100"
					   hasFocusableChildren="false">
		
		<!-- skin part -->
		<!--- @required -->
		<s:DataGroup id="dataGroup">
			<s:layout>
				<s:VerticalLayout gap="0" 
								  horizontalAlign="contentJustify"/>
			</s:layout>
			
			<s:itemRenderer>
				
				<fx:Component>
					<supportClasses:BasicItemRenderer height="45" 
													  implements="com.al.components.calendarList.interfaces.ICalendarListItemRenderer">
						
						<fx:Script>
							<![CDATA[
								import ardisia.utils.DateUtils;
								import ardisia.utils.SchedulingUtils;
								
								import com.al.components.calendarList.dataTypes.StoreData;
								
								//----------------------------------
								//  properties
								//----------------------------------
								
								private var _data:StoreData;
								override public function get data():Object
								{
									return _data;
								}
								override public function set data(value:Object):void
								{
									_data = value as StoreData;
									
									if (!_data)
										return;
									
									if (dayLabel)
										dayLabel.text = DateUtils.format(_data.dtStart, 'EEEE');
									if (dateLabel)
										dateLabel.text = DateUtils.format(_data.dtStart, 'MMMM D');
									
									if (calendarColorRectFill)
										calendarColorRectFill.color = _data.schedulingData.calendar.color;
									if (calendarLabel)
										calendarLabel.text = _data.schedulingData.calendar.title;
									if (summaryLabel)
										summaryLabel.text = _data.schedulingData.summary;
									if (timeLabel)
										timeLabel.text = SchedulingUtils.parseTimeString(_data.dtStart, 
										_data.dtEnd, _data.schedulingData.allDay, false);
									if (locationLabel)
									{
										locationLabel.text = _data.schedulingData.location;
										locationLabel.includeInLayout = locationLabel.visible = locationLabel.text != null && locationLabel.text != "";
									}
									
									// position the summary label differently depending on whether the 
									// location string exists
									if (summaryLabel)
										summaryLabel.top = _data.schedulingData.location && _data.schedulingData.location.length > 0 ? 8 : 16;
									
									invalidateRendererState();
								}
								
								private var _isFirst:Boolean;
								public function get isFirst():Boolean
								{
									return _isFirst;
								}
								public function set isFirst(value:Boolean):void
								{
									_isFirst = value;
									
									invalidateRendererState();
								}
								
								//----------------------------------
								//  methods
								//----------------------------------
								
								override protected function getCurrentRendererState():String
								{
									if (selected && _isFirst)    
										return "selectedAndFirst";
									
									if (selected)    
										return "selected";
									
									if (_isFirst)    
										return "first";
									
									return "normal";
								}
							]]>
						</fx:Script>
						
						<!-- states -->
						<supportClasses:states>
							<s:State name="normal" stateGroups="normalGroup"/>
							<s:State name="first" stateGroups="normalGroup, firstGroup"/>
							<s:State name="selected" stateGroups="selectedGroup"/>
							<s:State name="selectedAndFirst" stateGroups="selectedGroup, firstGroup"/>
						</supportClasses:states>
						
						<!--- background fill -->
						<s:Rect top="0" right="8" bottom="0" left="0" >
							<s:fill>
								<s:SolidColor id="bgFill" 
											  color="0xFFFFFF" color.selectedGroup="{outerDocument.getStyle('selectionColor')}"/>
							</s:fill>
						</s:Rect>
						
						<s:Line bottom="0" right="0" left="0">
							<s:stroke>
								<s:SolidColorStroke color="0x91BDE2"
													alpha="1"
													caps="square" 
													joints="miter"
													pixelHinting="true"/>
							</s:stroke>
						</s:Line>
						
						<!--- day of the week label -->
						<s:VGroup width="110" 
								  left="10"
								  verticalCenter="0"
								  horizontalAlign="justify"
								  gap="9">
							<s:Label id="dayLabel"
									 fontSize="11" fontWeight="bold"
									 visible="false" visible.firstGroup="true"
									 includeInLayout="false" includeInLayout.firstGroup="true"
									 color="0x333333" color.selectedGroup="0xFFFFFF"/>
							
							<!--- calendar ellipse -->
							<s:HGroup>
								<s:Ellipse width="10" height="10">
									<s:fill>
										<s:SolidColor id="calendarColorRectFill"/>
									</s:fill>
								</s:Ellipse>
								
								<!--- calendar label -->
								<s:Label id="calendarLabel"
										 fontSize="11"
										 color="0x333333" color.selectedGroup="0xFFFFFF"
										 paddingTop="-1"/>
								
							</s:HGroup>
						</s:VGroup>
						
						<s:VGroup right="105" left="120"
								  verticalCenter="0"
								  horizontalAlign="justify"
								  gap="9">
							
							<!--- summary label -->
							<s:Label id="summaryLabel"
									 maxDisplayedLines="1"
									 fontSize="11"
									 color="0x333333" color.selectedGroup="0xFFFFFF"/>
							
							<!--- location label -->
							<s:Label id="locationLabel"
									 fontSize="11"
									 color="0x4791CE" color.selectedGroup="0xFFFFFF"
									 paddingTop="-1"/>
						</s:VGroup>
						
						<s:VGroup width="95" 
								  right="10" 
								  verticalCenter="0"
								  horizontalAlign="justify"
								  gap="9">
							
							<!--- date label -->
							<s:Label id="dateLabel"
									 visible="false" visible.firstGroup="true"
									 includeInLayout="false" includeInLayout.firstGroup="true"
									 fontSize="11" fontWeight="bold"
									 color="0x333333" color.selectedGroup="0xFFFFFF"/>
							
							<!--- time label -->
							<s:Label id="timeLabel"
									 fontSize="11"
									 color="0x4791CE" color.selectedGroup="0xFFFFFF"
									 paddingTop="-1">
							</s:Label>
						</s:VGroup>
						
					</supportClasses:BasicItemRenderer>
				</fx:Component>
				
			</s:itemRenderer>
		</s:DataGroup>
	</scroller:Scroller>
	
</s:Skin>
