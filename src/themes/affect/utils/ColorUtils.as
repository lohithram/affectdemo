/** Theme Affect v.1.2 by Maiol Baraut **/

package themes.affect.utils {
public class ColorUtils {

//    Blue		    4791CE
//    Blue 90%	    599CD3
//    Blue 80%	    6CA7D8
//    Blue 75%      75ACDA
//    Blue 70%	    7EB2DD
//    Blue 60%	    91BDE2
//    Blue 50%	    A3C8E6
//    Blue 40%	    B5D3EB
//    Blue 30%	    C8DEF0
//    Blue 25%      D1E4F3
//    Blue 20%	    DAE9F5
//    Blue 10%	    EDF4FA

    public static const AffectBlue:uint = 0x4791CE;
    public static const AffectBlue_90:uint = 0x599CD3;
    public static const AffectBlue_80:uint = 0x6CA7D8;
    public static const AffectBlue_75:uint = 0x75ACDA;
    public static const AffectBlue_70:uint = 0x7EB2DD;
    public static const AffectBlue_60:uint = 0x91BDE2;
    public static const AffectBlue_50:uint = 0xA3C8E6;
    public static const AffectBlue_40:uint = 0xB5D3EB;
    public static const AffectBlue_30:uint = 0xC8DEF0;
    public static const AffectBlue_25:uint = 0xD1E4F3;
    public static const AffectBlue_20:uint = 0xDAE9F5;
    public static const AffectBlue_10:uint = 0xEDF4FA;

//    Yellow		FDBB45
//    Yellow 90%	FDC258
//    Yellow 80%	FDC96A
//    Yellow 70%	FECF7D
//    Yellow 75%	FECC74
//    Yellow 60%	FED68F
//    Yellow 50%	FEDDA2
//    Yellow 40%	FEE4B5
//    Yellow 30%	FEEBC7
//    Yellow 25%	FEEED0
//    Yellow 20%	FFF1DA
//    Yellow 10%	FFF8EC

    public static const AffectYellow:uint = 0xFDBB45;
    public static const AffectYellow_90:uint = 0xFDC258;
    public static const AffectYellow_80:uint = 0xFDC96A;
    public static const AffectYellow_75:uint = 0xFECC74;
    public static const AffectYellow_70:uint = 0xFECF7D;
    public static const AffectYellow_60:uint = 0xFED68F;
    public static const AffectYellow_50:uint = 0xFEDDA2;
    public static const AffectYellow_40:uint = 0xFEE4B5;
    public static const AffectYellow_30:uint = 0xFEEBC7;
    public static const AffectYellow_25:uint = 0xFEEED0;
    public static const AffectYellow_20:uint = 0xFFF1DA;
    public static const AffectYellow_10:uint = 0xFFF8EC;

//    Orange		FD7C45
//    Orange 90%	FD8958
//    Orange 80%	FD966A
//    Orange 75%	FE9D74
//    Orange 70%	FEA37D
//    Orange 60%	FEB08F
//    Orange 50%	FEBEA2
//    Orange 40%	FECBB5
//    Orange 30%	FED8C7
//    Orange 25%	FEDED0
//    Orange 20%	FFE5DA
//    Orange 10%	FFF2EC

    public static const AffectOrange:uint = 0xFD7C45;
    public static const AffectOrange_90:uint = 0xFD8958;
    public static const AffectOrange_80:uint = 0xFD966A;
    public static const AffectOrange_75:uint = 0xFE9D74;
    public static const AffectOrange_70:uint = 0xFEA37D;
    public static const AffectOrange_60:uint = 0xFEB08F;
    public static const AffectOrange_50:uint = 0xFEBEA2;
    public static const AffectOrange_40:uint = 0xFECBB5;
    public static const AffectOrange_30:uint = 0xFED8C7;
    public static const AffectOrange_25:uint = 0xFEDED0;
    public static const AffectOrange_20:uint = 0xFFE5DA;
    public static const AffectOrange_10:uint = 0xFFF2EC;

//    Black		    000000
//    Black 90%     191919
//    Black 80%	    333333
//    Black 70%	    4D4D4D
//    Black 60%	    666666
//    Black 50%	    808080
//    Black 40%	    999999
//    Black 30%	    B2B2B2
//    Black 20%	    CCCCCC
//    Black 10%	    E6E6E6
//    White		    FFFFFF

    public static const Black:uint = 0x000000;
    public static const Black_90:uint = 0x191919;
    public static const Black_80:uint = 0x333333;
    public static const Black_75:uint = 0x404040;
    public static const Black_70:uint = 0x4D4D4D;
    public static const Black_60:uint = 0x666666;
    public static const Black_50:uint = 0x808080;
    public static const Black_40:uint = 0x999999;
    public static const Black_30:uint = 0xB2B2B2;
    public static const Black_25:uint = 0xC0C0C0;
    public static const Black_20:uint = 0xCCCCCC;
    public static const Black_10:uint = 0xE6E6E6;
    public static const Black_5:uint = 0xF2F2F2;
    public static const White:uint = 0xFFFFFF;

//    User Status

    public static const Online:uint = 0x00CC00;
    public static const Offline:uint = 0xCC0000;

//    Approval Status

//    New : AffectBlue
//    Approved : Green : #46D693 / 50% #A2EAC9
//    Progress : AffectYellow
//    Pending : AffectOrange
//    Rejected : Red : #CE3945 / 50% #E69CA2

    public static const StatusNew:uint = 0x4791CE;
    public static const StatusNew_50:uint = 0xA3C8E6;
    public static const StatusApproved:uint = 0x46D693;
    public static const StatusApproved_50:uint = 0xA2EAC9;
    public static const StatusProgress:uint = 0xFDBB45;
    public static const StatusProgress_50:uint = 0xFEDDA2;
    public static const StatusPending:uint = 0xFD7C45;
    public static const StatusPending_50:uint = 0xFEBEA2;
    public static const StatusRejected:uint = 0xCE3945;
    public static const StatusRejected_50:uint = 0xE69CA2;
}
}