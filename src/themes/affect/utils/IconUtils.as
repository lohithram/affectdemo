/** Theme Affect v.1.2 by Maiol Baraut **/

package themes.affect.utils {
public class IconUtils {


//    CONTROLS

//    icon-times
    public static const Close:String = "\ue800";
//    icon-times
    public static const Remove:String = "\ue800";
//    icon-plus
    public static const Plus:String = "\ue801";
//    icon-minus
    public static const Minus:String = "\ue802";
//    icon-down-dir
    public static const CaretDown:String = "\ue803";
//     icon-up-dir
    public static const CaretUp:String = "\ue804";
//    icon-left-dir
    public static const CaretLeft:String = "\ue805";
//    icon-right-dir
    public static const CaretRight:String = "\ue806";
//    icon-up-open
    public static const fa_chevron_up:String = "\ue80a";
//    icon-down-open
    public static const fa_chevron_down:String = "\ue807";
//    icon-left-open
    public static const Prev:String = "\ue808";
//    icon-right-open
    public static const Next:String = "\ue809";


//    APPROVAL STATUS

//    icon-circle
    public static const Status:String = "\ue811";
//    icon-circle-thin
    public static const StatusNew:String = "\ue810";


//    UTILITY

//    icon-pencil
    public static const Tools:String = "\ue815";
//    icon-search
    public static const Search:String = "\ue816";
//    icon-chat
    public static const Conversations:String = "\ue817";
//    icon-bell-alt
    public static const Notifications:String = "\ue818";
//    icon-link
    public static const RelatedAssets:String = "\ue819";
//    icon-archive
    public static const Collections:String = "\ue81a";
//    icon-users
    public static const Users:String = "\ue81b";
//    icon-plus-circled
    public static const Create:String = "\ue81c";


//    TILE

//    icon-menu
    public static const Menu:String = "\ue80d";
//    icon-switch
    public static const Swap:String = "\ue822";
//    icon-plus-squared
    public static const Overlay:String = "\ue823";
//    icon-publish
    public static const DropDown:String = "\ue832";
//    icon-th-large
    public static const ViewThumb:String = "\ue80e";
//    icon-th-list
    public static const ViewListThumb:String = "\ue80f";


//    TILE MENU

//    icon-chat-empty
    public static const Collaboration:String = "\ue81d";
//    icon-download-cloud
    public static const Download:String = "\ue81e";
//    icon-easel
    public static const ViewExpand:String = "\ue834";
//    icon-resize-small-alt
    public static const ViewCompress:String = "\ue820";
//    icon-trash
    public static const Delete:String = "\ue821";
//    icon-chart-pie
    public static const Diagram:String = "\ue82e";


//    TOOLS

//    icon-select
    public static const ToolSelect:String = "\ue836";
//    icon-add-comment
    public static const ToolAnnotate:String = "\ue835";
//    icon-highlight
    public static const ToolHighlight:String = "\ue83f";
//    icon-arrow
    public static const ToolArrow:String = "\ue83a";
//    icon-line
    public static const ToolLine:String = "\ue83b";
//    icon-ellipse
    public static const ToolEllipse:String = "\ue83c";
//    icon-rectangle
    public static const ToolRectangle:String = "\ue83d";
//    icon-rounded
    public static const ToolRounded:String = "\ue83e";
//    icon-zoom-in
    public static const ToolZoomIn:String = "\ue824";
//    icon-zoom-out
    public static const ToolZoomOut:String = "\ue825";
//    icon-resize-full-alt
    public static const ToolExactFit:String = "\ue81f";
//    icon-mask
    public static const ToolMask:String = "\ue837";
//    icon-mask-square
    public static const ToolSquareMask:String = "\ue838";
//    icon-mask-circle
    public static const ToolCircleMask:String = "\ue839";
//    icon-font
    public static const ToolText:String = "\ue827";


//    ASSET TYPES

//    icon-tasks
    public static const Task:String = "\ue814";
//    icon-user
    public static const User:String = "\ue82c";


//    HTML

//    icon-desktop
    public static const Desktop:String = "\ue828";
//    icon-mobile
    public static const Mobile:String = "\ue829";


//    ZOOM SCROLLER

//    icon-ellipsis
    public static const EllipsisH:String = "\ue82a";
//    icon-ellipsis-vert
    public static const EllipsisV:String = "\ue82b";






//    icon-export > Open Link
    public static const OpenLink:String = "\ue82d";

//    fa_check
    public static const Check:String = "\uf00c";

//    fa_asterisk
    public static const Asterisk:String = "\uf069";

}
}